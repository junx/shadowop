#ifndef COREMATH_H
#define COREMATH_H

#include "core.h"

#ifndef CORECOMPLEX_H
#define CORECOMPLEX_H

template<typename T>
class Complex : public Vec<2,T>
{
public:
    Complex<T>(){this->v[0] = 0; this->v[1] = 0;}
    Complex<T>(T a){this->v[0] = a; this->v[1] = 0;}
    Complex<T>(T a, T b){this->v[0] = a; this->v[1] = b;}
    Complex<T>(const Vec<2,T> &a){this->v[0] = a[0]; this->v[1] = a[1];}

    inline Complex<T> logcomplex();
    inline Complex<T> expcomplex();

    inline double  real(){return this->v[0];}
    inline double  imaginary(){return this->v[1];}
    inline void    conjuction();
    inline Complex<T> conjuctioned();
};

template<typename T>
inline Complex<T>
Complex<T>::logcomplex()
{
    T r = T(log(sqrt(this->v[0]*this->v[0] + this->v[1] * this->v[1])));
    T i = T(j_to_angle(*this));
    return Complex<T>(r, i);
}

template<typename T>
inline Complex<T>
Complex<T>::expcomplex()
{
    qreal r = exp(sqrt(this->v[0] * this->v[0] + this->v[1] * this->v[1]));
    return Complex<T>(r*cos(this->v[1]), r*sin(this->v[1]));
}

template<typename T>
inline void
Complex<T>::conjuction()
{
    this->v[0] = -this->v[0];
}

template<typename T>
inline Complex<T>
Complex<T>::conjuctioned()
{
    return Complex(this->v[0], -this->v[1]);
}

#endif  // CORECOMPLEX_H



#ifndef COREMATRIX_H
#define COREMATRIX_H

// generic object (class) definition of matrix:
template <class V>
class Matrix
{
  // NOTE: maxsize determines available memory storage, but
  // actualsize determines the actual size of the stored matrix in use
  // at a particular time.
  QVector<QVector<V> >  data;      // where the data contents of the matrix are stored

public:
  Matrix<V>();
  Matrix<V>(int width, int height, V v0);
  Matrix<V>(const Matrix<V> &matrix);

  int M_width;  // max number of rows (same as max number of columns)
  int M_height;  // actual size (rows, or columns) of the stored matrix

  QVector<V> &operator[](int index){assert(index >= 0 && index < M_width);  return data[index];}
  const QVector<V> &operator[](int index) const{assert(index >= 0 && index < M_width);  return data[index];}
  Matrix<V> operator +=(const Matrix<V> &w);
  Matrix<V> operator +(const Matrix<V> &w) const;
  Matrix<V> operator -=(const Matrix<V> &w);
  Matrix<V> operator -(void) const;
  Matrix<V> operator -(const Matrix<V> &w) const;
  Matrix<V> operator *=(V a);
  Matrix<V> operator *(V a) const;
  Matrix<V> operator *=(const Matrix<V> &w);
  Matrix<V> operator *(const Matrix<V> &w) const;
  Matrix<V> operator /=(V a);
  Matrix<V> operator /(V a) const;
  Matrix<V> operator ^(V a) const;
  bool operator==(const Matrix<V> &a) const;
  bool operator!=(const Matrix<V> &a) const;
  inline void all(V value_for_all = 0);

  QVector<QVector<V> >        getData(){return data;}
  V               minV();
  V               maxV();
  void            identify();
  void            transpose();
  Matrix<V>       transposed() const;
  void            invert();  //need to overload operator: (V)/(V)
  Matrix<V>       inverted() const;
  Matrix<V>       cutrow(int start, int len);
  Matrix<V>       pseudo_inverse();
};

template <class V>
Matrix<V>::Matrix()
{
    M_height = 0;
    M_width = 0;
}

template <class V>
Matrix<V>::Matrix(int width, int height, V v0)
{ // the only public ctor
    if (width <= 0 || height <= 0)
        width = height = 0;
    M_height = height;
    M_width = width;
    data = QVector<QVector<V> >(M_width, QVector<V>(M_height, v0));
}


template <class V>
Matrix<V>::Matrix(const Matrix<V> &matrix)
{
    M_height = matrix.M_height;
    M_width  = matrix.M_width;
    data = matrix.data;
}


template<class V>
V
Matrix<V>::maxV()
{
    V m = data[0][0];
    for(int i = 0; i < M_width; i++)
        for(int j = 0; j < M_height; j++)
            if(data[i][j] > m)
                m = data[i][j];
    return m;
}



template<class V>
V
Matrix<V>::minV()
{
    V m = data[0][0];
    for(int i = 0; i < M_width; i++)
        for(int j = 0; j < M_height; j++)
            if(data[i][j] < m)
                m = data[i][j];
    return m;
}



template <class V>
void
Matrix<V>::identify()
{
    if(M_height != M_width || !M_height)
        return;
    data = QVector<QVector<V> >(M_width, QVector<V>(M_height, V(0)));
    for(int i = 0; i < M_height; i++)
        data[i][i] = 1;
}


template <class V>
inline Matrix<V>
Matrix<V>::operator +=(const Matrix<V> &w)
{
    if(M_width!=w.M_width||M_height!=w.M_height) return *this;
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            data[i][j]+=w[i][j];
    }
    return *this;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator +(const Matrix<V> &w) const
{
    if(M_width!=w.M_width||M_height!=w.M_height) return *this;
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]=data[i][j]+w[i][j];
    }
    return nw;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator -=(const Matrix<V> &w)
{
    if(M_width!=w.M_width||M_height!=w.M_height) return *this;
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            data[i][j]-=w[i][j];
    }
    return *this;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator -(const Matrix<V> &w) const
{
    if(M_width!=w.M_width||M_height!=w.M_height) return *this;
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]=data[i][j]-w[i][j];
    }
    return nw;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator -(void) const
{
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]-=data[i][j];
    }
    return nw;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator *=(V a)
{
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            data[i][j]*=a;
    }
    return *this;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator *(V a) const
{
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]=data[i][j]*a;
    }
    return nw;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator *=(const Matrix<V> &w)
{
    if(M_width != w.M_height)
        return *this;
    Matrix<V> nw(w.M_width, M_height, V(0));
    for (int j = 0; j < nw.M_height; j++)
    {
      for (int i = 0; i < nw.M_width; i++ )
      {
          V sum = 0.0;
          for (int c = 0; c < M_width; c++)
              sum += data[c][j] * w[i][c];
          nw[i][j] = sum;
      }
   }
   *this = nw;
    return *this;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator *(const Matrix<V> &w) const
{
    if(M_width != w.M_height)
        return *this;
    Matrix<V> nw(w.M_width, M_height, V(0));
    for (int j = 0; j < nw.M_height; j++)
    {
      for (int i = 0; i < nw.M_width; i++ )
      {
          V sum = 0.0;
          for (int c = 0; c < M_width; c++)
              sum += data[c][j] * w[i][c];
          nw[i][j] = sum;
      }
   }
    return nw;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator /=(V a)
{
    if(a == 0) return *this;
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            data[i][j]/=a;
    }
    return *this;
}

template <class V>
inline Matrix<V>
Matrix<V>::operator /(V a) const
{
    if(a == 0) return *this;
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]=data[i][j]/a;
    }
    return nw;
}


template <class V>
inline Matrix<V>
Matrix<V>::operator ^(V a) const
{
    Matrix<V> nw(M_width, M_height, V(0));
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            nw[i][j]= pow(data[i][j], a);
    }
    return nw;
}


template <class V>
inline bool
Matrix<V>::operator ==(const Matrix<V> &w) const
{
    if(M_width!=w.M_width||M_height!=w.M_height) return false;
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            if(w[i][j]!=data[i][j]) return false;
    }
    return true;
}

template <class V>
inline bool
Matrix<V>::operator !=(const Matrix<V> &w) const
{
    if(M_width!=w.M_width||M_height!=w.M_height) return true;
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            if(w[i][j]!=data[i][j]) return true;
    }
    return false;
}

template <class V>
inline void
Matrix<V>::all(V a)
{
    for(int j = 0; j < M_height; j++)
    {
        for(int i = 0; i < M_width; i++)
            data[i][j]=a;
    }
}

template <class V>
void
Matrix<V>::transpose()
{
    for(int i = 0; i < M_width; i++)
    {
       for(int j = 0; j < M_height; j++)
       {
           V m = data[i][j];
           data[i][j] = data[j][i];
           data[j][i] = m;
       }
    }
}

template <class V>
Matrix<V>
Matrix<V>::transposed() const
{
    Matrix<V> tm(M_height, M_width, V(0));
    for(int i = 0; i < M_width; i++)
       for(int j = 0; j < M_height; j++)
           tm[j][i] = data[i][j];
    return tm;
}

template <class V>
Matrix<V>
Matrix<V>::inverted() const
{
    Matrix<V> matrix = *this;
    matrix.invert();
    return matrix;
}


template <class V>
void
Matrix<V>::invert()
{
    if(M_width != M_height)
        return;
    int size = M_width;
    if (size <= 0) return;  // sanity check
    if (size == 1)  // must be of dimension >= 2
    {
        if(data[0][0] != V(0))
            data[0][0] = V(1) / data[0][0];
        return;
    }
    for (int i=1; i < size; i++)
        data[0][i] /= data[0][0]; // normalize row 0
    for (int i=1; i < size; i++)  {
      for (int j=i; j < size; j++)  { // do a column of L
          V sum(0);
          for (int k = 0; k < i; k++)
            sum += data[j][k] * data[k][i];
          data[j][i] -= sum;
        }
      if (i == size-1) continue;
      for (int j=i+1; j < size; j++)  {  // do a row of U
        V sum(0);
        for (int k = 0; k < i; k++)
            sum += data[i][k]*data[k][j];
        data[i][j] =
           (data[i][j]-sum) / data[i][i];
        }
      }
    for ( int i = 0; i < size; i++ )  // invert L
      for ( int j = i; j < size; j++ )  {
        V x(1.0);
        if ( i != j ) {
          x = V(0);
          for ( int k = i; k < j; k++ )
              x -= data[j][k]*data[k][i];
          }
        data[j][i] = x / data[j][j];
        }
    for ( int i = 0; i < size; i++ )   // invert U
      for ( int j = i; j < size; j++ )  {
        if ( i == j ) continue;
        V sum(0);
        for ( int k = i; k < j; k++ )
            sum += data[k][j]*( (i==k) ? V(1.0) : data[i][k] );
        data[i][j] = -sum;
        }
    for ( int i = 0; i < size; i++ )   // final inversion
      for ( int j = 0; j < size; j++ )  {
        V sum(0.0);
        for ( int k = ((i>j)?i:j); k < size; k++ )
            sum += ((j==k)?V(1.0):data[j][k])*data[k][i];
        data[j][i] = sum;
      }
}

template <class V>
Matrix<V>
Matrix<V>::cutrow(int start, int len)
{
    Matrix<V> newmatrix(len, M_height, V(0));
    for(int i = 0; i < len; i++)
        for(int j = 0; j < M_height; j++)
            if(i+start < M_width)
                newmatrix[i][j] = data[i+start][j];
    return newmatrix;
}

template <class V>
Matrix<V>
Matrix<V>::pseudo_inverse()
{
    Matrix<V> tran  = transposed();
    Matrix<V> thism = *this;
    Matrix<V> pre = tran * thism;
    pre.invert();
    thism = pre * tran;
    return thism;
}

#endif //COREMATRIX_H

#endif // COREMATH_H
