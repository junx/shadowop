#ifndef COREUTILTO_H
#define COREUTILTO_H

#include "core2.h"

/*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
(1) to vec
(2) to point
(3) to complex
(4) to direction
(5) to angle
(6) to matrix
(7) to list
(8) to path
(9) to bounding

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


//<<<<<<<<<trans to vec>>>>>>>>>>>>>>//
inline Vec2d     j_to_vec2d(QVector2D v){return Vec2d(v.x(), v.y());}
inline Vec2f     j_to_vec2f(QVector2D v){return Vec2f(v.x(), v.y());}
inline Vec2d     j_to_vec2d(QPointF v){return Vec2d(v.x(), v.y());}
inline Vec2f     j_to_vec2f(QPointF v){return Vec2f(v.x(), v.y());}
inline Vec2i     j_to_vec2i(QPointF v){return Vec2i(v.x(), v.y());}
inline Vec2s     j_to_vec2s(QPointF v){return Vec2s(v.x(), v.y());}

inline Vec3d     j_to_vec3d(QVector3D v){return Vec3d(v.x(), v.y(), v.z());}
inline Vec3f     j_to_vec3f(QVector3D v){return Vec3f(v.x(), v.y(), v.z());}

//<<<<<<<<<<<to_point>>>>>>>>>>>>>>>>//
template<class T>
inline QPointF    j_to_point(Vec<2,T> v){return QPointF(v[0], v[1]);}
template<class T>
inline  QPointF   j_to_point(Complex<T> c){return QPointF(c[0], c[1]);}
template<template<class> class L, class T>
inline  L<QPointF>j_to_point(L<T> clist){L<QPointF> plist; for(int i = 0; i < clist.size(); i++) plist.append(j_to_point(clist[i])); return plist;}


//<<<<<<<<<<to complex>>>>>>>>>>>>>>>>//
inline  Complexd       j_to_complex(QPointF p){return Complexd(p.x(), p.y());}
template<class T>
inline  Complex<T>     j_to_complex(Vec<2,T> p){return Complex<T>(p[0], p[1]);}
template<template<class> class L>
inline  L<Complexd>    j_to_complex(L<QPointF> plist){L<Complexd> clist; for(int i = 0; i < plist.size(); i++) clist.append(j_to_complex(plist[i])); return clist;}
template<template<class> class L, class T>
inline  L<Complex<T> > j_to_complex(L<Vec<2,T> > plist){L<Complex<T> > clist; for(int i = 0; i < plist.size(); i++) clist.append(j_to_complex(plist[i])); return clist;}


//<<<<<<<<to direction>>>>>>>>>>>>>>//
inline QVector2D  j_to_direction(float angle){return QVector2D(cos(angle), sin(angle));}
inline QVector2D  j_to_direction(double angle){return QVector2D(cos(angle), sin(angle));}
inline QVector2D  j_to_direction(QPointF from, QPointF to){QPointF diff = to - from;if(fabs(diff.x()) < 0.01 && fabs(diff.y()) < 0.01) return QVector2D(1, 0); return QVector2D(diff.x(), diff.y()).normalized();}
inline QVector2D  j_to_direction(QList<QPointF> plist, int index, int scale = 1)
{
    int size = plist.size();
    if(!size) return QVector2D(1,0);
    scale = scale<1?1:scale;
    int min = index-scale; min = min<0?0:min;
    int max = index+scale; max = max>(size-1)?(size-1):max;
    return j_to_direction(plist[min], plist[max]);
}
inline QList<QVector2D> j_to_directions(QList<QPointF> plist, int scale = 1)
{
    int size = plist.size();
    //<<<<<<<<<<input direction segment>>>>>>>>>//
    QList<QVector2D> cutDirList;
    for(int i = 0; i < size; i++)
        cutDirList.append(j_to_direction(plist, i, scale));
    return cutDirList;
}
template<template<class> class L>
inline QVector2D  j_to_direction(L<QPointF> plist)
{
    double lxx = 0, lxy = 0, lyy = 0;
    for(int i = 0; i < plist.size(); i++)
    {
        lxy += plist[i].x() * plist[i].y();
        lxx += plist[i].x() * plist[i].x();
        lyy += plist[i].y() * plist[i].y();
    }
    double k = 0;
    if(lxx == 0 && lxy == 0)
        return QVector2D(1,0);
    if(lxx != 0)
        k = lxy / lxx;
    else if(lxy != 0)
        k = lyy / lxy;
    else
        k = (lxy / lxx + lyy / lxy) * 0.5;

    QVector2D direction(1, k);
    direction.normalize();
    return direction;
}


//<<<<<<<<<to angle>>>>>>>>>>>>>>//
inline float      j_to_angle(float angle){ while(angle > 3.1416) angle -= 6.2832; while(angle < -3.1416) angle += 6.2832; return angle;}
inline double     j_to_angle(double angle){ while(angle > 3.1416) angle -= 6.2832; while(angle < -3.1416) angle += 6.2832; return angle;}
inline double     j_to_angle(QVector2D rotate)
{
    if(rotate.x() == 0 && rotate.y() == 0)
        return 0;
    qreal angle = acos(rotate.x());
    if(rotate.y() < 0)
        angle = 6.2816-angle;
    return j_to_angle(angle);
}
inline double     j_to_angle(QPointF from, QPointF to){return j_to_angle(j_to_direction(from, to));}
template<typename T>
inline double     j_to_angle(Vec<2,T> vex){j_normalize(vex); return j_to_angle(QVector2D(vex[0],vex[1]));}
template<template<class> class L>
inline double     j_to_angle(L<QPointF> plist){QVector2D vec = j_to_direction(plist); return j_to_angle(vec);}
inline float      j_to_angle(QList<QPointF> plist, int index, int scale = 1)
{
    int size = plist.size();
    if(!size) return 0;
    scale = scale<1?1:scale;
    int min = index-scale; min = (min<0)?0:min;
    int max = index+scale; max = (max>(size-1))?(size-1):max;
    return j_to_angle(plist[min], plist[max]);
}
inline QList<float> j_to_angles(QList<QPointF> plist, int scale = 1)
{
    int size = plist.size();
    QList<float> cutAngleList;
    for(int i = 0; i < size; i++)
        cutAngleList.append(j_to_angle(plist, i, scale));
    return cutAngleList;
}

//<<<<<<<<<to matrix>>>>>>>>>>>>//
inline QMatrix j_to_matrix(QPointF orig_center, QPointF t, qreal s, qreal rot)
{
    QMatrix matrix;
    matrix.translate(-orig_center.x(), -orig_center.y());
    matrix.scale(s, s);
    matrix.rotate(rot);
    matrix.translate(orig_center.x()+t.x(), orig_center.y()+t.y());
    return matrix;
}


inline QMatrix j_to_matrix(QPointF orig_center, QPointF t, qreal s, QVector2D rot)
{
    QMatrix matrix;
    matrix.translate(-orig_center.x(), -orig_center.y());
    matrix.scale(s, s);
    matrix.rotate(j_to_angle(rot));
    matrix.translate(orig_center.x()+t.x(), orig_center.y()+t.y());
    return matrix;
}


template<template<class> class L, class T>
inline Matrix<T>  j_to_matrix(L<L<T> > plist)
{
    int w = plist.size();
    if(w < 1)
        return Matrix<T>(0,0,T(0));
    int h = plist[0].size();
    Matrix<T> matrix(w, h, T(0));
    for(int i = 0; i < w; i++)
        for(int j = 0; j < h; j++)
            matrix[i][j] = plist[i][j];
    return matrix;
}

template<template<class> class L, class T>
inline Matrix<T>  j_to_matrix(L<T> list, bool is_vertical)
{
    int size = list.size();
    if(is_vertical)
    {
        Matrix<T> matrix(1, size, T(0));
        for(int i = 0; i < size; i++)
            matrix[i][0] = list[i];
        return matrix;
    }
    else
    {
        Matrix<T> matrix(size, 1, T(0));
        for(int i = 0; i < size; i++)
            matrix[0][i] = list[i];
        return matrix;
    }
}



//<<<<<<<<to List>>>>>>>>>>>>>>>//
template<class T>
inline QList<T>   j_to_list(Matrix<T> matrix){QVector<QVector<T> > data = matrix.getData(); QList<T> list; for(int i = 0; i < data.size(); i++) list.append(data[i].toList()); return list;}
template<class T>
inline QList<T>   j_to_list(const QList<QList<T> > &dlist){QList<T> list; for(int i = 0; i < dlist.size(); i++) list.append(dlist[i]); return list;}
template<class T>
inline QList<T>   j_to_list(const QList<QList<QList<T> > > &tlist){QList<T> list; for(int i = 0; i < tlist.size(); i++) list.append(j_to_list(tlist[i])); return list;}
template<unsigned int N, class T>
inline QList<T>   j_to_list(const Vec<N,T> &v){QList<T> list; for(int i = 0; i < N; i++) list.append(v[i]); return list;}

//<<<<<<<<<to path>>>>>>>>>>>>>>//
inline QPainterPath j_to_path(QList<QPointF> plist)
{
    QPainterPath path;
    if(plist.size() == 0)
        return path;
    plist.append(plist[0]);
    path.moveTo(plist[0]);
    for(int i = 0; i < plist.size(); i++)
        path.lineTo(plist[i]);
    return path;
}


//<<<<<<<to bounding>>>>>>>>>>>//
inline QList<QPointF> j_to_list(QRectF rect)
{
    QList<QPointF> plist;
    plist.append(rect.topLeft());
    plist.append(rect.topRight());
    plist.append(rect.bottomRight());
    plist.append(rect.bottomLeft());
    return plist;
}


template<template<class> class L>
inline QPolygonF j_to_tight_bounding(L<QPointF> plist, QRectF *standRect, QPointF *Center, qreal *mainDir)
{
    QPointF center = Core2<L,QPointF>::j_ave(plist);
    L<QPointF> plist1 = Core2<L,QPointF>::j_translate(plist, -center);
    if(Center)
       *Center = center;
    qreal maindir = j_to_angle(plist1);
    if(mainDir)
       *mainDir = maindir;
    QVector2D rot(cos(maindir), sin(-maindir));
    plist1 = Core2<L,QPointF>::j_rotate(plist1, rot);
    QRectF rect =  j_to_bounding(plist1);
    if(standRect)
       *standRect = rect;
    QVector2D inrot(cos(maindir), sin(maindir));
    QPointF p1 = j_rotate(rect.topLeft(), inrot) + center;
    QPointF p2 = j_rotate(rect.topRight(), inrot) + center;
    QPointF p3 = j_rotate(rect.bottomRight(), inrot) + center;
    QPointF p4 = j_rotate(rect.bottomLeft(), inrot) + center;
    QPolygonF poly;
    poly<<p1<<p2<<p3<<p4;
    return poly;
}

template<template<class> class L, class T>
inline QPolygonF j_to_tight_bounding(L<Vec<2,T> > vlist, QRectF *rectB, Vec<2,T> *center, qreal *mainDir)
{
    L<QPointF> plist = j_to_point(vlist);
    return j_to_tight_bounding(plist, rectB, center, mainDir);
}

template<template<class> class L>
inline QRectF   j_to_bounding(L<QPointF> vlist)
{
    if(vlist.isEmpty())
        return QRect(0,0,0,0);
    qreal minx = MAXVALUE, miny = MAXVALUE, maxx = MINVALUE, maxy = MINVALUE;
    QPointF   point;
    qreal x = 0, y = 0;
    for(int i = 0; i < vlist.size(); i++)
    {
        point = vlist[i];
        x = point.x(); y = point.y();
        if(x > maxx)  maxx = x;
        if(y > maxy)  maxy = y;
        if(x < minx)  minx = x;
        if(y < miny)  miny = y;
    }
    QRectF rect(minx, miny, maxx-minx , maxy-miny);
    return rect;
}

template<template<class> class L, class T>
inline QRectF    j_to_bounding(L<T> vlist)
{
    L<QPointF> plist = j_to_point(vlist);
    return j_to_bounding(plist);
}

inline QRect j_to_bounding(QRect r1, QRect r2)
{
    if(!r1.width() || !r1.height())
        return r2;
    if(!r2.width() || !r2.height())
        return r1;
    QList<QPointF> plist;
    plist.append(r1.topLeft());
    plist.append(r1.bottomRight());
    plist.append(r2.topLeft());
    plist.append(r2.bottomRight());
    return j_to_bounding(plist).toRect();
}



template<class T>
inline int
j_findElementInSequence(QList<T> &seq, T e)
{
    if(seq.isEmpty())
        return -1;
    if(e <= seq[0])
    {
        if(e == seq[0]) return 0;
        return -1;
    }
    if(e >= seq.last())
    {
        if(e == seq.last()) return seq.size()-1;
        return -1;
    }

    int head = 0, end = seq.size()-1;
    while(head < end-1)
    {
        int mid = (head+end)*0.5;
        if(seq[mid] == e) return mid; //existing, don't add it
        if(seq[mid] > e)
            end = mid;
        else
            head = mid;
    }
    return -1;
}



template<class T>
inline int
j_findElementInCluster(QList<T> &seq, T e)
{
    for(int i = 0; i < seq.size(); i++)
        if(seq[i] == e)
            return i;
    return -1;
}



template<class T>
inline bool
j_mergeElementToSequenceNoRepeat(QList<T> &seq, T e)
{//binary search
    if(seq.isEmpty())
    {
        seq.append(e);
        return true;
    }
    if(e <= seq[0])
    {
        if(e == seq[0]) return false;
        seq.prepend(e);
        return true;
    }
    if(e >= seq.last())
    {
        if(e == seq.last()) return false;
        seq.append(e);
        return true;
    }

    int head = 0, end = seq.size()-1;
    while(head < end-1)
    {
        int mid = (head+end)*0.5;
        if(seq[mid] == e) return false; //existing, don't add it
        if(seq[mid] > e)
            end = mid;
        else
            head = mid;
    }
    seq.insert(end, e);
    return true;
}



template<class T>
inline bool
j_mergeElementToClusterNoRepeat(QList<T> &seq, T e)
{//linear search
    for(int i = 0; i < seq.size(); i++)
        if(seq[i] == e)
            return false;
    seq.append(e);
    return true;
}



template<class T>
inline QList<T>
j_mergeSequenceOnlyCommon(QList<T> seq1, QList<T> seq2)
{
    QList<T> coms;
    if(seq1.size() <= seq2.size())
    {
        for(int i = 0; i < seq1.size(); i++)
        {
            int pos = j_findElementInSequence(seq2, seq1[i]);
            if(pos >= 0)
                coms.append(seq1[i]);
        }
        return coms;
    }
    for(int i = 0; i < seq2.size(); i++)
    {
        int pos = j_findElementInSequence(seq1, seq2[i]);
        if(pos >= 0)
            coms.append(seq2[i]);
    }
    return coms;
}



template<class T>
inline QList<T>
j_mergeSequenceNoRepeat(QList<T> seq1, QList<T> seq2)
{
    if(seq1.size() <= seq2.size())
    {
        for(int i = 0; i < seq1.size(); i++)
            j_mergeElementToSequenceNoRepeat(seq2, seq1[i]);
        return seq2;
    }
    for(int i = 0; i < seq2.size(); i++)
        j_mergeElementToSequenceNoRepeat(seq1, seq2[i]);
    return seq1;
}



template<class T>
inline QList<T>
j_mergeClusterNoRepeat(QList<T> seq1, QList<T> seq2)
{
    if(seq1.size() <= seq2.size())
    {
        for(int i = 0; i < seq1.size(); i++)
            j_mergeElementToClusterNoRepeat(seq2, seq1[i]);
        return seq2;
    }
    for(int i = 0; i < seq2.size(); i++)
        j_mergeElementToClusterNoRepeat(seq1, seq2[i]);
    return seq1;
}



#endif // COREUTILTO_H
