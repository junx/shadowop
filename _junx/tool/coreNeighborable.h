#ifndef CORENEIGHBORABLE_H
#define CORENEIGHBORABLE_H

#include "coreObject.h"


class coreNeighbor;
class coreGrider;
//<<<<interface for search>>>//
class coreNeighborable : public coreObject
{
    ushort  coreNeighborablePropertyFlags;
private:
    QList<coreNeighborable*> myNeighborBuffer;
public:
    QList<coreNeighborable*> mySimilarityBuffer;

    coreNeighborable():coreObject(){coreNeighborablePropertyFlags = 0x00; set_activeNeighborBuffer(false);}
    coreNeighborable(const coreNeighborable &neighbor);

    //<<<<<<<need be implemented before push to grider>>>>>>>//
    virtual int             getNeighborhoodSize(int){return 0;}
    virtual QList<coreNeighbor*> getNeighborhoods(int){return QList<coreNeighbor*>();}
    virtual coreNeighbor*   getNeighborhood(int, int){return 0;}
    virtual bool            updateNeighbors(){return true;}
    virtual bool            checkValid();

    virtual void            initial(){}
    virtual void            update(QList<QPointF> plist){
        QList<QPointF> nplist = Core1<QList, QPointF>::j_resize(plist, plist.size());
        set_GridPoints(nplist);
    }
    virtual void            set_GridPoints(QList<QPointF>){}
    virtual QList<QPointF>  get_GridPoints(){return QList<QPointF>();}

    virtual QPointF         get_GridPoint(int){return QPointF(0,0);}
    virtual QPointF         get_GridCenter(){return QPointF(0,0);}
    virtual int             get_GridPointSize(){return 0;}
    virtual QRectF          get_GridBounding(){return QRectF(0,0,0,0);}

    inline bool             get_activeNeighborBuffer(){return get_bit(0);}
    inline void             set_activeNeighborBuffer(bool a){set_bit(a, 0);}
    //<<<<<<<<has this been used>>>>>>>//
    inline bool             get_exhaust(){return get_bit(1);}
    inline void             set_exhaust(bool a){set_bit(a, 1);}
    //<<<<<<<<set by its host>>>>>>>>>>//
    inline int              get_griderId(){return coreNeighborablePropertyFlags >> 4;}
    inline void             set_griderId(ushort a){coreNeighborablePropertyFlags = (a << 4) | (coreNeighborablePropertyFlags & 0x0f);}


    //<<<<<<<used to calculate distance between elements>>>>>>>//
    virtual QVector<qreal>    compute_dis(coreNeighborable *op, int wSize, qreal *wList);
    //<<<<<<<search all elements for further  process>>>>>>//
    QList<coreNeighborable*>  find_neighborhood_core(coreGrider *grider, int wSize, qreal *wList);
    //<<<<<<<search neighbors from a search-space>>>>>>//
    QList<coreNeighborable*>  find_neighborhood_core(QList<coreNeighborable*> search_space, QList<qreal> *myDisList, int wSize, qreal *wList);
    //<<<<<<<need to add spatial-neighbor-buffer to do this>>>>>>>>>//
    QList<coreNeighborable*>  find_neighborhood_quick(QList<qreal> *myDisList, int wSize, qreal *wList);

    virtual qreal   compute_similarity_neighbor_core(coreNeighborable *, QVector<short>&, int, qreal *){return MAXVALUE;}
    virtual qreal   compute_similarity_self_core(coreNeighborable*, int, qreal *){return MAXVALUE;}
protected:
    bool    get_bit(int bitPos){return (coreNeighborablePropertyFlags & (0x01 << bitPos))?true:false;}
    void    set_bit(bool a, int bitPos){a?(coreNeighborablePropertyFlags |= 0x01 << bitPos):(coreNeighborablePropertyFlags &= ~(0x01 << bitPos));}
};




class coreNeighbor : public coreObject
{
    ushort          coreNeighborPropertyFlag;
protected:
    coreNeighborable* neighbor;
    QList<short>      neighbor_matchIds;

public:
    coreNeighbor():coreObject(){coreNeighborPropertyFlag = 0; neighbor = 0; set_align(false);}
    coreNeighbor(const coreNeighbor &n):coreObject(n){
        coreNeighborPropertyFlag = n.coreNeighborPropertyFlag;
        neighbor = n.neighbor;
        neighbor_matchIds = n.neighbor_matchIds;
    }

    inline bool      get_align(){return get_bit(0);}
    inline void      set_align(bool a){ set_bit(a, 0);}

    QList<short>    get_neighborMatchIds(){return neighbor_matchIds;}
    void            set_neighborMatchIds(QList<short> ids){neighbor_matchIds = ids;}

    coreNeighborable* get_neighbor(){return neighbor;}

    virtual void    update(){}
    virtual qreal   compute_similarity(coreNeighbor *, int, qreal*){return MAXVALUE;}

protected:
    bool    get_bit(int bitPos){return (coreNeighborPropertyFlag & (0x01 << bitPos))?true:false;}
    void    set_bit(bool a, int bitPos){a?(coreNeighborPropertyFlag |= 0x01 << bitPos):(coreNeighborPropertyFlag &= ~(0x01 << bitPos));}
};


#endif // CORENEIGHBORABLE_H
