#include "coreDeform.h"


DeformableCage::DeformableCage(QList<QPointF> origs)
{
    QPointF center; qreal mainDir;
    if(origs.size() == 0) return;
    dTranspoint.append(origs);
    QList<Complexd> cage;
    QPolygonF poly = j_to_tight_bounding(origs, 0, &center, &mainDir);
    for(int i = 0; i < poly.size(); i++)
    {
        QPointF np = (poly[i] - center)*1.2+center;
        cage.append(j_to_complex(np));
    }
    dOldCage = dNewCage = cage;
    QList<QList<Complexd> > parameter = j_Transform_calculate_param(origs, dOldCage);
    dCageParameter.append(parameter);
}



DeformableCage::DeformableCage(QList<QList<QPointF> > origs)
{
    QPointF center; qreal mainDir;
    QList<QPointF> plist = j_to_list(origs);
    if(plist.size() == 0) return;
    dTranspoint = origs;
    QList<Complexd> cage;
    QPolygonF poly = j_to_tight_bounding(plist, 0, &center, &mainDir);
    for(int i = 0; i < poly.size(); i++)
    {
        QPointF np = (poly[i] - center)*1.2+center;
        cage.append(j_to_complex(np));
    }
    dOldCage = dNewCage = cage;
    for(int i = 0; i < origs.size(); i++)
    {
        QList<QList<Complexd> > parameter = j_Transform_calculate_param(origs[i], dOldCage);
        dCageParameter.append(parameter);
    }
}



QList<QPointF>
DeformableCage::forwardDeform(const QList<QPointF> &plist)
{
    QList<QPointF> output;
    if(dNewCage.isEmpty() || plist.isEmpty())
        return output; //no transfromation yet, return it
    QList<QList<Complexd> > nparameter = j_Transform_calculate_param(plist, dOldCage);
    output = j_Transform_control(plist, nparameter, dNewCage);
    if(!output.isEmpty())
    {
        dTranspoint.append(plist);
        dCageParameter.append(nparameter);
    }
    return output;
}


QList<QPointF>
DeformableCage::forwardDeformed(const QList<QPointF> &plist)
{
    QList<QPointF> output;
    if(dNewCage.isEmpty() || plist.isEmpty())
        return output; //no transfromation yet, return it
    QList<QList<Complexd> > nparameter = j_Transform_calculate_param(plist, dOldCage);
    output = j_Transform_control(plist, nparameter, dNewCage);
    return output;
}


QList<QPointF>
DeformableCage::backwardDeform(const QList<QPointF> &output_points)
{
    QList<QPointF> input_points = backwardDeformed(output_points);
    if(input_points.size() == output_points.size())
        forwardDeform(input_points);
    return input_points;
}


QList<QPointF>
DeformableCage::backwardDeformed(const QList<QPointF> &output_points)
{
    if(dNewCage.isEmpty() || output_points.isEmpty())
        return QList<QPointF>();
    QRectF bounding = j_to_bounding(dOldCage);
    qreal  max_edge_size = 0.5;
    QPointF topleft = bounding.topLeft();
    QPointF bottomright = bounding.bottomRight();

    QList<QList<QPointF> > samples_input, samples_output;
    for(qreal x = topleft.x()+0.5; x < bottomright.x(); x+=max_edge_size)
    {
        QList<QPointF> slist;
        for(qreal y = topleft.y()+0.5; y < bottomright.y(); y+= max_edge_size)
            slist.append(QPointF(x,y));
        samples_input.append(slist);
    }
    samples_output = forwardDeform(samples_input);

    if(samples_input.isEmpty() || samples_input[0].isEmpty())
        return QList<QPointF>();
    int width = samples_input.size();
    int height = samples_input[0].size();
    QList<QPointF> input_points;
    Vec2i index(-1,-1);
    for(int i = 0; i < output_points.size(); i++)
    {
       QPointF output = output_points.at(i);
       int search_start_x = 0;
       int search_end_x   = width;
       int search_start_y = 0;
       int search_end_y   = height;
       if(index[0] >= 0 || index[1] >= 0)
       {
           QPointF diff = output_points[i-1] - output;
           int numx = (fabs(diff.x())+3) / max_edge_size + 3;
           int numy = (fabs(diff.y())+3) / max_edge_size + 3;
           search_start_x = (index[0] - numx)<=0?0:(index[0] - numx);
           search_end_x   = (index[0] + numx)>=width?width:(index[0]+numx);
           search_start_y = (index[1] - numy)<=0?0:(index[1] - numy);
           search_end_y   = (index[1] + numy)>=height?height:(index[1]+numy);
       }

       //<<<<<<<coarse level search>>>>>>>>>>>>>//
       qreal    mindis = MAXVALUE;
       Vec2i    minindex(0,0);
       for(int p = search_start_x; p < search_end_x; p+=5)
       {
           for(int q = search_start_y; q < search_end_y; q+=5)
           {
               qreal dis = j_dis2(samples_output[p][q], output);
               if(dis < mindis)
               {
                   mindis = dis;
                   minindex = Vec2i(p,q);
               }
           }
       }
       //<<<<<<<fine level search>>>>>>>>>>>>>>>//
       for(int p = (minindex[0]-4)>0?(minindex[1]-4):0; p < minindex[0]+4 && p < search_end_x; p++)
       {
           for(int q = (minindex[1]-4)>0?(minindex[1]-4):0; q < minindex[1]+4 && q < search_end_y; q++)
           {
               qreal dis = j_dis2(samples_output[p][q], output);
               if(dis < mindis)
               {
                   mindis = dis;
                   minindex = Vec2i(p,q);
               }
           }
       }
       QPointF input = samples_input[minindex[0]][minindex[1]];
       input_points.append(input);
       index = minindex;
    }
    return input_points;
}


