#ifndef COREMAPABLE_H
#define COREMAPABLE_H

#include "coreFunctionUtil.h"

/*
//<<<<<cluster>>>>>>//
class coreMapA
{
public:
};

//<<<<propagation>>>>>//
class coreMapB
{
public:
};*/


template<class coreMapA, class coreMapB>
class coreMapable
{
    QMultiHash<coreMapA*, coreMapB*>          mapData;
public:
    inline virtual void                  mapable_clear(){mapData.clear();}

    inline virtual QList<coreMapA*>      mapable_getAfromB(coreMapB *B){return B->mapable_getAfromB();}
    inline virtual QList<coreMapB*>      mapable_getBfromA(coreMapA *A){return mapData.values(A);}

    inline virtual void                  mapable_connection(coreMapB *B, coreMapA* A){if(A&&B) mapData.insertMulti(A,B);}
    inline virtual void                  mapable_connection(coreMapB *B, QList<coreMapA*> As){
        if(!B || As.isEmpty()) return;
        for(int i = 0; i < As.size(); i++)
            mapData.insertMulti(As[i], B);
    }
    inline virtual void                  mapable_connection(coreMapA *A, QList<coreMapB*> Bs){
        if(!A || Bs.isEmpty()) return;
        for(int i = 0; i < Bs.size(); i++)
            mapData.insertMulti(A, Bs[i]);
    }
    inline virtual void                  mapable_disconnection(coreMapB *B, coreMapA* A){if(A&&B) mapData.remove(A,B);}
    inline virtual void                  mapable_disconnection(coreMapB *B){
        if(!B) return;
        QList<coreMapA*> As = mapable_getAfromB(B);
        for(int i = 0; i < As.size(); i++)
            mapData.remove(As[i], B);
    }
    inline virtual void                  mapable_disconnection(coreMapA *A){
        if(!A) return;
        QList<coreMapB*> Bs = mapable_getBfromA(A);
        for(int i = 0; i < Bs.size(); i++)
            mapData.remove(A, Bs[i]);
    }
};


#endif // COREMAPABLE_H
