#ifndef CORESELECTABLE_H
#define CORESELECTABLE_H

#include "coreFunctionUtil.h"

template<class U>
class coreSelectable
{
protected:
    double      pathSelectionWidth;
    QList<U>    mySelectElements;
public:
    coreSelectable(){pathSelectionWidth = 20;}
    void                setPathSelectionWidth(double width){pathSelectionWidth = width;}
    QList<U>            getElementList(){return mySelectElements;}
    virtual bool        onStartSelection(QEvent*){return false;}
    virtual bool        onUpdateSelection(QEvent*){return false;}
    virtual bool        onFinishSelection(QEvent*){return false;}
    virtual void        onSelection(int, qreal *){}
};


#endif // CORESELECTABLE_H
