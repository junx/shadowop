#include "coreHistoryer.h"


coreUndoer::coreUndoer()
{
    myElementCount = 0;
    myCurElementCount = 0;
    myLastRecord = 0;
    myStepSize = 30;  //store it after "stepsize" operations
}


void
coreUndoer::reset()
{
    myElementCount = 0;
    myCurElementCount = 0;
    StateIndexList.clear();
    ElementIndexList.clear();
    myLastRecord = 0;
}


void
coreUndoer::onUndo(uint)
{
    if(myCurElementCount <= 0)  //this is alrealy the first
        return;
    myCurElementCount--;
    runto(myCurElementCount);
}


void
coreUndoer::onRedo(uint)
{
    if(myCurElementCount >= myElementCount)  //this is the last one
        return;
    myCurElementCount++;
    runto(myCurElementCount);
}


void
coreUndoer::pushElementToHistory(uint id)
{
    if(myElementCount != myCurElementCount)
        branch_clear();
    myElementCount++;
    myCurElementCount++;
    if (myElementCount - myLastRecord >= myStepSize)
    {//store step undo
        record_state();
        myLastRecord = myElementCount;
        ElementIndexList.append(id);
        StateIndexList.append(myCurElementCount-1);
    }
    else
    {
        ElementIndexList.append(id);
    }
}



//clear all the record from myCurUndo to myUndoCount
void
coreUndoer::branch_clear()
{
    for(int i = myElementCount-1; i >= myCurElementCount; i--)
    {
        if(!StateIndexList.isEmpty() && i <= int(StateIndexList.last()))
        {
            clear_state();
            StateIndexList.removeLast();
        }
        ElementIndexList.removeLast();
    }
    myElementCount = myCurElementCount;
}


void
coreUndoer::replay_all()
{
    runto(myCurElementCount);
}
