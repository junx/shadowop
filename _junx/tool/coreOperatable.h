#ifndef COREOPERATABLE_H
#define COREOPERATABLE_H

#include "coreObject.h"

class Operatable : public coreObject
{
protected:
    uint              operatablePropertyFlags;
private:
    QTime             time_stamp;
public:
    Operatable();
    Operatable(const Operatable &cop);
    ~Operatable(){}

    inline  void     set_time(QTime t){time_stamp = t;}

    //<<<<<<<this op is system generated>>>>>>>//
    inline bool      get_systemAuto(){return get_bit(0);}
    inline void      set_systemAuto(bool a){set_bit(a, 0);}

    inline  ushort   get_type(){return operatablePropertyFlags >> 4;}
    inline  void     set_type(ushort a){operatablePropertyFlags = (a << 4) | (operatablePropertyFlags & 0x0f);} //XXXX 0000 | 00000 XXXX

    virtual bool      on_start(QEvent *){return false;}
    virtual bool      on_update(QEvent *){return false;}
    virtual bool      on_finish(QEvent *){return false;}

    virtual void      replay(QPainter &, QPointF, float){}
    virtual void      write(QTextStream *out);
    virtual bool      read(QTextStream *in);

private:
   bool      get_bit(int bitPos){return (operatablePropertyFlags & (0x01 << bitPos))?true:false;}
   void      set_bit(bool a, int bitPos){a?(operatablePropertyFlags |= 0x01 << bitPos):(operatablePropertyFlags &= ~(0x01 << bitPos));}
};


#endif // COREOPERATABLE_H
