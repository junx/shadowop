#include "coreRunable.h"

void
coreThreadProcessable::on_start()
{
    on_process();
    emit on_finish();
}


bool
coreThreadProcessable::start_process_in_thread(QThread::Priority priority)
{
    QThread *thread = new QThread;
    this->moveToThread(thread);
    connect(thread, SIGNAL(started()), this, SLOT(on_start()));
    connect(this, SIGNAL(on_finish()), thread, SLOT(quit()));
    connect(this, SIGNAL(on_finish()), this, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start(priority);
    return true;
}

