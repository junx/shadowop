#include "coreNeighborable.h"
#include "coreGrider.h"

coreNeighborable::coreNeighborable(const coreNeighborable &neighbor)
    :coreObject(neighbor)
{
    coreNeighborablePropertyFlags = neighbor.coreNeighborablePropertyFlags;
    myNeighborBuffer = neighbor.myNeighborBuffer;
    mySimilarityBuffer = neighbor.mySimilarityBuffer;
}


bool
coreNeighborable::checkValid()
{//solve the problem as possible, report the invalid if necessary
    for(int i = mySimilarityBuffer.size()-1; i >= 0; i--)
        if(!mySimilarityBuffer[i]->get_active())
            mySimilarityBuffer.removeAt(i);
    for(int i = myNeighborBuffer.size()-1; i >= 0; i--)
        if(!myNeighborBuffer[i]->get_active())
            myNeighborBuffer.removeAt(i);
    return true;
}


QVector<qreal>
coreNeighborable::compute_dis(coreNeighborable *otherNeighbor, int wSize, qreal *wList)
{
    assert(wSize >= 2);
    QVector<qreal> disList(3, MAXVALUE);
    if(!otherNeighbor) return disList;
    qreal sumDis = 0;
    QList<QPointF> myGridPoints = get_GridPoints();
    QList<QPointF> otherGridPoints = otherNeighbor->get_GridPoints();
    if(wList[0] > 0)
    {
        qreal dis1 = MAXVALUE;
        dis1 = Core2<QList, QPointF>::j_dis_avemin_mutual(myGridPoints, otherGridPoints, true);
        disList[0] = dis1;
        sumDis += (wList[0]*dis1);
    }
    if(wList[1] > 0)
    {
        QPointF mid1 = get_GridCenter();
        QPointF mid2 = otherNeighbor->get_GridCenter();
        qreal dis2 = j_dis1(mid1, mid2);
        disList[1] = dis2;
        sumDis += (wList[1]*dis2);
    }
    disList[2] = sumDis / (wList[0]+wList[1]);
    return disList;
}


//<<<<<<<<?need to find spatial neighborhood of those similar ones?>>>>>>>//

QList<coreNeighborable*>
coreNeighborable::find_neighborhood_core(coreGrider *cgrider, int flagSize, qreal *flags)
{
    assert(flagSize >= 2);
    qreal maxdis = flags[0];
    if(maxdis < 1) return QList<coreNeighborable*>();
    int   stepSize = int(flags[1]);

    coreEGrider<coreEGridUnit> *grider = dynamic_cast<coreEGrider<coreEGridUnit>*>(cgrider);
    if(!grider) return QList<coreNeighborable*>();

    if(get_lock()) return QList<coreNeighborable*>();
    set_lock(true);  //avoid self-selection

    if(grider->get_lock()) return QList<coreNeighborable*>();
    grider->set_lock(true);

    QList<QPointF> plist = get_GridPoints();
    QList<coreNeighborable*> allList;
    for(int i = 0; i < plist.size(); i+=stepSize)
    {
        QPointF point = plist[i];
        QList<coreNeighborable*> Elist = grider->get_elements(Vec2d(point.x(),point.y()), maxdis, true);
        allList.append(Elist);
    }
    for(int i = 0; i < allList.size(); i++)
        allList[i]->set_lock(false);

    set_lock(false);
    grider->set_lock(false);
    return allList;
}



QList<coreNeighborable*>
coreNeighborable::find_neighborhood_core(QList<coreNeighborable *> search_space, QList<qreal> *myDisList, int flagSize, qreal *flags)
{
    assert(flagSize >= 2);
    qreal maxdis = flags[0];
    int   maxsize = int(flags[1]);

    if(myDisList) myDisList->clear();

    QList<coreNeighborable*> mySpaceNeighbors;
    if(maxdis <= 0 || get_lock())
        return mySpaceNeighbors;
    if(get_activeNeighborBuffer() && !myNeighborBuffer.isEmpty())
    {
        mySpaceNeighbors = find_neighborhood_quick(myDisList, flagSize, flags);
        if(!mySpaceNeighbors.isEmpty()) return mySpaceNeighbors;
    }
    QList<coreNeighborable*>  candidateNeighbor;
    QList<qreal>   candidateDis;
    QList<coreNeighborable*>  candidateBuffer;
    int wSize = 2;
    qreal wList[2] = {1, 0};
    for(int i = 0; i < search_space.size(); i++)
    {
        qreal dis = compute_dis(search_space[i], wSize, wList).last();
        if(dis > 2*maxdis) continue;
        candidateBuffer.append(search_space[i]);
        if(dis > maxdis) continue;
        candidateNeighbor.append(search_space[i]);
        candidateDis.append(dis);
    }
    if(get_activeNeighborBuffer())
         myNeighborBuffer.clear();
    QList<int> indexs = Core1<QList, qreal>::j_sort(candidateDis, 1.5*maxsize, true);

    for(int k = 0; k < indexs.size(); k++)
    {
        int index = indexs.at(k);
        if(k < maxsize)
        {
            mySpaceNeighbors.append(candidateNeighbor[index]);
            if(myDisList) myDisList->append(candidateDis[index]);
        }
        if(get_activeNeighborBuffer())
            myNeighborBuffer.append(candidateNeighbor[index]);
    }
    return mySpaceNeighbors;
}



QList<coreNeighborable*>
coreNeighborable::find_neighborhood_quick(QList<qreal> *myDisList, int flagSize, qreal *flags)
{
    assert(flagSize >= 2);
    qreal maxdis = flags[0];
    int   maxsize = int(flags[1]);

    QList<coreNeighborable*> mySpaceNeighbors;
    if(myNeighborBuffer.isEmpty() || !get_activeNeighborBuffer()) return mySpaceNeighbors;
    QList<coreNeighborable*>  candidateNeighbor;
    QList<qreal>   candidateDis;
    int wSize = 3;
    qreal wList[3] = {1, 0, 0};
    for(int i = 0; i < myNeighborBuffer.size(); i++)
    {
        if(!myNeighborBuffer[i])
            continue;
        qreal dis = compute_dis(myNeighborBuffer[i], wSize, wList).last();
        if(dis < maxdis)
        {
            candidateDis.append(dis);
            candidateNeighbor.append(myNeighborBuffer[i]);
        }
    }
    QList<int> indexs = Core1<QList, qreal>::j_sort(candidateDis, maxsize, true);
    for(int k = 0; k < indexs.size(); k++)
    {
        int index = indexs.at(k);
        mySpaceNeighbors.append(candidateNeighbor[index]);
        if(myDisList) myDisList->append(candidateDis[index]);
    }
    return mySpaceNeighbors;
}


