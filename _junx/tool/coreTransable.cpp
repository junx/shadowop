#include "coreTransable.h"


void
TransformTransable::core_translate(QPointF t)
{
    QList<QPointF> dTranspoint = transable_get_point();
    dTranspoint = Core2<QList, QPointF>::j_translate(dTranspoint, t);
    transable_set_point(dTranspoint);
}


void
TransformTransable::core_transform(QPointF t, QVector2D rot, qreal s, QPointF orig)
{
    QList<QPointF> dTranspoint = transable_get_point();
    dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, t, rot, s, orig);
    transable_set_point(dTranspoint);
}


void
TransformTransable::core_transform(QPointF t, qreal s, QPointF orig)
{
    QList<QPointF> dTranspoint = transable_get_point();
    dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, t, s, orig);
    transable_set_point(dTranspoint);
}


void
TransformTransable::core_transform(QMatrix matrix)
{
    QList<QPointF> dTranspoint = transable_get_point();
    dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, matrix);
    transable_set_point(dTranspoint);
}


//<<<<<<<<<<<<<<<<<<special trans function>>>>>>>>>>>>>>>//
void
TransformTransable::translate(QPointF t)
{
    if(is_multi_transform & TRANSABLE_GROUP)
    {
        QList<TransformTransable*> dTrans = transable_get_children();
        for(int i = 0; i < dTrans.size(); i++)
            dTrans[i]->translate(t);
    }
    if(is_multi_transform & TRANSABLE_BASE)
        core_translate(t);
    transable_finish_transfer();
}


void
TransformTransable::transform(QPointF t, QVector2D rot, qreal s, QPointF orig)
{
    if(is_multi_transform & TRANSABLE_GROUP)
    {
        QList<TransformTransable*> dTrans = transable_get_children();
        for(int i = 0; i < dTrans.size(); i++)
            dTrans[i]->transform(t, rot, s, orig);
    }
    if(is_multi_transform & TRANSABLE_BASE)
        core_transform(t,rot,s,orig);
    transable_finish_transfer();
}


void
TransformTransable::transform(QPointF t, qreal s, QPointF orig)
{
    if(is_multi_transform & TRANSABLE_GROUP)
    {
        QList<TransformTransable*> dTrans = transable_get_children();
        for(int i = 0; i < dTrans.size(); i++)
            dTrans[i]->transform(t, s, orig);
    }
    if(is_multi_transform & TRANSABLE_BASE)
        core_transform(t,s,orig);
    transable_finish_transfer();
}


void
TransformTransable::transform(QMatrix matrix)
{
    if(is_multi_transform & TRANSABLE_GROUP)
    {
        QList<TransformTransable*> dTrans = transable_get_children();
        for(int i = 0; i < dTrans.size(); i++)
            dTrans[i]->transform(matrix);
    }
    if(is_multi_transform & TRANSABLE_BASE)
        core_transform(matrix);
    transable_finish_transfer();
}
