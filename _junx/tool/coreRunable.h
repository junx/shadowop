#ifndef CORERUNABLE_H
#define CORERUNABLE_H

#include "coreFunctionUtil.h"

class coreRunable
{
public:
    coreRunable(){isPipeLineLocked = false;}

    virtual void      reset(){isPipeLineLocked = false;}
    virtual bool      on_start(QEvent *){return false;}
    virtual bool      on_update(QEvent *){return false;}
    virtual bool      on_finish(QEvent *){return false;}
    virtual void      lock(bool is_lock){isPipeLineLocked = is_lock;}
    virtual bool      isLocked(){return isPipeLineLocked;}
protected:
    bool              isPipeLineLocked;
};


class coreThreadProcessable : public QObject
{
    Q_OBJECT
public slots:
    virtual void     on_start();
protected:
    virtual void     on_process(){}
signals:
    void             on_finish();
public:
    virtual bool     start_process_in_thread(QThread::Priority = QThread::IdlePriority);
};


#endif // CORERUNABLE_H
