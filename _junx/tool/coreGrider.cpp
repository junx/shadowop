#include "coreGrider.h"


coreGrider::coreGrider()
    :coreObject()
{
    grider_min_position = Vec2d(0,0);
    grider_max_position = Vec2d(0,0);
    grid_unit_number  = Vec2i(0,0);
    grid_unit_length = Vec2d(0,0);
}



coreGrider::coreGrider(Vec2d minPos, Vec2d maxPos, Vec2i unit_num)
    :coreObject()
{
    grider_min_position = minPos;
    grider_max_position = maxPos;
    grid_unit_number  = unit_num;
    if(!(grid_unit_number > Vec2i(0,0)) || !(grider_max_position > grider_min_position))
        return;
    grid_unit_length[0] = (grider_max_position[0]-grider_min_position[0])/grid_unit_number[0];
    grid_unit_length[1] = (grider_max_position[1]-grider_min_position[1])/grid_unit_number[1];
}


Vec2d
coreGrider::get_pos(Vec2i index)
{
    Vec2d p(index[0]*grid_unit_length[0], index[1]*grid_unit_length[1]);
    return p+grider_min_position;
}


Vec2i
coreGrider::get_unit(Vec2d pos)
{
    if(!(pos >= grider_min_position) || !(grider_max_position > pos))
        return Vec2i(-1,-1);
    Vec2d diff = pos - grider_min_position;
    int x = diff[0] / grid_unit_length[0];
    int y = diff[1] / grid_unit_length[1];
    return Vec2i(x,y);
}


QList<Vec2i>
coreGrider::get_units(Vec2d pos, qreal width)
{
    QList<Vec2i> ulist;
    if(!(pos >= grider_min_position) || !(grider_max_position > pos))
        return ulist;
    Vec2d diff = pos - grider_min_position;
    int x = diff[0] / grid_unit_length[0];
    int y = diff[1] / grid_unit_length[1];
    int gwidth = width / grid_unit_length[0] + 1;
    int gheight = width / grid_unit_length[1] + 1;
    int x1 = x-gwidth>0?x-gwidth:0;
    int x2 = (x+gwidth)<grid_unit_number[0]?(x+gwidth):(grid_unit_number[0]-1);
    int y1 = y-gheight>0?y-gheight:0;
    int y2 = (y+gheight)<grid_unit_number[1]?(y+gheight):(grid_unit_number[1]-1);
    for(int i = x1; i <= x2; i++)
        for(int j = y1; j <= y2; j++)
            ulist.append(Vec2i(i,j));
    return ulist;
}
