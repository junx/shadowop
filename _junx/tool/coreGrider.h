#ifndef COREGRIDER_H
#define COREGRIDER_H

#include "coreNeighborable.h"

class coreGrider : public coreObject
{
protected:
    Vec2d                grider_min_position;
    Vec2d                grider_max_position;
public:
    Vec2i                grid_unit_number;
    Vec2d                grid_unit_length;

    coreGrider();
    coreGrider(Vec2d minPos, Vec2d maxPos, Vec2i unit_num);
    virtual Vec2d               get_pos(Vec2i index);
    virtual Vec2i               get_unit(Vec2d pos);
    virtual QList<Vec2i>        get_units(Vec2d pos, qreal width);
};


class coreEGridUnit : public coreObject
{
public:
    QList<coreNeighborable*> elementList;
    QList<int>               componentIndex;
    coreEGridUnit():coreObject(){}
    virtual void reset(){coreObject::reset(); elementList.clear(); componentIndex.clear();}
    virtual void push_element(coreNeighborable*e, int index);
    virtual bool remove_element(coreNeighborable*, int);
};


template<class U>
class coreEGrider : public coreGrider
{
public:
    QVector<QVector<U> >     grid_units_element;
    int                      elementCount;

    coreEGrider():coreGrider(){}
    coreEGrider(Vec2d minPos, Vec2d maxPos, Vec2i unit_num):coreGrider(minPos, maxPos, unit_num){
        elementCount = 0;
        grid_units_element = QVector<QVector<U> >(grid_unit_number[0], QVector<U>(grid_unit_number[1], U()));
    }
    virtual void                reset();
    virtual void                setActive(bool is_active); //set elements active, not itself
    virtual QList<Vec2d>        get_selectable_samples(coreNeighborable*);
    virtual bool                push_element(coreNeighborable *element, bool avoidSame = true);
    virtual bool                remove_element(coreNeighborable *element, bool avoidSame = true);
    virtual QList<coreNeighborable*> select_element(U*le){if(!le) return QList<coreNeighborable*>(); return le->elementList;}
    virtual QList<coreNeighborable*> get_elements(Vec2d pos, qreal width, bool lock_selected = false);
    virtual QList<coreNeighborable*> get_elements(QPainterPath pRegion, bool lock_selected = false);
    int     getCount(){return elementCount;}
};


template<class U>
class coreVGridUnit : public coreObject
{
public:
    U            aveValue;
    coreVGridUnit():coreObject(){}
    virtual void reset(){coreObject::reset();}
    virtual void push_value(U, qreal){}
    virtual bool remove_value(U, qreal){return false;}
    virtual U    get_value(){return aveValue;}
};


template<class LV, class V>
class coreVGrider : public coreGrider
{
public:
    QVector<QVector<LV> >     grid_units_value;
    coreVGrider():coreGrider(){}
    coreVGrider(Vec2d minPos, Vec2d maxPos, Vec2i unit_num):coreGrider(minPos, maxPos, unit_num){
        grid_units_value = QVector<QVector<LV> >(grid_unit_number[0], QVector<LV>(grid_unit_number[1], LV()));
    }

    virtual void   reset();
    virtual void   setActive(bool is_active);//set elements active, not itself

    virtual void   push_value(Vec2d, V);
    virtual bool   remove_value(Vec2d, V);
    virtual V      get_value(Vec2d pos);
};



template<class U>
void
coreEGrider<U>::reset()
{
    coreObject::reset();
    for(int i = 0; i < grid_unit_number[0]; i++)
        for(int j = 0; j < grid_unit_number[1]; j++)
            grid_units_element[i][j].reset();
}


template<class U>
void
coreEGrider<U>::setActive(bool is_active)
{
    for(int i = 0; i < grid_unit_number[0]; i++)
        for(int j = 0; j < grid_unit_number[1]; j++)
            grid_units_element[i][j].set_active(is_active);
}


template<class U>
QList<Vec2d>
coreEGrider<U>::get_selectable_samples(coreNeighborable *e)
{
    QList<Vec2d> list;
    if(!e) return list;
    QList<QPointF> plist = e->get_GridPoints();
    for(int i = 0; i < plist.size(); i++)
        list.append(Vec2d(plist[i].x(), plist[i].y()));
    return list;
}


template<class U>
bool
coreEGrider<U>::push_element(coreNeighborable *e, bool avoidSame)
{
    if(!e) return false;
    QList<Vec2d> plist = get_selectable_samples(e);
    Vec2i previousPos(-1, -1);
    for(int i = 0; i < plist.size(); i++)
    {
        Vec2i pos = get_unit(plist[i]);
        if(avoidSame)
        {
            if(pos == previousPos)
                continue;
            previousPos = pos;
        }
        if(pos[0] < 0 || pos[1] < 0) continue;
        grid_units_element[pos[0]][pos[1]].push_element(e, i);
    }
    elementCount++;
    return true;
}



template<class U>
bool
coreEGrider<U>::remove_element(coreNeighborable *e, bool avoidSame)
{
    if(!e) return false;
    QList<Vec2d> plist = get_selectable_samples(e);
    Vec2i previousPos(-1, -1);
    for(int i = 0; i < plist.size(); i++)
    {
        Vec2i pos = get_unit(plist[i]);
        if(avoidSame)
        {
            if(pos == previousPos)
                continue;
            previousPos = pos;
        }
        if(pos[0] < 0 || pos[1] < 0) continue;
        assert(grid_units_element[pos[0]][pos[1]].remove_element(e, i));
    }
    elementCount--;
    return true;
}



template<class U>
QList<coreNeighborable*>
coreEGrider<U>::get_elements(Vec2d point, qreal width, bool lock_selected)
{
    QList<Vec2i> ulist = get_units(point, width);
    QList<coreNeighborable*> elist;
    for(int i = 0; i < ulist.size(); i++)
    {
        Vec2i pos = ulist[i];
        if(pos[0]<0 || pos[1]<0) continue;
        QList<coreNeighborable*> list = select_element(&grid_units_element[pos[0]][pos[1]]);
        for(int j= 0; j < list.size(); j++)
        {
            if(!list[j]->get_lock()) //selected not locked ones
            { //use this to avoid repeat selection
                elist.append(list[j]);
                list[j]->set_lock(true); //lock the selected ones
            }
        }
    }
    if(!lock_selected)
        for(int i = 0; i < elist.size(); i++)
            elist[i]->set_lock(false); //unlock
    return elist;
}

template<class U>
QList<coreNeighborable*>
coreEGrider<U>::get_elements(QPainterPath pRegion, bool lock_selected)
{
    QRectF region = pRegion.boundingRect();
    QPointF p1 = region.topLeft(), p2 = region.bottomRight();
    Vec2i q1 = get_unit(Vec2d(p1.x(), p2.y())), q2 = get_unit(Vec2d(p2.x(), p2.y()));
    QList<coreNeighborable*> elist;
    for(int i = q1[0]; i < q1[1]; i++)
    {
        for(int j = q1[0]; j < q2[1]; j++)
        {
            Vec2d p = get_pos(Vec2i(i, j));
            if(!pRegion.contains(QPointF(p[0], p[1])))
                continue;
            QList<coreNeighborable*> list = select_element(&grid_units_element[i][j]);
            for(int j= 0; j < list.size(); j++)
            {
                if(!list[j]->get_lock()) //selected not locked ones
                { //use this to avoid repeat selection
                    elist.append(list[j]);
                    list[j]->set_lock(true); //lock the selected ones
                }
            }
        }
    }
    if(!lock_selected)
        for(int i = 0; i < elist.size(); i++)
            elist[i]->set_lock(false); //unlock
    return elist;
}



template<class LV, class V>
void
coreVGrider<LV, V>::reset()
{
    coreObject::reset();
    for(int i = 0; i < grid_unit_number[0]; i++)
        for(int j = 0; j < grid_unit_number[1]; j++)
            grid_units_value[i][j].reset();
}

template<class LV, class V>
void
coreVGrider<LV, V>::setActive(bool is_active)
{
    for(int i = 0; i < grid_unit_number[0]; i++)
        for(int j = 0; j < grid_unit_number[1]; j++)
            grid_units_value[i][j].set_active(is_active);
}



template<class LV, class V>
void
coreVGrider<LV,V>::push_value(Vec2d point, V v)
{
    Vec2i pos = get_unit(point);
    if(pos[0] < 0 || pos[1] < 0) return;
    grid_units_value[pos[0]][pos[1]].push_value(v, 1);
}


template<class LV, class V>
bool
coreVGrider<LV, V>::remove_value(Vec2d point, V v)
{
    Vec2i pos = get_unit(point);
    if(pos[0] < 0 || pos[1] < 0) return false;
    grid_units_value[pos[0]][pos[1]].remove_value(v, 1);
    return true;
}


template<class LV, class V>
V
coreVGrider<LV, V>::get_value(Vec2d point)
{
    Vec2i pos = get_unit(point);
    if(pos[0] < 0 || pos[1] < 0)
        pos = Vec2i(0,0);
    return grid_units_value[pos[0]][pos[1]].get_value();
}


#endif // coreGrider
