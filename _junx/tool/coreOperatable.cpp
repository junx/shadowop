#include "coreOperatable.h"


Operatable::Operatable()
    :coreObject()
{
    operatablePropertyFlags = 0x00;
    set_systemAuto(false);
}



Operatable::Operatable(const Operatable &cop)
    :coreObject(cop)
{
    operatablePropertyFlags = cop.operatablePropertyFlags;
    time_stamp = cop.time_stamp;
}


void
Operatable::write(QTextStream *out)
{
    coreObject::write(out);
    //<<<<<<<<<out>>>>>>>>>//
    time_stamp = QTime::currentTime();
    *out<<"operatable "<<operatablePropertyFlags<<"\n";
    *out<<time_stamp.toString()<<"\n";
}


bool
Operatable::read(QTextStream *in)
{
    coreObject::read(in);

    QString line;
    line = in->readLine();
    while(line.isEmpty())
        line = in->readLine();
    QStringList list = line.split(" ");
    if(list[0].compare("operatable"))
        return false;
    operatablePropertyFlags = list[1].toUInt();
    line = in->readLine();
    time_stamp = QTime::fromString(line);
    return true;
}
