#ifndef COREOBJECT_H
#define COREOBJECT_H

#include "coreFunctionUtil.h"

//the last 8 bits for most basic property settings, others for custom setting>>>>>>>//
class coreObject
{
    int             coreObjectIdentity; //can't be copied, but only assigned by outside process/manager
    ushort          coreObjectPropertyFlag;
public:
    coreObject(){reset();}
    coreObject(const coreObject &a){coreObjectPropertyFlag = a.coreObjectPropertyFlag; coreObjectIdentity = -1;}

    bool operator == (coreObject &a){return (coreObjectIdentity == a.get_identity())?true:false;}
    bool operator!=(coreObject &a){return (coreObjectIdentity != a.get_identity())?true:false;}
    bool operator <(coreObject &a){return (coreObjectIdentity < a.get_identity())?true:false;}
    bool operator >(coreObject &a){return (coreObjectIdentity > a.get_identity())?true:false;}
    bool operator <=(coreObject &a){return (coreObjectIdentity <= a.get_identity())?true:false;}
    bool operator >=(coreObject &a){return (coreObjectIdentity >= a.get_identity())?true:false;}

    virtual void     reset(){coreObjectIdentity = -1; coreObjectPropertyFlag = 0x00;
                             set_active(true); set_lock(false);
                             set_valid(true); set_busy(false);}

    //<<<<<<<for merge, cluster, and compare>>>>>>//
    inline  int      get_identity(){return coreObjectIdentity;}
    inline  void     set_identity(int id){coreObjectIdentity = id;}
    //<<<<<<implement via bits in coreObjectPropertyFlag>>>>>>>//
    //<<<<<<if not active, completed useless>>>>>>>>>>//
    inline bool      get_active(){return get_bit(0);}
    inline void      set_active(bool a){set_bit(a, 0);}

    //<<<<<if locked, couldn't be selected>>>>>>>>>>//
    //<<<<only for temporal use, appear in pairs to avoid dead lock>>>//
    inline bool      get_lock(){return get_bit(1);}
    inline void      set_lock(bool a){set_bit(a, 1);}

    //<<<<<<<if busy, can't be deleted, must appear in pairs>>>>>//
    inline bool      get_busy(){return get_bit(2);}
    inline void      set_busy(bool a){set_bit(a, 2);}

    //<<<<<<not complete, something is wrong, need check and fix before being added>>>>>//
    //<<<<<<<<<<only accecced by checkValid method, cann't be set outside>>>>>>>>>>//
    inline bool      get_valid(){return get_bit(3);}
    inline void      set_valid(bool a){set_bit(a, 3);}
    virtual bool     checkValid(){return true;} //solve the problem here if possible, report the invalid if necessary

    virtual void     write(QTextStream *out){*out<<"coreObject"<<" "<<coreObjectIdentity<<" "<<coreObjectPropertyFlag<<"\n";}
    virtual bool     read(QTextStream *in){
        QString line = in->readLine();
        while(line.isEmpty())
            line = in->readLine();
        QStringList list = line.split(" ");
        if(list[0].compare("coreObject"))
            return false;
        coreObjectIdentity = list[1].toInt();
        coreObjectPropertyFlag = list[2].toUShort();
        return true;
    }

protected:
    inline ushort    get_coreObjectFlags(){return coreObjectPropertyFlag;}
    inline void      set_coreObjectFlags(ushort a){coreObjectPropertyFlag = a;}

private:
    bool     get_bit(int bitPos){return (coreObjectPropertyFlag & (0x01 << bitPos))?true:false;}
    void     set_bit(bool a, int bitPos){a?(coreObjectPropertyFlag |= (0x01 << bitPos)):(coreObjectPropertyFlag &= ~(0x01 << bitPos));}
};

#endif // COREOBJECT_H
