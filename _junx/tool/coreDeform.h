#ifndef COREDEFORM_H
#define COREDEFORM_H

#include "coreFunctionUtil.h"

/*
template<class T>
class DeformCoreData
{
    QList<QPointF>                dTranspoint;
public:
    T                            *dSource;
    DeformCoreData(){dSource = 0;}
    DeformCoreData(T *source){dSource = source; dTranspoint = source->get_point();if(dTranspoint.isEmpty()) dSource = 0;}
};


template<class T>
class DeformCageData : public DeformCoreData
{
    QList<QList<Complexd> >       dCageParameter;
public:
    DeformCageData():DeformCoreData(){}
};


class DeformCtrlData
{
    QList<QPointF>                dTranspoint;
    QList<QList<Complexd> >       dCageParameter;
    QList<QList<Complexd> >       dCtrlParameter;
};
*/

class coreDeformable
{
public:
    QList<QList<QPointF> >          dTranspoint;

    virtual void                    translated(QPointF t){dTranspoint = Core2<QList, QPointF>::j_translate(dTranspoint, t);}
    virtual void                    transformed(QPointF t, QVector2D rot, qreal s, QPointF orig){dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, t, rot, s, orig);}
    virtual void                    transformed(QPointF t, qreal s, QPointF orig){dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, t, s, orig);}
    virtual void                    transformed(QMatrix matrix){dTranspoint = Core2<QList, QPointF>::j_transform(dTranspoint, matrix);}

    virtual QList<QList<QPointF> >  forwardDeformed(const QList<QList<QPointF> > &){return QList<QList<QPointF> >();}
    virtual QList<QList<QPointF> >  forwardDeform(const QList<QList<QPointF> > &){return QList<QList<QPointF> >();}
    virtual QList<QList<QPointF> >  backwardDeformed(const QList<QList<QPointF> > &){return QList<QList<QPointF> >();}
    virtual QList<QList<QPointF> >  backwardDeform(const QList<QList<QPointF> > &){return QList<QList<QPointF> >();}

    virtual QList<QPointF>          forwardDeformed(const QList<QPointF> &){return QList<QPointF>();}
    virtual QList<QPointF>          forwardDeform(const QList<QPointF> &){return QList<QPointF>();}
    virtual QList<QPointF>          backwardDeformed(const QList<QPointF> &){return QList<QPointF>();}
    virtual QList<QPointF>          backwardDeform(const QList<QPointF> &){return QList<QPointF>();}
};



class DeformableCage : public coreDeformable
{
public:
    DeformableCage(){}
    DeformableCage(QList<QPointF>);
    DeformableCage(QList<QList<QPointF> >);

    QList<Complexd>     dOldCage;
    QList<Complexd>     dNewCage;
    QList<QList<QList<Complexd> > > dCageParameter;

    virtual void           translated(QPointF t){dOldCage = Core2<QList, Complexd>::j_translate(dOldCage, j_to_complex(t));
                                                dOldCage = dNewCage = Core2<QList, Complexd>::j_translate(dNewCage, j_to_complex(t));}
    virtual void           transformed(QPointF t, QVector2D rot, qreal s, QPointF orig){dOldCage = Core2<QList, Complexd>::j_transform(dOldCage, j_to_complex(t), rot, s, j_to_complex(orig));
                                                                                       dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, j_to_complex(t), rot, s, j_to_complex(orig));}
    virtual void           transformed(QPointF t, qreal s, QPointF orig){dOldCage = Core2<QList, Complexd>::j_transform(dOldCage, j_to_complex(t), s, j_to_complex(orig));
                                                                        dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, j_to_complex(t), s, j_to_complex(orig));}
    virtual void           transformed(QMatrix matrix){dOldCage = Core2<QList, Complexd>::j_transform(dOldCage, matrix);
                                                      dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, matrix);}

    void                    translate(QPointF t){dNewCage = Core2<QList, Complexd>::j_translate(dNewCage, j_to_complex(t));}
    void                    transform(QPointF t, QVector2D rot, qreal s, QPointF orig){dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, j_to_complex(t), rot, s, j_to_complex(orig));}
    void                    transform(QPointF t, qreal s, QPointF orig){dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, j_to_complex(t), s, j_to_complex(orig));}
    void                    transform(QMatrix matrix){dNewCage = Core2<QList, Complexd>::j_transform(dNewCage, matrix);}

    QList<QList<QPointF> >  forwardDeformed(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(forwardDeformed(plist[i])); return dlist;}
    QList<QList<QPointF> >  forwardDeform(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(forwardDeform(plist[i])); return dlist;}
    QList<QList<QPointF> >  backwardDeformed(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(backwardDeformed(plist[i])); return dlist;}
    QList<QList<QPointF> >  backwardDeform(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(backwardDeform(plist[i])); return dlist;}

    QList<QPointF>          forwardDeformed(const QList<QPointF> &plist);
    QList<QPointF>          forwardDeform(const QList<QPointF> &plist);
    QList<QPointF>          backwardDeformed(const QList<QPointF> &plist);
    QList<QPointF>          backwardDeform(const QList<QPointF> &plist);
};




class DeformableCtrl : public DeformableCage
{
public:
    DeformableCtrl(){}
    DeformableCtrl(QList<QPointF>);
    DeformableCtrl(QList<QList<QPointF> >);
    QList<Complexd>       dOldConstrain;
    QList<Complexd>       dNewConstrain;
    Matrix<Complexd>      Nmatrix;
    QList<QList<QList<Complexd> > > dCtrlParameter;

    virtual void            translated(QPointF t){dOldConstrain = Core2<QList, Complexd>::j_translate(dOldConstrain, j_to_complex(t));
                                                dNewConstrain = Core2<QList, Complexd>::j_translate(dNewConstrain, j_to_complex(t));
                                                 DeformableCage::translated(t);}
    virtual void            transformed(QPointF t, QVector2D rot, qreal s, QPointF orig){dOldConstrain = Core2<QList, Complexd>::j_transform(dOldConstrain, j_to_complex(t), rot, s, j_to_complex(orig));
                                                                                       dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, j_to_complex(t), rot, s, j_to_complex(orig));
                                                                                        DeformableCage::transformed(t, rot, s, orig);}
    virtual void            transformed(QPointF t, qreal s, QPointF orig){dOldConstrain = Core2<QList, Complexd>::j_transform(dOldConstrain, j_to_complex(t), s, j_to_complex(orig));
                                                                        dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, j_to_complex(t), s, j_to_complex(orig));
                                                                         DeformableCage::transformed(t, s, orig);}
    virtual void            transformed(QMatrix matrix){dOldConstrain = Core2<QList, Complexd>::j_transform(dOldConstrain, matrix);
                                                      dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, matrix);
                                                       DeformableCage::transformed(matrix);}

    void                    translate(QPointF t){dNewConstrain = Core2<QList, Complexd>::j_translate(dNewConstrain, j_to_complex(t));}
    void                    transform(QPointF t, QVector2D rot, qreal s, QPointF orig){dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, j_to_complex(t), rot, s, j_to_complex(orig));}
    void                    transform(QPointF t, qreal s, QPointF orig){dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, j_to_complex(t), s, j_to_complex(orig));}
    void                    transform(QMatrix matrix){dNewConstrain = Core2<QList, Complexd>::j_transform(dNewConstrain, matrix);}

    QList<QList<QPointF> >  forwardDeformed(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(forwardDeformed(plist[i])); return dlist;}
    QList<QList<QPointF> >  forwardDeform(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(forwardDeform(plist[i])); return dlist;}
    QList<QList<QPointF> >  backwardDeformed(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(backwardDeformed(plist[i])); return dlist;}
    QList<QList<QPointF> >  backwardDeform(const QList<QList<QPointF> > &plist){QList<QList<QPointF> > dlist; for(int i = 0; i < plist.size(); i++) dlist.append(backwardDeform(plist[i])); return dlist;}

    QList<QPointF>          forwardDeformed(const QList<QPointF> &plist);
    QList<QPointF>          forwardDeform(const QList<QPointF> &plist);
    QList<QPointF>          backwardDeformed(const QList<QPointF> &plist);
    QList<QPointF>          backwardDeform(const QList<QPointF> &plist);
};


#endif // COREDEFORM_H
