#ifndef CORECONTAINER_H
#define CORECONTAINER_H

#include "coreObject.h"

template<class T>
class coreHistoryer
{
public:
    coreHistoryer(){myElementCount = myCurElementCount = 0; isDownUpPush = true; isUnRedo = false;}
    ~coreHistoryer(){}
    virtual void         reset(){
        isDownUpPush = true;
        isUnRedo = false;
        myCurElementCount = 0;
        branch_clear();
        myElementCount = 0;
        myElementList.clear();
    }
    T                    getFirstElement(){assert(!myElementList.isEmpty()); return myElementList.first();}
    T                    getLastElement(){assert(!myElementList.isEmpty()); return myElementList.last();}
    T                    getCurrentElement(){assert(myCurElementCount > 0 && myCurElementCount <= myElementList.size()); return myElementList[myCurElementCount-1];}
    QList<T>             getElementList(){return myElementList;}
    void                 setElementList(QList<T> elist){myElementList = elist;}
    int                  getCurElementCount(){return myCurElementCount;}
    void                 setCurElementCount(int curCount){myCurElementCount = curCount;}
    int                  getElementCount(){return myElementCount;}
    void                 setElementCount(int eCount){myElementCount = eCount;}
    virtual void         pushElementsToHistory(QList<T> elements){
        branch_clear();
        myElementCount+=elements.size();
        myCurElementCount += elements.size();
        myElementList.append(elements);
    }
    virtual void         pushElementToHistory(T element){
        branch_clear();
        myElementCount++;
        myCurElementCount++;
        myElementList.append(element);
    }
 //   virtual void         updateElement(T){}

    virtual void         branch_clear(){
        if(!isUnRedo) return;
        isUnRedo = false;
        for(int i = myElementCount-1; i >= myCurElementCount; i--)
            myElementList.removeLast();
        myElementCount = myCurElementCount;
    }

    virtual void         onRedo(T)
    {
        if(myCurElementCount >= myElementCount)  //this is the last one
            return;
        isUnRedo = true;
        myCurElementCount++;
    }

    virtual void         onUndo(T){
        if(myCurElementCount <= 0)  //this is alrealy the first
            return;
        isUnRedo = true;
        myCurElementCount--;
    }

    virtual bool      is_equal(T , T){return false;}

    int               search_at(T target);
    QList<T>          search_before(T target, int size);
    QList<T>          search_after(T target, int size);

    bool         isDownUpPush;
    bool         isUnRedo;
protected:
    int          myElementCount;
    int          myCurElementCount;
    QList<T>     myElementList;
    virtual void   record_state(){}
    virtual void   clear_state(){}
};


template<class T>
class coreHistoryData
{
    int          myElementCount;
    int          myCurElementCount;
    QList<T>     myElementList;
public:
    coreHistoryData(){myElementCount = myCurElementCount = 0;}
    int  getElementCount(){return myElementCount;}
    T    getElementAt(int id){return myElementList[id];}

    void importData(coreHistoryer<T> *historyer){
        myElementCount = historyer->getElementCount();
        myCurElementCount = historyer->getCurElementCount();
        myElementList = historyer->getElementList();
    }
    void exportData(coreHistoryer<T> *historyer)
    {
        historyer->setElementCount(myElementCount);
        historyer->setCurElementCount(myCurElementCount);
        historyer->setElementList(myElementList);
        historyer->isDownUpPush = true;
        historyer->isUnRedo = false;
    }
};



template<class T>
int
coreHistoryer<T>::search_at(T target)
{
    for(int i = myCurElementCount-1; i >= 0; i--)
        if(is_equal(myElementList[i], target))
            return i;
    return -1;
}


template<class T>
QList<T>
coreHistoryer<T>::search_before(T target, int size)
{
    QList<T> befores;
    int position = search_at(target);
    if(position < 0) return befores;
    for(int i = position-1; i >= 0 && size > 0; i--, size--)
        befores.append(myElementList[i]);
    return befores;
}

template<class T>
QList<T>
coreHistoryer<T>::search_after(T target, int size)
{
    QList<T> afters;
    int position = search_at(target);
    if(position < 0) return afters;
    for(int i = position+1; i < myCurElementCount && size > 0; i++, size--)
        afters.append(myElementList[i]);
    return afters;
}




class coreUndoer : public QObject, public coreHistoryer<uint>
{
    Q_OBJECT
public:
    coreUndoer();
    ~coreUndoer(){}

    int                getLastRecord(){return myLastRecord;}
    void               setLastRecord(int lr){myLastRecord = lr;}
    int                getStepSize(){return myStepSize;}
    void               setStepSize(int ss){myStepSize = ss;}
    QList<uint>        getElementIndexList(){return ElementIndexList;}
    void               setElementIndexList(QList<uint> il){ElementIndexList = il;}
    QList<uint>        getStateIndexList(){return StateIndexList;}
    void               setStateIndexList(QList<uint> sl){StateIndexList = sl;}

    virtual void       reset();
    virtual void       pushElementToHistory(uint elementId);
    virtual void       replay_all();

public slots:
    virtual void        onRedo(uint v = 0);
    virtual void        onUndo(uint v = 0);

protected:
    //<<<<<<<<need to be overload>>>>>>>>>>//
    virtual void       runto(uint){}
    virtual void       branch_clear();

protected:
    int          myLastRecord;
    int          myStepSize;

    QList<uint>  ElementIndexList;
    QList<uint>  StateIndexList;
};



class coreUndoerData
{
    coreHistoryData<uint>  myHisotryData;
//<<<<<<<<<<<<coreUndoer data>>>>>>>>>>>//
    int              myLastRecord;
    int              myStepSize;
    QList<uint>      ElementIndexList;
    QList<uint>      StateIndexList;

public:
    coreUndoerData(){myLastRecord = myStepSize = 0;}
    void importData(coreUndoer *undoer){
        myHisotryData.importData(undoer);

        myLastRecord = undoer->getLastRecord();
        myStepSize = undoer->getStepSize();
        ElementIndexList = undoer->getElementIndexList();
        StateIndexList = undoer->getStateIndexList();
    }
    void exportData(coreUndoer *undoer)
    {
        myHisotryData.exportData(undoer);
        //<<<<<<<<<<coreUndoer data>>>>>>>>>>//
        undoer->setLastRecord(myLastRecord);
        undoer->setStepSize(myStepSize);
        undoer->setElementIndexList(ElementIndexList);
        undoer->setStateIndexList(StateIndexList);
    }
};


#endif // CORECONTAINER_H
