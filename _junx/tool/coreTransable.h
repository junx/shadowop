#ifndef CORETRANSABLE_H
#define CORETRANSABLE_H

#include "coreFunctionUtil.h"

#define TRANSABLE_BASE    1
#define TRANSABLE_GROUP   2
#define TRANSABLE_MUTLI   3


class TransformTransable
{
    ushort is_multi_transform;
public:
    TransformTransable(){is_multi_transform = TRANSABLE_BASE;}
    TransformTransable(const TransformTransable *ctrans){is_multi_transform = ctrans->is_multi_transform;}
    TransformTransable(unsigned short flag){is_multi_transform = flag;}
    inline void         set_flag(unsigned short flag){is_multi_transform = flag;}
    inline unsigned int get_flag(){return is_multi_transform;}

protected:
    virtual QList<TransformTransable*>   transable_get_children(){return QList<TransformTransable*>();}
    virtual void                         transable_finish_transfer(){}
    //<<<<<<<<<<<<<need to be implemented>>>>>>>>>>>>>>>//
    virtual QList<QPointF> transable_get_point(){return QList<QPointF>();}
    virtual void           transable_set_point(QList<QPointF>){}

private:
    virtual void   core_translate(QPointF t);
    virtual void   core_transform(QPointF t, QVector2D rot, qreal s, QPointF orig);
    virtual void   core_transform(QPointF t, qreal s, QPointF orig);
    virtual void   core_transform(QMatrix matrix);

public:
    //<<<<<<<<<<<<<<<<<<special trans function>>>>>>>>>>>>>>>//
    virtual void  translate(QPointF t);
    virtual void  transform(QPointF t, QVector2D rot, qreal s, QPointF orig);
    virtual void  transform(QPointF t, qreal s, QPointF orig);
    virtual void  transform(QMatrix matrix);
};

#endif // CORETRANSABLE_H
