#include "coreGrider.h"
#include "coreNeighborable.h"

void
coreEGridUnit::push_element(coreNeighborable *e, int index)
{
    elementList.append(e);
    componentIndex.append(index);
}


bool
coreEGridUnit::remove_element(coreNeighborable *e, int index)
{
    for(int i = elementList.size()-1; i>= 0; i--)
    {
        if(elementList[i] == e && componentIndex[i] == index)
        {
            elementList.removeAt(i);
            componentIndex.removeAt(i);
            return true;
        }
    }
    return false;
}
