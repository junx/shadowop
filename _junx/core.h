#ifndef CORE_H
#define CORE_H

#include "coreglobal.h"

template<class T> inline double  core_mag2(T v){return v*v;}
template<class T> inline double  core_mag1(T v){return v;}
template<class T> inline double  core_dis2(T v1, T v2){return (v1-v2)*(v1-v2);}
template<class T> inline double  core_dis1(T v1, T v2){return core_abs(v1-v2);}
template<class T> inline T       core_norm(T v){return v;}
template<class T> inline T       core_max(T v1, T v2){return v1>v2?v1:v2;}
template<class T> inline T       core_min(T v1, T v2){return v1<v2?v1:v2;}
template<class T> inline T       core_ave(T v1, T v2){return (v1+v2)*0.5;}
template<class T> inline T       core_scale(T v, double scale){return v*scale;}
template<class T> inline T       core_transform(T v1, T t, double scale, T v2 = 0){return (v1-v2)*scale+v2+t;}
template<class T> inline T       core_interpolate(T v1, T v2, double scale){return v1*(1-scale)+v2*scale;}
template<class T> inline T       core_abs(T v){return v>0?v:-v;}
template<class T> inline T       core_diff(T v1, T v2){return v1-v2;}


#ifndef VEC_H
#define VEC_H
template<unsigned int N, typename T>
class Vec
{
protected:
    T v[N];
public:
    Vec<N,T>(void){}
    Vec<N,T>(const Vec<N,T> &c){assert(N > 0); for(unsigned int i=0; i<N; ++i) v[i]=c[i]; }
    Vec<N,T>(T v0){assert(N > 0); for(unsigned int i=0; i<N; i++) v[i]= v0; }
    template<class S>
    Vec<N,T>(const S *source){ for(unsigned int i=0; i<N; ++i) v[i]=(T)source[i];}
    Vec<N,T>(T v0, T v1){ assert(N == 2);  v[0]=v0; v[1]=v1;}
    Vec<N,T>(T v0, T v1, T v2){assert(N == 3); v[0]=v0; v[1]=v1; v[2]=v2;}
    Vec<N,T>(T v0, T v1, T v2, T v3){assert(N == 4); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4){assert(N == 5); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5){assert(N == 6); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6){assert(N == 7); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6, T v7){assert(N == 8); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8){assert(N == 9); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7; v[8] = v8;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9){assert(N == 10); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7; v[8] = v8; v[9] = v9;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9, T v10){assert(N == 11); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7; v[8] = v8; v[9] = v9; v[10] = v10;}
    Vec<N,T>(T v0, T v1, T v2, T v3, T v4, T v5, T v6, T v7, T v8, T v9, T v10, T v11){assert(N == 12); v[0]=v0; v[1]=v1; v[2]=v2; v[3]=v3; v[4] = v4; v[5] = v5; v[6] = v6; v[7] = v7; v[8] = v8; v[9] = v9; v[10] = v10; v[11] = v11;}

    T &operator[](int index){ assert(index >= 0 && (unsigned int)index < N); return v[index];}
    const T &operator [](int index) const{assert(index >= 0 && (unsigned int)index < N); return v[index]; return v[0];}
    Vec<N,T> operator +=(const Vec<N,T> &w){for(unsigned int i=0; i<N; ++i) v[i]+=w[i]; return *this;}
    Vec<N,T> operator +(const Vec<N,T> &w) const{Vec<N,T> sum(*this); sum+=w; return sum;}
    Vec<N,T> operator -=(const Vec<N,T> &w){for(unsigned int i=0; i<N; ++i) v[i]-=w[i]; return *this;}
    Vec<N,T> operator -(void) const { Vec<N,T> negative; for(unsigned int i=0; i<N; ++i) negative.v[i]=-v[i]; return negative;}
    Vec<N,T> operator -(const Vec<N,T> &w) const{Vec<N,T> diff(*this); diff-=w; return diff;}
    Vec<N,T> operator *=(T a) {for(unsigned int i=0; i<N; ++i) v[i]*=a; return *this;}
    Vec<N,T> operator *(T a) const{ Vec<N,T> w(*this); w*=a; return w;}
    Vec<N,T> operator *(const Vec<N,T> &w) const { Vec<N,T> componentwise_product; for(unsigned int i=0; i<N; ++i) componentwise_product[i]=v[i]*w.v[i]; return componentwise_product;}
    Vec<N,T> operator /=(T a){assert(a != 0); for(unsigned int i=0; i<N; ++i) v[i]/=a; return *this;}
    Vec<N,T> operator /(T a) const{assert(a != 0); Vec<N,T> w(*this); w/=a; return w;}
    bool operator ==(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] != a[i]) return false; return true;}
    bool operator!=(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] != a[i]) return true; return false;}
    bool operator <(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] >= a[i]) return false; return true;}
    bool operator >(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] <= a[i]) return false; return true;}
    bool operator <=(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] > a[i]) return false; return true;}
    bool operator >=(const Vec<N,T> &a){for(unsigned int i = 0; i < N; i++) if(v[i] < a[i]) return false; return true;}
    Vec<N,T> operator /=(Vec<N,T> b){*this = *this/b; return *this;}
    Vec<N,T> operator /(Vec<N,T> b) const{
        assert(N == 2);
        T breal = b[0], bimaginary = b[1];
        if(breal == 0 && bimaginary == 0)
            return Vec(0,0);
        T down = breal * breal + bimaginary * bimaginary;
        T r = v[0] * breal + v[1] * bimaginary;
        T i = v[1] * breal - v[0] * bimaginary;
        return Vec<N,T>(r/down, i/down);
    }
};

typedef Vec<2, bool>          Vec2b;
typedef Vec<2,double>         Vec2d;
typedef Vec<2,float>          Vec2f;
typedef Vec<2,int>            Vec2i;
typedef Vec<2,unsigned int>   Vec2ui;
typedef Vec<2,short>          Vec2s;
typedef Vec<2,unsigned short> Vec2us;
typedef Vec<2,char>           Vec2c;
typedef Vec<2,unsigned char>  Vec2uc;

typedef Vec<3, bool>          Vec3b;
typedef Vec<3,double>         Vec3d;
typedef Vec<3,float>          Vec3f;
typedef Vec<3,int>            Vec3i;
typedef Vec<3,unsigned int>   Vec3ui;
typedef Vec<3,short>          Vec3s;
typedef Vec<3,unsigned short> Vec3us;
typedef Vec<3,char>           Vec3c;
typedef Vec<3,unsigned char>  Vec3uc;

typedef Vec<4, bool>          Vec4b;
typedef Vec<4,double>         Vec4d;
typedef Vec<4,float>          Vec4f;
typedef Vec<4,int>            Vec4i;
typedef Vec<4,unsigned int>   Vec4ui;
typedef Vec<4,short>          Vec4s;
typedef Vec<4,unsigned short> Vec4us;
typedef Vec<4,char>           Vec4c;
typedef Vec<4,unsigned char>  Vec4uc;

typedef Vec<5, bool>          Vec5b;
typedef Vec<5,double>         Vec5d;
typedef Vec<5,float>          Vec5f;
typedef Vec<5,int>            Vec5i;
typedef Vec<5,unsigned int>   Vec5ui;
typedef Vec<5,short>          Vec5s;
typedef Vec<5,unsigned short> Vec5us;
typedef Vec<5,char>           Vec5c;
typedef Vec<5,unsigned char>  Vec5uc;

typedef Vec<6, bool>          Vec6b;
typedef Vec<6,double>         Vec6d;
typedef Vec<6,float>          Vec6f;
typedef Vec<6,int>            Vec6i;
typedef Vec<6,unsigned int>   Vec6ui;
typedef Vec<6,short>          Vec6s;
typedef Vec<6,unsigned short> Vec6us;
typedef Vec<6,char>           Vec6c;
typedef Vec<6,unsigned char>  Vec6uc;

typedef Vec<7, bool>          Vec7b;
typedef Vec<7,double>         Vec7d;
typedef Vec<7,float>          Vec7f;
typedef Vec<7,int>            Vec7i;
typedef Vec<7,unsigned int>   Vec7ui;
typedef Vec<7,short>          Vec7s;
typedef Vec<7,unsigned short> Vec7us;
typedef Vec<7,char>           Vec7c;
typedef Vec<7,unsigned char>  Vec7uc;

typedef Vec<8, bool>          Vec8b;
typedef Vec<8,double>         Vec8d;
typedef Vec<8,float>          Vec8f;
typedef Vec<8,int>            Vec8i;
typedef Vec<8,unsigned int>   Vec8ui;
typedef Vec<8,short>          Vec8s;
typedef Vec<8,unsigned short> Vec8us;
typedef Vec<8,char>           Vec8c;
typedef Vec<8,unsigned char>  Vec8uc;
#endif //VEC_H


//<<<<<<<list class template function, need to implement .size(), .append() (prepend()>>>>>>>//
template<template<class> class L, class T> inline double
core2_mag2(const L<T> &a){unsigned int N = a.size(); if(N <= 0) return a; double l= j_mag2(a[0]); for(unsigned int i=1; i<N; ++i) l+=j_mag2(a[i]); return l;}
template<template<class> class L, class T> inline double
core2_mag1(const L<T> &a){return sqrt(j_mag2(a));}
template<template<class> class L, class T> inline double
core2_dis2(const L<T> &a, const L<T> &b){unsigned int N = a.size();if(N <= 0) return 10e6;if(b.size()!=N) return 10e6; double d= j_dis2(a[0], b[0]);for(unsigned int i=1; i<N; ++i) d+=j_dis2(a[i], b[i]);return d;}
template<template<class> class L, class T> inline double
core2_dis1(const L<T> &a, const L<T> &b){unsigned int N = a.size();if(N <= 0) return 10e6;if(b.size()!=N) return 10e6; double d= j_dis2(a[0], b[0]);for(unsigned int i=1; i<N; ++i) d+=j_dis2(a[i], b[i]);return sqrt(d);}
template<template<class> class L, class T> inline L<T>
core2_norm(const L<T> &a){L<T> b = a; for(unsigned int i=0; i<a.size(); i++) b[i]=j_norm(a[i]);return b;}
template<template<class> class L, class T> inline T
core2_max(const L<T> &a){unsigned int N = a.size();if(N <= 0) return T();T m=a[0];for(unsigned int i=1; i<N; i++) if(a[i]>m) m=a[i];return m;}
template<template<class> class L, class T> inline T
core2_min(const L<T> &a){unsigned int N = a.size();if(N <= 0) return T();T m=a[0];for(unsigned int i=1; i<N; i++) if(a[i]<m) m=a[i];return m;}
template<template<class> class L, class T> inline T
core2_sum(const L<T> &a){unsigned int N = a.size();if(N <= 0) return T();T m=a[0];for(unsigned int i=1; i<N; i++) m+=a[i];return m;}
template<template<class> class L, class T> inline T
core2_ave(const L<T> &a){unsigned int N = a.size();if(N <= 0) return T();T m=a[0];for(unsigned int i=1; i<N; i++) m+=a[i];return m/N;}
template<template<class> class L, class T> inline T
core2_wt_ave(const L<T> &a, const L<double> &w){ int N = a.size();if(N <= 0) return T();if(w.size()<N)return core2_ave(a);T m=a[0]*w[0]; double sw = w[0]; for(int i=1; i<N; i++){ m+=a[i]*w[i]; sw+=w[i];} return (fabs(sw)<0.000001)?core2_ave(a):m/sw;}
template<template<class> class L, class T> inline L<T>
core2_abs(const L<T> &a){L<T> b = a;for(unsigned int i=0; i<a.size(); i++) b[i]=a[i]>0?a[i]:-a[i];return b;}
template<template<class> class L, class T> inline L<T>
core2_translate(const L<T> &a, T t){L<T> b = a;for(int i=0; i<a.size(); i++) b[i]=j_translate(a[i], t); return b;}
template<template<class> class L, class T> inline L<T>
core2_scale(const L<T> &a, double s){L<T> b = a;for(int i=0; i<a.size(); i++) b[i]=j_scale(a[i], s); return b;}
template<template<class> class L, class T, class R> inline L<T>
core2_rotate(const L<T> &a, R rot){L<T> b = a;for(int i=0; i<a.size(); i++) b[i]=j_rotate(a[i], rot);return b;}
template<template<class> class L, class T> inline L<T>
core2_transform(const L<T> &a, T t, double s, T orig){L<T> b = a;for( int i=0; i<a.size(); ++i) b[i] = j_transform(a[i], t, s, orig); return b;}
template<template<class> class L, class T, class R> inline L<T>
core2_transform(const L<T> &a, T t, R rot, double s, T orig){L<T> b = a; for( int i=0; i< a.size(); ++i) b[i] = j_transform(a[i], t, rot, s, orig); return b;}
template<template<class> class L, class T, class M> inline L<T>
core2_transform(const L<T> &a, M matrix){L<T> b = a; for(int i=0; i< a.size(); i++) b[i] = j_transform(a[i], matrix); return b;}
template<template<class> class L, class T> inline L<T>
core2_reverse(const L<T> &a){int N = a.size(); L<T> r = a; for(int i = 0; i < N; i++) r[i] = a[N-1-i]; return r;}
template<template<class> class L, class T> inline int
core2_nearest(const L<T> &a, T value, bool speedUp, double *minDis)
{
    qreal mindis = MAXVALUE;
    *minDis = MAXVALUE;
    int   minindex = 0;
    int   N = a.size();
    if(N <= 0) return -1;
    if(!speedUp || N < 10)
    {
        for(int j = 0; j < N; j++)
        {
            double dis = j_dis2(a[j], value);
            if(dis < mindis)
            {
                mindis = dis;
                minindex = j;
            }
        }
    }
    else{
        int step = int(N * 0.15);
        for(int j = 0; j < N; j+=step)
        {
            double dis = j_dis2(a[j], value);
            if(dis < mindis)
            {
                mindis = dis;
                minindex = j;
            }
        }
        int start = minindex-step*0.5-1; start = start>=0?start:0;
        int end = minindex+step*0.5+1; end = end <= N?end:N;
        for(int j = start; j < end; j++)
        {
            double dis = j_dis2(a[j], value);
            if(dis < mindis)
            {
                mindis = dis;
                minindex = j;
            }
        }
    }
    *minDis = mindis;
    return minindex;
}


template<template<class> class L, class T> inline Vec2i
core2_nearest2(const L<T> &a, const L<T> &b, bool speedUp, double *minDis)
{
    qreal mindis = MAXVALUE;
    *minDis = MAXVALUE;
    Vec2i  minindex = 0;
    int  N1 = a.size(), N2 = b.size();
    if(N1 <= 0 || N2 <= 0) return -1;
    if(!speedUp || (N1 < 10 && N2 < 10))
    {
        for(int i = 0; i < N1; i++)
        {
            for(int j = 0; j < N2; j++)
            {
                double dis = j_dis2(a[i], b[j]);
                if(dis < mindis)
                {
                    mindis = dis;
                    minindex = Vec2i(i, j);
                }
            }
        }
    }
    else{
        int step1 = int(N1 * 0.15); step1 = step1<1?1:step1;
        int step2 = int(N2 * 0.15); step2 = step2<1?1:step2;
        for(int i = 0; i < N1; i+=step1)
        {
            for(int j = 0; j < N2; j+=step2)
            {
                double dis = j_dis2(a[i], b[j]);
                if(dis < mindis)
                {
                    mindis = dis;
                    minindex = Vec2i(i, j);
                }
            }
        }
        int start1 = minindex[0]-step1*0.5-1; start1 = start1>=0?start1:0;
        int end1 = minindex[0]+step1*0.5+1; end1 = end1<=N1?end1:N1;
        int start2 = minindex[1]-step2*0.5-1; start2 = start2>=0?start2:0;
        int end2 = minindex[1]+step2*0.5+1; end2 = end2<=N2?end2:N2;
        for(int i = start1; i < end1; i++)
        {
            for(int j = start2; j < end2; j++)
            {
                double dis = j_dis2(a[i], b[j]);
                if(dis < mindis)
                {
                    mindis = dis;
                    minindex = Vec2i(i, j);
                }
            }
        }
    }
    *minDis = mindis;
    return minindex;
}

template<template<class> class L, class T> inline L<T>
core2_farest(const L<T> &a, L<int> *index = 0)
{
    double maxdis = MINVALUE;
    L<int> maxIndex; maxIndex.append(0); maxIndex.append(0);
    L<T>   maxV;
    unsigned int N = a.size();
    assert(N > 1);
    for(unsigned int i = 0; i < N-1; i++)
    {
        for(unsigned int j = i+1; j < N; j++)
        {
            double dis = j_dis2(a[j], a[i]);
            if(dis > maxdis)
            {
                maxdis = dis;
                maxIndex[0] = i;
                maxIndex[1] = j;
            }
        }
    }
    if(index)
        *index = maxIndex;
    maxV.append(a[maxIndex[0]]);
    maxV.append(a[maxIndex[1]]);
    return maxV;
}


template<template<class> class L, class T> inline L<T>
core2_resize(const L<T> &plist, int minsize, double mindis)
{
    int sSize = plist.size();
    if(sSize < 2)
    {
        L<T> tplist = plist;
        return tplist;
    }

    L<double> disList;
    double sumdis = 0;
    for(int i = 1; i < sSize; i++)
    {
        double dis = j_dis1(plist[i-1], plist[i]);
        disList.append(dis);
        sumdis += dis;
    }
    double minDis = 1;
    if(mindis > 0)
        minDis = mindis;
    else
        minDis = sumdis / (minsize-1);
    L<T> tplist;
    tplist.append(plist[0]);
    if(minDis <= 0) return tplist;
    double lastDis = 0;
    for(int i = 1; i < plist.size(); i++)
    {
        double dis = disList[i-1];
        lastDis += dis;
        while(lastDis >= minDis*0.95) //tolerance error
        {
            lastDis -= minDis;
            double scale = (dis - lastDis) / dis;
            T p = j_interpolate(plist[i-1], plist[i], scale);
            tplist.append(p);
        }
    }
    return tplist;
}

template<template<class> class L, class T> inline L<T>
core2_interpolate(const L<T> &plist1, const L<T> &plist2, double scale)
{
    int s = plist1.size();
    L<T> nlist = plist1;
    if(s!=plist2.size() || s==0) return nlist;
    for(int i = 0; i < s; i++)
    {
        T v = j_interpolate(plist1[i], plist2[i], scale);
        nlist.append(v);
    }
    return nlist;
}

template<template<class> class L, class T> inline L<T>
core2_extraction(const L<T> &plist, int index1, int index2, bool is_reverse = false)
{
    L<T> selection;
    int size = plist.size();
    if(size <= 0 || index1 < 0 || index1 >= size || index2 < 0 || index2 >= size)
        return selection;
    if(!is_reverse && index1 < index2)
        for(int i = index1; i <= index2; i++)
            selection.append(plist[i]);
    else if(!is_reverse && index1 >= index2)
    {
        for(int i = index1; i < size; i++)
            selection.append(plist[i]);
        for(int i = 0; i <= index2; i++)
            selection.append(plist[i]);
    }
    else if(is_reverse && index1 < index2)
        for(int i = index2; i >= index1; i--)
            selection.append(plist[i]);
    else
    {
        for(int i = index2; i >= 0; i--)
            selection.append(plist[i]);
        for(int i = size-1; i >= index1; i--)
            selection.append(plist[i]);
    }
    return selection;
}

template<template<class> class L, class T> inline double
core2_dis_avemin_mutual(const L<T> &plist1, const L<T> &plist2, bool speedUp){
    if(plist1.isEmpty() || plist2.isEmpty())
        return MAXVALUE;
    double ave_dis1 = 0, ave_dis2 = 0;
    int size1 = plist1.size(), size2 = plist2.size();
    for(int i = 0; i < size1;  i++)
    {
        double mindis = MAXVALUE;
        core2_nearest(plist2, plist1[i], speedUp, &mindis);
        ave_dis1 += sqrt(mindis);
    }

    for(int i = 0; i < size2; i++)
    {
        double mindis = MAXVALUE;
        core2_nearest(plist1, plist2[i], speedUp, &mindis);
        ave_dis2 += sqrt(mindis);
    }

    double ave_dis = (ave_dis1 / size1 + ave_dis2 / size2) * 0.5;
    return ave_dis;
}
template<template<class> class L, class T> inline double
core2_dis_maxmin_single(const L<T> &refer, const L<T> &target, bool speedUp)
{
    if(target.size == 0 || refer.size()==0)
        return MAXVALUE;
    L<double> dis_list;
    for(int i = 0; i < target.size(); i++)
    {
        double mindis = MAXVALUE;
        mindis = core2_nearest(refer, target[i], speedUp, &mindis);
        dis_list.append(mindis);
    }
    double max = 0;
    for(int i = 0; i < dis_list.size(); i++)
        if(dis_list.at(i) > max)
            max = dis_list.at(i);
    return sqrt(max);
}

template<template<class> class L, class T> inline double
core2_similarity(const L<T> &vlist1, const L<T> &vlist2){return j_dis_avemin_mutual(vlist1, vlist2);}

template<template<class> class L, class T> inline int
core2_contain(const L<T> &vlist, T v){
    for(int i = 0; i < vlist.size(); i++)
    {
        if(vlist[i] == v)
            return i;
    }
    return -1;
}


template<template<class> class L, class T> inline L<int>
core2_sort(const L<T> &cvalue, int candidateSize, bool is_min_sort)
{
    L<T> value = cvalue;
    int size = value.size();
    L<int>  flag;
    for(int i = 0; i < size; i++)
        flag.append(i);

    L<int> sortlist;
    if(candidateSize <= 0)
        return sortlist;
    if(is_min_sort)
    {
        for(int i = 0; i < size; i++)
        {
            T mindis = value[i];
            int   minindex = i;
            for(int j = i+1; j < size; j++)
            {
                if(value[j] < mindis)
                {
                    mindis = value[j];
                    minindex = j;
                }
            }
            if(minindex != i)
            {
                T mid = value[i];
                value[i] = value[minindex];
                value[minindex] = mid;

                int mid2 = flag[i];
                flag[i] = flag[minindex];
                flag[minindex] = mid2;
            }
            sortlist.append(flag[i]);
            if(sortlist.size()>=candidateSize)
                break;
        }
    }
    else
    {
        for(int i = 0; i < size; i++)
        {
            T maxdis = value[i];
            int   maxindex = i;
            for(int j = i+1; j < size; j++)
            {
                if(value[j] > maxdis)
                {
                    maxdis = value[j];
                    maxindex = j;
                }
            }
            if(maxindex != i)
            {
                T mid = value[i];
                value[i] = value[maxindex];
                value[maxindex] = mid;

                int mid2 = flag[i];
                flag[i] = flag[maxindex];
                flag[maxindex] = mid2;
            }
            sortlist.append(flag[i]);
            if(sortlist.size()>=candidateSize)
                break;
        }
    }
    return sortlist;
}



template<template<class> class L, class T> inline L<int>
core2_order(const L<T> &cvalue, int candidateSize, bool is_min_sort)
{
    L<int> indexs = core2_sort(cvalue, candidateSize, is_min_sort);
    L<int> orders = indexs;
    for(int i = 0; i < indexs.size(); i++)
        orders[indexs[i]] = i;
    return orders;
}



template<template<class> class L, class T> inline double
core2_dev(const L<T> &vlist)
{
    int size = vlist.size();
    assert(size > 0);
    if(size == 1) return 0;
    T ave = core2_ave(vlist);
    double dis2 = 0;
    for(int i = 0; i < size; i++)
        dis2 += (vlist[i]-ave)*(vlist[i]-ave);
    return dis2 / (size-1);
}



template<class T>
class Core
{
public:
    inline static T    j_norm(T v){return core_norm(v);}
    inline static T    j_max(T v1, T v2){return core_max(v1, v2);}
    inline static T    j_min(T v1, T v2){return core_min(v1, v2);}
    inline static T    j_ave(T v1, T v2){return core_ave(v1, v2);}
    inline static T    j_scale(T v, double scale){return core_scale(v, scale);}
    inline static T    j_transform(T v1, T t, double scale, T v2){return core_transform(v1, t, scale, v2);}
    inline static T    j_interpolate(T v1, T v2, double scale){return core_interpolate(v1, v2, scale);}
    inline static T    j_abs(T v){return core_abs(v);}
    inline static T    j_diff(T v1, T v2){return core_diff(v1, v2);}

    inline static double    j_mag2(T v){return core_mag2(v);}
    inline static double    j_mag1(T v){return core_mag1(v);}
    inline static double    j_dis2(T v1, T v2){return core_dis2(v1, v2);}
    inline static double    j_dis1(T v1, T v2){return core_abs1(v1, v2);}
};


template<template<class> class L, class T>
class Core1
{
public:
    //both
    inline static double     j_mag2(const L<T> &list){return core2_mag2(list);}
    inline static double     j_mag1(const L<T> &list){return core2_mag1(list);}
    inline static double     j_dis2(const L<T> &list1, const L<T> &list2){return core2_dis2(list1, list2);}
    inline static double     j_dis1(const L<T> &list1, const L<T> &list2){return core2_dis1(list1, list2);}
    inline static T          j_norm(const L<T> &list){return core2_norm(list);}
    inline static T          j_sum(const L<T> &list){return core2_sum(list);}
    inline static T          j_ave(const L<T> &list){return core2_ave(list);}
    inline static T          j_wt_ave(const L<T> &list, const L<double> &wlist){return core2_wt_ave(list,wlist);}
    inline static L<T>       j_reverse(const L<T> &vlist){return core2_reverse(vlist);}
    inline static int        j_nearest(const L<T> &vlist, T value){return core2_nearest(vlist, value);}
    inline static L<T>       j_farest(const L<T> &vlist, L<int> *index = 0){return core2_farest(vlist, index);}
    inline static L<T>       j_resize(const L<T> &plist, int size, double mindis = -1){return core2_resize(plist, size, mindis);}
    inline static L<T>       j_interpolate(const L<T> &plist1, const L<T> &plist2, double scale){return core2_interpolate(plist1, plist2, scale);}
    inline static L<T>       j_extraction(const L<T> &plist, int index1, int index2, bool is_reverse = false){return core2_extraction(plist, index1, index2, is_reverse);}
    //double, float, int...
    inline static T          j_max(const L<T> &list){return core2_max(list);}
    inline static T          j_min(const L<T> &list){return core2_min(list);}
    inline static L<T>       j_abs(const L<T> &vlist){return core2_abs(vlist);}
    inline static double     j_dev(const L<T> &vlist){return core2_dev(vlist);}
    inline static int        j_contain(const L<T> &vlist, T v){return core2_contain(vlist, v);}
    inline static L<int>     j_sort(const L<T> &value, int candidateSize, bool is_min_sort){return core2_sort(value, candidateSize, is_min_sort);}
    inline static L<int>     j_order(const L<T> &value, int candidateSize, bool is_min_sort){return core2_order(value, candidateSize, is_min_sort);}
};


template<template<class> class L, class T>
class Core2
{
public:
    //point, vector...
    inline static double     j_mag2(const L<T> &list){return core2_mag2(list);}
    inline static double     j_mag1(const L<T> &list){return core2_mag1(list);}
    inline static double     j_dis2(const L<T> &list1, const L<T> &list2){return core2_dis2(list1, list2);}
    inline static double     j_dis1(const L<T> &list1, const L<T> &list2){return core2_dis1(list1, list2);}
    inline static T          j_norm(const L<T> &list){return core2_norm(list);}
    inline static T          j_sum(const L<T> &list){return core2_sum(list);}
    inline static T          j_ave(const L<T> &list){return core2_ave(list);}
    inline static T          j_wt_ave(const L<T> &list, const L<double> &wlist){return core2_wt_ave(list,wlist);}
    inline static L<T>       j_reverse(const L<T> &vlist){return core2_reverse(vlist);}
    inline static int        j_nearest(const L<T> &vlist, T value, bool speedUp, double *mindis){return core2_nearest(vlist, value, speedUp, mindis);}
    inline static Vec2i      j_nearest2(const L<T> &vlist, const L<T> &vlist2, bool speedUp, double *mindis){return core2_nearest2(vlist, vlist2, speedUp, mindis);}
    inline static L<T>       j_farest(const L<T> &vlist, L<int> *index = 0){return core2_farest(vlist, index);}
    inline static L<T>       j_resize(const L<T> &plist, int size, double mindis = -1){return core2_resize(plist, size, mindis);}
    inline static L<T>       j_interpolate(const L<T> &plist1, const L<T> &plist2, double scale){return core2_interpolate(plist1, plist2, scale);}
    inline static L<T>       j_extraction(const L<T> &plist, int index1, int index2, bool is_reverse = false){return core2_extraction(plist, index1, index2, is_reverse);}
    //both
    inline static L<T>       j_translate(const L<T> &vlist, T trans){return core2_translate(vlist,trans);}
    inline static L<T>       j_scale(const L<T> &vlist, double scale){return core2_scale(vlist, scale);}
    template<class R> inline static L<T>  j_rotate(const L<T> &vlist, R rotate){return core2_rotate(vlist, rotate);}
    inline static L<T>       j_transform(const L<T> &vlist, T trans, double scale, T core){return core2_transform(vlist,trans, scale, core);}
    template<class R> inline static L<T>  j_transform(const L<T> &vlist, T trans, R rotate, double scale, T core){return core2_transform(vlist,trans,rotate, scale, core);}
    template<class M> inline static L<T>  j_transform(const L<T> &vlist, M matrix){return core2_transform(vlist, matrix);}
    inline static double     j_dis_avemin_mutual(const L<T> &vlist1, const L<T> &vlist2, bool speedUp){return core2_dis_avemin_mutual(vlist1, vlist2, speedUp);}
    inline static double     j_dis_maxmin_single(const L<T> &refer, const L<T> &target, bool speedUp){return core2_dis_maxmin_single(refer, target, speedUp);}
    inline static double     j_similarity(const L<T> &vlist1, const L<T> &vlist2){return core2_similarity(vlist1, vlist2);}

    inline static L<L<T> >       j_translate(const L<L<T> > &vlist, T trans){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_translate(vlist[i],trans); return dlist;}
    inline static L<L<T> >       j_scale(const L<L<T> > &vlist, double scale){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_scale(vlist[i],scale); return dlist;}
    template<class R> inline static L<L<T> >  j_rotate(const L<L<T> > &vlist, R rotate){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_rotate(vlist[i],rotate); return dlist;}
    inline static L<L<T> >       j_transform(const L<L<T> > &vlist, T t, double s, T core){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i], t, s, core); return dlist;}
    template<class R> inline static L<L<T> > j_transform(const L<L<T> > &vlist, T t, R rot, double s, T core){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i], t, rot, s, core); return dlist;}
    template<class M> inline static L<L<T> >  j_transform(const L<L<T> > &vlist, M matrix){L<L<T> > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i],matrix); return dlist;}

    inline static L<L<L<T> > >       j_translate(const L<L<L<T> > > &vlist, T trans){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_translate(vlist[i],trans); return dlist;}
    inline static L<L<L<T> > >       j_scale(const L<L<L<T> > > &vlist, double scale){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_scale(vlist[i],scale); return dlist;}
    template<class R> inline static L<L<L<T> > >  j_rotate(const L<L<L<T> > > &vlist, R rotate){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_rotate(vlist[i],rotate); return dlist;}
    inline static L<L<L<T> > >       j_transform(const L<L<L<T> > > &vlist, T t, double s, T core){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i], t, s, core); return dlist;}
    template<class R> inline static L<L<L<T> > > j_transform(const L<L<L<T> > > &vlist, T t, R rot, double s, T core){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i], t, rot, s, core); return dlist;}
    template<class M> inline static L<L<L<T> > >  j_transform(const L<L<L<T> > > &vlist, M matrix){L<L<L<T> > > dlist = vlist; for(int i = 0; i < vlist.size(); i++) dlist[i] = j_transform(vlist[i],matrix); return dlist;}
};



inline int     j_norm(int v){return v;}
inline double  j_norm(double v){return v;}
inline float   j_norm(float v){return v;}

inline int     j_max(int v1, int v2){return core_max(v1, v2);}
inline double  j_max(double v1, double v2){return core_max(v1, v2);}
inline float   j_max(float v1, float v2){return core_max(v1, v2);}

inline int     j_min(int v1, int v2){return core_min(v1, v2);}
inline double  j_min(double v1, double v2){return core_min(v1, v2);}
inline float   j_min(float v1, float v2){return core_min(v1, v2);}

inline int     j_ave(int v1, int v2){return core_ave(v1, v2);}
inline double  j_ave(double v1, double v2){return core_ave(v1, v2);}
inline float   j_ave(float v1, float v2){return core_ave(v1, v2);}

inline int     j_scale(int v, double scale){return core_scale(v, scale);}
inline double  j_scale(double v, double scale){return core_scale(v, scale);}
inline float   j_scale(float v, double scale){return core_scale(v, scale);}

inline int     j_interpolate(int v1, int v2, double scale){return core_interpolate(v1, v2, scale);}
inline double  j_interpolate(double v1, double v2, double scale){return core_interpolate(v1, v2, scale);}
inline float   j_interpolate(float v1, float v2, double scale){return core_interpolate(v1, v2, scale);}

inline int     j_abs(int v){return core_abs(v);}
inline double  j_abs(double v){return core_abs(v);}
inline float   j_abs(float v){return core_abs(v);}

inline int     j_diff(int v1, int v2){return core_diff(v1, v2);}
inline double  j_diff(double v1, double v2){return core_diff(v1, v2);}
inline float   j_diff(float v1, float v2){return core_diff(v1, v2);}

inline double  j_mag2(int v){return core_mag2(v);}
inline double  j_mag2(double v){return core_mag2(v);}
inline double  j_mag2(float v){return core_mag2(v);}

inline double  j_mag1(int v){return abs(v);}
inline double  j_mag1(double v){return fabs(v);}
inline double  j_mag1(float v){return fabs(v);}

inline int     j_dis2(int v1, int v2){return core_dis2(v1, v2);}
inline double  j_dis2(double v1, double v2){return core_dis2(v1,v2);}
inline float   j_dis2(float v1, float v2){return core_dis2(v1,v2);}

inline int     j_dis1(int v1, int v2){return abs(v1-v2);}
inline double  j_dis1(double v1, double v2){return abs(v1-v2);}
inline float   j_dis1(float v1, float v2){return abs(v1-v2);}



template<unsigned int N, typename T> inline double      j_mag2(const Vec<N,T> &a){assert(N>0); double l= a[0]*a[0]; for(unsigned int i=1; i<N; ++i) l+=a[i]*a[i]; return l;}
template<unsigned int N, typename T> inline double      j_mag1(const Vec<N,T> &a){assert(N>0); return sqrt(j_mag2(a));}
template<unsigned int N, typename T> inline double      j_dis2(const Vec<N,T> &a, const Vec<N,T> &b){assert(N>0); double d= j_dis2(a[0], b[0]); for(unsigned int i=1; i<N; ++i) d+=j_dis2(a[i], b[i]); return d;}
template<unsigned int N, typename T> inline double      j_dis1(const Vec<N,T> &a, const Vec<N,T> &b){assert(N>0); return sqrt(j_dis2(a,b));}
template<unsigned int N, typename T> inline Vec<N,T>    j_norm(const Vec<N,T> &a){assert(N>0); Vec<N,T> b = a; for(unsigned int i=1; i<N; ++i) b[i] = j_norm(b[i]); return b;}
template<unsigned int N, typename T> inline T           j_min(const Vec<N,T> &a){assert(N>0); T m=a.v[0]; for(unsigned int i=1; i<N; ++i) if(a.v[i]<m) m=a.v[i];return m;}
template<unsigned int N, typename T> inline T           j_max(const Vec<N,T> &a){assert(N>0); T m=a.v[0]; for(unsigned int i=1; i<N; ++i) if(a.v[i]>m) m=a.v[i];return m;}
template<unsigned int N, typename T> inline T           j_ave(const Vec<N,T> &a){assert(N>0); T m=a.v[0]; for(unsigned int i=1; i<N; ++i) m+=a.v[i];return m/N;}
template<unsigned int N, typename T> inline Vec<N,T>    j_interpolate(const Vec<N,T> &a, const Vec<N,T> &b, double scale){assert(N>0); return core_interpolate(a, b, scale);}

template<unsigned int N, typename T> inline T           j_normalize(Vec<N,T> &a){assert(N>0); T leng = j_mag1(a); assert(leng>0); a /= leng; return leng;}
template<unsigned int N, typename T> inline Vec<N,T>    j_normalized(const Vec<N,T> &a){assert(N>0); return a/j_mag1(a);}

template<unsigned int N, typename T> inline T           j_dot(const Vec<N,T> &a, const Vec<N,T> &b){assert(N>0); T d=a.v[0]*b.v[0]; for(unsigned int i=1; i<N; ++i) d+=a.v[i]*b.v[i];return d;}
template<typename T> inline T                           j_cross(const Vec<2,T> &a, const Vec<2,T> &b){return a.v[0]*b.v[1]-a.v[1]*b.v[0];}
template<typename T> inline Vec<3, T>                   j_cross(const Vec<3,T> &a, const Vec<3,T> &b){return Vec<3,T>(a.v[1]*b.v[2]-a.v[2]*b.v[1], a.v[2]*b.v[0]-a.v[0]*b.v[2], a.v[0]*b.v[1]-a.v[1]*b.v[0]);}
template<typename T> inline T                           j_triple(const Vec<3,T> &a, const Vec<3,T> &b, const Vec<3,T> &c){return a.v[0]*(b.v[1]*c.v[2]-b.v[2]*c.v[1]) +a.v[1]*(b.v[2]*c.v[0]-b.v[0]*c.v[2]) +a.v[2]*(b.v[0]*c.v[1]-b.v[1]*c.v[0]);}
//template<unsigned int N, typename T> inline Vec<N,int>  round(const Vec<N,T> &a){Vec<N,int> rounded;for(unsigned int i=0; i<N; ++i) rounded.v[i]=lround(a.v[i]); return rounded;}
//template<unsigned int N, typename T> inline Vec<N,int>  floor(const Vec<N,T> &a){Vec<N,int> rounded;for(unsigned int i=0; i<N; ++i) rounded.v[i]=(T)floor(a.v[i]); return rounded;}
//template<unsigned int N, typename T> inline Vec<N,int>  ceil(const Vec<N,T> &a){Vec<N,int> rounded;for(unsigned int i=0; i<N; ++i) rounded.v[i]=(T)ceil(a.v[i]); return rounded;}


#endif // CORE_H
