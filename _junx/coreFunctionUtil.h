#ifndef COREFUNCTIONUTIL_H
#define COREFUNCTIONUTIL_H


#include "coreUtilto.h"

#include "function/functionUtil_BSpline.h"
#include "function/functionUtil_Random.h"
#include "function/functionUtil_Partition.h"
#include "function/functionUtil_Transform.h"


#endif // COREFUNCTIONUTIL_H
