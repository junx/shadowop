#ifndef CORE2_H
#define CORE2_H

#include "coreMath.h"

typedef Complex<double>    Complexd;
typedef Complex<float>     Complexf;
typedef Complex<int>       Complexi;
typedef Complex<short>     Complexs;

typedef Matrix<short>        Matrixs;
typedef Matrix<int>          Matrixi;
typedef Matrix<double>       Matrixd;
typedef Matrix<float>        Matrixf;
typedef Matrix<Vec2s>        Matrixcs;
typedef Matrix<Vec2i>        Matrixci;
typedef Matrix<Vec2d>        Matrixcd;
typedef Matrix<Vec2f>        Matrixcf;

inline QPoint     j_diff(QPoint v1, QPoint v2){return QPoint(v1.x()-v2.x(), v1.y()-v2.y());}
inline QPointF    j_diff(QPointF v1, QPointF v2){return QPointF(v1.x()-v2.x(), v1.y()-v2.y());}
inline QVector2D  j_diff(QVector2D v1, QVector2D v2){return QVector2D(v1.x()-v2.x(), v1.y()-v2.y()).normalized();}
inline QVector3D  j_diff(QVector3D v1, QVector3D v2){return QVector3D(v1.x()-v2.x(), v1.y()-v2.y(), v1.z()-v2.z()).normalized();}
inline QColor     j_diff(QColor v1, QColor v2)
{
    int r = v1.red()-v2.red(); r = r<255?r:255; r = r>=0?r:0;
    int g = v1.green()-v2.green();g = g<255?g:255; g = g>=0?g:0;
    int b = v1.blue()-v2.blue(); b = b<255?b:255; b = b>=0?b:0;
    int a = v1.alpha()-v2.alpha(); a = a<255?a:255; a = a>=0?a:0;
    return QColor(r, g, b, a);
}


inline QPoint     j_norm(QPoint d){return d;}
inline QPointF    j_norm(QPointF d){return d;}
inline QVector2D  j_norm(QVector2D d){return d.normalized();}
inline QVector3D  j_norm(QVector3D d){return d.normalized();}

inline double     j_mag2(QPoint d){return d.x()*d.x()+d.y()*d.y();}
inline double     j_mag2(QPointF d){return d.x()*d.x()+d.y()*d.y();}
inline double     j_mag2(QVector2D d){return d.x()*d.x()+d.y()*d.y();}
inline double     j_mag2(QVector3D d){return d.x()*d.x()+d.y()*d.y()+d.z()*d.z();}

inline double     j_mag1(QPoint d){return sqrt(j_mag2(d));}
inline double     j_mag1(QPointF d){return sqrt(j_mag2(d));}
inline double     j_mag1(QVector2D d){return sqrt(j_mag2(d));}
inline double     j_mag1(QVector3D d){return sqrt(j_mag2(d));}

inline double     j_dis2(QPoint v1, QPoint v2){QPoint d = v1-v2; return d.x()*d.x()+d.y()*d.y();}
inline double     j_dis2(QPointF v1, QPointF v2){QPointF d = v1-v2; return d.x()*d.x()+d.y()*d.y();}
inline double     j_dis2(QVector2D v1, QVector2D v2){double d = QVector2D::dotProduct(v1,v2); return d*d;}
inline double     j_dis2(QVector3D v1, QVector3D v2){double d = QVector3D::dotProduct(v1,v2); return d*d;}
inline double     j_dis2(QColor v1, QColor v2)
{
    int r = v1.red()-v2.red();
    int g = v1.green()-v2.green();
    int b = v1.blue()-v2.blue();
    int a = v1.alpha()-v2.alpha();
    qreal dis = r*r+g*g+b*b+a*a;
    return dis/4;
}
inline double     j_dis1(QPoint v1, QPoint v2){return sqrt(j_dis2(v1,v2));}
inline double     j_dis1(QPointF v1, QPointF v2){return sqrt(j_dis2(v1,v2));}
inline double     j_dis1(QVector2D v1, QVector2D v2){return QVector2D::dotProduct(v1,v2);}
inline double     j_dis1(QVector3D v1, QVector3D v2){return QVector3D::dotProduct(v1,v2);}
inline double     j_dis1(QColor v1, QColor v2){return sqrt(j_dis2(v1,v2));}

inline QPoint     j_ave(QPoint  v1, QPoint  v2){return QPoint((v1.x()+v2.x())*0.5, (v1.y()+v2.y())*0.5);}
inline QPointF    j_ave(QPointF v1, QPointF v2){return QPointF((v1.x()+v2.x())*0.5, (v1.y()+v2.y())*0.5);}
inline QVector2D  j_ave(QVector2D v1, QVector2D v2){ QVector2D v((v1.x()+v2.x())*0.5, (v1.y()+v2.y())*0.5); return v.normalized();}
inline QVector3D  j_ave(QVector3D v1, QVector3D v2){ QVector3D v((v1.x()+v2.x())*0.5, (v1.y()+v2.y())*0.5, (v1.z()+v2.z())*0.5); return v.normalized();}
inline QColor     j_ave(QColor v1, QColor v2)
{
    int r = v1.red()+v2.red(); r = r<255?r:255;
    int g = v1.green()+v2.green(); g = g<255?g:255;
    int b = v1.blue()+v2.blue(); b = b<255?b:255;
    int a = v1.alpha()+v2.alpha(); a = a<255?a:255;
    return QColor(r,g,b,a);
}

inline QPoint      j_translate(QPoint v1, QPoint t){return QPoint(v1.x()+t.x(), v1.y()+t.y());}
inline QPointF     j_translate(QPointF v1, QPointF t){return QPointF(v1.x()+t.x(), v1.y()+t.y());}
inline QVector2D   j_translate(QVector2D v1, QVector2D t){return QVector2D(v1.x()+t.x(), v1.y()+t.y());}
inline QVector3D   j_translate(QVector3D v1, QVector3D t){return QVector3D(v1.x()+t.x(), v1.y()+t.y(), v1.z()+t.z());}

template<class T> inline Vec<2,T>   j_rotate(Vec<2,T> v, QVector2D rotate){if(fabs(rotate.y()) < 0.05 && rotate.x() > 0.95) return v; rotate.normalize(); assert(rotate.length() != 0); double cos = rotate.x(); double sin = rotate.y(); double x = v[0]*cos - v[1]*sin;double y = v[1]*cos + v[0]*sin;return Vec<2, T>(x, y);}
inline QPoint      j_rotate(QPoint v, QVector2D rotate){if(fabs(rotate.y()) < 0.05 && rotate.x() > 0.95) return v; rotate.normalize(); assert(rotate.length() != 0); double cos = rotate.x(); double sin = rotate.y(); int x = v.x()*cos - v.y()*sin;int y = v.y()*cos + v.x()*sin;return QPoint(x, y);}
inline QPointF     j_rotate(QPointF v, QVector2D rotate){if(fabs(rotate.y()) < 0.05 && rotate.x() > 0.95) return v; rotate.normalize(); assert(rotate.length() != 0); double cos = rotate.x(); double sin = rotate.y(); double x = v.x()*cos - v.y()*sin;double y = v.y()*cos + v.x()*sin;return QPointF(x, y);}
inline QVector2D   j_rotate(QVector2D v, QVector2D rotate){if(fabs(rotate.y()) < 0.05 && rotate.x() > 0.95) return v; rotate.normalize(); assert(rotate.length() != 0); double cos = rotate.x(); double sin = rotate.y(); double x = v.x()*cos - v.y()*sin;double y = v.y()*cos + v.x()*sin;return QVector2D(x, y);}
inline qreal       j_rotate(qreal v, QVector2D rotate){
    if(rotate.x() == 0 && rotate.y() == 0)
        return v;
    if(fabs(rotate.y()) < 0.05 && rotate.x() > 0.95) return v;
    qreal angle = acos(rotate.x());
    if(rotate.y() < 0)
        angle = 6.28-angle;
    v += angle;
    if(v < 0) angle += 6.28;
    if(v > 6.28) angle -= 6.28;
    return v;
}
//<<<<<<<<rotate point>>>>>>>>>>>>>//
inline QVector2D   j_rotated(QVector2D start, QVector2D end)
{
    start.normalize();
    assert(start.length() != 0);
    end.normalize();
    assert(end.length() != 0);

    qreal x2 = start.x();
    qreal y2 = start.y();
    qreal x1 = end.x();
    qreal y1 = end.y();

    qreal sin = x2*y1 - y2*x1;
    qreal cos = x1*x2 + y1*y2;
    return QVector2D(cos, sin);
}

inline QColor      j_scale(QColor v1, double s)
{
    int r = v1.red()*s; r = r<255?r:255;
    int g = v1.green()*s; g = g<255?g:255;
    int b = v1.blue()*s; b = b<255?b:255;
    return QColor(r,g,b,v1.alpha());
}


inline QPoint      j_scale(QPoint v1, double s){return QPoint(v1.x()*s, v1.y()*s);}
inline QPointF     j_scale(QPointF v1, double s){return QPointF(v1.x()*s, v1.y()*s);}
inline QVector2D   j_scale(QVector2D v1, double s){return QVector2D(v1.x()*s, v1.y()*s);}
inline QVector3D   j_scale(QVector3D v1, double s){return QVector3D(v1.x()*s, v1.y()*s, v1.z()*s);}


inline QPoint      j_transform(QPoint v1, QPoint t, double s, QPoint v2 = QPoint(0,0)){return QPoint((v1.x()-v2.x())*s+v2.x()+t.x(), (v1.y()-v2.y())*s+v2.y()+t.y());}
inline QPointF     j_transform(QPointF v1, QPointF t, double s, QPointF v2 = QPointF(0,0)){return QPointF((v1.x()-v2.x())*s+v2.x()+t.x(), (v1.y()-v2.y())*s+v2.y()+t.y());}
inline QVector2D   j_transform(QVector2D v1, QVector2D t, double s, QVector2D v2 = QVector2D(0,0)){return QVector2D((v1.x()-v2.x())*s+v2.x()+t.x(), (v1.y()-v2.y())*s+v2.y()+t.y());}
//inline QVector3D   j_transform(QVector3D v1, QVector3D t, double s, QVector3D v2 = QVector3D(0,0,0)){return QVector3D((v1.x()-v2.x())*s+v2.x()+t.x(), (v1.y()-v2.y())*s+v2.y()+t.y(), (v1.z()-v2.z())*s+v2.z()+t.z());}

inline QPoint      j_transform(QPoint v1, QPoint t, QVector2D rot, double s, QPoint v2 = QPoint(0,0)){rot.normalize(); assert(rot.length() != 0); QPoint p((v1.x()-v2.x())*s, (v1.y()-v2.y())*s); p = j_rotate(p, rot); return QPoint(p.x()+v2.x()+t.x(), p.y()+v2.y()+t.y());}
inline QPointF     j_transform(QPointF v1, QPointF t, QVector2D rot, double s, QPointF v2 = QPointF(0,0)){rot.normalize(); assert(rot.length() != 0); QPointF p((v1.x()-v2.x())*s, (v1.y()-v2.y())*s); p = j_rotate(p, rot); return QPointF(p.x()+v2.x()+t.x(), p.y()+v2.y()+t.y());}
inline QVector2D   j_transform(QVector2D v1, QVector2D t, QVector2D rot, double s, QVector2D v2 = QVector2D(0,0)){rot.normalize(); assert(rot.length() != 0); QVector2D p((v1.x()-v2.x())*s, (v1.y()-v2.y())*s); p = j_rotate(p, rot); return QVector2D(p.x()+v2.x()+t.x(), p.y()+v2.y()+t.y());}
//inline QVector3D   j_transform(QVector3D v1, QVector3D t, QVector2D rot, double s, QVector3D v2 = QVector3D(0,0,0)){QVector3D p((v1.x()-v2.x())*s, (v1.y()-v2.y())*s, (v1.z()-v2.z())*s); p = rotate(p, rot); return QVector3D(p.x()+v2.x()+t.x(), p.y()+v2.y()+t.y(), p.z()+v2.z()+t.z());}

inline QPoint      j_transform(QPoint v1, QMatrix matrix){return matrix.map(v1);}
inline QPointF     j_transform(QPointF v1, QMatrix matrix){return matrix.map(v1);}

inline QPoint      j_interpolate(QPoint v1, QPoint v2, double s){return QPoint(v1.x()*(1-s)+v2.x()*s, v1.y()*(1-s)+v2.y()*s);}
inline QPointF     j_interpolate(QPointF v1, QPointF v2, double s){return QPointF(v1.x()*(1-s)+v2.x()*s, v1.y()*(1-s)+v2.y()*s);}
inline QVector2D   j_interpolate(QVector2D v1, QVector2D v2, double s){return QVector2D(v1.x()*(1-s)+v2.x()*s, v1.y()*(1-s)+v2.y()*s);}
inline QVector3D   j_interpolate(QVector3D v1, QVector3D v2, double s){return QVector3D(v1.x()*(1-s)+v2.x()*s, v1.y()*(1-s)+v2.y()*s, v1.z()*(1-s)+v2.z()*s);}
inline QColor      j_interpolate(QColor v1, QColor v2, double scale)
{
    int r = j_interpolate(v1.red(), v2.red(), scale); r = r<255?r:255; r = r>=0?r:0;
    int g = j_interpolate(v1.green(), v2.green(), scale); g = g<255?g:255; g = g>=0?g:0;
    int b = j_interpolate(v1.blue(), v2.blue(), scale); b = b<255?b:255; b = b>=0?b:0;
    int a = j_interpolate(v1.alpha(), v2.alpha(), scale); a = a<255?a:255; a = a>=0?a:0;
    return QColor(r,g,b,a);
}


template<unsigned int N, class T> inline Vec<N,T>       j_translate(const Vec<N,T> &a, Vec<N,T> t){return a+t;}
template<unsigned int N, class T> inline Vec<N,T>       j_scale(const Vec<N,T> &a, double s){return a*s;}
template<class T> inline Vec<2,T>                       j_rotate(const Vec<2,T> &v, QVector2D rotate){rotate.normalize(); assert(rotate.length() != 0); qreal cos = rotate.x(); qreal sin = rotate.y(); T x = v[0]*cos - v[1]*sin; T y = v[1]*cos + v[0]*sin; return Vec<2,T>(x, y);}
template<unsigned int N, class T> inline Vec<N,T>       j_transform(const Vec<N,T> &a, Vec<N,T> t, double s, Vec<N,T> core){return (a-core)*s+core+t;}
template<class T> inline Vec<2,T>                       j_transform(const Vec<2,T> &v1, Vec<2,T> t, QVector2D rot, double s, Vec<2,T> v2){rot.normalize(); assert(rot.length() != 0); Vec<2,T>p((v1[0]-v2[0])*s, (v1[1]-v2[1])*s); p = j_rotate(p, rot); return Vec<2,T>(p[0]+v2[0]+t[0], p[1]+v2[1]+t[1]);}
template<class T> inline Vec<2,T>                       j_transform(const Vec<2,T> &a, QMatrix matrix){QPointF p = j_transform(QPointF(a[0],a[1]), matrix); return Vec<2,T>(p.x(), p.y());}

template<class T> inline Complex<T>     j_translate(const Complex<T> &a, Complex<T> t){return a+t;}
template<class T> inline Complex<T>     j_scale(const Complex<T> &a, double s){return a*s;}
template<class T> inline Complex<T>     j_rotate(const Complex<T> &v, QVector2D rotate){rotate.normalize(); assert(rotate.length() != 0); qreal cos = rotate.x(); qreal sin = rotate.y(); T x = v[0]*cos - v[1]*sin; T y = v[1]*cos + v[0]*sin; return Complex<T>(x, y);}
template<class T> inline Complex<T>     j_transform(const Complex<T> &a, Complex<T> t, double s, Complex<T> core){return (a-core)*s+core+t;}
template<class T> inline Complex<T>     j_transform(const Complex<T> &v1, Complex<T> t, QVector2D rot, double s, Vec<2,T> v2){rot.normalize(); assert(rot.length() != 0); Complex<T>p((v1[0]-v2[0])*s, (v1[1]-v2[1])*s); p = j_rotate(p, rot); return Complex<T>(p[0]+v2[0]+t[0], p[1]+v2[1]+t[1]);}
template<class T> inline Complex<T>     j_transform(const Complex<T> &a, QMatrix matrix){QPointF p = matrix.map(QPointF(a[0],a[1])); return Complex<T>(p.x(), p.y());}


typedef Core1<QList, double>     _QListd;
typedef Core1<QList, float>      _QListf;
typedef Core1<QList, int>        _QListi;

typedef Core2<QList, QPoint>     _QListp;
typedef Core2<QList, QPointF>    _QListpf;
typedef Core2<QList, QVector2D>  _QList2v;
typedef Core2<QList, QVector3D>  _QList3v;

typedef Core1<QVector, double>   _QVectord;
typedef Core1<QVector, float>    _QVectorf;
typedef Core1<QVector, int>      _QVectori;

typedef Core2<QVector, QPoint>     _QVectorp;
typedef Core2<QVector, QPointF>    _QVectorpf;
typedef Core2<QVector, QVector2D>  _QVector2v;
typedef Core2<QVector, QVector3D>  _QVector3v;



#endif // CORE2_H

