#ifndef CORETOOLUTIL_H
#define CORETOOLUTIL_H

#include "coreFunctionUtil.h"

#include "tool/coreGrider.h"
#include "tool/coreHistoryer.h"
#include "tool/coreTransable.h"
#include "tool/coreDeform.h"
#include "tool/coreMapable.h"
#include "tool/coreOperatable.h"
#include "tool/coreRunable.h"
#include "tool/coreSelectable.h"

#endif // CORETOOLUTIL_H
