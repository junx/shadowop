#ifndef COREGLOBAL_H
#define COREGLOBAL_H

#include <QtCore>
#include <QtGui>
#include <math.h>
#include <cassert>


#define MAXVALUE 10e8
#define MINVALUE -10e8


#endif // COREGLOBAL_H
