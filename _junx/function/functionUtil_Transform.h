#ifndef FUNCTIONUTIL_TRANSFORM_H
#define FUNCTIONUTIL_TRANSFORM_H

#include "coreUtilto.h"

//<<<<<<<<Transformer>>>>>>>>>>//
template<class T> inline QList<QList<Complex<T> > >
core_Transform_calculate_param(QList<Complex<T> > transpoints, QList<Complex<T> >  old_cage);
template<class T> inline QList<Complex<T> >
core_Transform_calculate_param(Complex<T> innerp, QList<Complex<T> > old_cage);
template<class T> inline QList<QList<Complex<T> > >
core_Transform_cageToControlParam(QList<QList<Complex<T> > > parameter, Matrix<Complex<T> > Nmatrix);
template<class T> inline Matrix<Complex<T> >
core_Transform_extract_Nmatrix(QList<Complex<T> >  old_cage, QList<Complex<T> >  old_constrain_points);
template<class T> inline QList<Complex<T> >
core_Transform_calculate_derivative_param(Complex<T>innerp, qreal lamda, QList<Complex<T> > old_cage);
template<class T> inline QList<Complex<T> >
core_Transform_calculate_derivative1_param(Complex<T>innerp, qreal lamd, QList<Complex<T> >  old_cage);
template<class T> inline Complex<T>
core_Transform_control(Complex<T> oldp, QList<Complex<T> > param, QList<Complex<T> >  nControl);
template<class T> inline QList<Complex<T> >
core_Transform_control(QList<Complex<T> > old_points, QList<QList<Complex<T> > > param, QList<Complex<T> > nControl);

//<<<<<<<<<<<<<<<<API>>>>>>>>>>>>>>//
template<class T> inline QList<QList<Complex<T> > >
j_Transform_calculate_param(QList<QPointF> transpoints, QList<Complex<T> >  old_cage)
{
    QList<Complexd> clist = j_to_complex(transpoints);
    return core_Transform_calculate_param(clist, old_cage);
}


template<class T> inline QPointF
j_Transform_control(QPointF oldp, QList<Complex<T> > param, QList<Complex<T> >  nControl)
{
    Complexd c = core_Transform_control(Complexd(oldp.x(),oldp.y()), param, nControl);
    return j_to_point(c);
}

template<class T> inline QList<QPointF>
j_Transform_control(QList<QPointF> old_points, QList<QList<Complex<T> > > param, QList<Complex<T> > nControl)
{
    QList<Complexd> clist = j_to_complex(old_points);
    clist = core_Transform_control(clist, param, nControl);
    return j_to_point(clist);
}





//<<<<<<<<<<<<<//<<<<<<<<<<<<Implementation>>>>>>>>>>>>>>//
template<class T>
inline QList<QList<Complex<T> > >
core_Transform_calculate_param(QList<Complex<T> > transpoints, QList<Complex<T> >  old_cage)
{
    QList<QList<Complex<T> > > parameter;
    for(int i = 0; i < transpoints.size(); i++)
    {
        Complex<T> innerp = transpoints[i];
        QList<Complex<T> >  paramlist2 = core_Transform_calculate_param(innerp, old_cage);
        parameter.append(paramlist2);
    }
    return parameter;
}



template<class T>
inline QList<Complex<T> >
core_Transform_calculate_param(Complex<T> innerp, QList<Complex<T> > old_cage)
{
    QList<Complex<T> > pa;
    int size = old_cage.size();
    QList<Complex<T> > cage = old_cage;
    cage.prepend(old_cage.last());
    cage.append(old_cage.first());
    size = cage.size();
    qreal angle = 0;
    QList<Complex<T> > B;
    QList<Complex<T> > LogB;
    Complex<T> B3 = cage[0];
    B3 -= innerp;
    B.append(B3);
    for(int i = 1; i < size; i++)
    {
        Complex<T> B3 = cage[i-1];
        B3 -= innerp;
        Complex<T> B1 = cage[i];
        B1 -= innerp;
        Complex<T> Log10 = Complex<T>(B1/B3).logcomplex();
        if(Log10[1] > 3.1416)
            Log10 = Complex<T>(Log10[0], 6.2832-Log10[1]);
        LogB.append(Log10);
        B.append(B1);
        angle += Log10[1];
    }

    bool is_verse = false;

    //this point is outside
    if(fabs(angle) < 2)
        return pa;
    else if(angle < 0)  //reverse direction
    {
        is_verse = true;
    }

    for(int i = 1; i < size-1; i++)
    {
        Complex<T>A1 = cage.at(i);
        A1 -= cage.at(i-1);
        Complex<T>A2 = cage.at(i+1);
        A2 -= cage.at(i);
        Complex<T>imag(0, -1.0 / 6.2832);
        Complex<T>Log21 = LogB.at(i);
        Complex<T>Log10 = LogB.at(i-1);
        if(is_verse)
        {
            Log21.conjuction();
            Log10.conjuction();
        }

        Complex<T>C  = (Log21 * B.at(i+1) / A2 - Log10 * B.at(i-1) / A1) * imag;
        pa.append(C);
    }
    return pa;
}




template<class T>
inline QList<QList<Complex<T> > >
core_Transform_cageToControlParam(QList<QList<Complex<T> > > parameter, Matrix<Complex<T> > Nmatrix)
{
    QList<QList<Complex<T> > >  control_parameter;
    if(Nmatrix.M_height == 0 || Nmatrix.M_width == 0)
        return control_parameter;
    for(int i = 0; i < parameter.size(); i++)
    {
        QList<Complex<T> >  controlparam2;
        QList<Complex<T> >  paramlist2 = parameter[i];
        Matrix<Complex<T> > C = j_to_matrix(paramlist2, false);
        Matrix<Complex<T> > P = C * Nmatrix;
        controlparam2 = j_to_list(P);
        control_parameter.append(controlparam2);
    }
    return control_parameter;
}



template<class T>
inline Matrix<Complex<T> >
core_Transform_extract_Nmatrix(QList<Complex<T> >  old_cage, QList<Complex<T> >  old_constrain_points)
{
    int p = old_constrain_points.size();
    Matrix<Complex<T> > Nmatrix;
    QList<QList<Complex<T> > > cplist;
    for(int i = 0; i < p; i++)
    {
        Complex<T>innerp = old_constrain_points.at(i);
        QList<Complex<T> > cplist1 = core_Transform_calculate_param(innerp, old_cage);
        if(cplist1.isEmpty())
            return Nmatrix;
        cplist.append(cplist1);
    }

    qreal lamda = 10;
 /*   for(int i = 0; i < 2*n; i++)
    {
        Complex<T>sample = get_sample(old_cage, 2, i);
        QList<Complex<T> >  cplist1 = calculate_derivative_param(sample, lamda);
        cplist.append(cplist1);
    } */
    for(int i = 0; i < p; i++)
    {
        Complex<T> sample = old_constrain_points.at(i);
        QList<Complex<T> >  cplist1 = core_Transform_calculate_derivative_param(sample, lamda, old_cage);
        cplist.append(cplist1);
    }
    Matrix<Complex<T> > A = j_to_matrix(cplist);
    A = A.pseudo_inverse();
    Nmatrix = A.cutrow(0, p);
    return Nmatrix;
}




template<class T>
inline QList<Complex<T> >
core_Transform_calculate_derivative_param(Complex<T>innerp, qreal lamda, QList<Complex<T> > old_cage)
{
    QList<Complex<T> > param;
    int size = old_cage.size();
    if(size < 3)
        return param;

    QList<Complex<T> > cage = old_cage;
    cage.prepend(old_cage.last());
    cage.append(old_cage.first());

    size = cage.size();
    QList<Complex<T> > B;
    QList<Complex<T> > IvB;
    for(int i = 0; i < size; i++)
    {
        Complex<T> B1 = cage.at(i);
        B1 = B1 - innerp;
        B.append(B1);
        B1 = Complex<T>(1, 0) / B1;
        IvB.append(B1);
    }

    Complex<T>imag(0, -1.0 / 6.2832);

    for(int i = 1; i < size-1; i++)
    {
        Complex<T>A1 = cage.at(i);
        A1 -= cage.at(i-1);
        Complex<T>A2 = cage.at(i+1);
        A2 -= cage.at(i);

        if((A1[0] == 0 && A1[1] == 0) || (A2[0] == 0 && A2[1] == 0))
        {
            Complex<T>B3 = IvB.at(i-1);
            Complex<T>B1 = IvB.at(i);
            Complex<T>B2 = IvB.at(i+1);
            Complex<T>C  = (B3*B1-B1*B2) * imag * lamda;
            param.append(C);
        }
        else
        {
            A1 = Complex<T>(1,0) / A1;
            A2 = Complex<T>(1,0) / A2;

            Complex<T>B3 = B.at(i-1);
            Complex<T>B1 = B.at(i);
            Complex<T>B2 = B.at(i+1);

            Complex<T>IvB0 = IvB.at(i-1);
            Complex<T>IvB1 = IvB.at(i);
            Complex<T>IvB2 = IvB.at(i+1);

            Complex<T>C1 = A1 * ((B3 - B1) * (IvB1 * IvB1 - IvB1 * IvB0));
            Complex<T>C2 = A2 * ((B2 - B1) * (IvB1 * IvB1 - IvB1 * IvB2));

            Complex<T>C = (C1+C2) * imag * lamda;
            param.append(C);
        }
    }
    return param;
}





template<class T>
inline QList<Complex<T> >
core_Transform_calculate_derivative1_param(Complex<T>innerp, qreal lamd, QList<Complex<T> >  old_cage)
{
    QList<Complex<T> > pa;

    int size = old_cage.size();

    QList<Complex<T> > cage = old_cage;
    cage.prepend(old_cage.last());
    cage.append(old_cage.first());

    size = cage.size();

    qreal angle;
    QList<Complex<T> > B;
    QList<Complex<T> > LogB;
    Complex<T>B3 = cage.at(0);
    B3 = B3 - innerp;
    B.append(B3);
    for(int i = 1; i < size; i++)
    {
        Complex<T>B3 = cage.at(i-1);
        B3 = B3 - innerp;
        Complex<T>B1 = cage.at(i);
        B1 = B1 - innerp;
        Complex<T>Log10 = (B1/B3).logcomplex();
        if(Log10[1] > 3.1416)
            Log10 = Complex<T>(Log10[0], 6.2832-Log10[1]);
        LogB.append(Log10);
        B.append(B1);
        angle += Log10[1];
    }

    bool is_verse = false;

    //this point is outside
    if(fabs(angle) < 2)
        return pa;
    else if(angle < 0)  //reverse direction
    {
        is_verse = true;
    }

    for(int i = 1; i < size-1; i++)
    {
        Complex<T>A1 = cage.at(i);
        A1 -= cage.at(i-1);
        Complex<T>A2 = cage.at(i+1);
        A2 -= cage.at(i);
        Complex<T>imag(0, -1.0 / 6.2832);
        Complex<T>Log21 = LogB.at(i);
        Complex<T>Log10 = LogB.at(i-1);

        Complex<T>B3 = B.at(i-1);
        Complex<T>B1 = B.at(i);
        Complex<T>B2 = B.at(i+1);

        if(is_verse)
        {
            Log21.conjuction();
            Log10.conjuction();
        }

        Complex<T>C  = (B2 / B1 - Complex<T>(1,0)) / A2 - Log21 + (B3 / B1 - Complex<T>(1,0)) / A1 + Log10;
        C = C * imag * lamd;
        pa.append(C);
    }
    return pa;
}




template<class T>
inline Complex<T>
core_Transform_control(Complex<T> oldp, QList<Complex<T> > param, QList<Complex<T> >  nControl)
{
    int size = nControl.size();
    if(param.size() != nControl.size())
        return oldp;
    Complex<T> newp(0,0);
    for(int i = 0; i < size; i++)
    {
        Complex<T>z = param[i];
        newp += z * nControl[i];
    }
    return newp;
}



template<class T>
inline QList<Complex<T> >
core_Transform_control(QList<Complex<T> > old_points, QList<QList<Complex<T> > > param, QList<Complex<T> > nControl)
{
    QList<Complex<T> >  newplist;
    if(param.size() != old_points.size())
        return old_points;
    for(int i = 0; i < old_points.size(); i++)
    {
        Complex<T> innerp = old_points[i];
        innerp = core_Transform_control(innerp, param[i], nControl);
        newplist.append(innerp);
    }
    return newplist;
}

#endif // FUNCTIONUTIL_TRANSFORM_H
