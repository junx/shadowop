#ifndef FUNCTIONUTIL_RANDOM_H
#define FUNCTIONUTIL_RANDOM_H

#include "coreUtilto.h"


//<<<<<<<Randomer>>>>>>>>>//
template<unsigned int N, class T> inline QVector<Vec<N,T> >
Random_generateSample(Vec<N,T> pMin, Vec<N,T> pMax, int pMaxSize, double minDis, QVector<Vec<N,T> > vecPoint);
inline QVector<int>
Random_generateOrder(int vMin, int vMax);
inline QList<QPointF>
Random_generatePoint(QPainterPath region, int pMinSize, double minDis);
inline QList<QPointF>
Random_generatePoint(QPolygonF region, int pMinSize, double minDis);




//<<<<<<<<<<<implementation>>>>>>>>>>>>//
template<unsigned int N, class T>
inline QVector<Vec<N,T> >
Random_generateSample(Vec<N,T> pMin, Vec<N,T> pMax, int pMaxSize, double minDis, QVector<Vec<N,T> > vecPoint)
{
    minDis = minDis*minDis;
    int unsuccessfulTimes = 0;
    Vec<N,T> pMean = (pMin + pMax) * 0.5;
    Vec<N,T> pd = pMax - pMin;
    while (unsuccessfulTimes < 1000)
    {
        Vec<N,T> pRand;
        for(unsigned int i = 0; i < N; i++)
            pRand[i] = (rand() / double(RAND_MAX) - 0.5) * pd[i] + pMean[i];
        bool flag = false;
        for ( int i=0; i< vecPoint.size(); i++ )
        {
            double dis = j_dis2(vecPoint[i], pRand);
            if ( dis < minDis )
            {
                flag = true;
                break;
            }
        }
        if ( flag == false )
        {
            unsuccessfulTimes = 0;
            vecPoint.append(pRand);
            if(vecPoint.size() >= pMaxSize)
                return vecPoint;
        }
        else
        {
            unsuccessfulTimes ++;
        }
    }
    return vecPoint;
}




inline QVector<int>
Random_generateOrder(int min, int max)
{
    QVector<int> rd;
    if(max < min)
        return rd;
    int size = max - min + 1;
    QVector<bool> used_flag(size);
    for(int i = 0; i < size; i++)
        used_flag[i] = false;
    for(int i = 0; i < size; i++)
    {
        int start = rand()%size;
        int step = rand()%(size-i)+1;
        int pos = 0;
        for(int j = 0; j < step;)
        {
            pos = (start++)%size;
            if(used_flag[pos])
                continue;
            j++;
        }
        used_flag[pos] = true;
        rd.append(pos+min);
    }
    return rd;
}



inline QList<QPointF>
Random_generatePoint(QPainterPath region, int pMinSize, double minDis)
{
    QList<QPointF> vecPoints;
    QVector<Vec2d> vecList;
    QRectF rect = region.boundingRect();
    int repeat = 0;
    while(vecPoints.size() < pMinSize && repeat < 5)
    {
        QVector<Vec2d> candPoints = Random_generateSample(j_to_vec2d(rect.topLeft()), j_to_vec2d(rect.bottomRight()), pMinSize, minDis, vecList);
        for(int i = 0; i < candPoints.size(); i++)
        {
            QPointF point = j_to_point(candPoints[i]);
            if(region.contains(point))
            {
                vecList.append(candPoints[i]);
                vecPoints.append(point);
            }
        }
        repeat++;
    }
    return vecPoints;
}



inline QList<QPointF>
Random_generatePoint(QPolygonF poly, int pMinSize, double minDis)
{
    QList<QPointF> vecPoints;
    QVector<Vec2d> vecList;
    QRectF rect = poly.boundingRect();
    int repeat = 0;
    while(vecPoints.size() < pMinSize && repeat < 5)
    {
        QVector<Vec2d> candPoints = Random_generateSample(j_to_vec2d(rect.topLeft()), j_to_vec2d(rect.bottomRight()), pMinSize, minDis, vecList);
        for(int i = 0; i < candPoints.size(); i++)
        {
            QPointF point = j_to_point(candPoints[i]);
            if(poly.containsPoint(point, Qt::FillRule(0)))
            {
                vecList.append(candPoints[i]);
                vecPoints.append(point);
            }
        }
        repeat++;
    }
    return vecPoints;
}


#endif // FUNCTIONUTIL_RANDOM_H
