#ifndef FUNCTIONUTIL_BSPLINE_H
#define FUNCTIONUTIL_BSPLINE_H

#include "coreUtilto.h"

//<<<<<<<<<BSpliner>>>>>>>>>//
template<template<class> class L, class T> inline T
core_BSpline_interpolation(L<T> ctrlpoints, double u, int smooth);
template<template<class> class L, class T> inline L<T>
core_BSpline_interpolation2(L<T> ctrlpoints, double minDis, int smooth);
inline double
core_BSpline_basis(int i, int k, double u);

template<template<class> class L, class T> T
BSpline_interpolation(L<T> ctrlpoints, double u, int smooth){return core_BSpline_interpolation(ctrlpoints, u, smooth);}
template<template<class> class L, class T> inline L<T>
BSpline_interpolation2(L<T> ctrlpoints, double minDis, int smooth, double step = 1){return core_BSpline_interpolation2(ctrlpoints, minDis, smooth, step);}

//<<<<<<<<<<<implementation>>>>>>>>>>>>>//
template<template<class> class L, class T>
inline T
core_BSpline_interpolation(L<T> ctrlpoints, double u, int smooth)
{
    if(u <= 0)  return ctrlpoints[0];
    double sum2 = core_BSpline_basis(0, smooth, u);
    T sum1 = sum2 * ctrlpoints[0];
    for (int i=1; i< ctrlpoints.size(); i++)
    {
        double b = core_BSpline_basis(i, smooth, u);
        sum1 += ctrlpoints[i] * b;
        sum2 += b;
    }
    return (sum1 / sum2);
}



template<template<class> class L, class T>
inline L<T>
core_BSpline_interpolation2(L<T> ctrlpoints, double minDis, int smooth, double step)
{
    L<T> nplist, filters;
    if(ctrlpoints.size() < 3)
        return ctrlpoints;
    if(step < 0)
        step = 1;
    for(int i = 0; i < ctrlpoints.size(); i++)
    {
        double u = i*step+1;
        T point = core_BSpline_interpolation(ctrlpoints, u, smooth);
        nplist.append(point);
    }
    if(minDis <= 0)
        return nplist;
    double lastDis = 0;
    filters.append(nplist[0]);
    for(int i = 1; i < nplist.size(); i++)
    {
        double dis = j_dis1(nplist[i-1], nplist[i]);
        lastDis += dis;
        if(lastDis < minDis)
            continue;
        while(lastDis >= minDis)
        {
            lastDis -= minDis;
            double scale = (dis - lastDis) / dis;
            T p = j_interpolate(nplist[i-1], nplist[i], scale);
            filters.append(p);
        }
    }
    return filters;
}



inline double
core_BSpline_basis(int i, int k, double u)
{
    // Implement according to \url{http://web.cs.wpi.edu/~matt/courses/cs563/talks/nurbs.html}
    if ( k == 0 )
    {
        if ( u >= i && u < i+1)
            return 1.0;
        else
            return 0.0;
    }
    else
    {
        double w1 = (u - i) / k;
        double w2 = (i+k+1 - u) / k;
        return (w1 * core_BSpline_basis(i, k-1, u) + w2 * core_BSpline_basis(i+1, k-1, u));
    }
}


#endif // FUNCTIONUTIL_BSPLINE_H
