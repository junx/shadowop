#ifndef FUNCTIONUTIL_PARTITION_H
#define FUNCTIONUTIL_PARTITION_H

#include "coreUtilto.h"

//<<<<<<<<<<<<Partitioner>>>>>>>>>>>>>>>//
class Partitioner
{
private:
    double           control_rate;
    int              control_depth;
    QVector<QVector<QPointF> > branch_points;
    QVector<QPointF>           cluster_centers;
    Vec<2, QVector<QPointF> > core_Partition_split(QVector<QPointF> plist, QPointF center);
    void                      core_Partition_cluster(QVector<QPointF> plist, QPointF parentcenter, int depth);
public:
    Partitioner(){control_depth = 5; control_rate = 5.0;}
    QVector<QPointF>          Partition_cluster(QVector<QPointF> plist, double scale, int depth);
};





template<class T> inline QVector<T>
Partition_kMeans1(const QVector<T> &oldCenters, const QVector<T> &data);
template<class T> inline QVector<QVector<T> >
Partition_kMeans2(QVector<T> data, int cluNum, QVector<QVector<int> > *clusterIndex);
inline QVector<QVector<int> >
Partition_kMeans3(QVector<QVector<double> > dis_matrix, double maxDis);




//<<<<<<<<<<<<implementation>>>>>>>>>>>>>>>//
template<class T>
inline QVector<T>
Partition_kMeans1(const QVector<T> &oldCenters, const QVector<T> &data)
{
    int size = oldCenters.size();
    if(size <= 1)
        return oldCenters;
    QVector<QVector<T> >  clusters(size);
    for(int i = 0; i < data.size(); i++)
    {
        T p = data[i];
        int cluster_index = 0;
        double mindis = MAXVALUE;
        for(int j = 0; j < size; j++)
        {
            double dis = j_dis2(p, oldCenters[j]);
            if(dis < mindis)
            {
                mindis = dis;
                cluster_index = j;
            }
        }
        clusters[cluster_index].append(p);
    }

    QVector<T> newCenters;
    for(int i = 0; i < size; i++)
    {
        if(data[i].size() > 0)
        {
           T center = j_norm(Core2<QVector,T>::j_ave(clusters[i]));
           newCenters.append(center);
        }
    }
    return newCenters;
}



template<class T>
inline QVector<QVector<T> >
Partition_kMeans2(QVector<T> data, int cluNum, QVector<QVector<int> > *clusterIndex)
{
    QVector<QVector<T> > clusters(cluNum);
    QVector<QVector<int> > cluIndex(cluNum);
    if(cluNum <= 0 || data.size() < cluNum*2) return clusters;
    QVector<T>   cluMean(cluNum);
    for(int i = 0; i < cluNum; i++)
        cluMean[i] = data[i*2];
    int iterationTime = 4;
    for(int i = 0; i < iterationTime; i++)
    {
        cluIndex = QVector<QVector<int> >(cluNum);
        clusters = QVector<QVector<T> >(cluNum);
        for(int j = 0; j < data.size(); j++)
        {
            double minv = MAXVALUE;
            int   minIndex = 0;
            for(int k = 0; k < cluNum; k++)
            {
                double dis = j_dis2(data[j], cluMean[k]);
                if(dis < minv)
                {
                    minv = dis;
                    minIndex = k;
                }
            }
            cluIndex[minIndex].append(j);
            clusters[minIndex].append(data[j]);
        }
        for(int j = 0; j < cluNum; j++)
            cluMean[j] =  j_norm(Core2<QVector, T>::j_ave(clusters[j]));
    }
    if(clusterIndex)
       *clusterIndex = cluIndex;
    return clusters;
}




inline QVector<QVector<int> >
Partition_kMeans3(QVector<QVector<double> > dis_matrix, double maxDis)
{
    QVector<QVector<int> > clusters;
    int size = dis_matrix.size();
    if(size == 0 || size != dis_matrix[0].size())
        return clusters;
    QVector<bool> used_flag(size);
    for(int i = 0; i < size; i++)
        used_flag[i] = false;
    while(true)
    {
        QVector<int> cluster;
        QVector<int> addOnCluster;
        QVector<int> addOnCluster2;
        //<<<<<<<<<<<<find a sord to start with>>>>>>>//
        int sord = -1;
        for(int i = 0; i < size; i++)
        {
            if(!used_flag[i])
            {
                sord = i;
                break;
            }
        }
        if(sord == -1)
            break;

        addOnCluster.append(sord);
        used_flag[sord] = true;
        //<<<<<<<for each sord find new connection>>>>//
        while(!addOnCluster.isEmpty())
        {
            for(int i = 0; i < addOnCluster.size(); i++)
            {
                int refer = addOnCluster.at(i);
                for(int j = 0; j < size; j++)
                {
                    if(used_flag[j])
                        continue;
                    if(dis_matrix[refer][j] < maxDis)
                    {
                        addOnCluster2.append(j);
                        used_flag[j] = true;
                    }
                }
            }
            clusters.append(addOnCluster);
            addOnCluster = addOnCluster2;
            addOnCluster2.clear();
        }
        clusters.append(cluster);
    }
    return clusters;
}



#endif // FUNCTIONUTIL_PARTITION_H
