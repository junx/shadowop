#include "functionUtil_Partition.h"


Vec<2, QVector<QPointF> >
Partitioner::core_Partition_split(QVector<QPointF> plist, QPointF center)
{
    Vec<2, QVector<QPointF> > partions;
    int size = plist.size();
    if(size < 2)
        return partions;
    QVector<QPointF> cplist;
    for(int i = 0; i < size; i++)
        cplist.append(plist.at(i) - center);

    qreal lxx = 0, lxy = 0, lyy = 0;
    for(int i = 0; i < size; i++)
    {
        lxy += cplist.at(i).x() * cplist.at(i).y();
        lxx += cplist.at(i).x() * cplist.at(i).x();
        lyy += cplist.at(i).y() * cplist.at(i).y();
    }
    qreal linearity = lxx*lyy;
    if(linearity != 0)
        linearity = lxy / sqrt(linearity);
 /*   if(fabs(linearity) > 0.99)
        return partions; */
    QVector<QPointF> left;
    QVector<QPointF> right;
    for(int i = 0; i < size; i++)
    {
        qreal p = cplist.at(i).x()*lxy + cplist.at(i).y()*lyy;
        if(p > 0)
            left.append(plist.at(i));
        else
            right.append(plist.at(i));
    }
    partions[0] = left;
    partions[1] = right;
    return partions;
}



void
Partitioner::core_Partition_cluster(QVector<QPointF> plist, QPointF parentcenter, int depth)
{
    if(plist.isEmpty())
        return;
    QPointF center = Core2<QVector, QPointF>::j_ave(plist);
    if(depth >= control_depth)
    {
        cluster_centers.append(center);
        return;
    }
    Vec<2,QVector<QPointF> > partions;
    if(depth <= 0 || j_dis2(parentcenter, center) >= control_rate*control_rate)
         partions = core_Partition_split(plist, center);
    if(partions[0].size() == 0 || partions[1].size() == 0)
    {
        cluster_centers.append(center);
        return;
    }
    core_Partition_cluster(partions[0], center, depth+1);
    core_Partition_cluster(partions[1], center, depth+1);
}




QVector<QPointF>
Partitioner::Partition_cluster(QVector<QPointF> plist, double scale, int depth)
{
    control_depth = depth;
    control_rate = scale;
    core_Partition_cluster(plist, QPointF(-1,-1), 0);
    return cluster_centers;
}
