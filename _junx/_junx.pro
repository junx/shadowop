#-------------------------------------------------
#
# Project created by QtCreator 2013-10-11T13:24:22
#
#-------------------------------------------------

TARGET = _junx
TEMPLATE = lib
CONFIG += staticlib


DEPENDPATH += function \
              tool \
INCLUDEPATH += function \
               tool \

SUBDIRS = function tool

SOURCES += \
    tool/coreDeform_cage.cpp \
    tool/coreDeform_ctrl.cpp \
    function/functionUtil_Partition.cpp \
    tool/coreTransable.cpp \
    tool/coreOperatable.cpp \
    tool/coreHistoryer_undoer.cpp \
    tool/coreRunable.cpp \
    tool/coreGrider.cpp \
    tool/coreNeighborable.cpp \
    tool/coreEGrider.cpp


HEADERS += \
    core2.h \
    core.h \
    function/functionUtil_BSpline.h \
    function/functionUtil_Partition.h \
    function/functionUtil_Random.h \
    function/functionUtil_Transform.h \
    tool/coreDeform.h \
    tool/coreTransable.h \
    tool/coreMapable.h \
    tool/coreOperatable.h \
    coreToolUtil.h \
    coreGlobal.h \
    coreFunctionUtil.h \
    coreMath.h \
    coreUtilto.h \
    tool/coreHistoryer.h \
    tool/coreRunable.h \
    tool/coreNeighborable.h \
    tool/coreSelectable.h \
    tool/coreObject.h \
    tool/coreGrider.h \



unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}

