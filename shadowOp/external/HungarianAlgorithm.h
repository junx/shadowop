#ifndef HUNGARIANALGORITHM_H
#define HUNGARIANALGORITHM_H

#define CHECK_FOR_INF
#define ONE_INDEXING
#define INF 10000000

#include "globalShadowCore.h"

class CHungarianAlgorithm
{
public:
    void FindOptimalAssignment(QVector<short>& assignment, Matrixd distMatrixIn);

    void FindOptimalAssignmentOld(QVector<int>& assignment, qreal* cost, QVector<qreal>& distMatrixIn, const int nOfRows, const int nOfColumns);

private:
    inline qreal haGetInf() { return 1e8; }
    inline bool haIsFinite(qreal d) { return d < haGetInf(); }

    void SetMatCost(QVector<QVector<qreal> >& matCost);

    void InitLabel(int n);

    void UpdateLabel();

    void AddToTree(int i, int previ);

    void Augment();

    inline QVector<int>& GetXY() { return m_vecXY; }

    int m_N; // number of vertices in one part
    int m_maxMatch;
    QVector<qreal> m_vecLx, m_vecLy; // labels of X and Y parts
    QVector<int> m_vecXY, m_vecYX;
    QVector<bool> m_vecS, m_vecT;
    QVector<qreal> m_vecSlack;
    QVector<qreal> m_vecSlackx;
    QVector<int> m_vecPrev; // to record alternating paths
    QVector<QVector<qreal> > m_matCost; // cost matrix
};

#endif
