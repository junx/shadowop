#ifndef THREAD_H
#define THREAD_H

#include "ShadowCoreSystem.h"

class ThreadProcess : public coreThreadProcessable
{
    Q_OBJECT
public slots:
    virtual void      on_start();
signals:
    void              on_return(int);
public:
    int               thread_id;
    ThreadProcess(){thread_id = 0;}
public:
    virtual bool      start_process_in_thread(QThread::Priority = QThread::IdlePriority);
};



#endif // THREAD_H
