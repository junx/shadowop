#include "operation.h"

Sketchable::Sketchable()
    :Operatable(), TransformTransable(TRANSABLE_BASE)
{       
    set_type(ADD_SKETCHABLE);

    op_color = QColor(Qt::black);
    op_size = 3;
    op_opacity = 100;
    op_path = Path();
    op_bounding_box = QRectF(0,0,0,0);
}


Sketchable::Sketchable(const OpParam &param)
    :Operatable(), TransformTransable(TRANSABLE_BASE)
{
    set_type(ADD_SKETCHABLE);

    op_color = param.color;
    op_opacity = param.opacity;
    op_size = param.size;
    op_path = Path();
    op_bounding_box = QRectF(0,0,0,0);
}


Sketchable::Sketchable(const Sketchable &cop)
    :Operatable(cop),TransformTransable(cop)
{
    op_path = cop.op_path;
    op_color = cop.op_color;
    op_opacity = cop.op_opacity;
    op_size = cop.op_size;
    op_bounding_box = cop.op_bounding_box;
}


bool
Sketchable::on_start(QEvent *e, QPixmap *pixmap)
{
    bool state = op_path.trigger_device_pressed(e);
    //<<<<<<<<<<<render>>>>>>>>>>>>//
    if(!pixmap || !state) return false;
    QPainter painter(pixmap);
    painter.setBrush(QBrush(op_color));
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setOpacity(op_opacity);
    QPointF p = op_path.get_orig_points().last();
    qreal s = op_size * op_path.get_path_pressure().last();
    painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
    painter.end();
    return true;
}


bool
Sketchable::on_update(QEvent *e, QPixmap *pixmap)
{
    int startIndex = op_path.get_orig_points().size();
    bool state = op_path.trigger_device_move(e);
    //<<<<<<<<<<<render>>>>>>>>>>>>//
    if(!pixmap || !state || startIndex < 1) return false;
    QPainter painter(pixmap);
    painter.setBrush(QBrush(op_color));
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setOpacity(op_opacity);
    QList<QPointF> points = op_path.get_orig_points();
    QList<float>   pressures = op_path.get_path_pressure();
    for(int i = startIndex; i< points.size(); i++)
    {
        qreal numSteps = j_dis1(points[i-1], points[i]) + 1;
        for (int j = 0; j <= numSteps; j++)
        {
            qreal s = j_interpolate(pressures[i-1], pressures[i], j/numSteps) * op_size;
            QPointF p = j_interpolate(points[i-1], points[i], j/numSteps);
            painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
        }
    }
    painter.end();
    return true;
}


bool
Sketchable::on_finish(QEvent *e, QPixmap *)
{
    bool state = op_path.trigger_device_released(e);
    if(state) update_bounding();
    return state;
}


void
Sketchable::replay(QPainter &painter, QPointF move, float scale)
{
    if(op_path.is_empty() || !Operatable::get_active())
        return;
    painter.setBrush(QBrush(op_color));
    painter.setOpacity(op_opacity);
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);
    QList<QPointF> points = op_path.get_orig_points();
    QList<float>   pressures = op_path.get_path_pressure();
    float baseSize = op_size * op_path.get_pressure() * scale;
    qreal s = pressures.first()*baseSize;
    QPointF p = points.first()+move;
    painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
    for(int i = 1; i< points.size(); i++)
    {
        qreal numSteps = j_dis1(points[i-1], points[i]) + 1;
        for (int j = 1; j <= numSteps; j++)
        {
            s = j_interpolate(pressures[i-1], pressures[i], j/numSteps)*baseSize;
            p = j_interpolate(points[i-1], points[i], j/numSteps) + move;
            painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
        }
    }
}


void
Sketchable::dynamicReplay(Sketchable *prediction, qreal ratio, QPainter &painter)
{
    if(ratio <= 0)
    {
        replay(painter);
        return;
    }
    if(ratio >= 1)
    {
        prediction->replay(painter);
        return;
    }
    if(op_path.is_empty() || prediction->get_points().isEmpty())
        return;

    QColor pcolor = prediction->get_color();
    QColor color = j_interpolate(op_color, pcolor, ratio);
    painter.setBrush(QBrush(color));
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);

    QList<QPointF> points1 = op_path.get_orig_points();
    QList<float>   pressures1 = op_path.get_path_pressure();
    float baseSize1 = op_size * op_path.get_pressure();
    QList<QPointF> points2 = prediction->get_path().get_orig_points();
    QList<float>   pressures2 = prediction->get_path().get_path_pressure();
    float baseSize2 = prediction->get_size() * prediction->get_path().get_pressure();

    int size1 = points1.size(), size2 = points2.size();
    int size = j_interpolate(size1*1.0, size2*1.0, ratio);
    QList<QPointF> points;
    QList<float> pressures;
    points.append(j_interpolate(points1[0], points2[0], ratio));
    pressures.append(j_interpolate(pressures1[0], pressures2[0], ratio));
    for(int i = 1; i < size; i++)
    {
        float scale = i*1.0/(size-1);
        QPointF p = j_interpolate(points1[scale*(size1-1)], points2[scale*(size2-1)], ratio);
        qreal s = j_interpolate(pressures1[scale*(size1-1)], pressures2[scale*(size2-1)], ratio);
        points.append(p);
        pressures.append(s);
    }
    float baseSize = j_interpolate(baseSize1, baseSize2, ratio);
    qreal s = pressures[0]*baseSize;
    QPointF p = points[0];
    painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
    for(int i = 1; i< size; i++)
    {
        qreal numSteps = j_dis1(points[i-1], points[i]) + 1;
        for (int j = 1; j <= numSteps; j++)
        {
            s = j_interpolate(pressures[i-1], pressures[i], j/numSteps)*baseSize;
            p = j_interpolate(points[i-1], points[i], j/numSteps);
            painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
        }
    }
}


void
Sketchable::write(QTextStream *out)
{
    Operatable::write(out);
    *out<<"param\n"<<op_color.red()<<" "<<op_color.green()<<" "<<op_color.blue()<<" "<<op_opacity<<" "
       <<op_size<<" "<<op_bounding_box.left()<<" "<<op_bounding_box.top()<<" "<<op_bounding_box.width()<<" "<<op_bounding_box.height()<<"\n";
    op_path.write(out);
    *out<<"\n";
}


bool
Sketchable::read(QTextStream *in)
{
    Operatable::read(in);

    QString line = in->readLine();
    while(line.isEmpty())
        line = in->readLine();
    if(line.compare("param"))
        return false;
    line = in->readLine();
    QStringList list = line.split(" ");
    op_color = QColor(list.at(0).toInt(), list.at(1).toInt(), list.at(2).toInt());
    op_opacity = list.at(3).toFloat();
    op_size = list.at(4).toUShort();
    op_bounding_box = QRectF(list.at(5).toDouble(), list.at(6).toDouble(), list.at(7).toDouble(), list.at(8).toDouble());

    Path path;
    if(!path.read(in))
        return false;
    op_path = path;
    return true;
}



bool
Stipple::on_start(QEvent *e, QPixmap *pixmap)
{
    bool state = op_path.trigger_device_pressed_anchor(e);
    //<<<<<<<<<<<render>>>>>>>>>>>>//
    if(!pixmap || !state) return false;
    QPainter painter(pixmap);
    painter.setBrush(QBrush(op_color));
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);
    QPointF p = op_path.get_orig_points().last();
    qreal s = op_size * op_path.get_path_pressure().last();
    painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
    painter.end();
    return true;
}




bool
Stipple::on_update(QEvent *e, QPixmap *pixmap)
{
    bool state = op_path.trigger_device_move_anchor(e);
    //<<<<<<<<<<<render>>>>>>>>>>>>//
    if(!pixmap || !state) return false;
    QPainter painter(pixmap);
    painter.setBrush(QBrush(op_color));
    painter.setPen(Qt::NoPen);
    painter.setRenderHint(QPainter::Antialiasing);
    QPointF p = op_path.get_orig_points().last();
    qreal s = op_size * op_path.get_path_pressure().last();
    painter.drawEllipse(QPointF(p.x() - s/2.0, p.y()-s/2.0), s, s);
    painter.end();
    return true;
}



bool
Stipple::on_finish(QEvent *e, QPixmap *)
{
    Operatable::on_finish(e);  //record time
    return op_path.trigger_device_released_anchor(e);
}
