#include "synthesis.h"
#include "synthesiser.h"

coreSynthesis::coreSynthesis(coreNeighborable *cutE)
{
    myCutElement = cutE;
    identity = cutE->get_identity();
    myFlag = -1;
    is_abort = false;
    optimization_iteration = 10;
    myGrider = 0;
    isRendered = false;
    myRendRect = QRect(0,0,0,0);
    connect(ShadowCoreSystem::getSynthesiser(), SIGNAL(on_delete_synthesis(int)), this, SLOT(on_receive_delete(int)));
}


void
coreSynthesis::on_process()
{
    QTime time; time.start();

    int w = ShadowCoreSystem::getMainCanvas()->width(), h = ShadowCoreSystem::getMainCanvas()->height();
    myGrider = new EGrider(Vec2d(0,0), Vec2d(w, h), Vec2i(80, 60));

    dynamic_cast<Operatable*>(myCutElement)->set_busy(true);  //busy now, don't delete it
    on_initial();
    on_iterate();
    on_render();

    myCutElement->set_exhaust(true); //has been synthesized
    for(int i = 0; i < myElementList.size(); i++)
    {
        myElementList[i]->set_active(false); //default value is *true*
        myElementList[i]->set_exhaust(true); //has been synthesized
    }

    ShadowCoreSystem::getSynthesiser()->pushElementToHistory(this);

    dynamic_cast<Operatable*>(myCutElement)->set_busy(false);  //finished, can delete it now

    qDebug()<<"synthesis time = "<<time.elapsed()*0.001;

    qreal synFlags[3] = {myFlag*1.0, identity*1.0, myElementList.size()*1.0};
    emit onFinshSynthesis(3, synFlags);
}


void
coreSynthesis::on_render()
{
    if(isRendered) return;
    isRendered = true;
    if(myElementList.isEmpty()) return;

    QImage image(ShadowCoreSystem::getMainCanvas()->width(), ShadowCoreSystem::getMainCanvas()->height(), QImage::Format_ARGB32);
    image.fill(Qt::transparent);
    QPainter painter(&image);
    QList<QPointF> plist;
    //<<<<<<<<Switch to sketchable when rendering>>>>>>>>//
    QList<Sketchable*> oplist = coreNeighborable_to_sketchable(myElementList);
    for(int i = 0; i < oplist.size(); i++)
    {
        if(!oplist[i]) continue;
        float origOpacity = oplist[i]->get_opacity();
        QColor origColor = oplist[i]->get_color();
        oplist[i]->set_opacity(0.1);  //set it to shadow mode
        oplist[i]->set_color(QColor(Qt::red));
        oplist[i]->replay(painter);
        oplist[i]->set_opacity(origOpacity);
        oplist[i]->set_color(origColor);
        QRectF rect = oplist[i]->get_bounding();
        plist.append(rect.topLeft());
        plist.append(rect.bottomRight());
    }
    myRendRect = j_to_bounding(plist).adjusted(-15, -15, 15, 15).toRect();
    myRendPixmap = image.copy(myRendRect);
    painter.end();
}


void
coreSynthesis::remove_elements(QList<coreNeighborable*> elist)
{
    bool isValid = false;
    for(int i = elist.size()-1; i >= 0; i--)
    {
        for(int j = myElementList.size()-1; j >= 0; j--)
        {
            if(elist[i] == myElementList[j])
            {
                myElementList.removeAt(j);
                if(myGrider)
                    myGrider->remove_element(elist[i]);
                isValid = true;
                break;
            }
        }
    }
    if(isValid)
    {
        isRendered = false;
        myRendRect = QRect(0,0,0,0);
        myRendPixmap = QImage();
        on_render();
    }
}


void
coreSynthesis::freeit()
{
    //<<<<<<<<<switch to sketchable when delete>>>>>>>//
    for(int i = 0; i < myElementList.size(); i++)
        operation_delete(dynamic_cast<Sketchable*>(myElementList[i]));
    myElementList.clear();
    if(myGrider)
        delete myGrider;
    isRendered = false;
}

