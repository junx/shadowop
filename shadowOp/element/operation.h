#ifndef OPERATION_H
#define OPERATION_H

#include "opParam.h"
#include "cxNeighborable.h"

class Sketchable : public Operatable, public TransformTransable
{
protected:
    Path      op_path;
    QColor    op_color;
    float     op_opacity;
    ushort    op_size;
    QRectF    op_bounding_box;
//<<<<<<<<<<<implement of transformable>>>>>>>>>>>>>//
    virtual inline void      transable_finish_transfer(){update_bounding();}
    inline QList<QPointF>    transable_get_point(){return get_points();}
    inline void              transable_set_point(QList<QPointF> plist){set_points(plist);}

public:
    Sketchable();
    Sketchable(const OpParam &param);
    Sketchable(const Sketchable &cop);
    virtual ~Sketchable(){}
    virtual Sketchable* copy(){return 0;}

    inline Path      get_path(){return  op_path;}
    inline int       get_length(){return op_path.get_size();}
    inline ushort    get_size(){return op_size;}
    inline float     get_opacity(){return op_opacity;}
    inline QColor    get_color(){return op_color;}

    inline void      set_path(Path path){op_path = path;}
    inline void      set_size(ushort size){op_size = size;}
    inline void      set_opacity(float opac){op_opacity = opac;}
    inline void      set_color(QColor color){op_color = color;}
    virtual QList<QPointF> get_points(){return op_path.get_orig_points();}
    inline void      set_points(QList<QPointF> plist){
        if(plist.size() != op_path.get_size()) return;
        op_path.set_orig_points(plist);
        update_bounding();
    }

    virtual void           set_paramter(OpParam param){op_color = param.color; op_size = param.size; op_color.setAlpha(param.opacity);}
    inline QRectF          get_bounding(){return op_bounding_box;}
    inline virtual void    update_bounding(){op_bounding_box = j_to_bounding(get_points());}

    virtual bool          checkValid(){if(op_path.get_orig_points().size() == op_path.get_path_pressure().size()) return true; return false;}

    virtual bool      on_start(QEvent *e, QPixmap *pixmap);
    virtual bool      on_update(QEvent *e, QPixmap *pixmap);
    virtual bool      on_finish(QEvent *e, QPixmap *);

    virtual void      replay(QPainter &painter, QPointF move = QPointF(0,0), float scale = 1);
    virtual void      dynamicReplay(Sketchable *prediction, qreal ratio, QPainter &painter);
    virtual void      write(QTextStream *out);
    virtual bool      read(QTextStream *in);
};


class Brush : public Sketchable, public CxNeighborable
{
public:
    Brush():Sketchable(),CxNeighborable(){
        set_type(OPERATION_BRUSH);
    }
    Brush(const OpParam &param):Sketchable(param),CxNeighborable(){
        set_type(OPERATION_BRUSH);
    }
    Brush(const Brush &cop):Sketchable(cop),CxNeighborable(){}
    virtual Brush* copy(){Brush *op = new Brush(*this); return op;}

    inline void           set_GridPoints(QList<QPointF> plist){set_points(plist);}
    inline QList<QPointF> get_GridPoints(){return get_points();}
    inline QPointF        get_GridPoint(int id){return op_path.get_orig_point(id);}
    inline QPointF        get_GridCenter(){return op_path.get_middle_point();}
    inline int            get_GridPointSize(){return op_path.get_size();}
    inline QRectF         get_GridBounding(){return op_bounding_box;}

    virtual bool          checkValid(){return Sketchable::checkValid()&&CxNeighborable::checkValid();}
};


class Pencil : public Brush
{
public:
    Pencil():Brush(){
        set_type(OPERATION_PENCIL);
        op_opacity = float(0.3);
        op_size = 2;
    }
    Pencil(const OpParam &param):Brush(param){set_type(OPERATION_PENCIL);}
    Pencil(const Pencil &cop):Brush(cop){set_type(OPERATION_PENCIL);}
    virtual Pencil* copy(){Pencil *op = new Pencil(*this); return op;}
};


class Stipple : public Brush
{
protected:
    inline void      transable_finish_transfer(){}    
public:
    Stipple():Brush(){
        set_type(OPERATION_STIPPLE);
        QList<QPointF> plist; plist.append(QPointF(0,0)); op_path.set_orig_points(plist);
        QList<float> prelist; prelist.append(float(0.01)); op_path.set_path_pressure(prelist);
    }
    Stipple(const OpParam &param):Brush(param){set_type(OPERATION_STIPPLE);}
    Stipple(const Stipple &cop):Brush(cop){set_type(OPERATION_STIPPLE);}
    virtual Stipple* copy(){Stipple *op = new Stipple(*this); return op;}

    inline void      update_bounding(){op_bounding_box = j_to_bounding(get_points());}
    inline float     get_stipple_pressure(){
        assert(op_path.get_path_pressure().size() == 1);
        return op_path.get_path_pressure().first();
    }
    inline void      set_stipple_pressure(float p){
        QList<float> plist; plist.append(p);
        op_path.set_path_pressure(plist);
    }

    virtual bool   checkValid(){return (op_path.get_size()==1)&&Sketchable::checkValid()&&CxNeighborable::checkValid();}

    bool      on_start(QEvent *e, QPixmap *pixmap);
    bool      on_update(QEvent *e, QPixmap *pixmap);
    bool      on_finish(QEvent *e, QPixmap *);
};



class SketchCluster : public Sketchable
{
protected:
    QList<Sketchable *>        operationlist;
    inline virtual QList<TransformTransable*>   transable_get_children(){
        QList<TransformTransable*> childern;
        for(int i = 0; i < operationlist.size(); i++)
            childern.append(operationlist[i]);
        return childern;
    }
public:
    SketchCluster();
    SketchCluster(const SketchCluster &cluop);
    SketchCluster(QList<Sketchable*> ops);
    virtual ~SketchCluster();
    virtual SketchCluster* copy(){SketchCluster *op = new SketchCluster(*this); return op;}

    //<<<<<<<<<alway rotate around cluster_center>>>>>>>>>>//
    virtual QList<QPointF>        get_points(){QList<QPointF> plist; for(int i = 0; i < operationlist.size(); i++) plist.append(operationlist[i]->get_points()); return plist;}
    virtual QList<Sketchable*>    get_direct_childern(){return operationlist;}
    virtual QList<Sketchable*>    get_pure_childern(){QList<Sketchable*> childern;
                                                    for(int i = 0; i < operationlist.size(); i++)
                                                    {
                                                        SketchCluster* cop = dynamic_cast<SketchCluster*>(operationlist[i]);
                                                        if(cop) childern.append(cop->get_pure_childern());
                                                        else childern.append(operationlist[i]);
                                                    }
                                                    return childern;
                                                   }

    virtual bool      on_start(QEvent *){return true;}
    virtual bool      on_update(QEvent *){return true;}
    virtual bool      on_finish(QEvent *){return true;}

    virtual void      replay(QPainter &painter,QPointF move = QPointF(0,0), float scale = 1);
    virtual void      write(QTextStream *);
    virtual bool      read(QTextStream *);
};


class OpUndo : public Operatable
{
public:
    OpUndo():Operatable(){
        set_type(OPERATION_UNDO);
    }
};


class OpRedo : public Operatable
{
public:
    OpRedo():Operatable(){
        set_type(OPERATION_REDO);
    }
};



//<<<<<<<<<<<<static methods>>>>>>>>>>>//
inline QList<coreNeighborable*>
operatable_to_coreNeighborable(QList<Operatable*> slist)
{
    QList<coreNeighborable*> nlist;
    for(int i = 0; i < slist.size(); i++)
        nlist.append(dynamic_cast<coreNeighborable*>(slist[i]));
    return nlist;
}



inline QList<Operatable*>
coreNeighborable_to_operatable(QList<coreNeighborable*> slist)
{
    QList<Operatable*> nlist;
    for(int i = 0; i < slist.size(); i++)
        nlist.append(dynamic_cast<Operatable*>(slist[i]));
    return nlist;
}


inline QList<Operatable*>
sketchable_to_operatable(QList<Sketchable*> slist)
{
    QList<Operatable*> oplist;
    for(int i = 0; i < slist.size(); i++)
        oplist.append(slist[i]);
    return oplist;
}

inline QList<Sketchable*>
operatable_to_sketchable(QList<Operatable*> oplist)
{
    QList<Sketchable*> slist;
    for(int i = 0; i < oplist.size(); i++)
        slist.append(dynamic_cast<Sketchable*>(oplist[i]));
    return slist;
}


inline QList<coreNeighborable*>
sketchable_to_coreNeighborable(QList<Sketchable*> slist)
{
    QList<coreNeighborable*> nlist;
    for(int i = 0; i < slist.size(); i++)
        nlist.append(dynamic_cast<coreNeighborable*>(slist[i]));
    return nlist;
}


inline QList<Sketchable*>
coreNeighborable_to_sketchable(QList<coreNeighborable*> clist)
{
    QList<Sketchable*> nlist;
    for(int i = 0; i < clist.size(); i++)
        nlist.append(dynamic_cast<Sketchable*>(clist[i]));
    return nlist;
}


inline QList<OpNeighborable*>
sketchable_to_opNeighborable(QList<Sketchable*> slist)
{
    QList<OpNeighborable*> nlist;
    for(int i = 0; i < slist.size(); i++)
        nlist.append(dynamic_cast<OpNeighborable*>(slist[i]));
    return nlist;
}


inline QList<Sketchable*>
opNeighborable_to_sketchable(QList<OpNeighborable*> clist)
{
    QList<Sketchable*> nlist;
    for(int i = 0; i < clist.size(); i++)
        nlist.append(dynamic_cast<Sketchable*>(clist[i]));
    return nlist;
}



inline Operatable*
operation_create(OpParam op_param, ulong opType)
{
    switch (opType)
    {
    case OPERATION_BRUSH:  //brush
    {
        return new Brush(op_param);
    }
    case OPERATION_PENCIL:
    {
        return new Pencil(op_param);
    }
    case OPERATION_STIPPLE:
    {
        return new Stipple(op_param);
    }
    default:
        break;
    }
    return NULL;
}


inline Operatable*
operation_copy(Operatable *op)
{
    if(!op) return 0;
    switch (op->get_type())
    {
    case OPERATION_BRUSH:  //brush
    {
        return new Brush(*dynamic_cast<Brush*>(op));
    }
    case OPERATION_PENCIL:
    {
        return new Pencil(*dynamic_cast<Pencil*>(op));
    }
    case OPERATION_STIPPLE:
    {
        return new Stipple(*dynamic_cast<Stipple*>(op));
    }
    default:
        break;
    }
    return NULL;
}


inline void
operation_delete(Operatable *op)
{
    if(!op) return;
    switch (op->get_type())
    {
    case OPERATION_BRUSH:  //brush
    {
        Brush *brush =  dynamic_cast<Brush*>(op);
        delete brush;
        break;
    }
    case OPERATION_PENCIL:
    {
        Pencil *pencil = dynamic_cast<Pencil*>(op);
        delete pencil;
        break;
    }
    case OPERATION_STIPPLE:
    {
        Stipple *stipple = dynamic_cast<Stipple*>(op);
        delete stipple;
        break;
    }
    default:
        break;
    }
    op = 0;
}


inline bool
operation_reply(Operatable *op, QPixmap *pixmap)
{
    if(!op || !pixmap) return false;
    QPainter painter(pixmap);
    op->replay(painter, QPointF(0,0), 1);
    return true;
}


inline bool
operation_write(Operatable *op, QTextStream *out)
{
    if(!op || !out) return false;
    *out<<"startop1\n";
    *out<<"startop2\n";
    *out<<op->get_type()<<"\n";
    op->write(out);
    *out<<"endop\n\n"<<endl;
    return true;
}


inline Operatable*
operation_read(QTextStream *in)
{
    if(!in) return 0;
    QString line = in->readLine();
    while(!line.contains("startop2"))
    {
        if(in->atEnd()) return 0;
        line = in->readLine();
    }
    line = in->readLine();
    while(line.isEmpty())
        line = in->readLine();
    uint optype = line.toInt();
    Operatable *returnOp = 0;
    switch (optype)
    {
    case OPERATION_BRUSH:
    {
        Brush *op = new Brush();
        assert(op->Sketchable::read(in));
        returnOp = op;
        break;
    }
    case OPERATION_PENCIL:
    {
        Pencil *op = new Pencil();
        assert(op->Sketchable::read(in));
        returnOp = op;
        break;
    }
    case OPERATION_STIPPLE:
    {
        Stipple *op = new Stipple();
        assert(op->Sketchable::read(in));
        returnOp = op;
        break;
    }
    case OPERATION_UNDO:
    {
        OpUndo *op = new OpUndo();
        assert(op->read(in));
        returnOp = op;
        break;
    }
    case OPERATION_REDO:
    {
        OpRedo *op = new OpRedo();
        assert(op->read(in));
        returnOp = op;
        break;
    }
    default:
    {//bad operation
        while(!line.contains("endop"))
        {
            if(in->atEnd()) return 0;
            line = in->readLine();
        }
        Operatable *op = new Operatable();
        return op;
    }
    }
    line = in->readLine();
    while(line.isEmpty())
        line = in->readLine();
    if(line.contains("endop"))
       return returnOp;
    return 0;
}



#endif // OPERATION_H
