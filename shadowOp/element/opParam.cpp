#include "opParam.h"
#include "coreWidget_canvas.h"

bool
Path::start(QPointF point, float pressure)
{
    //intialize global para
    orig_points.clear();
    orig_points.append(point);
    path_pressure.clear();
    path_pressure.append(pressure);
    return true;
}


bool
Path::add_point(QPointF point, float pressure)
{
    float dis = j_dis1(point, orig_points.last());
    if(dis < 3)
        return false;
    if(dis > 3.2)
    {
        float     step = dis / (3.1);
        QPointF step_add1 = (point - orig_points.last()) / step;
        float   step_add2 = (pressure - path_pressure.last()) / step;
        QPointF startp = orig_points.last();
        float   startpr = path_pressure.last();
        for(int i = 1; i <= step; i++)
        {
            add_point(startp+step_add1*i, startpr+step_add2*i);
        }
        return true;
    }

    orig_points.append(point);
    path_pressure.append(pressure);
    return true;
}


bool
Path::end(QPointF, float)
{
    return true;
}



bool
Path::trigger_device_pressed(QEvent *e)
{
    if(e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *event = dynamic_cast<QMouseEvent *>(e);
        return start(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e), j_to_pressure(event));
    }
    else if(e->type() == QEvent::TabletPress)
    {
        QTabletEvent *event = dynamic_cast<QTabletEvent *>(e);
        return start(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e), j_to_pressure(event));
    }
    return false;
}


bool
Path::trigger_device_move(QEvent *e)
{
    if(orig_points.isEmpty()) return false;
    if(e->type() == QEvent::MouseMove)
    {
        QMouseEvent *event = dynamic_cast<QMouseEvent *>(e);
        return add_point(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e), j_to_pressure(event));
    }
    else if(e->type() == QEvent::TabletMove)
    {
        QTabletEvent *event = dynamic_cast<QTabletEvent *>(e);
        return add_point(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e), j_to_pressure(event));
    }
    return false;
}


bool
Path::trigger_device_released(QEvent *)
{
    if(path_pressure.isEmpty() || path_pressure.size() != orig_points.size())
        return false;
    return true;
}



bool
Path::trigger_device_pressed_anchor(QEvent *e)
{
    orig_points.clear();
    path_pressure.clear();
    if(e->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *event = dynamic_cast<QMouseEvent *>(e);
        orig_points.append(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e));
        path_pressure.append(j_to_pressure(event));
    }
    else if(e->type() == QEvent::TabletPress)
    {
        QTabletEvent *event = dynamic_cast<QTabletEvent *>(e);
        orig_points.append(ShadowCoreSystem::getMainCanvas()->get_eventPosition(e));
        path_pressure.append(j_max(float(0.01), j_to_pressure(event)));
    }
    else
        return false;
    return true;
}


bool
Path::trigger_device_move_anchor(QEvent *e)
{
    if(path_pressure.size() != 1)
        return false;
    if(e->type() == QEvent::TabletMove)
    {
        QTabletEvent *event = dynamic_cast<QTabletEvent *>(e);
        float pressure = j_to_pressure(event);
        if(path_pressure[0] >= pressure)
            return false;
        path_pressure[0] = pressure;
        return true;
    }
    return false;
}


bool
Path::trigger_device_released_anchor(QEvent *)
{
    if(path_pressure.size() != 1 || orig_points.size() != 1)
        return false;
    return true;
}


void
Path::path_smooth(int smooth, qreal minDis, int step)
{
    if(orig_points.size() < 3) return;
    assert(orig_points.size() == path_pressure.size());
    if(step < 0) step = 1;
    QList<QPointF> nPoints;
    QList<float> nPressures;
    for(int i = 0; i < orig_points.size(); i++)
    {
        double u = i*step+1;
        QPointF point = BSpline_interpolation(orig_points, u, smooth);
        float press = BSpline_interpolation(path_pressure, u, smooth);
        nPoints.append(point);
        nPressures.append(press);
    }
    nPoints.append(orig_points.last());
    nPressures.append(path_pressure.last());
    if(minDis < 0)
    {
        assert(nPoints.size() == nPressures.size());
        orig_points = nPoints;
        path_pressure = nPressures;
        return;
    }
    double lastDis = 0;
    QList<QPointF> filters;
    QList<float> filters2;
    filters.append(nPoints[0]);
    filters2.append(nPressures[0]);
    for(int i = 1; i < nPoints.size(); i++)
    {
        double dis = j_dis1(nPoints[i-1], nPoints[i]);
        lastDis += dis;
        if(lastDis < minDis)
            continue;
        while(lastDis >= minDis)
        {
            lastDis -= minDis;
            double scale = (dis - lastDis) / dis;
            QPointF p = j_interpolate(nPoints[i-1], nPoints[i], scale);
            float s = j_interpolate(nPressures[i-1], nPressures[i], scale);
            filters.append(p);
            filters2.append(s);
        }
    }
    assert(nPoints.size() == nPressures.size());
    orig_points = nPoints;
    path_pressure = nPressures;
}


void
Path::write(QTextStream *out)
{
    *out<<"path\n"<<orig_points.size()<<" "<<pressure_scale<<" "<<"\n";
    for(int i = 0; i < orig_points.size(); i++)
    {
        *out<<orig_points.at(i).x()<<" "<<orig_points.at(i).y()<<" "
            <<path_pressure.at(i)<<"\n";
    }
}


bool
Path::read(QTextStream *in)
{
    QString line = in->readLine();
    while(line.isEmpty())
        line = in->readLine();
    if(line.compare("path"))
        return false;
    QStringList list = in->readLine().split(" ");
    int pointnum = list.at(0).toInt();
    pressure_scale = list.at(1).toDouble();

    orig_points.clear();
    path_pressure.clear();

    for(int i = 0; i < pointnum; i++)
    {
        list = in->readLine().split(" ");
        if(list.size() < 2)
            return false;
        QPointF point(list.at(0).toDouble(), list.at(1).toDouble());
        orig_points.append(point);
        path_pressure.append(list.at(2).toDouble());
    }
    return true;
}



