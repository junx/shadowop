#include "thread.h"

void
ThreadProcess::on_start()
{
    on_process();
    emit on_finish();
    emit on_return(thread_id);
}


bool
ThreadProcess::start_process_in_thread(QThread::Priority priority)
{
    QThread *thread = new QThread;
    this->moveToThread(thread);
    connect(thread, SIGNAL(started()), this, SLOT(on_start()), Qt::DirectConnection);
   //connect(this, SIGNAL(on_finish()), this, SLOT(deleteLater()));
    connect(this, SIGNAL(on_finish()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    thread->start(priority);
    return true;
}

