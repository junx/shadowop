#ifndef OPPARAM_H
#define OPPARAM_H

#include "ShadowCoreSystem.h"

class OpParam
{
public:
    ushort    size;
    float     opacity;
    QColor    color;
    OpParam()
    {
        size = 5;
        color = QColor(Qt::black);
        opacity = 1;
    }
    OpParam(const OpParam &param)
    {
        size = param.size;
        opacity = param.opacity;
        color = param.color;
    }
};


class Path
{
    QList<QPointF>         orig_points;  //after filter, dis between two neightbor points > 2
    QList<float>           path_pressure;
    float                  pressure_scale;
public:
    Path(){pressure_scale = 1;}
    Path(QList<QPointF> plist, QList<float> presslist){assert(plist.size() == presslist.size()); orig_points = plist; path_pressure = presslist; pressure_scale = 1;}
    inline bool              is_empty(){return orig_points.isEmpty();}
    inline int               get_size(){return orig_points.size();}
    inline void              set_pressure(float v){pressure_scale = v;}
    inline float             get_pressure(){return pressure_scale;}
    inline QPointF           get_middle_point(){return orig_points.at(orig_points.size()/2); }
    inline QPointF           get_orig_point(int id){return orig_points.at(id);}
    inline QList<QPointF>    get_orig_points(){return orig_points;}
    inline void              set_orig_points(QList<QPointF> plist){assert(plist.size() == orig_points.size()); orig_points = plist;}
    inline QList<float>      get_path_pressure(){return path_pressure;}
    inline void              set_path_pressure(QList<float> plist){assert(plist.size() == path_pressure.size()); path_pressure = plist;}
    inline QVector2D         get_direction(){if(orig_points.size() < 2) return QVector2D(1,0); return j_to_direction(orig_points.first(), orig_points.last());}

    bool             trigger_device_pressed(QEvent *e);
    bool             trigger_device_move(QEvent *e);
    bool             trigger_device_released(QEvent *);

    bool             trigger_device_pressed_anchor(QEvent *e);
    bool             trigger_device_move_anchor(QEvent *e);
    bool             trigger_device_released_anchor(QEvent *);

    bool             start(QPointF point, float pressure);
    bool             add_point(QPointF point, float pressure);
    bool             end(QPointF point, float pressure);

    void             path_smooth(int smooth, qreal minDis, int step);
    void             write(QTextStream *out);  //record to file
    bool             read(QTextStream *in);   //read from file
//    void             write_points(QTextStream *out);
//    bool             read_points(QTextStream *in);
};


#endif // OPPARAM_H
