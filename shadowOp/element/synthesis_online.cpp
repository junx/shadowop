#include "synthesis.h"
#include "analysiser.h"

int
OnlineSynthesis::on_iterate()
{
    coreNeighborable *current = myCutElement;
    QRectF boundingBox(QPointF(0,0), ShadowCoreSystem::getMainCanvas()->size());
    boundingBox.adjust(10, 10, -10, -10); //make it smaller a bit
    while(optimization_iteration > 0)
    {
        QList<coreNeighborable*> similars = current->mySimilarityBuffer;

        QList<QList<coreNeighborable*> > predictions;
        QList<qreal> predictionValues;
        for(int i = 0; i < similars.size() && i < 3; i++)
        {
            QList<coreNeighborable*> elist; //prediction, current, match1, matchPrediction1, match2, matchPrediction2, ...
            elist.append(0);  //prediction
            elist.append(0);  //mprediction
            elist.append(current);  //current
            elist.append(similars[i]);  //match

            qreal flagsEI[1] = {1}; //scale the size
            element_initial(elist, 1, flagsEI);
            if(!elist[0] || !boundingBox.contains(elist[0]->get_GridBounding())) //outside
            {
                operation_delete(dynamic_cast<Brush*>(elist[0]));
                continue;
            }
            qreal value = element_evalulate(elist, 0, 0);
            if(value < 0.1)
            { //not valid
                operation_delete(dynamic_cast<Brush*>(elist[0]));
                continue;
            }

            predictions.append(elist);
            predictionValues.append(value);
        }

        QList<int> indexs = Core1<QList, qreal>::j_sort(predictionValues, predictionValues.size(), false); //get the max one
        if(indexs.isEmpty()) return myElementList.size();

        QList<coreNeighborable*> elist = predictions[indexs[0]];
        for(int i = 1; i < indexs.size(); i++)  //remove other predictions
            operation_delete(dynamic_cast<Brush*>(predictions[indexs[i]][0]));

        if(!element_optimize(elist, 0, 0)
                || !element_finalize(elist, 0, 0)
                || !boundingBox.contains(elist[0]->get_GridBounding()))
        {
            operation_delete(dynamic_cast<Brush*>(elist[0]));
            break;
        }

        myGrider->push_element(elist[0]);
        myElementList.append(elist[0]);

        current = elist[0];
        optimization_iteration--;
    }
    return myElementList.size();
}



bool
OnlineSynthesis::element_initial(QList<coreNeighborable *>& elist, int flagSize, qreal *flags)
{
    //to initialize an operation:
    //1: operation parameter, identity (push grider, undoer after confirm);
    //2: keypoints, contextpoints;
    //3:neighborhoods, similars
    assert(flagSize >= 1);
    if(elist.size() < 4) return false;
    CxNeighborable *current = dynamic_cast<CxNeighborable*>(elist[2]),
            *match = dynamic_cast<CxNeighborable*>(elist[3]);
    if(!current || !match) return false;

    CxNeighborable *mPrediction = dynamic_cast<CxNeighborable*>(ShadowCoreSystem::getOperationer()->getOperationAt(match->get_identity()+1, myElementList));
    if(!mPrediction) return false;
    elist[1] = mPrediction;
    //<<<<<<<<<<<<scale>>>>>>>>>>>>>//
    qreal scale = 1;
    if(flags[0] > 0) scale = (current->get_GridPointSize()+1.0) / (match->get_GridPointSize()+1);
    //<<<<<<<<<rotation>>>>>>>>>>>//
    float c_aveDir = current->get_keyDirectionAve();
    float m_aveDir = match->get_keyDirectionAve();
    QVector2D rotate = j_to_direction(j_to_angle(c_aveDir-m_aveDir));
    //<<<<<<<<<<translation>>>>>>>>//
    QList<QPointF> cp_keys = j_transformLine(mPrediction->get_keyPoints(), match->get_keyPoints(), current->get_keyPoints(), rotate, scale);

    //<<<<<<<<<<switch to Brush for creation>>>>>>>>>//
    CxNeighborable *prediction = dynamic_cast<Brush*>(mPrediction)->copy();

    CxNeighborable *lastOp = dynamic_cast<CxNeighborable*>(myCutElement);
    if(!myElementList.isEmpty()) lastOp = dynamic_cast<CxNeighborable*>(myElementList.last());
    prediction->set_identity(lastOp->get_identity()+1);
    prediction->set_griderId(lastOp->get_griderId());
    prediction->initial();
    prediction->OpNeighborable::update(cp_keys);
    elist[0] = prediction;

    return true;
}



qreal
OnlineSynthesis::element_evalulate(QList<coreNeighborable *> &elist, int, qreal *)
{
    if(elist.size() < 4) return -1;
    coreNeighborable *prediction = elist[0];
    if(!prediction) return -1;

    QList<coreNeighborable*> space = ShadowCoreSystem::getOperationer()->getOperationRange(prediction->get_identity()-1, prediction->get_identity()-SEARCH_SIZE_TEMPORAL, myElementList);
    QVector<QVector<QVector<short> > > allMatchList;
    QVector<QVector<qreal> > allCostList;

    //<<<<<<<<<<find temporal neighborhood>>>>>>>>//
    //prediction is initialized via mPrediction, temporal matching cost = 0, not useful
    int flagsN[6] = {0x0b, 0x0b, 0x0b, 0x0b, 0x00, 0x00}; //0000 0???, match-find-active
    ShadowCoreSystem::getAnalysiser()->search(prediction, myElementList, space, allMatchList, allCostList, 6, flagsN);
    if(space.size() < 2) return 0; //only has the mPrediction itself, invalid

    int flagsN3[6] = {0x00, 0x00, 0x00, 0x00, 0x1b, 0x00}; //0000 1011, reverseMatch-match-find-active
    ShadowCoreSystem::getAnalysiser()->search(prediction, myElementList, space, allMatchList, allCostList, 6, flagsN3);
    if(space.isEmpty()) return 0;

    prediction->mySimilarityBuffer = space;
    //<<<<<<<<<<<<<<evaluate the similarity>>>>>>>>>>>//
    qreal value = 0;
    for(int i = 0; i < allCostList.size(); i++) //more matching ops, larger value
        value += exp(-(allCostList[i][1]*0.5+allCostList[i][4])*0.1);  //more small spatial cost, larger value
    return value;
}


bool
OnlineSynthesis::element_optimize(QList<coreNeighborable *>& elist, int, qreal *)
{
    if(elist.size() < 4) return false;
    coreNeighborable *prediction = elist[0];
    if(!prediction) return false;

    QList<coreNeighborable*> searchSpace = prediction->mySimilarityBuffer;

    QVector<QVector<QVector<short> > > allMatchList;
    QVector<QVector<qreal> > allCostList;
    //<<<<<<<<<<struct & context neighborhood>>>>>>>//
    int flagsN[6] = {0x00, 0x00, 0x09, 0x00, 0x09, 0x03}; //0000 0???, match-find-active
    ShadowCoreSystem::getAnalysiser()->search(prediction, myElementList, searchSpace, allMatchList, allCostList, 6, flagsN);
    int flagsOp[4] = {0x07, 0x07, 0x07, 1}; //global, struct, context constraints, update
    ShadowCoreSystem::getAnalysiser()->optimization(prediction, searchSpace, allMatchList, 3, flagsOp);

    prediction->mySimilarityBuffer = searchSpace;

    return true;
}


bool
OnlineSynthesis::element_finalize(QList<coreNeighborable *>& elist, int, qreal *)
{
    if(elist.size() < 1) return false;
    coreNeighborable *op = elist[0];

    op->updateNeighbors();

    return true;
}



