#include "pixmap.h"


void
Pixmap::load_pixmap(QPixmap pixmap)
{
    if(pixmap.isNull()) return;
    if(pixmap.width() > myPixmapMaxSize.width() || pixmap.height() > myPixmapMaxSize.height())
    {
        qreal scale1 =  myPixmapMaxSize.width() / pixmap.width();
        qreal scale2 =  myPixmapMaxSize.height() / pixmap.height();
        qreal scale = scale1>scale2?scale1:scale2;
        *myPixmap = pixmap.scaled(pixmap.width()*scale, pixmap.height()*scale);
        set_pixmapRect(QRect(0, 0, pixmap.width()*scale, pixmap.height()*scale));
    }
    else
    {
        * myPixmap = pixmap;
        set_pixmapRect(QRect(0, 0, pixmap.width(), pixmap.height()));
    }
    set_pixmapVisible(true);
    set_pixmapActive(true);
}


void
Pixmap::drawPoint(QPointF point, QColor color, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setBrush(QBrush(color));
    painter.setPen(Qt::NoPen);
    painter.drawEllipse(point-QPointF(width/2, width/2), width, width);
    painter.end();
}


void
Pixmap::drawDirection(QPointF point, QVector2D d, QColor color, qreal len, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setBrush(QBrush(color));
    painter.setPen(Qt::NoPen);
    QPointF point2(point.x()+d.x()*len, point.y()+d.y()*len);
    painter.drawEllipse(point-QPointF(width/2, width/2), width, width);
    QPen pen;
    pen.setWidth(width/2);
    pen.setColor(color);
    painter.setPen(pen);
    painter.drawLine(point, point2);
    painter.end();
}



void
Pixmap::drawPoints(QList<QPointF> plist, QColor color, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setBrush(QBrush(color));
    painter.setPen(Qt::NoPen);
    for(int i = 0; i < plist.size(); i++)
        painter.drawEllipse(plist[i]-QPointF(width/2, width/2), width, width);
    painter.end();
}


void
Pixmap::drawLine(QPointF point1, QPointF point2, QColor color, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setPen(QPen(color, width));
    painter.drawLine(point1, point2);
    painter.end();
}


void
Pixmap::drawLines(QList<QPointF> plist, QColor color, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setPen(QPen(color, width));
    for(int i = 0; i < plist.size()-1; i++)
       painter.drawLine(plist[i], plist[i+1]);
    painter.end();
}


void
Pixmap::drawAnchorLines(QList<QPointF> plists, QColor pcolor, qreal pwidth,  QColor lcolor, qreal lwidth, bool is_close)
{
    if(plists.isEmpty())
        return;
    if(is_close)
        plists.append(plists.first());
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    painter.setPen(QPen(lcolor, lwidth));
    for(int i = 0; i < plists.size()-1; i++)
        painter.drawLine(plists.at(i), plists.at(i+1));
    painter.setBrush(QBrush(pcolor));
    painter.setPen(Qt::NoPen);
    for(int i = 0; i < plists.size()-1; i++)
        painter.drawEllipse(plists[i]-QPointF(pwidth/2, pwidth/2), pwidth, pwidth);
    if(!is_close)
        painter.drawEllipse(plists.last()-QPointF(pwidth/2, pwidth/2), pwidth, pwidth);
    painter.end();
}



void
Pixmap::drawCircle(QPointF center, qreal rad, bool is_edge, QColor ecolor, qreal ewidth, bool is_fill, QColor fcolor)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    if(is_edge)
        painter.setPen(QPen(ecolor, ewidth));
    else
        painter.setPen(Qt::NoPen);
    if(is_fill)
        painter.setBrush(QBrush(fcolor));
    else
        painter.setBrush(Qt::NoBrush);
    painter.drawEllipse(center, rad, rad);
    painter.end();
}


void
Pixmap::drawRect(QRectF rect, bool is_edge, QColor ecolor, qreal ewidth, bool is_fill, QColor fcolor)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    if(is_edge)
        painter.setPen(QPen(ecolor, ewidth));
    else
        painter.setPen(Qt::NoPen);
    if(is_fill)
        painter.setBrush(QBrush(fcolor));
    else
        painter.setBrush(Qt::NoBrush);
    painter.drawRect(rect);
    painter.end();
}



void
Pixmap::drawPolygon(QPolygonF poly, bool is_edge, QColor ecolor, qreal ewidth, bool is_fill, QColor fcolor)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    if(is_edge)
        painter.setPen(QPen(ecolor, ewidth));
    else
        painter.setPen(Qt::NoPen);
    if(is_fill)
        painter.setBrush(QBrush(fcolor));
    else
        painter.setBrush(Qt::NoBrush);
    painter.drawPolygon(poly);
    painter.end();
}



void
Pixmap::drawPath(QPainterPath path, bool is_edge, QColor ecolor, qreal ewidth, bool is_fill, QColor fcolor)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    if(is_edge)
        painter.setPen(QPen(ecolor, ewidth));
    else
        painter.setPen(Qt::NoPen);
    if(is_fill)
        painter.setBrush(QBrush(fcolor));
    else
        painter.setBrush(Qt::NoBrush);
    painter.drawPath(path);
    painter.end();
}



void
Pixmap::drawText(QString string, QRectF rect, QColor color, qreal width)
{
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    QPen pen(color);
    pen.setWidth(width);
    painter.setPen(pen);
    painter.fillRect(rect, Qt::transparent);
    painter.drawText(rect, string);
    painter.end();
}



void
Pixmap::drawNetMap(QPointF topleft, QPointF butright, qreal ratio, int background_adjustFlag, qreal width, QColor color)
{
    qreal x = butright.x() - topleft.x();
    qreal y = butright.y() - topleft.y();
    QPointF topright(butright.x(), topleft.y());
    QPointF butleft(topleft.x(), butright.y());
    QPainter painter(myPixmap);
    painter.setRenderHints(QPainter::Antialiasing);
    QPen pen(color);
    pen.setWidth(width);
    pen.setStyle(Qt::DotLine);
    painter.setPen(pen);
    painter.drawLine(topleft, topright);
    painter.drawLine(topleft+QPointF(0,y*ratio), topright+QPointF(0, y*ratio));
    painter.drawLine(butleft-QPointF(0,y*ratio), butright-QPointF(0, y*ratio));
    painter.drawLine(butleft, butright);
    painter.drawLine(topleft, butleft);
    painter.drawLine(topleft+QPointF(x*ratio,0), butleft+QPointF(x*ratio,0));
    painter.drawLine(topright-QPointF(x*ratio,0), butright-QPointF(x*ratio,0));
    painter.drawLine(topright, butright);

    painter.setPen(QPen(QColor(Qt::red),width));
    switch (background_adjustFlag) {
    case 0:
        painter.drawLine(topleft, topleft+QPointF(x*ratio, 0));
        painter.drawLine(topleft, topleft+QPointF(0, y*ratio));
        break;
    case 1:
        painter.drawLine(topleft, topright);
        break;
    case 2:
        painter.drawLine(topright, topright-QPointF(x*ratio, 0));
        painter.drawLine(topright, topright+QPointF(0, y*ratio));
        break;
    case 3:
        painter.drawLine(topleft, butleft);
        break;
    case 4:
        painter.drawLine(topleft, topright);
        painter.drawLine(topright, butright);
        painter.drawLine(butright, butleft);
        painter.drawLine(butleft, topleft);
        break;
    case 5:
        painter.drawLine(topright, butright);
        break;
    case 6:
        painter.drawLine(butleft, butleft+QPointF(x*ratio, 0));
        painter.drawLine(butleft, butleft-QPointF(0, y*ratio));
        break;
    case 7:
        painter.drawLine(butleft, butright);
        break;
    case 8:
        painter.drawLine(butright, butright-QPointF(x*ratio, 0));
        painter.drawLine(butright, butright-QPointF(0, y*ratio));
        break;
    default:
        break;
    }
    painter.end();
}
