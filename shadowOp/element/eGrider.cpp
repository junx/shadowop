#include "eGrider.h"
#include "coreWidget_canvas.h"

QList<coreNeighborable*>
EGrider::get_selections(QPointF pos, qreal radius, bool lockE)
{
    return get_elements(Vec2d(pos.x(), pos.y()), radius, lockE);
}


void
EGrider::drawGrider()
{
    QList<QPointF> plist;
    for(int i = 0; i < grid_unit_number[0]; i++)
    {
        for(int j = 0; j < grid_unit_number[1]; j++)
        {
          //  plist.append(j_to_point(grider->get_pos(Vec2i(i,j)))); continue;
            if(grid_units_element[i][j].elementList.isEmpty())
                continue;
            for(int k = 0; k < grid_units_element[i][j].elementList.size(); k++)
                plist.append(grid_units_element[i][j].elementList[k]->get_GridPoints());
        }
    }
    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawPoints(plist, QColor(Qt::green), 2);
}
