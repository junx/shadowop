#ifndef EGRIDER_H
#define EGRIDER_H

#include "tool/coreGrider.h"


class EGrider : public coreEGrider<coreEGridUnit>
{
    int                       mySelectionFlag;
    int                       mySelectionRadius;
public:
    QList<coreNeighborable*>  mySelectElements;
    QList<coreNeighborable*>  mySelectCandidates;

    EGrider(Vec2d topleft, Vec2d botright, Vec2i resolution):coreEGrider<coreEGridUnit>(topleft, botright, resolution){}

    virtual QList<coreNeighborable*>  get_selections(QPointF pos, qreal radius, bool lockE);

    void      drawGrider();
};


#endif // EGRIDER_H
