#include "synthesis.h"
#include "analysiser.h"

int
ParallelSynthesis::on_iterate()
{
    if(myCutElement->getNeighborhoods(4).isEmpty()) return 0;

    coreNeighborable *contextN = myCutElement->getNeighborhood(4, 0)->get_neighbor();

    QList<coreNeighborable*> space = operatable_to_coreNeighborable(ShadowCoreSystem::getOperationer()->getElementList());
    QVector<QVector<QVector<short> > > allMatchList;
    QVector<QVector<qreal> > allCostList;
    int flagsS1[6] = {0x00, 0x00, 0x05, 0x00, 0x00, 0x00}; //XXXX 0111, match-find-active
    ShadowCoreSystem::getAnalysiser()->search(contextN, QList<coreNeighborable*>(), space, allMatchList, allCostList, 6, flagsS1);
    int flagsS2[6] = {0x00, 0x09, 0x00, 0x00, 0x00, 0x00}; //XXXX 0111, match-find-active
    ShadowCoreSystem::getAnalysiser()->search(contextN, QList<coreNeighborable*>(), space, allMatchList, allCostList, 6, flagsS2);

    for(int i = 0; i < space.size(); i++)
    {
        QList<coreNeighborable*> elist;
        elist.append(0);   //prediction
        elist.append(myCutElement);  //mPrediction
        QVector<short> matchList = allMatchList[i][2];
        for(int j = 0; j < matchList.size(); j++)
        {
            if(matchList[j] < 0) continue;
            elist.append(space[i]->getNeighborhood(2, matchList[j])->get_neighbor()); //current
            elist.append(contextN->getNeighborhood(2, j)->get_neighbor()); //match
        }
        if(elist.size() < 4) continue;
        qreal flagsEI[1] = {1}; //scale the size
        if(!element_initial(elist, 1, flagsEI) ||
                !element_optimize(elist, 0, 0) ||
                !element_finalize(elist, 0, 0) || !elist[0])
        {
            operation_delete(dynamic_cast<Brush*>(elist[0]));
            continue;
        }
        myElementList.append(elist[0]);
        myGrider->push_element(elist[0]);
    }
    return myElementList.size();
}


bool
ParallelSynthesis::element_initial(QList<coreNeighborable *> &elist, int flagSize, qreal *flags)
{
    assert(flagSize >= 1 && elist.size() >= 4);

    CxNeighborable *mPrediction = dynamic_cast<CxNeighborable*>(elist[1]);
    QList<CxNeighborable*> currents, matches;
    for(int i = 2; i < elist.size(); i+=2)
    {
        currents.append(dynamic_cast<CxNeighborable*>(elist[i]));
        matches.append(dynamic_cast<CxNeighborable*>(elist[i+1]));
    }
    if(!mPrediction || matches.isEmpty()) return false;

    QList<QList<QPointF> > matchCtrls, currentCtrls;
    QList<qreal> scales;
    QList<QVector2D> rotates;
    for(int i = 0; i < currents.size(); i++)
    {
        matchCtrls.append(matches[i]->get_keyPoints());
        currentCtrls.append(currents[i]->get_keyPoints());
        qreal scale = (flags[0]<=0)?1:(currents[i]->get_GridPointSize()+1.0)/(matches[i]->get_GridPointSize()+1);
        scales.append(scale);
        QVector2D rotate = j_to_direction(j_to_angle(currents[i]->get_keyDirectionAve()-matches[i]->get_keyDirectionAve()));
        rotates.append(rotate);
    }
    QList<QPointF> cp_keys = j_transformLines(mPrediction->get_keyPoints(), matchCtrls, currentCtrls, rotates, scales);

    //<<<<<<<<<<switch to Brush for creation>>>>>>>>>//
    CxNeighborable *prediction = dynamic_cast<Brush*>(mPrediction)->copy();

    CxNeighborable *lastOp = dynamic_cast<CxNeighborable*>(myCutElement);
    if(!myElementList.isEmpty()) lastOp = dynamic_cast<CxNeighborable*>(myElementList.last());
    prediction->set_identity(lastOp->get_identity()+1);
    prediction->set_griderId(lastOp->get_griderId());
    prediction->initial();
    prediction->OpNeighborable::update(cp_keys);
    elist[0] = prediction;
    return true;
}


bool
ParallelSynthesis::element_optimize(QList<coreNeighborable *> &, int, qreal *)
{

    return true;
}


bool
ParallelSynthesis::element_finalize(QList<coreNeighborable *> &, int, qreal *)
{
    return true;
}
