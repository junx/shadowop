#ifndef SYNTHESIS_H
#define SYNTHESIS_H

#include "operation.h"
#include "thread.h"
#include "eGrider.h"

#define ONLINE_SYNTHESIS 1
#define PARALLEL_SYNTHESIS 2
#define BATCH_SYNTHESIS_PATH 3
#define BATCH_SYNTHESIS_REGION 4


class coreSynthesis : public ThreadProcess
{
    Q_OBJECT
public slots:
    void  on_receive_delete(int id){if(id != identity) return; is_abort = true;}

signals:
    void  onFinshSynthesis(int, qreal*);
protected:
    int                identity;
    short              myFlag;
    bool               is_abort;
    ushort             optimization_iteration;

    coreNeighborable*             myCutElement;
    QList<coreNeighborable*>      myElementList;
    EGrider           *myGrider;

    bool               isRendered;
    QImage             myRendPixmap;
    QRect              myRendRect;

public:
    coreSynthesis(coreNeighborable *cutE);
    virtual void      on_process();
    virtual void      on_initial(){}
    virtual int       on_iterate(){return 0;}
    virtual void      on_render();

    int               get_identity(){return identity;}
    short             get_flag(){return myFlag;}
    EGrider          *get_grider(){return myGrider;}
    QList<coreNeighborable*>     get_elements(){return myElementList;}
    bool              is_rend(){return isRendered;}
    QImage            get_rendPixmap(){return myRendPixmap;}
    QRect             get_rendPixmapRect(){return myRendRect;}
    void              remove_elements(QList<coreNeighborable*> elist);

    virtual bool      element_initial(QList<coreNeighborable*>&, int, qreal *){return false;}
    virtual bool      element_optimize(QList<coreNeighborable*>&, int, qreal *){return false;}
    virtual qreal     element_evalulate(QList<coreNeighborable*>&, int, qreal *){return MAXVALUE;}
    virtual bool      element_finalize(QList<coreNeighborable*>&, int, qreal *){return false;}

    virtual void      freeit();
};


class OnlineSynthesis : public coreSynthesis
{
    Q_OBJECT
public:
    OnlineSynthesis(coreNeighborable *cutE)
        :coreSynthesis(cutE){
        myFlag = ONLINE_SYNTHESIS;
        optimization_iteration = 40;
    }

protected:

    int               on_iterate();

    bool              element_initial(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
    bool              element_optimize(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
    qreal             element_evalulate(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
    bool              element_finalize(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
};


class ParallelSynthesis : public coreSynthesis
{
    Q_OBJECT
public:
    ParallelSynthesis(coreNeighborable *cutE)
        :coreSynthesis(cutE){
        myFlag = PARALLEL_SYNTHESIS;
    }

protected:
    int               on_iterate();

    bool              element_initial(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
    bool              element_optimize(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
    bool              element_finalize(QList<coreNeighborable*>& elist, int flagSize, qreal *flags);
};


class BatchPathSynthesis : public coreSynthesis
{
    Q_OBJECT
public:
    BatchPathSynthesis(coreNeighborable *cutE)
        :coreSynthesis(cutE){
        myFlag = BATCH_SYNTHESIS_PATH;
    }
};

#endif // SYNTHESIS_H
