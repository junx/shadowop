#include "operation.h"
#include "operationer.h"


SketchCluster::SketchCluster()
    :Sketchable()
{
    TransformTransable::set_flag(TRANSABLE_GROUP);

    set_type(ADD_SKETCHABLE | ADD_CLUSTERABLE);
}



SketchCluster::SketchCluster(QList<Sketchable *> oplist)
    :Sketchable()
{
    TransformTransable::set_flag(TRANSABLE_GROUP);

    set_type(ADD_SKETCHABLE | ADD_CLUSTERABLE);

    if(oplist.isEmpty()) return;
    for(int i = 0; i < oplist.size(); i++)
        operationlist.append(oplist[i]->copy());
}



SketchCluster::SketchCluster(const SketchCluster &cluop)
    :Sketchable(cluop)
{
    operationlist.clear();
    for(int i = 0; i < cluop.operationlist.size(); i++)
        operationlist.append(cluop.operationlist[i]->copy());
}


SketchCluster::~SketchCluster()
{
    for(int i = 0; i < operationlist.size(); i++)
        delete operationlist.at(i);
    operationlist.clear();
}


void
SketchCluster::replay(QPainter &painter, QPointF move, float scale)
{
    for(int i = 0; i < operationlist.size(); i++)
        operationlist[i]->replay(painter, move, scale);
}



void
SketchCluster::write(QTextStream *out)
{
    *out<<"clusterStart "<<operationlist.size()<<"\n";
    for(int i = 0; i < operationlist.size(); i++)
        if(operationlist[i])
           operation_write(operationlist[i], out);
    *out<<"clusterEnd\n";
}



bool
SketchCluster::read(QTextStream *in)
{
    QString line = in->readLine();
    QStringList list = line.split(" ");
    if(list[0].compare("clusterStart"))
        return false;
    int size = list[1].toInt();
    operationlist.clear();
    for(int i = 0; i < size; i++)
    {
        Sketchable *op = dynamic_cast<Sketchable*>(operation_read(in));
        if(op) operationlist.append(op);
    }
    while(line.isEmpty())
        line = in->readLine();
    if(line.compare("clusterEnd"))
        return false;
    return true;
}

