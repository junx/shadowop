#ifndef PIXMAP_H
#define PIXMAP_H

#include "PixmapHandle.h"

class Pixmap
{   
    QPixmap      *myPixmap;
    bool          myPixmapActivate;
    bool          myPixmapVisible;
    QRect         myPixmapRect;
    QSize         myPixmapMaxSize;

    PixmapHandle *myHandle;
public:
    Pixmap(int width, int height, bool isAct, bool isVis, QSize maxSize){
        myPixmap = new QPixmap(width, height);
        myPixmap->fill(Qt::transparent);
        myPixmapActivate = isAct;
        myPixmapVisible = isVis;
        myPixmapRect = QRect(0, 0, width, height);
        myPixmapMaxSize = maxSize;
        myHandle = new PixmapHandle(this);
    }

    PixmapHandle*get_handle(){return myHandle;}
    void         clear(){myPixmap->fill(Qt::transparent);}

    void         load_pixmap(QPixmap map);
    QPixmap*     get_pixmap(){return myPixmap;}
    void         set_pixmap(QPixmap map){*myPixmap = map;}
    bool         get_pixmapActive(){return myPixmapActivate;}
    void         set_pixmapActive(bool active){myPixmapActivate = active;}
    bool         get_pixmapVisible(){return myPixmapVisible;}
    void         set_pixmapVisible(bool active){myPixmapVisible = active;}
    QRect        get_pixmapRect(){return myPixmapRect;}
    void         set_pixmapRect(QRect rc){myPixmapRect = rc;}
    QSize        get_pixmapMaxSize(){return myPixmapMaxSize;}
    void         set_pixmapMaxSize(QSize size){myPixmapMaxSize = size;}

    void         drawPoint(QPointF point, QColor color, qreal width = 3);
    void         drawPoints(QList<QPointF> plist, QColor color, qreal width = 3);
    void         drawDirection(QPointF point, QVector2D direction, QColor color, qreal len = 20, qreal width = 3);
    void         drawLines(QList<QPointF> plist, QColor color, qreal width = 2);
    void         drawLine(QPointF point1, QPointF point2, QColor color, qreal width = 2);
    void         drawAnchorLines(QList<QPointF> plists, QColor pcolor, qreal pwidth,  QColor lcolor, qreal lwidth, bool is_close);
    void         drawRect(QRectF rect, bool is_edge, QColor ecolor, qreal ewidth = 2, bool is_fill = false, QColor fcolor = QColor(Qt::white));
    void         drawPolygon(QPolygonF poly, bool is_edge, QColor ecolor, qreal ewidth = 2, bool is_fill = false, QColor fcolor = QColor(Qt::white));
    void         drawCircle(QPointF center, qreal rad, bool is_edge, QColor ecolor, qreal ewidth = 2, bool is_fill = false, QColor fcolor = QColor(Qt::white));
    void         drawPath(QPainterPath path, bool is_edge, QColor color, qreal ewidth = 2, bool is_fill = false, QColor fcolor = QColor(Qt::white));
    void         drawText(QString, QRectF, QColor color, qreal width = 2);
    void         drawNetMap(QPointF topleft, QPointF butright, qreal ratio, int background_adjustFlag, qreal width = 2, QColor color = QColor(0, 200, 200));
};


#endif // PIXMAP_H
