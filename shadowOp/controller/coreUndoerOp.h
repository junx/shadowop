#ifndef COREUNDOEROP_H
#define COREUNDOEROP_H

#include <QtCore>

#include "operation.h"

//use this class to do the undo, redo


class Operationer;
class coreUndoerOp : public coreUndoer
{
    Q_OBJECT
public:
    coreUndoerOp(QPixmap *mainP, QPixmap *subP);

    virtual void  reset();
    QList<QPixmap>     getStatePixmapList(){return myStatePixmap;}
    void               setStatePixmapList(QList<QPixmap> sl){myStatePixmap = sl;}
    QPixmap*           getStoredPixmap(){return myStoredPixmap;}
    void               setStoredPixmap(QPixmap *pixmap){myStoredPixmap = pixmap;}
    QPixmap*           getFlushPixmap(){return myFlushPixmap;}
    void               setFlushPixmap(QPixmap *pixmap){myFlushPixmap = pixmap;}
    //<<<<<<<<<<implemented method>>>>>>>>>//
    virtual void record_state();
    virtual void clear_state();
    virtual void runto(uint);

protected:
    QPixmap       *myStoredPixmap;
    QPixmap       *myFlushPixmap;
    QList<QPixmap> myStatePixmap;
};


class coreUndoerOpData
{
    coreUndoerData    myCoreUndoerData;
    //<<<<<<<coreUndoerOp data>>>>>>>>>>//
    QList<QPixmap>    myStatePixmap;
    QPixmap          *myStoredPixmap;
    QPixmap          *myFlushPixmap;
public:
    coreUndoerOpData(){}
    void   importData(coreUndoerOp *undoer)
    {
        myCoreUndoerData.importData(undoer);
        myStatePixmap = undoer->getStatePixmapList();
        myStoredPixmap = undoer->getStoredPixmap();
        myFlushPixmap = undoer->getFlushPixmap();
    }
    void   exportData(coreUndoerOp *undoer)
    {
        myCoreUndoerData.exportData(undoer);
        undoer->setStatePixmapList(myStatePixmap);
        undoer->setStoredPixmap(myStoredPixmap);
        undoer->setFlushPixmap(myFlushPixmap);
    }
};


#endif // COREUNDOEROP_H
