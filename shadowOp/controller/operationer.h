#ifndef OPERATIONER_H
#define OPERATIONER_H

#include "coreUndoerOp.h"
#include "eGrider.h"
#include "coreWidget_canvas.h"

class Operationer: public QObject, public coreRunable, public coreHistoryer<Operatable*>
{
     Q_OBJECT
public:
    Operationer(coreWidgetCanvas *canvas);

    void            reset();
    coreUndoerOp*   get_opUndoer(int id){if(id < 0 || id >= myOpUndoers.size()) return 0; return myOpUndoers[id];}
    void            set_opUndoers(QList<coreUndoerOp*> undoers){myOpUndoers = undoers;}
    QList<coreUndoerOp*> get_opUndoers(){return myOpUndoers;}

    EGrider*        get_opGrider(int id){if(id < 0 || id >= myOpGriders.size()) return 0; return myOpGriders[id];}
    void            set_opGriders(QList<EGrider*> griders){myOpGriders = griders;}
    QList<EGrider*> get_opGriders(){return myOpGriders;}

    coreWidgetCanvas*  get_canvas(){return myCanvas;}
    void               set_canvas(coreWidgetCanvas* canvas){myCanvas = canvas;}

    //<<<<<<<<<for quick and simple process>>>>>>>>//
    template<class T> T*  getOperationAt(int id, QList<T*> subspace);
    template<class T> QList<T*> getOperationRange(int from, int to, QList<T*> subspace); //include both ends *from* and *to*

    int                  getOperationLayer(int opType);
    Operatable *         getOperationAt(int id);
    QList<Operatable*>   getOperationRange(int from, int to);

    bool writeOperations(QTextStream *out);
    bool readOperations(QTextStream *in);
    bool saveOperations(const QString &fileName);
    bool loadOperations(const QString &file);
    void replayOperations();

    bool on_start(QEvent *e);
    bool on_update(QEvent *e);
    bool on_finish(QEvent *e);

    void pushElementToHistory(Operatable *op);
    void pushElementsToHistory(QList<Operatable *> elements);

signals:
    void        onStepCount(int);
    void        onFinishOperation(int);
    void        onUndoOperation(int);
    void        onDeleteOperation(int);

public slots:
    void        onRedo(Operatable *op = 0);
    void        onUndo(Operatable *op = 0);
    void        branch_clear();

private:
    coreWidgetCanvas      *myCanvas;
    QList<coreUndoerOp*>   myOpUndoers;
    QList<EGrider*>        myOpGriders;

    Operatable             *myOperation;  //current operation
    QList<Operatable *>     myLoadOperationlist;
};



template<class T> T*
Operationer::getOperationAt(int id, QList<T*> subspace)
{
    for(int i = 0; i < subspace.size(); i++)
        if(subspace[i]->get_identity() == id)
             return subspace[i];
    if(id < 0 || id >= myElementList.size())
        return NULL;
    assert(myElementList[id]->get_identity() == id);
    return dynamic_cast<T*>(myElementList[id]);
}


template<class T> QList<T*>
Operationer::getOperationRange(int from, int to, QList<T*> subspace)
{
    QList<T*> oplist;
    if(from < to)
    {
        for(int i = from; i <= to; i++)
        {
            T* op = getOperationAt(i, subspace);
            if(op) oplist.append(op);
        }
    }
    else
    {
        for(int i = from; i >= to; i--)
        {
            T* op = getOperationAt(i, subspace);
            if(op) oplist.append(op);
        }
    }
    return oplist;
}





class OperationerData
{
    coreHistoryData<Operatable*>    myHistoryData;
    //<<<<<<<operationer data>>>>>>>>>>//
    coreWidgetCanvasData            myCanvasData;
    QList<coreUndoerOp*>            myUndoers;
    QList<EGrider*>                 myGriders;
public:
    OperationerData(){
        int w = ShadowCoreSystem::getMainCanvas()->width(), h = ShadowCoreSystem::getMainCanvas()->height();
        myUndoers.append(new coreUndoerOp(myCanvasData.get_pixmap(CANVASPIXMAP_MAIN)->get_pixmap(),myCanvasData.get_pixmap(CANVASPIXMAP_MAINFLUID)->get_pixmap()));
        myUndoers.append(new coreUndoerOp(myCanvasData.get_pixmap(CANVASPIXMAP_DRAFT)->get_pixmap(),myCanvasData.get_pixmap(CANVASPIXMAP_DRAFTFLUID)->get_pixmap()));
        myGriders.append(new EGrider(Vec2d(0,0), Vec2d(w, h), Vec2i(80, 60)));
        myGriders.append(new EGrider(Vec2d(0,0), Vec2d(w, h), Vec2i(80, 60)));
    }
    ~OperationerData(){
        for(int i = 0; i < myUndoers.size(); i++)
            delete myUndoers[i];
        myUndoers.clear();
        for(int i = 0; i < myGriders.size(); i++)
            delete myGriders[i];
        myGriders.clear();
    }

    coreWidgetCanvasData*  getCoreWidgetCanvasData(){return &myCanvasData;}
    coreHistoryData<Operatable*>* getOperationData(){return &myHistoryData;}
    QList<EGrider*>*        getEGriderData(){return &myGriders;}
    QList<coreUndoerOp*>*   getUndoerData(){return &myUndoers;}

    void    importData(Operationer *oper){
        myHistoryData.importData(oper);
        myCanvasData.importData(oper->get_canvas());
        myUndoers = oper->get_opUndoers();
        myGriders = oper->get_opGriders();
    }
    void    exportData(Operationer *oper){
        myHistoryData.exportData(oper);
        myCanvasData.exportData(oper->get_canvas());
        oper->set_opUndoers(myUndoers);
        oper->set_opGriders(myGriders);
    }
};



#endif // OPERATIONER_H
