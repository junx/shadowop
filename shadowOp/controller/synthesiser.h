#ifndef SYNTHESISER_H
#define SYNTHESISER_H

#include "synthesis.h"
#include "operationer.h"


class Synthesiser : public QObject, public coreRunable, public coreHistoryer<coreSynthesis*>
{
    Q_OBJECT

    Operationer     *myOperationer;

    int              cachSize;
    int              myFocus;
    QImage           mySelectionBrushImage;

signals:
    void     on_delete_synthesis(int id);

public slots:
    void     onAutoComplete();
    void     onSelectionBrush();

    void     onUpdate(int);
    void     onRemove(int);

public:
    Synthesiser(Operationer *opner);

    void     fresh();
    void     pushElementToHistory(coreSynthesis* e);

    bool     on_start(QEvent *);
    bool     on_update(QEvent *);
    bool     on_finish(QEvent *){return true;}
};

#endif // SYNTHESISER_H
