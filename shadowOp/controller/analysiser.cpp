#include "analysiser.h"
#include "synthesis.h"


Analysiser::Analysiser(Operationer *opner)
{
    myOperationer = opner;
}


void
Analysiser::on_finish_operation(int id)
{
    Operatable *op = myOperationer->getOperationAt(id);
    coreNeighborable* nb = dynamic_cast<coreNeighborable*>(op);
    assert(nb);
    op->set_busy(true);
    online_analysis(nb);
    op->set_busy(false);
}


void
Analysiser::on_finish_synthesis(int flagSize, qreal *flags)
{
    return;
    assert(flagSize >= 3);
    int synFlag = int(flags[0]), opId = int(flags[1]), synSize = int(flags[2]);
    if(synFlag != ONLINE_SYNTHESIS) return;
    if(synSize >= 3) return;  //no enough synthesis

    Operatable *op = myOperationer->getOperationAt(opId);
    op->set_busy(true);
   // propagation_analysis(dynamic_cast<coreNeighborable*>(op));
    op->set_busy(false);
}



void
Analysiser::online_analysis(coreNeighborable *target)
{
    if(!target) return;
    //<<<<<<<this operation is system generated and is valid>>>>>>>>>>>//
    //<<<<<<<<main to remove the conflict caused by partial selection,>//
    //<<which might make neighborhoods of selected op NOT selected>>>>>//    
    if(target->get_exhaust() && target->checkValid())  return;

    target->initial();
    //<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>//
    //<<<<<<find neighborhoods, and matching operations>>>>>>>//
    //<<<<<<<<<<<<<<<<intra keyframe analysis>>>>>>>>>>>>>>>>>//
    QList<coreNeighborable*> space = myOperationer->getOperationRange(target->get_identity()-1, target->get_identity()-SEARCH_SIZE_TEMPORAL, QList<coreNeighborable*>());
    QVector<QVector<QVector<short> > > allMatchList;
    QVector<QVector<qreal> > allCostList;
    int flagsN[6] = {0x0b, 0x0b, 0x0b, 0x0b, 0x0b, 0x03}; //XXXX 0111, match-find-active
    search(target, QList<coreNeighborable*>(), space, allMatchList, allCostList, 6, flagsN);
    target->mySimilarityBuffer = space;

    if(target->get_exhaust())  return; //has been synthesized before
    if(target->mySimilarityBuffer.isEmpty()) return;  //no matching found

    OnlineSynthesis *osynthesis = new OnlineSynthesis(target);
    osynthesis->start_process_in_thread();
    connect(osynthesis, SIGNAL(onFinshSynthesis(int,qreal*)), this, SLOT(on_finish_synthesis(int,qreal*)), Qt::DirectConnection);
}



void
Analysiser::onPropagation()
{
    Operatable *op = myOperationer->getCurrentElement();
    coreNeighborable *target = dynamic_cast<coreNeighborable*>(op);
    if(target->getNeighborhoodSize(4) == 0) return;  //no propagation context
    //<<<<<<<<the matchId is stored>>>>>>>//
    ParallelSynthesis *psynthesis = new ParallelSynthesis(target);
    psynthesis->start_process_in_thread();
    connect(psynthesis, SIGNAL(onFinshSynthesis(int,qreal*)), this, SLOT(on_finish_synthesis(int,qreal*)), Qt::DirectConnection);
}


void
Analysiser::propagation_analysis(coreNeighborable *)
{
}


void
Analysiser::clone_analysis(QList<coreNeighborable*>)
{
}



//find neighborhood
int findNeighborhoodParameters[6][5]=
{
    {0, 0, 0, 0, 0},   //self
    {1, 0, 0, 0, 0},   //temp
    {0, 0, 1, 0, 0},   //struct
    {0, 1, 0, 0, 0},   //spatial
    {0, 0, 0, 1, 0},   //context
    {0, 0, 0, 0, 1},   //propagation
};

void
Analysiser::find_neighborhood_core(coreNeighborable *input, QList<coreNeighborable*> neighborSpace, QList<coreNeighborable*> &searchSpace, int flagSize, int *flags)
{
    CxNeighborable *target = dynamic_cast<CxNeighborable*>(input);
    if(!target) return;
    assert(flagSize >= 5);
    if(flags[0] & 0x01)
    {//<<<<<<<temporal neighborhood>>>>>>>>>//
        QList<coreNeighborable*>  temporalN;
        int targetId = target->get_identity();
        for(int i = 1; i <= NEIGHBORHOOD_SIZE_TEMPORAL; i++)
        {
            coreNeighborable *tn = myOperationer->getOperationAt(targetId-i, neighborSpace);
            if(tn) temporalN.append(tn);
        }
        target->find_temporal_neighborhood_core(temporalN, 0, 0);
    }
    if(flags[1] & 0x01)
    {//<<<<<<<<get close operations in spatial space range>>>>>>>>>>//
        QList<coreNeighborable*> spatialN;

        if(!target->getNeighborhood(2, 0) || target->getNeighborhood(2, 0)->get_neighborMatchIds().isEmpty())
        {
            qreal spatilFlags[2] = {NEIGHBORHOOD_RANGE_SPATIAL, NEIGHBORHOOD_SIZE_SPATIAL};
            spatialN = target->find_neighborhood_core(searchSpace, 0, 2, spatilFlags);
        }
        else
        {
            //<<<<<<<calculate proper spatial neighborhood search range>>>>>>>>//
            qreal spatilFlags[2] = {NEIGHBORHOOD_RANGE_SPATIAL, NEIGHBORHOOD_SIZE_SPATIAL};
            QList<qreal> disList;
            target->find_neighborhood_core(searchSpace, &disList, 2, spatilFlags);
            qreal sRange = disList.isEmpty()?50:(disList.last()+1);

            //<<<<<<<<<get all operations within the range>>>>>>>>>//
            int gridId = target->get_griderId();
            qreal flagsFNC[2] = {sRange, 3};  //range, step
            spatialN = target->find_neighborhood_core(myOperationer->get_opGrider(gridId), 2, flagsFNC);

            //<<<<<<<<<<<merge with existing ones>>>>>>>>>//
            spatialN = j_mergeClusterNoRepeat(searchSpace, spatialN);

            //<<<<<<<<<<quickly search those matching ops via matchId>>>>>>>>//
            QVector<QVector<QVector<short> > > allMatchList;
            QVector<QVector<qreal> > allCostList;
            int flagsSM1[6] = {0x00, 0x00, 0x05, 0x00, 0x00, 0x00};  //select via matchpair, it will not proun
            search(target, QList<coreNeighborable*>(), spatialN, allMatchList, allCostList, 6, flagsSM1);
            int flagsSM2[6] = {0x00, 0x09, 0x00, 0x00, 0x00, 0x00};  //select via matching
            search(target, QList<coreNeighborable*>(), spatialN, allMatchList, allCostList, 6, flagsSM2);

            //<<<<<<<<<<<find the nearest ones>>>>>>>>>>>//
            qreal flagsFN[2] = {NEIGHBORHOOD_RANGE_SPATIAL, NEIGHBORHOOD_SIZE_SPATIAL};
            spatialN = target->find_neighborhood_core(spatialN, 0, 2, flagsFN);
        }
        target->find_spatial_neighborhood_core(spatialN, 0, 0);
    }
    if(flags[2] & 0x01)
    {//<<<<<<<<get close operations in temporal range>>>>>>>>>>//
        QList<coreNeighborable*>  structN;
        int targetId = target->get_identity();
        for(int i = 1; i <= NEIGHBORHOOD_SIZE_STRUCT; i++)
        {
            coreNeighborable *tn = myOperationer->getOperationAt(targetId-i, neighborSpace);
            if(tn) structN.append(tn);
        }
        target->find_tpstruct_neighborhood_core(structN, 0, 0);
    }
    if(flags[3] & 0x01)
    {//<<<<<<<<get long context operations in spatial space range>>>>>>>>//
        int gridId = target->get_griderId();
        qreal flagsFNC[2] = {NEIGHBORHOOD_RANGE_CONTEXT, 3};  //range, step
        QList<coreNeighborable*> spatialN = target->find_neighborhood_core(myOperationer->get_opGrider(gridId), 2, flagsFNC);
        qreal flagsFCN[2] = {NEIGHBORHOOD_RANGE_CONTEXT, NEIGHBORHOOD_SIZE_CONTEXT};
        target->find_context_neighborhood_core(spatialN, 2, flagsFCN);
    }
    if(flags[4] & 0x01)
    {//<<<<<<<<<get close operations in spatial range>>>>>>>>//
        int gridId = target->get_griderId();
        qreal flagsFNC[2] = {NEIGHBORHOOD_RANGE_PROPAGATION, 3};  //range, step
        QList<coreNeighborable*> spatialN = target->find_neighborhood_core(myOperationer->get_opGrider(gridId), 2, flagsFNC);
        qreal flagsFPC[2] = {NEIGHBORHOOD_RANGE_PROPAGATION, NEIGHBORHOOD_SIZE_PROPAGATION};
        target->find_spstruct_neighborhood_core(spatialN, 2, flagsFPC);
    }
}




//search matching ops by calculating neighborhood similarity
qreal matchNeighborhoodParameters[6][7] =
{//neighborId, neighborMatchBias, matchSelectionMax, matchSelectionScale, matchSelectionBase, sensitive, matchRecordSensitive
    {-1,  0,       20, 2, 10, 0, 0},
    { 0, 0.5,      50, 2, 10, 0, 3},
    { 2, 1.5,      50, 2, 10, 2, 3},
    { 1, MAXVALUE, 50, 2, 10, 2, 3},
    { 3, MAXVALUE, 50, 2, 10, 2, 3},
    { 4, MAXVALUE, 50, 2, 10, 2, 3},
};

//<<<<<<<<<search in space those similar enough and repeatitive enough>>>>>>>>>//
void
Analysiser::match_neighborhood_core(coreNeighborable *target, QList<coreNeighborable *> &space, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal*wList)
{
    //sensitive, neighborId, neighborMatchBias, matchSelectionMax, matchSelectionScale, matchSelectionBase
    assert(wSize >= 7 && allMatchList.size() == allCostList.size());

    int neighborId = wList[0];
    qreal neighborMatchBias = wList[1];
    qreal matchSelectionMax = wList[2], matchSelectionScale = wList[3], matchSelectionBase = wList[4];  //adaptive matching
    int sensitive = wList[5];
    int matchRecordSensitive = wList[6];

    int neighborSize = (neighborId<0)?1:target->getNeighborhoodSize(neighborId);

    bool isMatchInfo = (allMatchList.size()==space.size())?true:false;
    isMatchInfo = (neighborId >= 0)?isMatchInfo:false;
    if(isMatchInfo)
        for(int i = 0; i < space.size(); i++) //reset the matchingList
            allMatchList[i][neighborId] = QVector<short>(neighborSize, -1);

    for(int k = 0; k < neighborSize; k++)
    {
        if(target->getNeighborhood(neighborId, k) && !target->getNeighborhood(neighborId, k)->get_active()) continue;  //for temp_struct neighborhood, if it is too far, don't use it to proun

        qreal minDis = matchSelectionMax;
        QVector<qreal> disList(space.size(), MAXVALUE);
        for(int i = 0; i < space.size(); i++)
        {
            qreal flagsCSNC[4] = {neighborId*1.0, k*1.0, neighborMatchBias, matchSelectionMax}; //neighborId, activeNeighborId, neighborMatchBias
            if(neighborId < 0)
                 disList[i] = target->compute_similarity_self_core(space[i], 0, 0);
            else if(isMatchInfo)
                 disList[i] = target->compute_similarity_neighbor_core(space[i], allMatchList[i][neighborId], 4, flagsCSNC);
            else
            {
                QVector<short> assignment(neighborSize, -1);
                disList[i] =  target->compute_similarity_neighbor_core(space[i], assignment, 4, flagsCSNC);
            }
            if(disList[i] < minDis) minDis = disList[i];

            if(isMatchInfo) allCostList[i][neighborId+1] += disList[i];
        }

        qreal threashold = j_min(matchSelectionMax, matchSelectionScale*minDis+matchSelectionBase);
        //<<<<<<<<<<count the number of matchings>>>>>>>>>>>//
        int similarSize = 0;
        for(int i = 0; i < disList.size(); i++)
            if(disList[i] <= threashold)
                similarSize++;

        //invalid matching process k, don't use it to select matchings>>>>//
        if(similarSize < sensitive) continue;
        //<<<<<<<<valid matching process k, use it to select matchings>>>>//
        //<<<<<<<<<<<<only select those similar enough>>>>>>>>>>//
        for(int i = disList.size()-1; i >= 0; i--)
        {
            //valid element i in process k, proun this matching id
            if(disList[i] <= threashold) continue;
            //<<<<<invalid element i in process k>>>>//
            space.removeAt(i);

            if(!isMatchInfo) continue;
            allMatchList.remove(i);
            allCostList.remove(i);
        }

        if(!isMatchInfo || similarSize < matchRecordSensitive) continue;
        //<<<<<<<<<<<assign matching info. for quick searh>>>>>>>>//
        QList<short> matchIds = target->getNeighborhood(neighborId, k)->get_neighborMatchIds();
        int minId = (target->get_identity() << 4) | k;
        for(int i = 0; i < space.size(); i++)
        {
            int nid = allMatchList[i][neighborId][k]; //the corresponding neighbor
            QList<short> omatchIds = space[i]->getNeighborhood(neighborId, nid)->get_neighborMatchIds();
            matchIds = j_mergeClusterNoRepeat(matchIds, omatchIds);

            nid |= (space[i]->get_identity() << 4);
            if(nid < minId) minId = nid;
        }
        if(matchIds.isEmpty()) //no matchId assigned before,
        {
            matchIds.append(minId);  //assign it with the eariest id
        }
        for(int i= 0; i < space.size(); i++) //push the matchId to all
        {
            int nid = allMatchList[i][neighborId][k]; //the corresponding neighbor
            space[i]->getNeighborhood(neighborId, nid)->set_neighborMatchIds(matchIds);
        }
        target->getNeighborhood(neighborId, k)->set_neighborMatchIds(matchIds); //and push matchId to itself
    }
}





qreal reverseMatchNeighborhoodParameters[6][5] =
{//neighborId, neighborMatchBias, matchSelectionMax, matchSelectionScale, matchSelectionBase
    {-1},
    {-1},
    { 2, 1.5,      50, 2, 10},
    { 1, MAXVALUE, 50, 2, 10},
    { 3, MAXVALUE, 50, 2, 10},
    { 4, MAXVALUE, 50, 2, 10},
};

//<<<<<for measuring the quality of prediction, A is known op and has serveal key neighborhood (aligned) that should be well matched in me>>>>>//
//<<<<<for example, previous ops are all context constrainted by B, then the prediction should also found similar context op>>>>>>//
void
Analysiser::reverse_match_neighborhood_core(coreNeighborable *target, QList<coreNeighborable *> &space, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal*wList)
{
    //neighborId, neighborMatchBias, matchSelectionMax, matchSelectionScale, matchSelectionBase
    assert(wSize >= 5 && allMatchList.size() == allCostList.size());

    int neighborId = wList[0];
    if(neighborId < 0) return;
    qreal neighborMatchBias = wList[1];
    qreal matchSelectionMax = wList[2], matchSelectionScale = wList[3], matchSelectionBase = wList[4];  //adaptive matching

    int neighborSize = target->getNeighborhoodSize(neighborId);
    bool isMatchInfo = (allMatchList.size()==space.size())?true:false;

    qreal minDis = matchSelectionMax;
    QVector<qreal> disList(space.size(), 0);
    for(int i = 0; i < space.size(); i++)
    {
        if(isMatchInfo)
            allMatchList[i][neighborId] = QVector<short>(neighborSize, -1);

        int oNeighborSize = space[i]->getNeighborhoodSize(neighborId);
        QVector<short> assignment(oNeighborSize, -1);
        for(int k = 0; k < oNeighborSize; k++)
        {
            if(!space[i]->getNeighborhood(neighborId, k)->get_align()) continue;
            qreal flagsCSNC[4] = {neighborId*1.0, k*1.0, neighborMatchBias, matchSelectionMax}; //neighborId, activeNeighborId, neighborMatchBias
            disList[i] += space[i]->compute_similarity_neighbor_core(target, assignment, 4, flagsCSNC);

            if(isMatchInfo && assignment[k] >= 0)
                allMatchList[i][neighborId][assignment[k]] = k;
        }
        if(disList[i] < minDis)
            minDis = disList[i];

        if(isMatchInfo)
            allCostList[i][neighborId+1] = disList[i];
    }

    qreal threashold = j_min(matchSelectionMax, matchSelectionScale*minDis+matchSelectionBase);

    for(int i = disList.size()-1; i >= 0; i--)
    {   //<<<<<<<<<<<<only select those similar enough>>>>>>>>>>//
        if(disList[i] <= threashold) continue;
        space.removeAt(i);

        if(!isMatchInfo) continue;

        allCostList.remove(i);
        allMatchList.remove(i);
    }
}




//quick search matching ops via matchId
qreal searchMatchNeighborhoodParameters[6][2]=
{//neighborId,neighborMatchBias
    {-1,  0},      //self
    { 0,  0.5},     //temp
    { 2,  1.5},      //struct
    { 1,  MAXVALUE}, //spatial
    { 3,  MAXVALUE}, //context
    { 4,  MAXVALUE}, //propagation
};


//<<<<<for quickly search matching operations, use the *neighborhoodMatchIds* to found global matchings>>>//
void
Analysiser::search_match_neighborhood_core(coreNeighborable *target, QList<coreNeighborable *> &space, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal *wList)
{
    assert(wSize >= 2 && allMatchList.size() == allCostList.size());

    int neighborId = wList[0];
    if(neighborId < 0) return;
    qreal neighborMatchBias = wList[1];
    int neighborSize = target->getNeighborhoodSize(neighborId);

    bool isMatchInfo = (allMatchList.size()==space.size())?true:false;
    if(isMatchInfo)
        for(int i = 0; i < space.size(); i++) //reset the matchingList
            allMatchList[i][neighborId] = QVector<short>(neighborSize, -1);

    for(int k = 0; k < neighborSize; k++)
    {
        if(target->getNeighborhood(neighborId, k) && !target->getNeighborhood(neighborId, k)->get_active()) continue;  //for temp_struct neighborhood, if it is too far, don't use it to proun

        QList<short> matchIds = target->getNeighborhood(neighborId, k)->get_neighborMatchIds();
        if(matchIds.isEmpty()) continue;
        //<<<<<<<<this is key neighborhood, must be contained by others>>>>>>>//
        for(int i = space.size()-1; i >= 0; i--)
        {
            bool isFound = false;
            for(int j = 0; j < space[i]->getNeighborhoodSize(neighborId) && !isFound; j++)
            {
                if(abs(j-k) > neighborMatchBias) continue;

                QList<short> omatchIds = space[i]->getNeighborhood(neighborId, j)->get_neighborMatchIds();
                if(!j_mergeSequenceOnlyCommon(omatchIds, matchIds).isEmpty()) //has common
                {
                    isFound = true;
                    if(isMatchInfo) allMatchList[i][neighborId][k] = j;
                }
            }
            if(isFound) continue;
            space.removeAt(i);

            if(!isMatchInfo) continue;

            allMatchList.remove(i);
            allCostList.remove(i);
        }
    }
}



void
Analysiser::search(coreNeighborable *target, QList<coreNeighborable*> neighborSpace, QList<coreNeighborable*> &searchSpace, QVector<QVector<QVector<short> > >& allMatchList, QVector<QVector<qreal> > &allCostList, int flagSize, int *flags)
{
    assert(flagSize >= 6);
    for(int i = 0; i < flagSize; i++)
    {
        if(!(flags[i] & 0x01)) continue;

        if(i >= 1 && allMatchList.isEmpty())
        {
            allMatchList = QVector<QVector<QVector<short> > >(searchSpace.size(), QVector<QVector<short> >(5));
            allCostList = QVector<QVector<qreal> >(searchSpace.size(), QVector<qreal>(6));
        }
        if(flags[i] & 0x02)
        {
            find_neighborhood_core(target, neighborSpace, searchSpace, 5, findNeighborhoodParameters[i]);
        }
        if((flags[i] & 0x04) && !searchSpace.isEmpty())
        {
            search_match_neighborhood_core(target, searchSpace, allMatchList, allCostList, 2, searchMatchNeighborhoodParameters[i]);
        }
        if((flags[i] & 0x08) && !searchSpace.isEmpty())
        {
            match_neighborhood_core(target, searchSpace, allMatchList, allCostList, 7, matchNeighborhoodParameters[i]);
        }
        if((flags[i] & 0x10) && !searchSpace.isEmpty())
        {
            reverse_match_neighborhood_core(target, searchSpace, allMatchList, allCostList, 5, reverseMatchNeighborhoodParameters[i]);
        }
    }
}



bool
Analysiser::analysize(coreNeighborable *target, QList<coreNeighborable*> elements, QVector<QVector<short> > &contextMatchList, QVector<QVector<QVector<Vec2f> > > &analysisList, int wSize, qreal *wList)
{
    assert(elements.size() == contextMatchList.size());
    if(elements.size() < 2)
        return false;

    assert(wSize >= 5); //neighborId, index dev, pos val, pos dev, dir dev
    int neighborId = wList[0];

    int contextSize = analysisList.size();
    int contextPointSize = ShadowCoreSystem::getContextSampleSize();
    int contextParamSize = 4;
    //<<<<<<<<<<<statistic analysis of context neighborhoods>>>>>>>>>>>>>>//
    //<<<<<<<<<<<<<local analysis>>>>>>>>>>>//
    for(int k = 0; k < contextSize; k++)
    {
        QVector<short>  mlist;
        int validSize = 0;
        for(int i = 0; i < contextMatchList.size(); i++)
        {
            if(k >= contextMatchList[i].size()) return false;  //not valid
            mlist.append(contextMatchList[i][k]);
            if(contextMatchList[i][k] >= 0)
                validSize++;
        }
        if(validSize < 2) continue;

        if(neighborId >= 0)
        {
            CxNeighbor *neighbor = static_cast<CxNeighbor*>(target->getNeighborhood(neighborId, k));
            neighbor->set_align(true);
        }

        //<<<<<<for each context point, analysize its relative position/direction, global direction>>>>>>//
        QVector<QVector<QList<float> > > valueList = QVector<QVector<QList<float> > >(contextPointSize, QVector<QList<float> >(contextParamSize, QList<float>()));
        QList<int> idDiffList;
        for(int i = 0; i < elements.size(); i++)
        {
            if(mlist[i] < 0) continue;
            CxNeighborable *op = dynamic_cast<CxNeighborable*>(elements[i]);

            int idDis = abs(target->get_identity()-op->get_identity());
            idDiffList.append(idDis);

            for(int j = 0; j < contextPointSize; j++)
            {
                if(neighborId < 0)
                {//global analysis
                    int index = op->myContextPointIndexs[j];
                 //   valueList[j][0].append(MAXVALUE);  //disable global index analysis
                    valueList[j][1].append(op->get_keyPoint(index).x()); //the user might draw horizontally or vertically
                    valueList[j][2].append(op->get_keyPoint(index).y());
                    valueList[j][3].append(op->get_keyDirection(index));
                }
                else
                {//local analysis
                    int minIndex = mlist[i];
                    CxNeighbor *neighbor = static_cast<CxNeighbor*>(op->getNeighborhood(neighborId, minIndex));
                    if(neighbor->myCutPointIndexs[j] < 0) continue;

                    valueList[j][0].append(neighbor->myCutPointIndexs[j]*1.0/(neighbor->get_neighbor()->get_GridPointSize()-1));
                    valueList[j][1].append(neighbor->relative_shapes[j][0]);
                    valueList[j][2].append(neighbor->relative_shapes[j][1]);
                    valueList[j][3].append(neighbor->relative_angles[j]);
                }
            }
        }

        //<<<<<<<<<<<<favor the recent matching ops>>>>>>>>>//
        QList<int> orders = Core1<QList, int>::j_order(idDiffList, idDiffList.size(), true);
        QList<qreal> weightList;
        for(int i = 0; i < orders.size(); i++)
            weightList.append(j_weight_order(orders[i]));

        //<<<<<<<for each measurement above, we analysize its average, deviation, and size>>>>>//
        for(int i = 0; i < contextPointSize; i++)
        {
            if(valueList[i][0].size() >= 2)
            { //index
                analysisList[k][i][0][0] = Core1<QList, float>::j_wt_ave(valueList[i][0], weightList);
                analysisList[k][i][0][1] = sqrt(Core1<QList, float>::j_dev(valueList[i][0]));
            }
            if(valueList[i][1].size() >= 2)
            { // position
                analysisList[k][i][1][0] = Core1<QList, float>::j_wt_ave(valueList[i][1], weightList);
                analysisList[k][i][1][1] = sqrt(Core1<QList, float>::j_dev(valueList[i][1]));
            }
            if(valueList[i][2].size() >= 2)
            { // position
                analysisList[k][i][2][0] = Core1<QList, float>::j_wt_ave(valueList[i][2], weightList);
                analysisList[k][i][2][1] = sqrt(Core1<QList, float>::j_dev(valueList[i][2]));
            }
            if(valueList[i][3].size() >= 2)
            { //direction
                float base = valueList[i][3][0];
                for(int t = 0; t < valueList[i][3].size(); t++)
                    valueList[i][3][t] = j_to_angle(valueList[i][3][t] - base);
                float dirAve = Core1<QList, float>::j_wt_ave(valueList[i][3], weightList);
                analysisList[k][i][3][0] = j_to_angle(dirAve+base);
                analysisList[k][i][3][1] = sqrt(Core1<QList, float>::j_dev(valueList[i][3]));
            }
        }
    }

    //{0.2, 20, 10, 0.2} {0.2, 5, 3, 0.2}; //index dev, pos val, pos dev, dir dev
    qreal flags[4] = {wList[1], wList[2], wList[3], wList[4]};
    for(int i = 0; i < contextSize; i++)
    {
        for(int j = 0; j < contextPointSize; j++)
        {
            if(analysisList[i][j][0][0] < MAXVALUE-1)
            {//<<<<<<index>>>>>>//
                qreal wDev = j_weight_context(analysisList[i][j][0][1] / flags[0]);
                analysisList[i][j][0][1] = wDev;
            }
            if(analysisList[i][j][1][0] < MAXVALUE-1)
            {//<<<<<<<position>>>>>>>>//
                qreal wDis = j_weight_context(analysisList[i][j][1][0] / flags[1]);
                qreal wDev = j_weight_context(analysisList[i][j][1][1] / flags[2]);
                analysisList[i][j][1][1] = wDis*wDev;
            }
            if(analysisList[i][j][2][0] < MAXVALUE-1)
            {//<<<<<<<position>>>>>>>>//
                qreal wDis = j_weight_context(analysisList[i][j][2][0] / flags[1]);
                qreal wDev = j_weight_context(analysisList[i][j][2][1] / flags[2]);
                analysisList[i][j][2][1] = wDis*wDev;
            }
            if(analysisList[i][j][3][0] < MAXVALUE-1)
            {//<<<<<<<<direction>>>>>>>>//
                qreal wDev = j_weight_context(analysisList[i][j][3][1] / flags[3]);
                analysisList[i][j][3][1] = wDev;
            }
        }
    }
    return true;
}



void
Analysiser::optimization(coreNeighborable *input, QList<coreNeighborable*> &searchSpace, QVector<QVector<QVector<short> > > &allMatchList, int wSize, int *wList)
{
    CxNeighborable *target = dynamic_cast<CxNeighborable*>(input);
    if(!target || searchSpace.size() < 2) return;

    assert(wSize >= 3);
    int globalFlagsOp = wList[0]; //0x07
    int structFlagsOp = wList[1]; //0x07
    int contextFlagsOp = wList[2]; //0x07
    int isUpdate = wList[3];

    //<<<<<<<<<<context analysis>>>>>>>>>>>//
    /*the context optimization considers two parameters: position and direction.
      Position is a relative and local paramter. For example, we can restrick the
      distance to a context stroke so that it can be only parallelly slided along the context
      stroke. While Direction is similar and can be processed as the derivation of position*/
    //<<<<<<contextPointSize, contextParamSize, 1+contextSize>>>>>>//
    int cxPointSize = ShadowCoreSystem::getContextSampleSize();
    int cxParamSize = 4;

    int kSize = target->get_keyPoints().size();
    Matrixd optimizationMatrix(2*kSize+1, 2*kSize+1, 0); //const, x1, y1, x2, y2, ...
    qreal selfFlagsCS[2] = {100, 1};
    constraint_self(target, optimizationMatrix, 2, selfFlagsCS); //as rigid as possible
    if(globalFlagsOp & 0x001)
    {//<<<<<<<<<<<<<<global constraint>>>>>>>>>>//
        QVector<QVector<short> > selfMatchList(allMatchList.size());
        for(int i = 0; i < allMatchList.size(); i++)
            selfMatchList[i] = QVector<short>(1, 1);
        QVector<QVector<QVector<Vec2f> > > globalParams(1, QVector<QVector<Vec2f> >(cxPointSize, QVector<Vec2f>(cxParamSize, Vec2f(MAXVALUE,0))));

        //<<<<<<<irrelated to the specific global position value>>>>>>>>>//
        qreal globalFlagsAC[5] = {-1, 0.2, MAXVALUE, 5, 0.1};//neighborId, index dev, pos val, pos dev, dir dev
        bool isV = analysize(target, searchSpace, selfMatchList, globalParams, 5, globalFlagsAC);

        qreal globalFlagsCC[5] = {-1, 0.1, 0.1, 1, 10}; // {-1, 0.5, 0.5, 1, 10}; //neighborId, weightHBase, weightVBase, weightLBase, weightDBase
        if(isV) constraint_context(target, globalParams, optimizationMatrix, 5, globalFlagsCC);
    }
    if(structFlagsOp & 0x001)
    {//<<<<<<<<<<local struct constraint>>>>>>>>//
        QVector<QVector<short> > structMatchList(allMatchList.size());
        for(int i = 0; i < allMatchList.size(); i++)
            structMatchList[i] = allMatchList[i][2];
        QVector<QVector<QVector<Vec2f> > > structParams(target->myTemporalStructNeighbors.size(), QVector<QVector<Vec2f> >(cxPointSize, QVector<Vec2f>(cxParamSize, Vec2f(MAXVALUE,0))));

        qreal structFlagsAC[5] = {2, 0.2, 10, 5, 0.1};//neighborId, index dev, pos val, pos dev, dir dev
        bool isV = analysize(target, searchSpace, structMatchList, structParams, 5, structFlagsAC);

        qreal structFlagsCC[5] = {2, 50, 50, 1, 1}; //{2, 10, 10, 1, 1}; //neighborId, weightHBase, weightVBase, weightLBase, weightDBase
        if(isV) constraint_context(target, structParams, optimizationMatrix, 5, structFlagsCC);
    }
    if(contextFlagsOp & 0x001)
    {//<<<<<<<local context constraint>>>>>>>//
        QVector<QVector<short> > contextMatchList(allMatchList.size());
        for(int i = 0; i < allMatchList.size(); i++)
            contextMatchList[i] = allMatchList[i][3];
        QVector<QVector<QVector<Vec2f> > > contextParams(target->myContextNeighbors.size(), QVector<QVector<Vec2f> >(cxPointSize, QVector<Vec2f>(cxParamSize, Vec2f(MAXVALUE,0))));

        qreal contextFlagsAC[5] = {3, 0.2, 20, 10, 0.1};//neighborId, index dev, pos val, pos dev, dir dev
        bool isV = analysize(target, searchSpace, contextMatchList, contextParams, 5, contextFlagsAC);

        qreal contextFlagsCC[5] = {3, 0.1, 30, 1, 10};     //{3, 5, 10, 0.1, 10}; //neighborId, weightHBase, weightVBase, weightLBase, weightDBase
        if(isV) constraint_context(target, contextParams, optimizationMatrix, 5, contextFlagsCC);
    }
    if(!isUpdate) return;

    QList<QPointF> keys = target->get_keyPoints();
    optimization_HLBFGS(keys, optimizationMatrix);
    target->OpNeighborable::update(keys);
}



void
Analysiser::constraint_self(coreNeighborable *input, Matrixd &Value, int wSize, qreal *wList)
{
    CxNeighborable *target = dynamic_cast<CxNeighborable*>(input);

    assert(wSize >= 2);
    qreal weightShape = wList[0];
    qreal weightSize = wList[1];

    int kSize = target->get_keyPoints().size();
    assert(Value.M_height == Value.M_width && Value.M_height == 2*kSize+1);
    if(wList[0] > 0) //shape constraint
    {
        for(int i = 0; i < kSize-2; i++)
        {
            QVector2D dir21 = j_to_direction(target->get_keyPoint(i+1), target->get_keyPoint(i));
            QVector2D dir32 = j_to_direction(target->get_keyPoint(i+2), target->get_keyPoint(i+1));
            QVector2D v = j_rotated(dir32, dir21);
            qreal len21 = j_dis1(target->get_keyPoint(i+1), target->get_keyPoint(i));
            qreal len32 = j_dis1(target->get_keyPoint(i+2), target->get_keyPoint(i+1));
            qreal s = (len21+1) / (len32+1);
            int   indexs1[6] = {2*i+1, 2*i+2, 2*i+3, 2*i+4, 2*i+5, 2*i+6}; //index: x1,y1,x2,y2,x3,y3
            qreal params1[6] = {1, 0, -1-s*v.x(), s*v.y(), s*v.x(), -s*v.y()};
            qreal params2[6] = {0, -1, s*v.y(), 1+s*v.x(), -s*v.y(), -s*v.x()};
            for(int p = 0; p < 6; p++)
            {
                for(int q = 0; q < 6; q++)
                {
                    qreal dis1 = params1[p] * params1[q];
                    qreal dis2 = params2[p] * params2[q];
                    Value[indexs1[p]][indexs1[q]] += weightShape*(dis1+dis2);
                }
            }
        }
    }
    if(wList[1] > 0)  //1 //size constraint
    {
        for(int i = 0; i < kSize-1; i++)
        {
            QPointF d = target->get_keyPoint(i+1) - target->get_keyPoint(i);
            int   indexs[2][3] = {{0, 2*i+3, 2*i+1}, {0, 2*i+4, 2*i+2}};
            qreal params[2][3] = {{-d.x(), 1, -1}, {-d.y(), 1, -1}};
            for(int p = 0; p < 3; p++)
            {
                for(int q = 0; q < 3; q++)
                {
                    for(int k = 0; k < 2; k++)
                    {
                        qreal dis = params[k][p] * params[k][q]; //length
                        Value[indexs[k][p]][indexs[k][q]] += dis*weightSize;
                    }
                }
            }
        }
    }
}



void
Analysiser::constraint_context(coreNeighborable *input, QVector<QVector<QVector<Vec2f> > > &analysisList, Matrixd &Value, int wSize, qreal *wList)
{
    CxNeighborable *target = dynamic_cast<CxNeighborable*>(input);

    assert(wSize >= 5);
    int neighborId = wList[0];
    qreal weightHBase = wList[1];
    qreal weightVBase = wList[2];
    qreal weightLBase = wList[3];
    qreal weightDBase = wList[4];

    int cxSize = analysisList.size();
    int cxPointSize = ShadowCoreSystem::getContextSampleSize();
    for(int i = 0; i < cxSize; i++)
    {
        CxNeighbor *cxN = 0;
#ifdef OPTIMIZATIONPARAMTEST
        QList<float> testParams;
#endif
        if(neighborId >= 0)
        {//<<<<<<<<<this neighborhood is not stably aligned>>>>>>//
            cxN = static_cast<CxNeighbor*>(target->getNeighborhood(neighborId, i));
            if(!cxN->get_align()) continue;
        }
        for(int j = 0; j < cxPointSize; j++)
        {
            int id = target->myContextPointIndexs[j];

            QVector2D r(1, 0); //local direction
            QPointF   p = target->get_keyPoint(id);  //desired position
            QVector2D v = j_to_direction(target->get_keyDirection(id)); //desired direction

            QVector<Vec2f> params = analysisList[i][j];
            //<<<<<<<<<<<<position control>>>>>>>>>>>//
            qreal weightHContext = params[1][1]*weightHBase;  //pos.x
            weightHContext = weightHContext < 0.1?0:weightHContext;
            qreal weightVContext = params[2][1]*weightVBase;  //pos.y
            weightVContext = weightVContext < 0.1?0:weightVContext;

            if(neighborId == 2)
            {//struct neighborhood
                weightHContext = j_min(weightHContext, weightVContext);
                weightVContext = weightHContext;
            }

            qreal weightDContext = params[3][1]*weightDBase;
            weightDContext = weightDContext < 0.1?0:weightDContext;
            qreal weightLContext = params[3][1]*weightLBase;
            weightLContext = weightLContext < 0.1?0:weightLContext;


            if(neighborId < 0)
            {//global
                p = QPointF(params[1][0], params[2][0]);  //global position
                v = j_to_direction(params[3][0]);  //global dir
            }
            else
            {//struct neighborhood
                QPointF diff(params[1][0], params[2][0]);  //local relative position
                int cid = 0;
                if(neighborId == 2) //struct neighborhood
                    cid = params[0][0] * (cxN->get_neighbor()->get_GridPointSize()-1); //averaged cut index
                else //context neighborhood
                    cid = cxN->myCutPointIndexs[j];
                if(cid < 0 || cid >= cxN->get_neighbor()->get_GridPointSize()) continue;  //-1: invalid

                r = j_to_direction(cxN->get_neighbor()->get_GridPoints(), cid, 5); //cutDir

                diff = j_rotate(diff, r); //local position
                p = cxN->get_neighbor()->get_GridPoint(cid) + diff;  //global position

                QVector2D diffV = j_to_direction(params[3][0]); //local direction
                v = j_rotate(r, diffV); //global direction
            }

#ifdef OPTIMIZATIONPARAMTEST
            if(neighborId < 0)
                testParams.append(target->get_keyPoint(id).x());
            else
                testParams.append(p.x());
            testParams.append(weightHContext);
            if(neighborId < 0)
                testParams.append(target->get_keyPoint(id).y());
            else
                testParams.append(p.y());
            testParams.append(weightVContext);
            testParams.append(v.x());
            testParams.append(v.y());
            testParams.append(weightDContext);
#endif

            if(weightHContext > 0.1 || weightVContext > 0.1)
            {
                int   indexs1[3] = {0, 2*id+1, 2*id+2};
                qreal params1[3] = {-p.x()*r.x()-p.y()*r.y(), r.x(), r.y()};
                qreal params2[3] = {p.x()*r.y()-p.y()*r.x(), -r.y(), r.x()};
                for(int p = 0; p < 3; p++)
                {
                    for(int q = 0; q < 3; q++)
                    {
                        qreal dis1 = params1[p] * params1[q] * weightHContext;
                        qreal dis2 = params2[p] * params2[q] * weightVContext;
                        Value[indexs1[p]][indexs1[q]] += (dis1+dis2);
                    }
                }
            }

            if(weightDContext > 0.1 || weightLContext > 0.1)
            {
                int id1 = (id-1)>=0?(id-1):0;
                int id2 = (id+1)<target->get_keyPoints().size()?(id+1):(target->get_keyPoints().size()-1);
                qreal l = j_dis1(target->get_keyPoint(id1), target->get_keyPoint(id2));
                int indexs2[5] =   {0, 2*id1+1, 2*id1+2, 2*id2+1, 2*id2+2};
                qreal params3[5] = {-l, -v.x(), -v.y(),  v.x(), v.y()};
                qreal params4[5] = { 0,  v.y(), -v.x(), -v.y(), v.x()};
                for(int p = 0; p < 5; p++)
                {
                    for(int q = 0; q < 5; q++)
                    {
                        qreal dis1 = params3[p] * params3[q] * weightLContext; //length
                        qreal dis2 = params4[p] * params4[q] * weightDContext; //direction
                        Value[indexs2[p]][indexs2[q]] += (dis1+dis2);
                    }
                }
            }
        }
#ifdef OPTIMIZATIONPARAMTEST
        if(cxN)
            cxN->optimizationParams = testParams;
        else
            target->optimizationParams = testParams;
#endif
    }
}
