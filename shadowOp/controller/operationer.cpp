#include "operationer.h"
#include "analysiser.h"
#include "canvasHandle.h"

Operationer::Operationer(coreWidgetCanvas *canvas)
    :coreRunable(), coreHistoryer<Operatable*>()
{
    myCanvas = canvas;
    myOperation = NULL;

    OperationerData *myData = new OperationerData();
    myData->exportData(this);
}


void
Operationer::reset()
{
    coreHistoryer<Operatable*>::reset();
    coreRunable::reset();
    myElementList.clear();
    myLoadOperationlist.clear();
    myOperation = NULL;
    isPipeLineLocked = false;
    for(int i = 0; i < myOpUndoers.size(); i++)
        myOpUndoers[i]->reset();
    for(int i = 0; i < myOpGriders.size(); i++)
        myOpGriders[i]->reset();
}


int
Operationer::getOperationLayer(int type)
{
    switch (type) {
    case OPERATION_BRUSH:
    case OPERATION_STIPPLE:
        return 0;
    case OPERATION_PENCIL:
        return 1;
    default:
        break;
    }
    return -1;
}


Operatable*
Operationer::getOperationAt(int id)
{
    if(id < 0 || id >= myElementList.size())
        return NULL;
    assert(myElementList[id]->get_identity() == id);
    return myElementList[id];
}



QList<Operatable*>
Operationer::getOperationRange(int from, int to)
{
    QList<Operatable*> oplist;
    if(from < to)
    {
        from = from>=0?from:0;
        to = to < (myElementList.size()-1)?to:(myElementList.size()-1);
        for(int i = from; i <= to; i++)
           oplist.append(myElementList[i]);
    }
    else
    {
        to = to>=0?to:0;
        from = from < (myElementList.size()-1)?from:(myElementList.size()-1);
        for(int i = from; i >= to; i--)
           oplist.append(myElementList[i]);
    }
    return oplist;
}



void
Operationer::onUndo(Operatable*)
{
    if(myCurElementCount <= 0) return;
    isUnRedo = true;
    //<<<<<<<from up to buttom level>>>>//
    myOperation = myElementList.at(myCurElementCount-1);
    if(myOperation->get_busy()) return;  //is busy now, can't undo
    //<<<<<<<current level undo>>>>>>>//
    myCurElementCount--;
    OpUndo undo;
    operation_write(&undo, ShadowCoreSystem::getOpWriteStream());
    //<<<<<<<<<<<<<<<undo management>>>>>>>>>>>>>//
    coreUndoerOp *undoer = get_opUndoer(getOperationLayer(myOperation->get_type()));
    if(undoer) undoer->onUndo();

    EGrider *grider = get_opGrider(getOperationLayer(myOperation->get_type()));
    if(grider) grider->remove_element(dynamic_cast<coreNeighborable*>(myOperation));

    myCanvas->fresh();
    emit onUndoOperation(myOperation->get_identity()-1);
}


void
Operationer::onRedo(Operatable*)
{
    if(myCurElementCount >= myElementCount) return;
    isUnRedo = true;
    //<<<<<<<from up to buttom level>>>>//
    myOperation = myElementList.at(myCurElementCount);
    if(myOperation->get_busy()) return;
    //<<<<<<<current level redo>>>>>>>//
    myCurElementCount++;
    OpRedo redo;
    operation_write(&redo, ShadowCoreSystem::getOpWriteStream());
    //<<<<<<<<<<<<<<<undo management>>>>>>>>>>>>>//
    coreUndoerOp *undoer = get_opUndoer(getOperationLayer(myOperation->get_type()));
    if(undoer) undoer->onRedo();

    EGrider *grider = get_opGrider(getOperationLayer(myOperation->get_type()));
    if(grider) grider->push_element(dynamic_cast<coreNeighborable*>(myOperation));

    myCanvas->fresh();
    emit onUndoOperation(myOperation->get_identity());
}


void
Operationer::branch_clear()
{
    if(!isUnRedo) return;
    isUnRedo = false;
    assert(myCurElementCount <= myElementCount);
    //<<<<<<<<from up to down clear>>>>>>>//
    for(int i = myElementCount-1; i >= myCurElementCount; i--)
    {
        assert(!myElementList.last()->get_busy());  //is not busy now
        emit onDeleteOperation(myElementList.last()->get_identity());
        operation_delete(myElementList.last());
        myElementList.removeLast();
    }
    myElementCount = myCurElementCount;
}


bool
Operationer::on_start(QEvent *e)
{
    if(isLocked())  //freezed
        return false;
    ulong type = ShadowCoreSystem::getApplicationState();
    OpParam *param = ShadowCoreSystem::getPaintingParam();
    myOperation = operation_create(*param, type);
    if(!myOperation) return false;
    QPixmap *pixmap = operation_pixmap(myOperation->get_type(), 0);
    return dynamic_cast<Sketchable*>(myOperation)->on_start(e, pixmap);
}


bool
Operationer::on_update(QEvent *e)
{
    if(isLocked() || !myOperation)  //freezed
        return false;
    QPixmap *pixmap = operation_pixmap(myOperation->get_type(), 0);
    return dynamic_cast<Sketchable*>(myOperation)->on_update(e, pixmap);
}


bool
Operationer::on_finish(QEvent *e)
{
    if(isLocked() || !myOperation)
        return false;
    QTime pTime; pTime.start();

    myCanvas->clear_pixmap(CANVASPIXMAP_TEMP);
    if(!dynamic_cast<Sketchable*>(myOperation)->on_finish(e,0))
    {
        if(myOperation) operation_delete(myOperation);
        myOperation = NULL;
        return false;
    }
    if(is_operation_sketchable(myOperation->get_type()))
    {
        Sketchable *sketch = dynamic_cast<Sketchable*>(myOperation);
        if(false)
        {
            Path path = sketch->get_path();
            path.path_smooth(10, 3, 5);
            sketch->set_path(path);
        }
        //<<<<<<<set OpNeighborable infor>>>>>>>>//
    }

    pushElementToHistory(myOperation);

    myOperation = NULL;

    qDebug()<<"operationer time = "<<pTime.elapsed()*0.001;
    return true;
}


void
Operationer::pushElementToHistory(Operatable *op)
{
    if(!op || !op->get_active()) return;
    //<<<<<<<current level push>>>>>>>//
    op->set_identity(myCurElementCount);
    branch_clear(); //clear all levels from up to bottom
    myElementCount++;
    myCurElementCount++;
    myElementList.append(op);
    operation_write(op, ShadowCoreSystem::getOpWriteStream());
    //<<<<<<<<<<<<<<rend management>>>>>>>>>>>>>>>//
    QPixmap *pixmap = operation_pixmap(op->get_type(), 1);
    operation_reply(op, pixmap);    //time = 0.003s
    myCanvas->fresh();
    //<<<<<<<<<<<<<<undo management>>>>>>>>>>>>>>>//
    coreUndoerOp *undoer = get_opUndoer(getOperationLayer(op->get_type()));
    if(undoer) undoer->pushElementToHistory(op->get_identity());
    //<<<<<<<<<<<<<grid management>>>>>>>>>>>>>>>//
    coreNeighborable *nb = dynamic_cast<coreNeighborable*>(op);
    EGrider *grider = get_opGrider(getOperationLayer(op->get_type()));
    if(grider) grider->push_element(nb);
    //<<<<<<<<<<<higher level management>>>>>>>>//
    nb->set_identity(op->get_identity());
    nb->set_griderId(getOperationLayer(op->get_type()));

    emit onFinishOperation(op->get_identity());  //pushElementToHistory() can be called from other place
}


void
Operationer::pushElementsToHistory(QList<Operatable*> elements)
{//pushed from outsider
    for(int i = 0; i < elements.size(); i++)
    {
        if(!elements[i]) continue;
        elements[i]->set_systemAuto(true);
        pushElementToHistory(elements[i]);
    }
}


bool
Operationer::loadOperations(const QString &fileName)
{
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;
    QTextStream in(&file);
    ShadowCoreSystem::setOpReadStream(&in);

    bool state = readOperations(&in);
    if(!state) return false;
    file.close();
    replayOperations();
    return true;
}


void
Operationer::replayOperations()
{
    for(int i = 0; i < myLoadOperationlist.size(); i++)
    {
        Operatable *op = myLoadOperationlist[i];
        //<<<<<<<<<<<<<<undo management>>>>>>>>>>>>>>>//
        QPixmap *pixmap = operation_pixmap(op->get_type(), 1);
        operation_reply(op, pixmap);
        //<<<<<<<<<<<<<<undo management>>>>>>>>>>>>>>>//
        coreUndoerOp *undoer = get_opUndoer(getOperationLayer(op->get_type()));
        if(undoer) undoer->pushElementToHistory(op->get_identity());
        //<<<<<<<<<<<<<grid management>>>>>>>>>>>>>>>//
        EGrider *grider = get_opGrider(getOperationLayer(op->get_type()));
        if(grider) grider->push_element(dynamic_cast<coreNeighborable*>(op));
    }
    ShadowCoreSystem::getMainCanvas()->fresh();
}



bool
Operationer::saveOperations(const QString &fileName)
{
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    QTextStream out(&file);
    writeOperations(&out);
    file.close();
    return true;
}


bool
Operationer::writeOperations(QTextStream *out)
{
    (*out)<<"startframe1"<<"\n";
    (*out)<<"startframe2"<<"\n";
    for(int i = 0; i < myElementList.size(); i++)
        operation_write(myElementList[i], out);
    (*out)<<"endframe\n\n"<<endl;
    return true;
}


bool
Operationer::readOperations(QTextStream *in)
{
    QString line = in->readLine();
    while(!line.contains("startframe2"))
    {
        if(in->atEnd()) return false;
        line = in->readLine();
    }
    //<<<<<<<<<out>>>>>>>>>//
    myLoadOperationlist.clear();
    branch_clear(); //clear all levels from up to bottom
    while(true)
    {
        line = in->readLine();
        while(!line.contains("startop1"))
        {
            if(line.contains("endframe"))
                return true;
            if(in->atEnd()) return false;
            line = in->readLine();
        }
        Operatable *op = operation_read(in);
        if(!op) return false;
        if(!is_operation_sketchable(op->get_type())) continue;
        myLoadOperationlist.append(op);
        op->set_identity(myCurElementCount);
        myElementCount++;
        myCurElementCount++;
        myElementList.append(op);
    }
    return true;
}
