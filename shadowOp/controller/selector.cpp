#include "selector.h"
#include "coreWidget_canvas.h"
#include "operation.h"
#include "analysiser.h"


Selector::Selector()
{
    myProcessFlag = true;
    mySelectionFlag = 0;
    mySelectionRadius = 20;
}


QList<coreNeighborable*>
Selector::get_selections(QPointF pos, qreal width, bool lockE)
{
    QList<coreNeighborable*> selections;
    for(int i = 0; i < mySelectGriders.size(); i++)
        selections.append(mySelectGriders[i]->get_elements(j_to_vec2d(pos), width, lockE));
    return selections;
}


coreNeighborable*
Selector::get_selection(QPointF pos, qreal radius, bool lockE)
{
    QList<coreNeighborable*> eList = get_selections(pos, radius, lockE);
    qreal minDis = MAXVALUE;
    coreNeighborable *minNeighbor = 0;
    for(int i = 0; i < eList.size(); i++)
    {
        qreal dis = MAXVALUE;
        Core2<QList, QPointF>::j_nearest(eList[i]->get_GridPoints(), pos, true, &dis);
        if(dis < minDis)
        {
            minDis = dis;
            minNeighbor = eList[i];
        }
    }
    return minNeighbor;
}



void
Selector::on_start(QEvent *e, int id, int radius)
{
    mySelectionFlag = id;
    mySelectionRadius = radius;
    myProcessFlag = true;
    mySelectElements.clear();
    ShadowCoreSystem::getMainCanvas()->clear_pixmap(CANVASPIXMAP_TEMP);
    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_handle()->on_start(e);
}


int
Selector::on_update(QEvent *e)
{
    QPointF pos = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_handle()->on_update(e);
    if(mySelectionFlag == 0)
    {
        QList<coreNeighborable*> InfoList = get_selections(pos, mySelectionRadius, true);
        mySelectElements.append(InfoList);
    }
    else if(mySelectionFlag == 1)
    {
        //<<<<<<<do instant move>>>>>>>>>//
        QPointF pos = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
        coreNeighborable* minN = get_selection(pos, mySelectionRadius, false);
        if(minN && (mySelectElements.isEmpty() || mySelectElements[0] != minN))
        {
            mySelectElements.clear();
            mySelectElements.append(minN);

            ShadowCoreSystem::getMainCanvas()->clear_pixmap(CANVASPIXMAP_TEMP);
            QString message;
         //   message.append(on_test_context(minN));
            message.append(on_test_neighbor(minN));
            if(!message.isEmpty())
                emit on_message(message);
        }
    }
    return 0;
}


int
Selector::on_finish(QEvent *e)
{
    ShadowCoreSystem::getMainCanvas()->clear_pixmap(CANVASPIXMAP_TEMP);
    bool is_circle = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_handle()->on_finish(e);
    if(mySelectionFlag == 0)
    {
        for(int i = 0; i < mySelectElements.size(); i++)
            mySelectElements[i]->set_lock(false);
        if(is_circle)
        {//<<<<<<<do circle>>>>>>>>//
            mySelectElements.clear();
            QPainterPath path = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_handle()->myPath;
            QRectF region = path.boundingRect();
            for(int i = 0; i < mySelectCandidates.size(); i++)
            {
                QPointF center = mySelectCandidates[i]->get_GridCenter();
                if(!region.contains(center)) continue;
                if(!path.contains(center)) continue;
                mySelectElements.append(mySelectCandidates[i]);
            }
        }
    }
    else if(mySelectionFlag == 1) //select the nearest op
    {
        QPointF pos = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
        coreNeighborable* minN = get_selection(pos, mySelectionRadius, false);
        if(minN)  mySelectElements.append(minN);
    }
    return 0;
}



QString
Selector::on_test_neighbor(coreNeighborable *core)
{
    QString message;
    if(!core) return message;
    QPixmap *pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_pixmap();
    Brush *op = dynamic_cast<Brush*>(core);
    if(true)
    {
        QColor color = op->get_color();
        op->set_color(ShadowCoreSystem::getGlobalColor(0));
        operation_reply(op, pixmap);
        op->set_color(color);

        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawPoints(op->get_keyPoints(), QColor(Qt::black));
    }
    if(ShadowCoreSystem::getNeighborTest(TEMP_NEIGHBORHOOD))
    {
        message.append("\ntemporal: \n");
        for(int i = 0; i < op->myTempNeighbors.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->myTempNeighbors[i].get_neighbor());
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(1));
            operation_reply(n, pixmap);
            n->set_color(color);

            message.append(QString::number(op->myTempNeighbors[i].get_neighbor()->get_identity())+": ");
            QList<short> mathIds = op->myTempNeighbors[i].get_neighborMatchIds();
            for(int j = 0; j < mathIds.size(); j++)
                message.append(QString::number(mathIds[j])+" ");
            message.append("\n");
        }
    }
    if(ShadowCoreSystem::getNeighborTest(SPATIAL_NEIGHBORHOOD))
    {
        message.append("\n spatial: \n");
        for(int i = 0; i < op->mySpaceNeighbors.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->mySpaceNeighbors[i].get_neighbor());
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(2));
            operation_reply(n, pixmap);
            n->set_color(color);

            message.append(QString::number(op->mySpaceNeighbors[i].get_neighbor()->get_identity())+": ");
            QList<short> mathIds = op->mySpaceNeighbors[i].get_neighborMatchIds();
            for(int j = 0; j < mathIds.size(); j++)
                message.append(QString::number(mathIds[j])+" ");
            message.append("\n");
        }
    }
    if(ShadowCoreSystem::getNeighborTest(STRUCT_NEIGHBORHOOD))
    {
        message.append("\n struct: \n");
        for(int i = 0; i < op->myTemporalStructNeighbors.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->myTemporalStructNeighbors[i].get_neighbor());
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(3));
            operation_reply(n, pixmap);
            n->set_color(color);

            message.append(QString::number(op->myTemporalStructNeighbors[i].get_neighbor()->get_identity())+": ");
            QList<short> mathIds = op->myTemporalStructNeighbors[i].get_neighborMatchIds();
            for(int j = 0; j < mathIds.size(); j++)
                message.append(QString::number(mathIds[j])+" ");
            message.append("\n");
         //   ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawPoints(op->myTemporalStructNeighbors[i].get_CutPoints(), QColor(Qt::red));
        }
    }
    if(ShadowCoreSystem::getNeighborTest(CONTEXT_NEIGHBORHOOD))
    {
        for(int i = 0; i < op->myContextNeighbors.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->myContextNeighbors[i].get_neighbor());
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(4));
            operation_reply(n, pixmap);
            n->set_color(color);

        //    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawPoints(op->myContextNeighbors[i].get_CutPoints(), QColor(Qt::red));
        }
    }
    if(ShadowCoreSystem::getNeighborTest(PROPAGATION_NEIGHBORHOOD))
    {
        for(int i = 0; i < op->mySpatialStructNeighbors.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->mySpatialStructNeighbors[i].get_neighbor());
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(5));
            operation_reply(n, pixmap);
            n->set_color(color);
        //    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawPoints(op->myContextNeighbors[i].get_CutPoints(), QColor(Qt::red));
        }
    }
    if(ShadowCoreSystem::getNeighborTest(NEIGHBORHOOD_SIZE))
    {
        for(int i = 0; i < op->mySimilarityBuffer.size(); i++)
        {
            Sketchable *n = dynamic_cast<Sketchable*>(op->mySimilarityBuffer[i]);
            QColor color = n->get_color();
            n->set_color(ShadowCoreSystem::getGlobalColor(6));
            operation_reply(n, pixmap);
            n->set_color(color);
        }
    }
    return message;
}



QString
Selector::on_test_context(coreNeighborable *coreOp)
{
    QString message;
    if(!coreOp) return message;

    QPixmap *pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_pixmap();
    Sketchable *op = dynamic_cast<Sketchable*>(coreOp);
    op->set_color(QColor(Qt::blue));
    operation_reply(op, pixmap);
    op->set_color(QColor(Qt::black));

    Brush *lastOp = dynamic_cast<Brush*>(ShadowCoreSystem::getOperationer()->getCurrentElement());

    QList<float> params;
    QList<short> matchIds;
    int neighborId = -1;
#ifdef OPTIMIZATIONPARAMTEST
    if(lastOp == op)
        params = op->optimizationParams;
    else
    {
#endif
        bool isFound = false;
        coreNeighborable *neighbor = dynamic_cast<coreNeighborable*>(op);
        for(int i = 0; i < lastOp->myTemporalStructNeighbors.size() && !isFound; i++){
            if(lastOp->myTemporalStructNeighbors[i].get_neighbor() == neighbor){
                isFound = true;
                neighborId = 2;
#ifdef OPTIMIZATIONPARAMTEST
                params = lastOp->myTemporalStructNeighbors[i].optimizationParams;
#endif
                matchIds = lastOp->myTemporalStructNeighbors[i].get_neighborMatchIds();
            }
        }
        for(int i = 0; i < lastOp->myContextNeighbors.size() && !isFound; i++){
            if(lastOp->myContextNeighbors[i].get_neighbor() == neighbor){
                isFound = true;
                neighborId = 3;
#ifdef OPTIMIZATIONPARAMTEST
                params = lastOp->myContextNeighbors[i].optimizationParams;
#endif
                matchIds = lastOp->myContextNeighbors[i].get_neighborMatchIds();
            }
        }
#ifdef OPTIMIZATIONPARAMTEST
    }
#endif

    for(int i = 0; i < matchIds.size(); i++)
    {
        short id = matchIds[i] >> 4;
        for(int j = id; j < op->Operatable::get_identity(); j++)
        {
            Operatable *oper = ShadowCoreSystem::getOperationer()->getOperationAt(j);
            if(!oper) continue;
            Brush *opersketch = dynamic_cast<Brush*>(oper);
            bool isFound = false;
            for(int k = 0; k < opersketch->getNeighborhoodSize(neighborId) && !isFound; k++)
            {
                QList<short> omatchIds = opersketch->getNeighborhood(neighborId, k)->get_neighborMatchIds();
                if(j_findElementInSequence(omatchIds, matchIds[i]) >= 0)
                    isFound = true;
            }
            if(!isFound) continue;

            opersketch->set_color(QColor(Qt::red));
            operation_reply(opersketch, pixmap);
            opersketch->set_color(QColor(Qt::black));
        }
    }

    if(params.size() != 14) return message;

    QPointF p1(params[0], params[2]);
    QVector2D d1(params[4], params[5]);
    message.append(QString("P1 X_weight = ")+QString::number(params[1])+" \n");
    message.append(QString("P1 Y_weight = ")+QString::number(params[3])+" \n");
    message.append(QString("P1 D_weight = ")+QString::number(params[6])+" \n");

    QPointF p2(params[7], params[9]);
    QVector2D d2(params[11], params[12]);
    message.append(QString("P2 X_weight = ")+QString::number(params[8])+" \n");
    message.append(QString("P2 Y_weight = ")+QString::number(params[10])+" \n");
    message.append(QString("P2 D_weight = ")+QString::number(params[13])+" \n");

    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawDirection(p1, d1, QColor(Qt::red), 40, 5);
    ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->drawDirection(p2, d2, QColor(Qt::red), 40, 5);

    return message;
}
