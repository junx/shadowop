#ifndef ANALYSISER_H
#define ANALYSISER_H

#include "operationer.h"


#define NEIGHBORHOOD_SIZE_TEMPORAL 1
#define NEIGHBORHOOD_SIZE_SPATIAL  3
#define NEIGHBORHOOD_SIZE_STRUCT   4
#define NEIGHBORHOOD_SIZE_CONTEXT  4
#define NEIGHBORHOOD_SIZE_PROPAGATION 3
#define NEIGHBORHOOD_SIZE_KEYFRAME 2

#define NEIGHBORHOOD_RANGE_SPATIAL 200
#define NEIGHBORHOOD_RANGE_CONTEXT 50
#define NEIGHBORHOOD_RANGE_PROPAGATION 20
#define NEIGHBORHOOD_RANGE_KEYFRAME 100

#define SEARCH_SIZE_TEMPORAL 40
#define SEARCH_SIZE_KEYFRAME 2

class Analysiser : public QObject
{
    Q_OBJECT

    Operationer      *myOperationer;
public slots:
    void             onPropagation();
    void             on_finish_operation(int id);
    void             on_finish_synthesis(int flagSize, qreal *flags);
public:
    Analysiser(Operationer *opner);

    void   online_analysis(coreNeighborable *target);
    void   propagation_analysis(coreNeighborable *target);
    void   clone_analysis(QList<coreNeighborable*> targets);

    void   search(coreNeighborable *target, QList<coreNeighborable*> neighborSpace, QList<coreNeighborable*> &searchSpace, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int flagSize, int *flags);
    void   find_neighborhood_core(coreNeighborable *target, QList<coreNeighborable*> neighborSpace, QList<coreNeighborable*> &searchSpace, int flagSize, int *flags);
    void   match_neighborhood_core(coreNeighborable *target, QList<coreNeighborable *> &searchSpace, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal*wList);
    void   reverse_match_neighborhood_core(coreNeighborable *input, QList<coreNeighborable *> &space, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal*wList);
    void   search_match_neighborhood_core(coreNeighborable *target, QList<coreNeighborable *> &space, QVector<QVector<QVector<short> > > &allMatchList, QVector<QVector<qreal> > &allCostList, int wSize, qreal *wList);

    bool   analysize(coreNeighborable *target, QList<coreNeighborable*> searchSpace, QVector<QVector<short> > &contextMatchList, QVector<QVector<QVector<Vec2f> > > &analysisList, int wSize, qreal *wList);
    void   optimization(coreNeighborable *target, QList<coreNeighborable*> &searchSpace, QVector<QVector<QVector<short> > > &allMatchList, int wSize, int *wList);
    void   constraint_self(coreNeighborable *target, Matrixd &Value, int wSize, qreal *wList);
    void   constraint_context(coreNeighborable *target, QVector<QVector<QVector<Vec2f> > > &analysisList, Matrixd &Value, int wSize, qreal *wList);
};


#endif // ANALYSISER_H
