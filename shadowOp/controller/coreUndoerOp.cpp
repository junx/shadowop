#include "coreUndoerOp.h"
#include "operationer.h"


coreUndoerOp::coreUndoerOp(QPixmap *mainP, QPixmap *subP)
    :coreUndoer()
{
    myStoredPixmap = mainP;
    myFlushPixmap = subP;
}


void
coreUndoerOp::reset()
{
    coreUndoer::reset();
    myStatePixmap.clear();
}


void
coreUndoerOp::record_state()
{
    QPixmap map = *myFlushPixmap;
    myStatePixmap.append(map);
    //push the flushpixmap into mainpixmap
    QPainter painter(myStoredPixmap);
    painter.drawPixmap(0,0,*myFlushPixmap);
    painter.end();
    myFlushPixmap->fill(Qt::transparent);
}


void
coreUndoerOp::clear_state()
{
    if(myStatePixmap.isEmpty())
    {
        qDebug()<<"error in branch_clear in undoer"<<endl;
        return;
    }
    myStatePixmap.removeLast();
}



void
coreUndoerOp::runto(uint position)
{
    myStoredPixmap->fill(Qt::transparent);
    myFlushPixmap->fill(Qt::transparent);
    if(position <= 0)
        return;
    QPainter painter(myStoredPixmap);
    uint last_separate = 0;
    for(int i = 0; i < StateIndexList.size(); i++)
    {
        if(StateIndexList[i] < position)
        {
            painter.drawPixmap(0,0,myStatePixmap.at(i));
            last_separate = StateIndexList[i]+1;
        }
        else
            break;
    }
    painter.end();
    for(uint i = last_separate; i < position; i++)
    {
        uint index = ElementIndexList[i];
        Operatable* op = ShadowCoreSystem::getOperationer()->getOperationAt(index);
        operation_reply(op, myFlushPixmap);
    }
}

