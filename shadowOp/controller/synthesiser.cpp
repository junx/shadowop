#include "synthesiser.h"


Synthesiser::Synthesiser(Operationer *opner)
    :coreRunable()
{
    myOperationer = opner;
    cachSize = 10;
}


void
Synthesiser::pushElementToHistory(coreSynthesis *e)
{
    if(!e) return;
    myElementList.append(e);
    if(myElementList.size() > cachSize)
    {
        myElementList.first()->freeit();
        delete myElementList.first();
        myElementList.removeFirst();
    }
    myFocus = myElementList.size()-1;
    fresh();
}


void
Synthesiser::fresh()
{
    if(myFocus < 0 || !myElementList[myFocus] || !myElementList[myFocus]->is_rend())
    {
        myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->get_handle()->prepareUpdate(QImage());
        myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->set_pixmapRect(QRect(0,0,0,0));
        return;
    }
    //<<<<<<<update shadow hint>>>>>>>>//
    mySelectionBrushImage = myElementList[myFocus]->get_rendPixmap();
    myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->set_pixmapRect(myElementList[myFocus]->get_rendPixmapRect());
    myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->get_handle()->prepareUpdate(mySelectionBrushImage);
    ShadowCoreSystem::getMainCanvas()->fresh();
}


void
Synthesiser::onSelectionBrush()
{
    mySelectionBrushImage.fill(Qt::transparent);
    if(ShadowCoreSystem::getSystemSelectBrushOn())
    {
        QCursor  cursor = get_cursor(QColor(200, 0, 0, 150), ShadowCoreSystem::getPaintingParam()->size*2);
        myOperationer->get_canvas()->setCursor(cursor);

        for(int i = 1; i < 4; i++)
            emit on_delete_synthesis(i);
        fresh();
    }
    else
    {
        QCursor  cursor = get_cursor(QColor(200, 200, 200, 150), ShadowCoreSystem::getPaintingParam()->size*2);
        myOperationer->get_canvas()->setCursor(cursor);

        myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->get_handle()->prepareUpdate(QImage());
        myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->set_pixmapRect(QRect(0,0,0,0));
        if(myFocus < 0 || !myElementList[myFocus]) return;

        myOperationer->lock(true);
        QList<coreNeighborable*> allList = myElementList[myFocus]->get_elements();
        QList<coreNeighborable*> elist;
        QList<int> idList;
        for(int i = 0; i < allList.size(); i++)
        {
            if(!allList[i]->get_lock()) continue; //been selected
            allList[i]->set_lock(false); //unlock
            allList[i]->set_active(true); //has been confirmed by user
            elist.append(allList[i]);
            idList.append(allList[i]->get_identity());
        }
        //<<<<<<<<<<<the user migh not select all the ops, need resort>>>>>>>>//
        QList<int> indexs = Core1<QList, int>::j_sort(idList, idList.size(), true);
        QList<coreNeighborable*> sequence;
        for(int i = 0; i < indexs.size(); i++)
            sequence.append(elist[indexs[i]]);

        myElementList[myFocus]->remove_elements(sequence);
        fresh();

        myOperationer->pushElementsToHistory(coreNeighborable_to_operatable(sequence)); //pushed into workflow
        myOperationer->lock(false);
    }
}


void
Synthesiser::onAutoComplete()
{
    if(myFocus < 0 || !myElementList[myFocus]) return;

    myOperationer->lock(true);
    QList<coreNeighborable*> allList = myElementList[myFocus]->get_elements();
    for(int i = 0; i < allList.size(); i++)
    {
        allList[i]->set_lock(false);
        allList[i]->set_active(true); //has been confirmed by user
    }
    myElementList[myFocus]->remove_elements(allList);
    fresh();
    myOperationer->pushElementsToHistory(coreNeighborable_to_operatable(allList)); //pushed into workflow
    myOperationer->lock(false);
}


void
Synthesiser::onUpdate(int id)
{
    myFocus = -1;
    for(int i = 0; i < myElementList.size(); i++)
    {
        if(myElementList[i]->get_identity() == id)
        {
            myFocus = i;
            break;
        }
    }
    fresh();
}


void
Synthesiser::onRemove(int id)
{
    for(int i = 0; i < myElementList.size(); i++)
    {
        if(myElementList[i]->get_identity() == id)
        {
            myElementList[i]->freeit();
            delete myElementList[i];
            myElementList.removeAt(i);
            break;
        }
    }
    if(myFocus >= myElementList.size())
        myFocus = myElementList.size()-1;
    fresh();
}


bool
Synthesiser::on_start(QEvent *)
{//conflict when (1)undo ops--->(2)confirm ops
    myOperationer->branch_clear();
    return true;
}


bool
Synthesiser::on_update(QEvent *e)
{
    qreal width = j_to_pressure(e)*50+5;
    QCursor  cursor = get_cursor(QColor(200, 0, 0, 150), 2*width);
    myOperationer->get_canvas()->setCursor(cursor);

    if(myFocus < 0 || !myElementList[myFocus] || !myElementList[myFocus]->get_grider())
        return false;
    QPointF pos = myOperationer->get_canvas()->get_eventPosition(e);
    QList<coreNeighborable*> elist = myElementList[myFocus]->get_grider()->get_elements(Vec2d(pos.x(), pos.y()), width, true);
    if(elist.isEmpty()) return false;

    QRect rect = myElementList[myFocus]->get_rendPixmapRect();
    QPainter painter(&mySelectionBrushImage);
    for(int i = 0; i < elist.size(); i++)
    {
        Sketchable *op = dynamic_cast<Sketchable*>(elist[i]);   
        if(!op) continue;
        op->replay(painter, -rect.topLeft());
    }
    painter.end();
    myOperationer->get_canvas()->get_pixmap(CANVASPIXMAP_SHADOW)->get_handle()->prepareUpdate(mySelectionBrushImage);
    ShadowCoreSystem::getMainCanvas()->fresh();
    return true;
}
