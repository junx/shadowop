#ifndef SELECTOR_H
#define SELECTOR_H

#include "eGrider.h"

class Selector : public QObject
{
    Q_OBJECT

    int                       mySelectionFlag;
    int                       mySelectionRadius;
    bool                      myProcessFlag;

    QList<coreNeighborable*>  mySelectCandidates;
    QList<EGrider*>           mySelectGriders;

signals:
    void     on_message(QString);

public:
    QList<coreNeighborable*>  mySelectElements;

    Selector();

    void     set_candidatas(QList<coreNeighborable*> ops){mySelectCandidates = ops;}
    void     set_griders(QList<EGrider*> griders){mySelectGriders = griders;}
    void     set_griders(EGrider *grider){mySelectGriders.clear(); mySelectGriders.append(grider);}

    coreNeighborable*  get_selection(QPointF pos, qreal radius, bool lockE);
    virtual QList<coreNeighborable*>   get_selections(QPointF pos, qreal radius, bool lockE);

    void      on_start(QEvent *e, int id, int radius);
    int       on_update(QEvent *e);
    int       on_finish(QEvent *e);

    QString   on_test_context(coreNeighborable* op);
    QString   on_test_neighbor(coreNeighborable *op);
};

#endif // SELECTOR_H
