#include "coreWidget_setting.h"


coreWidgetSetting::coreWidgetSetting(QWidget *parent)
    :coreWidget(parent)
{
    setting_button_group = new QButtonGroup();
}


void
coreWidgetSetting::initial()
{
    coreWidget::initial();
    int w = width(); //215,270
    //<<<<<<system function>>>>>>>//
    int lleft = 10, ltop = 10, lheight = 40;
    QLabel *setting_label = new QLabel(myFrame);
    setting_label->setGeometry(lleft, ltop, w, lheight);
    setting_label->setStyleSheet("font:15pt;");
    setting_label->setText("Setting");

    myFrame->setGeometry(0, 0, w, ltop+lheight+20);
    myWidget->setGeometry(0, 0, w, ltop+lheight+20);
    myMoveStartBottomRight = QPoint(w, ltop+lheight+20);

    add_button("", "loadBackground:");
    add_button("", "showBackground:");
    add_button("", "showMain");
    add_button("", "showDraft:");
    add_button("", "showTemporalN");
    add_button("", "showSpatialN");
    add_button("", "showStructN");
    add_button("", "showContextN");
    add_button("", "showPropagationN");
    add_button("", "showSimilar");
}


void
coreWidgetSetting::connect_ui()
{
    //<<<<<<<<<<<connect UI>>>>>>>>>>//
    connect(setting_button_group, SIGNAL(buttonClicked(int)), this, SLOT(on_setting_button_click(int)));
}


void
coreWidgetSetting::add_button(QString iconName, QString labelName)
{
    //<<<<<<<<<<<outlines>>>>>>>>>>>>>>>>>>>//
    int w = width(); //215,270
    //<<<<<<label>>>>>>>//
    int ltop = 10, lheight = 30;
    //<<<<<<<setting buttons>>>>>>>>//
    int tleft = 15, ttop = ltop+lheight, theight = 30, tspace = 40;
    QLabel *label = new QLabel(myFrame);
    int setting_button_number = setting_button_group->buttons().size();
    label->setGeometry(tleft, ttop+setting_button_number*tspace, w-tleft, theight);
    label->setText(labelName);
    QPushButton *systembutton = new QPushButton(myFrame);
    systembutton->setGeometry(w-tleft-theight, ttop+setting_button_number*tspace, theight, theight);
    systembutton->setStyleSheet("border: 4px solid gray");
    if(!iconName.isEmpty())
    {
        QIcon icon;
        icon.addFile((iconName), QSize(), QIcon::Selected, QIcon::Off);
        systembutton->setIcon(icon);
        systembutton->setIconSize(QSize(theight, theight));
    }
    setting_button_group->addButton(systembutton, setting_button_number);

    myFrame->setGeometry(0, 0, w, ttop+tspace*(setting_button_number+1)+10);
    myWidget->setGeometry(0, 0, w, ttop+tspace*(setting_button_number+1)+10);
    myMoveStartBottomRight = QPoint(w, ttop+tspace*(setting_button_number+1)+10);
}


void
coreWidgetSetting::on_setting_button_click(int id)
{
    switch (id) {
    case 0:  //background
    {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open Picture"), QDir::currentPath());

        QPixmap pixmap(fileName);
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUND)->load_pixmap(pixmap);
        *(ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUNDORIG)->get_pixmap()) = pixmap;
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUNDORIG)->set_pixmapRect(pixmap.rect());

        ShadowCoreSystem::getMainCanvas()->get_handle()->on_backgroundAdjust(QPointF(0,0), -1);
        break;
    }
    case 1:  //background hide
    {
        bool isVis = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapVisible();
        isVis = !isVis;
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUND)->set_pixmapVisible(isVis);
        if(isVis) ShadowCoreSystem::getMainCanvas()->get_handle()->on_backgroundAdjust(QPointF(0,0), -1);
        break;
    }
    case 2:  //main
    {
        bool isVis = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_MAIN)->get_pixmapVisible();
        isVis = !isVis;
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_MAIN)->set_pixmapVisible(isVis);
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_MAINFLUID)->set_pixmapVisible(isVis);
        break;
    }
    case 3:  //draft
    {
        bool isVis = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_DRAFT)->get_pixmapVisible();
        isVis = !isVis;
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_DRAFT)->set_pixmapVisible(isVis);
        ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_DRAFTFLUID)->set_pixmapVisible(isVis);
        break;
    }
    default:
        bool isTest = ShadowCoreSystem::getNeighborTest(id-4);
        isTest = !isTest;
        ShadowCoreSystem::setNeighborTest(id-4, isTest);
        break;
    }
    ShadowCoreSystem::getMainCanvas()->fresh();
    on_fresh_setting();
}


void
coreWidgetSetting::on_fresh_setting()
{
    bool isVis[10] = {false};
    //<<<<<<<<background>>>>>>//
    isVis[0] = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapActive();
    //<<<<<background vis>>>>>//
    isVis[1] = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapVisible();
    //<<<<<<<<main vis>>>>>>>>>>>//
    isVis[2] = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_MAINFLUID)->get_pixmapVisible();
    //<<<<<<<<draft vis>>>>>>>>>>>//
    isVis[3] = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_DRAFTFLUID)->get_pixmapVisible();
    //<<<<<<<<<neighborhood test>>>>>>//
    for(int i = 0; i < 6; i++)
        isVis[i+4] = ShadowCoreSystem::getNeighborTest(i);

    for(int i = 0; i < setting_button_group->buttons().size(); i++)
    {
        if(!isVis[i])
            setting_button_group->button(i)->setStyleSheet("border: 4px solid gray");
        else
            setting_button_group->button(i)->setStyleSheet("border: 4px solid green");
    }
    update();
}
