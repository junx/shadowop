#ifndef COREWIDGETCOLOR_H
#define COREWIDGETCOLOR_H

#include "ShadowCoreSystem.h"
#include "qwidget.h"

class coreWidgetColor : public QWidget
{
    Q_OBJECT
public:
    coreWidgetColor(QWidget *parent);

signals:
    void     value_changed(QColor);

protected:
    void      resizeEvent(QResizeEvent *event);
    void      paintEvent(QPaintEvent *event);
    void      mousePressEvent(QMouseEvent *event);

private:
    QColor      bg_color;
    QColor      fg_color;

    QColor      default_bg_color;
    QColor      default_fg_color;

    int         x_cell_num;
    int         y_cell_num;
    QSize       cell_size;
    QPointF     off_set;

    void      initial(QSize nsize);
    void      on_color_change(QColor newfg_col);  //forground color
    void      on_exchange();
    void      on_default();   //active the default color setting
};


#endif // COREWIDGETCOLOR_H
