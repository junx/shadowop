#include "coreWidget.h"


coreWidget::coreWidget(QWidget *parent)
    :QWidget(parent)
{
    isStartIn = false;
    isMoveOn = false;
    myTopLeft = QPointF(0,0);
    myMoveStartPos = QPointF(0,0);
    myMoveStartFlag = -1;
    myFrame = 0;
    myWidget = 0;
    myMoveStartTopLeft = QPoint(0, 0);
    myMoveStartBottomRight = QPoint(0, 0);
}


void
coreWidget::initial()
{
    update_topLeft();

    myFrame = new QFrame(this);
    myFrame->setFrameShape(QFrame::StyledPanel);
    myFrame->setGeometry(0,0,width(),height());
    //<<<<<<<<<<<outlines>>>>>>>>>>>>>>>>>>>//
    myWidget = new QWidget(myFrame);
    myWidget->setStyleSheet("border: 2px solid green");
    myWidget->setGeometry(0,0,width(),height());

    myMoveButton = new coreButton(myWidget);
    myMoveButton->setStyleSheet("border: 2px solid green");
    myMoveButton->setGeometry(width()-15, 5, 10, 10);

    myMoveStartTopLeft = QPoint(0, 0);
    myMoveStartBottomRight = QPoint(width(), height());

    update_topLeft();
}


QPointF
coreWidget::get_eventPosition(QEvent *e)
{
    if(e->type() == QEvent::MouseButtonRelease ||
            e->type() == QEvent::MouseMove ||
            e->type() == QEvent::MouseButtonPress)
        return dynamic_cast<QMouseEvent*>(e)->pos() - myTopLeft;
    else if(e->type() == QEvent::TabletRelease ||
            e->type() == QEvent::TabletMove ||
            e->type() == QEvent::TabletPress)
        return dynamic_cast<QTabletEvent*>(e)->pos() - myTopLeft;
    return QPointF(0, 0);
}


bool
coreWidget::receiveEvent(QEvent *event)
{
    if(!isVisible()) return false;

    switch (event->type()) {
    case QEvent::TabletMove:
    case QEvent::MouseMove:
    {
        if(isStartIn)
            on_update(event);
        break;
    }
    case QEvent::TabletPress:
    case QEvent::MouseButtonPress:
    {
        QPointF point = get_eventPosition(event);
        if(point.x() < myMoveStartTopLeft.x() || point.y() < myMoveStartTopLeft.y() || point.x() > myMoveStartBottomRight.x() || point.y() > myMoveStartBottomRight.y())
            isStartIn = false;
        else
        {
            isStartIn = true;
            on_start(event);
        }
        break;
    }
    case QEvent::TabletRelease:
    case QEvent::MouseButtonRelease:
    {
        if(isStartIn)
            on_finish(event);
        isStartIn = false;
        break;
    }
    default:
        break;
    }
    return isStartIn;
}


bool
coreWidget::on_start(QEvent *e)
{
    myMoveStartPos = get_eventPosition(e);
    QPoint p1 = myMoveButton->mapToParent(QPoint(0, 0));
    QPoint p2 = myMoveButton->mapToParent(QPoint(myMoveButton->width(), myMoveButton->height()));

    if(p1.x() <= myMoveStartPos.x() && myMoveStartPos.y() <= p2.x()
            && p1.y() <= myMoveStartPos.y() && myMoveStartPos.y() <= p2.y())
    {
        isMoveOn = true;
        myWidget->setStyleSheet("border: 3px solid red");
        myMoveButton->myColor = QColor(Qt::red);
    }
    else
        isMoveOn = false;
    e->ignore();
    return isMoveOn;
}


bool
coreWidget::on_update(QEvent *e)
{
    if(isMoveOn)
    {
        QPointF np = get_eventPosition(e);
        QPointF mv = (np-myMoveStartPos) + myTopLeft;
        set_topLeft(mv.toPoint());
    }
    e->ignore();
    return isMoveOn;
}


bool
coreWidget::on_finish(QEvent *e)
{
    if(isMoveOn) myWidget->setStyleSheet("border: 2px solid green");
    myMoveButton->myColor = QColor(Qt::green);
    isMoveOn = false;
    e->ignore();
    return false;
}
