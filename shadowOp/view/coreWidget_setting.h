#ifndef COREWidgetSetting_H
#define COREWidgetSetting_H


#include "coreWidget.h"
#include "coreWidget_canvas.h"



class coreWidgetSetting : public coreWidget
{
    Q_OBJECT
public:
    coreWidgetSetting(QWidget *parent);

    void   initial();
    void   connect_ui();
    void   add_button(QString iconName, QString labelName);

public slots:
    void   on_setting_button_click(int);

private:
    QButtonGroup   *setting_button_group;
    QList<QString>  setting_button_labels;

    void   on_fresh_setting();
};


#endif //
