#ifndef COREWIDGET_CANVAS_H
#define COREWIDGET_CANVAS_H

#include "ShadowCoreSystem.h"
#include "coreWidget.h"
#include "pixmap.h"
#include "canvasHandle.h"

#define CANVASPIXMAP_BACKGROUND       0
#define CANVASPIXMAP_BACKGROUNDORIG   1
#define CANVASPIXMAP_KEYFRAME         2
#define CANVASPIXMAP_SHADOW           3
#define CANVASPIXMAP_DRAFT            4
#define CANVASPIXMAP_DRAFTFLUID       5
#define CANVASPIXMAP_MAIN             6
#define CANVASPIXMAP_MAINFLUID        7
#define CANVASPIXMAP_TEMP             8
#define CANVASPIXMAP_OVERALL          9


class coreWidgetCanvas : public coreWidget
{
    Q_OBJECT
public:
    coreWidgetCanvas(QWidget *parent);
    void     reset();
    void     initial();

    void             set_handle(CanvasHandle* handle){myCanvasHandle = handle;}
    CanvasHandle    *get_handle(){return myCanvasHandle;}

    int              get_pixmapSize(){return myPixmaps.size();}
    void             clear_pixmap(int id);
    Pixmap*          get_pixmap(int id);
    void             set_pixmap(int id, Pixmap *pixmap);
    void             paint_pixmap(int main, int sub);
    QPixmap*         export_pixmap(int flagSize, bool *isVis);

    QList<Pixmap*>   get_pixmaps(){return myPixmaps;}
    void             set_pixmaps(QList<Pixmap*> pixmaps){myPixmaps = pixmaps; update();}

    void             fresh(){update();}
private:
    CanvasHandle        *myCanvasHandle;
    QList<Pixmap*>       myPixmaps;
    void                 paintEvent(QPaintEvent *);
};


class coreWidgetCanvasData
{
    QList<Pixmap*>   myPixmaps;
public:
    coreWidgetCanvasData(){
        int w = ShadowCoreSystem::getMainCanvas()->width(), h = ShadowCoreSystem::getMainCanvas()->height();
        QSize maxSize(w, h);
        myPixmaps.append(new Pixmap(0, 0, false, false, maxSize)); //background
        myPixmaps.append(new Pixmap(0, 0, false, false, maxSize)); //backgroundOrig
        myPixmaps.append(new Pixmap(w, h, true, false, maxSize)); //keyframe
        myPixmaps.append(new Pixmap(w, h, true, true, maxSize)); //shadow
        myPixmaps.append(new Pixmap(w, h, true, false, maxSize));  //draft
        myPixmaps.append(new Pixmap(w, h, true, false, maxSize));  //draftFlush
        myPixmaps.append(new Pixmap(w, h, true, true, maxSize));  //main
        myPixmaps.append(new Pixmap(w, h, true, true, maxSize));  //mainFlush
        myPixmaps.append(new Pixmap(w, h, true, false, maxSize)); //temp
        myPixmaps.append(new Pixmap(w, h, false, false, maxSize)); //overall
    }
    ~coreWidgetCanvasData(){
        for(int i = 0; i < myPixmaps.size(); i++)
            delete myPixmaps[i];
        myPixmaps.clear();
    }
    void    importData(coreWidgetCanvas *canvas){
        myPixmaps = canvas->get_pixmaps();
        freshData();
    }
    void    exportData(coreWidgetCanvas *canvas){
        canvas->set_pixmaps(myPixmaps);
        myPixmaps[CANVASPIXMAP_SHADOW]->clear();
    }
    void    freshData(){
        myPixmaps[CANVASPIXMAP_OVERALL]->clear();
        QPainter painter(myPixmaps[CANVASPIXMAP_OVERALL]->get_pixmap());
        for(int i = 0; i < myPixmaps.size(); i++)
            if(myPixmaps[i]->get_pixmapVisible() && myPixmaps[i]->get_pixmapActive())
                painter.drawPixmap(myPixmaps[i]->get_pixmapRect(), *(myPixmaps[i]->get_pixmap()));
        painter.end();
    }
    int      get_pixmapSize(){return myPixmaps.size();}
    Pixmap*  get_pixmap(int id){if(id < 0 || id >= myPixmaps.size()) return 0; return myPixmaps[id];}
    QPixmap  exportImage()
    {
        int w = ShadowCoreSystem::getMainCanvas()->width(), h = ShadowCoreSystem::getMainCanvas()->height();
        QPixmap pixmap(w, h);
        pixmap.fill(Qt::white);
        QPainter painter(&pixmap);
        for(int i = 0; i < myPixmaps.size(); i++)
            if(myPixmaps[i]->get_pixmapVisible() && myPixmaps[i]->get_pixmapActive())
                painter.drawPixmap(myPixmaps[i]->get_pixmapRect(), *(myPixmaps[i]->get_pixmap()));
        painter.end();
        return pixmap;
    }
};


inline QPixmap* operation_pixmap(ulong flag, int isMain)
{
    QPixmap *pixmap = 0;
    switch (flag) {
    case OPERATION_BRUSH:
    case OPERATION_STIPPLE:
        if(isMain > 0)
            pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_MAINFLUID)->get_pixmap();
        else
            pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_pixmap();
        break;
    case OPERATION_PENCIL:
        if(isMain > 0)
            pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_DRAFTFLUID)->get_pixmap();
        else
            pixmap = ShadowCoreSystem::getMainCanvas()->get_pixmap(CANVASPIXMAP_TEMP)->get_pixmap();
    default:
        break;
    }
    return pixmap;
}




#endif // WIDGET_CANVAS_H
