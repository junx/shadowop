#include "coreWidget_color.h"

coreWidgetColor::coreWidgetColor(QWidget *parent)
    :QWidget(parent)
{
    default_bg_color = QColor(Qt::white);
    default_fg_color = QColor(Qt::black);
    bg_color         = default_bg_color;
    fg_color         = default_fg_color;
    off_set          = QPointF(0,0);

    QRect rect = contentsRect();

    initial(QSize(rect.width(), rect.height()));
}


void
coreWidgetColor::initial(QSize nsize)
{
    x_cell_num = 4;
    y_cell_num = 3;

    int w = nsize.width()/(x_cell_num + 2);
    int h = w;
    if(w > 30)
    {
        w = 30;
        h = w;
    }

    off_set = QPointF((nsize.width()-w*x_cell_num)/2, 0);

    cell_size = QSize(w, h);
    update();
}



void
coreWidgetColor::on_exchange()
{
    if(bg_color == fg_color)
        return;

    QColor  m = bg_color;
    bg_color  = fg_color;
    fg_color  = m;

    emit value_changed(fg_color);
    update();
}



void
coreWidgetColor::on_color_change(QColor newfg_col)
{
    if( fg_color == newfg_col)
        return;

    fg_color  = newfg_col;

    emit value_changed(fg_color);
    update();
}



void
coreWidgetColor::on_default()
{
    if(bg_color == default_bg_color && fg_color == default_fg_color)
        return;

    bg_color  = default_bg_color;
    fg_color  = default_fg_color;

    emit value_changed(fg_color);
    update();
}




void
coreWidgetColor::paintEvent(QPaintEvent *)
{//update the back and forground color
     QPainter  painter(this);
     painter.setPen(Qt::gray);
     painter.translate(off_set);
    //<<<<<<<<<<<<<<foreground and backgourd>>>>>>>>>>>>//
    QRect     backrect(cell_size.width(), cell_size.height(), 3*cell_size.width(), 2*cell_size.height());
    QRect     forgrect(0, 0, 3*cell_size.width(), 2*cell_size.height());

    painter.drawRect(backrect);
    painter.fillRect(backrect, bg_color);

    painter.drawRect(forgrect);
    painter.fillRect(forgrect, fg_color);

    //<<<<<<<<<<default foreground and background>>>>>>>//
    QRect     default_backrect(0.4*cell_size.width(), 2.4*cell_size.height(), 0.4*cell_size.width(), 0.4*cell_size.height());
    QRect     default_forgrect = default_backrect.translated( -0.2*cell_size.width(), -0.2*cell_size.height());

    painter.drawRect(default_backrect);
    painter.fillRect(default_backrect, default_bg_color);

    painter.drawRect(default_forgrect);
    painter.fillRect(default_forgrect, default_fg_color);

    //<<<<<<<<<<<exchanger>>>>>>>>>>>>>>>>>>>>>>>>//
    QRect     exchanger(3.1*cell_size.width(), 0.1*cell_size.height(), 0.8*cell_size.width(), 0.8*cell_size.height());

    QString filename = tr("/myfile/colordialog_exchanger.png");
    QPixmap pix(filename);
    QPixmap newpix = pix.scaled(cell_size.width()*0.8, cell_size.height()*0.8);
    painter.fillRect(exchanger, newpix);
    painter.drawRect(exchanger);
}



void
coreWidgetColor::mousePressEvent(QMouseEvent *event)
{
    QPointF point = event->pos() - off_set;
    if(point.x() < 0 || point.x() >= cell_size.width()*x_cell_num || point.y() < 0 || point.y() >= cell_size.height()*y_cell_num)
         return;

    if(point.x() >= cell_size.width()*(x_cell_num-1) && point.y() <= cell_size.height())
    {
        on_exchange();
        return;
    }

    if(point.y() >= cell_size.height()*(y_cell_num-1) && point.x() <= cell_size.width())
    {
        on_default();
        return;
    }

    QColor newcol = QColorDialog::getColor(Qt::white, 0, "color");
    on_color_change(newcol);
}


void
coreWidgetColor::resizeEvent(QResizeEvent *event)
{
    QSize newsize = event->size();
    initial(newsize);
}


