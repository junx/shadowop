#ifndef UI_H
#define UI_H


#include "coreWidget_tool.h"
#include "coreWidget_param.h"
#include "coreWidget_setting.h"
#include "coreWidget_canvas.h"

//QT_BEGIN_NAMESPACE

#define WIDGET_MAINCANVAS   0
#define WIDGET_TOOL         1
#define WIDGET_PARAM        2
#define WIDGET_SETTING      3

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QFrame *frame;

    coreWidgetCanvas   *mainCanvas;
    coreWidgetTool     *toolWidget;
    coreWidgetParam    *paramWidget;
    coreWidgetSetting  *settingWidget;

    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(G_window_width, G_window_height);
        frame = new QFrame(MainWindow);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(0);

//<<<<<<<<<<<<<<<<<<<tool layout>>>>>>>>>>>>>>>>>>>>>>>>//
        mainCanvas = new coreWidgetCanvas(frame);
        mainCanvas->setGeometry(200, 90, 800, 600);
        mainCanvas->initial();

        toolWidget = new coreWidgetTool(frame);
        toolWidget->setGeometry(30, 50, 120, 360);
        toolWidget->initial();
        toolWidget->connect_ui();
        toolWidget->setGeometry(30, 50, 120, toolWidget->get_actualHeight());

        paramWidget = new coreWidgetParam(frame);
        paramWidget->setGeometry(30, 450, 120, 300);
        paramWidget->initial();
        paramWidget->connect_ui();
        paramWidget->setGeometry(30, 450, 120, paramWidget->get_actualHeight());

        settingWidget = new coreWidgetSetting(frame);
        settingWidget->setGeometry(G_window_width-210, 50, 180, 300);
        settingWidget->initial();
        settingWidget->connect_ui();
        settingWidget->setGeometry(G_window_width-210, 50, 180, settingWidget->get_actualHeight());

//<<<<<<<<<<<<<<<<<<<<work layout>>>>>>>>>>>>>>>>>>>>>>>>//

        MainWindow->setCentralWidget(frame);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(("menubar"));
        menubar->setGeometry(QRect(0, 0, 1200, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(("statusbar"));
        MainWindow->setStatusBar(statusbar);
        retranslateUi(MainWindow);
        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
    } // retranslateUi

};

#endif // UI_H
