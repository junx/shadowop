#ifndef COREWidgetParam_H
#define COREWidgetParam_H

#include "coreWidget.h"

class OpParam;
class  coreWidgetParam : public coreWidget
{
    Q_OBJECT

public:
    coreWidgetParam(QWidget *parent);

    void       initial();
    void       connect_ui();

    OpParam*   get_param(){return op_param;}

    QSlider    *param_sizeSlider;
    QTextEdit  *param_sizeTextEdit;
    QSlider    *param_opacitySlider;
    QTextEdit  *param_opacityTextEdit;

signals:
    void      changed_param(OpParam *param);
    void      changed_cursor();

private slots:
    void      on_color(QColor color);  //received from colorbox
    void      on_size(int size_index);
    void      on_opacity(int opacity);

private:
    OpParam   *op_param;
};



#endif // COREPARAM_WIDGET_H
