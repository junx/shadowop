#include "coreWidget_canvas.h"

coreWidgetCanvas::coreWidgetCanvas(QWidget *parent)
    :coreWidget(parent)
{
    myPixmaps.clear();

    QPalette *p = new QPalette;
    p->setColor(QPalette::Background, Qt::white);
    this->setAutoFillBackground(true);
    this->setPalette(*p);
}


void
coreWidgetCanvas::initial()
{
    coreWidget::initial();
    QPoint p1 = myMoveButton->mapToParent(QPoint(0, 0));
    QPoint p2 = myMoveButton->mapToParent(QPoint(myMoveButton->width(), myMoveButton->height()));
    set_startIn(p1, p2);
}


void
coreWidgetCanvas::reset()
{
    for(int i = 0; i < myPixmaps.size(); i++)
        myPixmaps[i]->get_pixmap()->fill(Qt::transparent);
    update();
}


void
coreWidgetCanvas::clear_pixmap(int id)
{
    if(id >= myPixmaps.size() || myPixmaps.isEmpty())
        return;
    if(id < 0)
        myPixmaps.last()->clear();
    else
        myPixmaps[id]->clear();
    update();
}


Pixmap*
coreWidgetCanvas::get_pixmap(int id)
{
    if(id >= myPixmaps.size() || myPixmaps.isEmpty())
        return 0;
    if(id < 0)
        return myPixmaps.last();
    else
        return myPixmaps[id];
}


void  //get the refresh signal and update the image
coreWidgetCanvas::set_pixmap(int id, Pixmap *pixmap)
{
    if(id < 0 || id >= myPixmaps.size()) return;
    myPixmaps[id] = pixmap;
    update();
}



void
coreWidgetCanvas::paint_pixmap(int main, int sub)
{
    if(main < 0 || sub < 0 || main >= myPixmaps.size() || sub >= myPixmaps.size() || main == sub)
        return;
    QPainter painter(myPixmaps[main]->get_pixmap());
    painter.drawPixmap(0,0,*(myPixmaps[sub]->get_pixmap()));
    painter.end();
    myPixmaps[sub]->clear();
    update();
}


QPixmap*
coreWidgetCanvas::export_pixmap(int flagSize, bool *isVis)
{
    assert(flagSize == myPixmaps.size()+1);
    QPixmap *pixmap = new QPixmap(width(), height());
    QPainter painter(pixmap);
    if(isVis[0])
        pixmap->fill(Qt::white);
    else
        pixmap->fill(Qt::transparent);
    for(int i = 0; i < myPixmaps.size(); i++)
        if(isVis[i+1] && myPixmaps[i]->get_pixmapActive())
            painter.drawPixmap(myPixmaps[i]->get_pixmapRect(), *(myPixmaps[i]->get_pixmap()));
    painter.end();
    return pixmap;
}


void
coreWidgetCanvas::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    for(int i = 0; i < myPixmaps.size(); i++)
    {
        if(myPixmaps[i]->get_pixmapVisible() && myPixmaps[i]->get_pixmapActive())
        {
            if(myPixmaps[i]->get_handle())
                myPixmaps[i]->get_handle()->freshUpdate();
            painter.drawPixmap(myPixmaps[i]->get_pixmapRect(), *(myPixmaps[i]->get_pixmap()));
        }
    }
}

