#include "coreButton.h"

coreButton::coreButton(QWidget *parent)
     : QAbstractButton(parent)
{
    myColor = QColor(Qt::green);
    setStyleSheet("background: transparent");
}


QSize
coreButton::minimumSizeHint() const
{
    return QSize(50, 50);
}


QSize
coreButton::sizeHint() const
{
    return QSize(180, 180);
}


void coreButton::paintEvent(QPaintEvent *)
{
     QPainter painter(this);
     painter.setRenderHint(QPainter::Antialiasing);
     QBrush brush(myColor);
     painter.setBrush(brush);
     painter.setPen(Qt::NoPen);
     painter.drawEllipse(0, 0, width(), height());
}
