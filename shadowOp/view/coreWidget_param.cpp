#include "opParam.h"
#include "coreWidget_param.h"


coreWidgetParam::coreWidgetParam(QWidget *parent)
    :coreWidget(parent)
{
    op_param = new OpParam();
}


void
coreWidgetParam::initial()
{
    coreWidget::initial();
    int w = width();

    int lleft = 10, ltop = 0, lheight = 40;
    QLabel *paramLabel = new QLabel(this);
    paramLabel->setGeometry(lleft, ltop, w, lheight);
    paramLabel->setText("Parameters");

    int sleft = 15, stop = ltop+lheight, sheight = 50;
    QLabel *sizeLabel = new QLabel(myFrame);
    sizeLabel->setText("size");
    sizeLabel->setGeometry(QRect(sleft, stop, w/2, sheight/2));
    param_sizeTextEdit = new QTextEdit(myFrame);
    param_sizeTextEdit->setGeometry(QRect(w/2, stop, w/2-sleft, sheight/2));
    param_sizeTextEdit->setReadOnly(true);
    param_sizeTextEdit->setText("5");
    param_sizeSlider = new QSlider(myFrame);
    param_sizeSlider->setGeometry(QRect(sleft, sheight/2+stop, w-2*sleft, sheight/2));
    param_sizeSlider->setOrientation(Qt::Horizontal);
    param_sizeSlider->setRange(1, 30);
    param_sizeSlider->setSliderPosition(5);

    int oleft = 15, otop = stop+sheight+10;
    QLabel *opacityLabel = new QLabel(myFrame);
    opacityLabel->setText("opacity");
    opacityLabel->setGeometry(QRect(oleft, otop, w/2, sheight/2));
    param_opacityTextEdit = new QTextEdit(myFrame);
    param_opacityTextEdit->setGeometry(QRect(w/2, otop, w/2-oleft, sheight/2));
    param_opacityTextEdit->setText("99");
    param_opacityTextEdit->setReadOnly(true);
    param_opacitySlider = new QSlider(myFrame);
    param_opacitySlider->setGeometry(QRect(oleft, sheight/2+otop, w-2*oleft, sheight/2));
    param_opacitySlider->setOrientation(Qt::Horizontal);
    param_opacitySlider->setSliderPosition(100);

    myFrame->setGeometry(QRect(0, 0, w, otop+sheight+20));
    myWidget->setGeometry(QRect(0, 0, w, otop+sheight+20));
    myMoveStartBottomRight = QPoint(w, otop+sheight+20);
}


void
coreWidgetParam::connect_ui()
{
    //<<<<<<<<connect UI>>>>>>>>>>>>//
    connect(param_sizeSlider, SIGNAL(valueChanged(int)), this, SLOT(on_size(int)));
    connect(param_opacitySlider, SIGNAL(valueChanged(int)), this, SLOT(on_opacity(int)));
}


void
coreWidgetParam::on_size(int size_index)
{
    op_param->size = size_index;
    param_sizeTextEdit->setText(QString::number(size_index));
    emit  changed_param(op_param);
}


void
coreWidgetParam::on_opacity(int opacity)
{
    op_param->opacity = opacity * 0.01;
    param_opacityTextEdit->setText(QString::number(opacity+1));
    emit   changed_param(op_param);
}


void
coreWidgetParam::on_color(QColor color)
{
    op_param->color = color;
    emit  changed_param(op_param);
}


