#ifndef WIDGETTOOL_H
#define WIDGETTOOL_H

#include <QWidget>
#include <QtCore>

#include "coreWidget.h"
#include "coreWidget_color.h"

class coreWidgetTool : public coreWidget
{
    Q_OBJECT

public:
    coreWidgetTool(QWidget *parent);
    virtual void  initial();
    virtual void  connect_ui();
    void          add_button(QString iconName, QString labelName, ulong toolIndex);

signals:
    void          on_leave(ulong totype);
    void          on_enter(ulong totype);
    void          on_redo();
    void          on_undo();

public slots:
    void          on_tool_button_click(int);
    void          on_undo_press();
    void          on_undo_release();
    void          on_redo_press();
    void          on_redo_release();

public:
    coreWidgetColor*tool_color_picker;   //send color directly
private:
    QLabel         *tool_head_label;
    int             tool_button_number;
    int             tool_button_lastId;
    QButtonGroup   *tool_button_group;
    QList<QString>  tool_button_labels;
    QList<ulong>    tool_button_indexs;
    QToolButton    *tool_undo_button;
    QToolButton    *tool_redo_button;
    QToolButton    *tool_setting_button;
};



#endif // WIDGETTOOL_H
