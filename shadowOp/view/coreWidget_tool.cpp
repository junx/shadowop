#include "coreWidget_tool.h"

coreWidgetTool::coreWidgetTool(QWidget *parent)
    :coreWidget(parent)
{
    tool_button_number = 0;
    tool_button_lastId = -1;
    tool_button_group = new QButtonGroup();
}


void
coreWidgetTool::initial()
{
    coreWidget::initial();
    int width = this->width();
    //<<<<<<<<<tool label>>>>>>>>>>>>//
    int lleft = 10, ltop = 0, lheight = 40;
    tool_head_label = new QLabel(this);
    tool_head_label->setText("Setting");
    tool_head_label->setGeometry(QRect(lleft, ltop, width, lheight));

    //<<<<<<<<<<color picker>>>>>>>>>//
    int ptop = ltop+lheight, pwidth = 80, pheight = 70;
    tool_color_picker = new coreWidgetColor(myFrame);
    tool_color_picker->setGeometry(QRect((width-80)*0.5, ptop, pwidth, pheight));

    //<<<<<<<<<<undo redo>>>>>>>>>>>//
    int utop = ptop+pheight, uleft = 20, usize = 35;
    tool_undo_button= new QToolButton(myFrame);
    tool_undo_button->setGeometry(QRect(uleft, utop, usize, usize));
    tool_undo_button->setStyleSheet("border: 1px solid gray");
    QIcon icon6;
    icon6.addFile((":/myfile/myfile/undo.png"), QSize(), QIcon::Selected, QIcon::On);
    tool_undo_button->setIcon(icon6);
    tool_undo_button->setIconSize(QSize(usize, usize));

    tool_redo_button= new QToolButton(myFrame);
    tool_redo_button->setGeometry(QRect(width-uleft-usize, utop, usize, usize));
    tool_redo_button->setStyleSheet("border: 1px solid gray");
    QIcon icon7;
    icon7.addFile((":/myfile/myfile/redo.png"), QSize(), QIcon::Selected, QIcon::On);
    tool_redo_button->setIcon(icon7);
    tool_redo_button->setIconSize(QSize(usize, usize));

    //<<<<<<<<<system setting>>>>>>>>>>>//
    int stop = utop+usize+20, sleft = 10, sheight = 20;
    tool_setting_button = new QToolButton(myFrame);
    tool_setting_button->setGeometry(QRect(sleft, stop, width-2*sleft, sheight));
    tool_setting_button->setText("system setting");
    tool_setting_button->setStyleSheet("border: 1px solid gray");
    tool_button_group->addButton(tool_setting_button, 0);
    tool_button_labels.prepend(QString("Setting"));
    tool_button_indexs.prepend(SYSTEM_SETTING_TOOL);

    tool_button_number = 1;
    tool_button_lastId = 0;
    tool_button_group->button(0)->setStyleSheet("border: 3px solid red");

    myFrame->setGeometry(0, 0, width, stop+sheight+20);
    myWidget->setGeometry(0, 0, width, stop+sheight+20);
    myMoveStartBottomRight = QPoint(width, stop+sheight+20);

    add_button(":/myfile/myfile/pencil.png", "Pencil", SYSTEM_PENCIL_TOOL);
    add_button(":/myfile/myfile/brush.png", "Brush", SYSTEM_BRUSH_TOOL);
    add_button(":/myfile/myfile/stipple.png", "Stipple", SYSTEM_STIPPLE_TOOL);
}


void
coreWidgetTool::connect_ui()
{
    //<<<<<<<<connect ui>>>>>>>>>//
    connect(tool_button_group, SIGNAL(buttonClicked(int)), this, SLOT(on_tool_button_click(int)));
    connect(tool_undo_button, SIGNAL(pressed()), this, SLOT(on_undo_press()));
    connect(tool_undo_button, SIGNAL(released()), this, SLOT(on_undo_release()));
    connect(tool_redo_button, SIGNAL(pressed()), this, SLOT(on_redo_press()));
    connect(tool_redo_button, SIGNAL(released()), this, SLOT(on_redo_release()));
}


void
coreWidgetTool::add_button(QString iconName, QString labelName, ulong toolIndex)
{
    int width = this->width();
    //<<<<<<<<<<label>>>>>>>>>//
    int lleft = 10, ltop = 0, lheight = 40;
    tool_head_label->setGeometry(QRect(lleft, ltop, width, lheight));

    //<<<<<<<<<<tool buttons>>>>>>>>>//
    int tleft = 15, ttop = ltop+lheight, tsize = 35, thspace = width/2, tvspace = tsize+20;
    int i = (tool_button_number-1)%2, j = (tool_button_number-1)/2;
    QToolButton *tool_button = new QToolButton(myFrame);
    tool_button->setGeometry(QRect(tleft+i*thspace, ttop+j*tvspace, tsize, tsize));
    tool_button->setStyleSheet("border: 1px solid gray");
    if(!iconName.isEmpty())
    {
        QIcon icon;
        icon.addFile((iconName), QSize(), QIcon::Selected, QIcon::Off);
        tool_button->setIcon(icon);
        tool_button->setIconSize(QSize(tsize, tsize));
    }
    tool_button_group->addButton(tool_button, tool_button_number);
    tool_button_number++;

    //<<<<<<<<<<color picker>>>>>>>>>//
    int ptop = tvspace*(j+1)+ttop, pwidth = 80, pheight = 70;
    tool_color_picker->setGeometry(QRect((width-80)*0.5, ptop, pwidth, pheight));
    //<<<<<<<<<<undo redo>>>>>>>>>>>//
    int utop = ptop+pheight, uleft = 20, usize = 35;
    tool_undo_button->setGeometry(QRect(uleft, utop, usize, usize));
    tool_redo_button->setGeometry(QRect(width-uleft-usize, utop, usize, usize));
    //<<<<<<<<<system setting>>>>>>>>>>>//
    int stop = utop+usize+20, sleft = 10, sheight = 20;
    tool_setting_button->setGeometry(QRect(sleft, stop, width-2*sleft, sheight));
    tool_button_labels.append(labelName);
    tool_button_indexs.append(toolIndex);

    myFrame->setGeometry(0, 0, width, stop+sheight+20);
    myWidget->setGeometry(0, 0, width, stop+sheight+20);
    myMoveStartBottomRight = QPoint(width, stop+sheight+20);
    update();
}


void
coreWidgetTool::on_tool_button_click(int id)
{
    if(id == tool_button_lastId || id < 0 || id >= tool_button_number)
        return;
    tool_button_group->button(tool_button_lastId)->setStyleSheet("border: 1px solid gray");
    tool_button_group->button(id)->setStyleSheet("border: 3px solid red");
    tool_head_label->setText(tool_button_labels[id]);
    emit on_leave(tool_button_indexs[tool_button_lastId]);
    emit on_enter(tool_button_indexs[id]);
    tool_button_lastId = id;
    update();
}


void
coreWidgetTool::on_undo_press()
{
    tool_undo_button->setStyleSheet("border: 3px solid red");
    emit on_undo();
}

void
coreWidgetTool::on_undo_release()
{
    tool_undo_button->setStyleSheet("border: 1px solid gray");
}

void
coreWidgetTool::on_redo_press()
{
    tool_redo_button->setStyleSheet("border: 3px solid red");
    emit on_redo();
}

void
coreWidgetTool::on_redo_release()
{
    tool_redo_button->setStyleSheet("border: 1px solid gray");
}
