#ifndef COREBUTTON_H
#define COREBUTTON_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

class coreButton : public QAbstractButton
{
    Q_OBJECT

public:
    QColor   myColor;

    coreButton(QWidget *parent = 0);

    void  setColor(QColor color){myColor = color; update();}

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);
};


#endif // COREBUTTON_H
