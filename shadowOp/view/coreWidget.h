#ifndef COREWIDGET_H
#define COREWIDGET_H

#include "ShadowCoreSystem.h"
#include "coreButton.h"

class coreWidget : public QWidget
{
    Q_OBJECT
public:
    coreWidget(QWidget *parent);
    virtual void  initial();
    virtual void  connect_ui(){}
    virtual void  reset(){}
    virtual void  lock(bool isLock){this->setEnabled(isLock);}

    QPointF          get_topLeft(){return myTopLeft;}
    void             update_topLeft(){myTopLeft = mapToParent(QPoint(0,0));}
    void             set_topLeft(QPoint p){move(p); update_topLeft();}
    QPointF          get_eventPosition(QEvent *e);
    void             set_startIn(QPoint topleft, QPoint bottomright){myMoveStartTopLeft = topleft; myMoveStartBottomRight = bottomright;}

    int              get_actualHeight(){if(!myFrame) return 0; return myFrame->height();}
    int              get_actualWidth(){if(!myFrame) return 0; return myFrame->width();}

    virtual bool     receiveEvent(QEvent *event);

protected:
    bool              isStartIn;
    bool              isMoveOn;
    int               myMoveStartFlag;  //for move
    QPointF           myMoveStartPos;
    QPoint            myMoveStartTopLeft;
    QPoint            myMoveStartBottomRight;

    QPointF           myTopLeft;
    QFrame            *myFrame;
    QWidget           *myWidget;
    coreButton        *myMoveButton;

    virtual bool      on_start(QEvent *e);
    virtual bool      on_update(QEvent *e);
    virtual bool      on_finish(QEvent *e);
};

#endif // COREWIDGET_H
