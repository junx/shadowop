#ifndef OPTIMIZATION_H
#define OPTIMIZATION_H

#include "ShadowCoreSystem.h"
#include "HLBFGS.h"
#include "Lite_Sparse_Matrix.h"




inline void
evalfunc(int N, double *x, double *, double *f, double *g, const Matrixd &optimizationMatrix)
{
    *f = 0;
    for(int i = 0; i < N; i++)
        g[i] = 0;
    for(int i = 0; i < optimizationMatrix.M_width; i++)
    {
        for(int j = 0; j < optimizationMatrix.M_height; j++)
        {
            double a = 1, b = 1;
            if(i > 0) a = x[i-1];
            if(j > 0) b = x[j-1];
            *f += optimizationMatrix[i][j]*a*b;
            if(i > 0)
                g[i-1] += optimizationMatrix[i][j]*b;
            if(j > 0)
                g[j-1] += optimizationMatrix[i][j]*a;
        }
    }
}

inline void
newiteration(int /*iter*/, int /*call_iter*/, double */*x*/, double* /*f*/, double */*g*/,  double* /*gnorm*/)
{
}

inline void
evalfunc_h(int /*N*/, double */*x*/, double */*prev_x*/, double */*f*/, double */*g*/, HESSIAN_MATRIX& /*hessian*/)
{
}

inline double
Optimize_by_HLBFGS(int N, double *init_x, int num_iter, int M, int T, bool with_hessian, const Matrixd &value)
{
    double parameter[21]; //added one
    int info[21];  //added one
    //initialize
    INIT_HLBFGS(parameter, info);
    info[4] = num_iter;
    info[6] = T;
    info[7] = with_hessian?1:0;
    info[10] = 0;
    info[11] = 1;

    if (with_hessian)
    {
        return HLBFGS(N, M, init_x, evalfunc, evalfunc_h, HLBFGS_UPDATE_Hessian, newiteration, parameter, info, value);
    }
    else
    {
        return HLBFGS(N, M, init_x, evalfunc, 0, HLBFGS_UPDATE_Hessian, newiteration, parameter, info, value);
    }
}


inline qreal
optimization_HLBFGS(QList<QPointF> &cKeys, const Matrixd &optimizationMatrix)
{
    int kSize = cKeys.size();
    std::vector<double> x(kSize*2);
    for(int j = 0; j < kSize; j++)
    {
        x[2*j] = cKeys[j].x();
        x[2*j+1] = cKeys[j].y();
    }
    int N = 6;
    int M = 7;
    int T = 0;
    qreal cost = Optimize_by_HLBFGS(N, &x[0], 100, M, T, false, optimizationMatrix);

    for(int j = 0; j < kSize; j++)
        cKeys[j] = QPointF(x[2*j], x[2*j+1]);
    return cost;
}


#endif // OPTIMIZATION_H
