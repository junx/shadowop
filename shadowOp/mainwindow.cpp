#include "mainwindow.h"

 MainWindow::MainWindow(QWidget *parent)
     :QMainWindow(parent)
 {
     ui  = new Ui_MainWindow();
     ui->setupUi(this);
     uiSize = this->size();

     uiFrame             = ui->frame;
     uiFrame->setFixedSize(uiSize);

     myMainCanvas        = ui->mainCanvas;
     myToolWidget        = ui->toolWidget;
     myParamWidget       = ui->paramWidget;
     mySettingWidget     = ui->settingWidget;

     ShadowCoreSystem::setGlobalColor();
     ShadowCoreSystem::setPaintingParam(myParamWidget->get_param());
     setCursor(get_cursor(QColor(Qt::black), ShadowCoreSystem::getPaintingParam()->size*2));

     //<<<<<<<<<selector>>>>>>>>>//
     mySelector =  new Selector();
     ShadowCoreSystem::setSelector(mySelector);
     //<<<<<<<<<<window>>>>>>>>>>//
     ShadowCoreSystem::setMainWindow(this);
     //<<<<<<<<<<<canvas>>>>>>>>>//
     myMainCanvasHandle = new CanvasHandle(myMainCanvas);
     ShadowCoreSystem::setMainCanvas(myMainCanvas);
     //<<<<<<<<<operationer>>>>>>//
     myOperationer = new Operationer(myMainCanvas);
     ShadowCoreSystem::setOperationer(myOperationer);
     //<<<<<<<<<analysiser>>>>>>>>//
     myAnalysiser = new Analysiser(myOperationer);
     ShadowCoreSystem::setAnalysiser(myAnalysiser);
     //<<<<<<<<<synthesiser>>>>>>>//
     mySynthesiser = new Synthesiser(myOperationer);
     ShadowCoreSystem::setSynthesiser(mySynthesiser);

     myToolWidget->update();
     mySettingWidget->update();

     //<<<<<<<<connect ui>>>>>>>>>//
     connect(myToolWidget->tool_color_picker, SIGNAL(value_changed(QColor)), myParamWidget, SLOT(on_color(QColor)));
     connect(myToolWidget, SIGNAL(on_undo()), myOperationer, SLOT(onUndo()));
     connect(myToolWidget, SIGNAL(on_redo()), myOperationer, SLOT(onRedo()));
     connect(myParamWidget, SIGNAL(changed_param(OpParam*)), this, SLOT(onPaintingParam(OpParam*)));
     connect(myToolWidget, SIGNAL(on_leave(ulong)), this, SLOT(onLeaveApplication(ulong)));
     connect(myToolWidget, SIGNAL(on_enter(ulong)), this, SLOT(onEnterApplication(ulong)));
     connect(myOperationer, SIGNAL(onFinishOperation(int)), myAnalysiser, SLOT(on_finish_operation(int)), Qt::DirectConnection);
     connect(myOperationer, SIGNAL(onDeleteOperation(int)), mySynthesiser, SLOT(onRemove(int)), Qt::DirectConnection);
     connect(myOperationer, SIGNAL(onUndoOperation(int)), mySynthesiser, SLOT(onUpdate(int)), Qt::DirectConnection);

     createActions();
     createMenus();

     setWindowTitle(tr("RepOp"));
     setFocusProxy(myMainCanvas);
     QPalette *p = new QPalette;
     p->setColor(QPalette::Background, Qt::lightGray);
     this->setAutoFillBackground(true);
     this->setPalette(*p);
 }


 void
 MainWindow::onLeaveApplication(ulong state)
 {
     switch (state) {
     case SYSTEM_SETTING_TOOL:
         myMainCanvasHandle->on_backgroundAdjust(QPointF(0,0), -2);
       //  mySettingWidget->hide();
         mySettingWidget->update();
         break;
     default:
         break;
     }
 }


 void
 MainWindow::onEnterApplication(ulong state)
 {
     ShadowCoreSystem::setApplicationState(state);
     switch (state) {
     case SYSTEM_SETTING_TOOL:
      //   mySettingWidget->show();
         mySettingWidget->update();
         break;
     default:
         break;
     }
 }


 void
 MainWindow::onPaintingParam(OpParam *param)
 {
     ShadowCoreSystem::setPaintingParam(param);
 }


 bool
 MainWindow::open_setting()
 {
     QString path = QDir::currentPath() + "/animation";
     QString baseName = path; // QFileDialog::getSaveFileName(this, tr("Specify the file name"), path);
     ShadowCoreSystem::setOpFileName(baseName);
     QString fileName = baseName;
     QFile *file = new QFile(fileName.append(".txt"));
     if(!file->open(QIODevice::WriteOnly | QIODevice::Text))
         return false;
     ShadowCoreSystem::setOpFile(file);
     QTextStream *writeStream = new QTextStream(file);
     if(!writeStream) return false;
     ShadowCoreSystem::setOpWriteStream(writeStream);
     *writeStream<<"recorded operation history of "<<baseName<<"\n\n";
     return true;
 }


 bool
 MainWindow::close_setting()
 {
     ShadowCoreSystem::getOpFile()->close();
     delete ShadowCoreSystem::getOpFile();
     myOperationer->saveOperations(ShadowCoreSystem::getOpFileName());
     return true;
 }


 void
 MainWindow::tabletEvent(QTabletEvent *e)
 {
     if(!ACTIVETABLET) return;
     if(!myToolWidget->receiveEvent(e))
         if(!myParamWidget->receiveEvent(e))
             if(!mySettingWidget->receiveEvent(e))
                  if(!myMainCanvas->receiveEvent(e))
                       if(!ShadowCoreSystem::getSystemPlayOn())
                             myMainCanvasHandle->receiveEvent(e);
}


void
MainWindow::mousePressEvent(QMouseEvent *e)
{
    if(!ACTIVEMOUSE) return;
    if(!myToolWidget->receiveEvent(e))
        if(!myParamWidget->receiveEvent(e))
            if(!mySettingWidget->receiveEvent(e))
                 if(!myMainCanvas->receiveEvent(e))
                      if(!ShadowCoreSystem::getSystemPlayOn())
                            myMainCanvasHandle->receiveEvent(e);
}


void
MainWindow::mouseMoveEvent(QMouseEvent *e)
{
    if(!ACTIVEMOUSE) return;
    if(!myToolWidget->receiveEvent(e))
        if(!myParamWidget->receiveEvent(e))
            if(!mySettingWidget->receiveEvent(e))
                 if(!myMainCanvas->receiveEvent(e))
                      if(!ShadowCoreSystem::getSystemPlayOn())
                            myMainCanvasHandle->receiveEvent(e);
}


void
MainWindow::mouseReleaseEvent(QMouseEvent *e)
{
    if(!ACTIVEMOUSE) return;
    if(!myToolWidget->receiveEvent(e))
        if(!myParamWidget->receiveEvent(e))
            if(!mySettingWidget->receiveEvent(e))
                 if(!myMainCanvas->receiveEvent(e))
                      if(!ShadowCoreSystem::getSystemPlayOn())
                            myMainCanvasHandle->receiveEvent(e);
}


 void
 MainWindow::keyPressEvent(QKeyEvent *event)
 {
     switch (event->key())
     {
         case Qt::Key_Shift:
         {
              ShadowCoreSystem::setShiftKey(true);
              break;
         }
         case Qt::Key_Control:
         {
              ShadowCoreSystem::setCommandKey(false);
              break;
         }
         case Qt::Key_B:
         {//background visible
              bool isVis = myMainCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapVisible();
              myMainCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND)->set_pixmapVisible(!isVis);
              break;
         }
         case Qt::Key_U:
         {
             myOperationer->onUndo();
             break;
         }
         case Qt::Key_R:
         {
             myOperationer->onRedo();
             break;
         }
         case Qt::Key_S:
         {
             ShadowCoreSystem::setSystemSelectBrushOn(!ShadowCoreSystem::getSystemSelectBrushOn());
             mySynthesiser->onSelectionBrush();
             break;
         }
         case Qt::Key_D:
         {
             ShadowCoreSystem::getSynthesiser()->onAutoComplete();
             break;
         }
         default:
             break;
     }
 }


 void
 MainWindow::keyReleaseEvent(QKeyEvent *event)
 {
     switch (event->key())
     {
         case Qt::Key_Shift:
             ShadowCoreSystem::setShiftKey(false);
             break;
         case Qt::Key_Control:
             ShadowCoreSystem::setCommandKey(false);
             break;
         case Qt::Key_Space:
         {
            myAnalysiser->onPropagation();
            break;
         }
         case Qt::Key_Left:
         {
             break;
         }
         case Qt::Key_Right:
         {
              break;
         }
         default:
             break;
     }
 }

 void MainWindow::resetAct()
 {
     myMainCanvas->reset();
 }


 void MainWindow::aboutAct()
 {
     QMessageBox::about(this, tr("About Tablet Example"),
                        tr("This example shows use of a Wacom tablet in Qt"));
 }

 void MainWindow::createActions()
 {
     exitAction = new QAction(tr("E&xit"), this);
     exitAction->setShortcuts(QKeySequence::Quit);
     exitAction->setShortcut(tr("Command+Q"));
     connect(exitAction, SIGNAL(triggered()),
             this, SLOT(close()));

     loadAction = new QAction(tr("&Load Image..."), this);
     loadAction->setShortcut(tr("L"));
     loadAction->setShortcuts(QKeySequence::Open);
     connect(loadAction, SIGNAL(triggered()),
             this, SLOT(loadImageAct()));

     loadAction2 = new QAction(tr("&Load Operation..."), this);
     loadAction2->setShortcut(tr("Ctrl+L"));
     connect(loadAction2, SIGNAL(triggered()),
             this, SLOT(loadOperationAct()));

     saveAction = new QAction(tr("&Save As..."), this);
     saveAction->setShortcut(tr("Ctrl+S"));
     connect(saveAction, SIGNAL(triggered()),
             this, SLOT(saveAllAct()));

     resetAction = new QAction(tr("&Clear"), this);
     saveAction->setShortcut(tr("Ctrl+C"));
     connect(resetAction, SIGNAL(triggered()), this, SLOT(resetAct()));

     aboutAction = new QAction(tr("A&bout"), this);
     connect(aboutAction, SIGNAL(triggered()),
             this, SLOT(aboutAct()));

     aboutQtAction = new QAction(tr("About &Qt"), this);
     connect(aboutQtAction, SIGNAL(triggered()),
             qApp, SLOT(aboutQt()));
 }

 void MainWindow::createMenus()
 {
     fileMenu = menuBar()->addMenu(tr("&File"));
     fileMenu->addAction(loadAction);
     fileMenu->addAction(loadAction2);
     fileMenu->addAction(saveAction);
     fileMenu->addAction(resetAction);
     fileMenu->addSeparator();
     fileMenu->addAction(exitAction);

     helpMenu = menuBar()->addMenu("&Help");
     helpMenu->addAction(aboutAction);
     helpMenu->addAction(aboutQtAction);
 }


