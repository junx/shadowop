#ifndef OPNEIGHBORABLE_H
#define OPNEIGHBORABLE_H

#include "globalFunction.h"

#define TEMP_NEIGHBORHOOD        0
#define SPATIAL_NEIGHBORHOOD     1
#define STRUCT_NEIGHBORHOOD      2
#define CONTEXT_NEIGHBORHOOD     3
#define PROPAGATION_NEIGHBORHOOD 4
#define NEIGHBORHOOD_SIZE        5

class OpNeighborable;
//<<<<<<<<<<<<<<to store the shape infor>>>>>>>>>>>>>>//
class OpNeighbor : public coreNeighbor
{
public:
    Matrixcs             relation_shape;
    Matrixf              relation_angle;
    Matrixf              relation_weight;

    OpNeighbor(const OpNeighbor &n);
    OpNeighbor(coreNeighborable *myNeighbor, coreNeighborable* otherNeighbor, bool isR);

    inline bool      get_relative(){return get_bit(4);}
    inline void      set_relative(bool a){ set_bit(a, 4);}

    virtual bool     update(coreNeighborable *myN);
    virtual qreal    compute_similarity(coreNeighbor *otherNeighbor, int wSize, qreal *wList);
};


class OpNeighborable : public coreNeighborable
{
public:
    //<<<<<<<<key position info>>>>>>>>>//
    QList<short>         myKeyPointIndexs;
    QList<float>         myKeyDirs;
    QPointF              myKeyPointAve;
    float                myKeyDirAve;
    //<<<<<<<<<neighborhood infor>>>>>>>//
    QList<OpNeighbor>    mySpaceNeighbors; //inter repunit neighbor
    QList<OpNeighbor>    myTempNeighbors;  //inner repunit neighbor

    OpNeighborable():coreNeighborable(){}
    OpNeighborable(const OpNeighborable &neighbor);
    virtual OpNeighborable* copy(){OpNeighborable *op = new OpNeighborable(*this); return op;}

    virtual int     getNeighborhoodSize(int id);
    virtual QList<coreNeighbor*>   getNeighborhoods(int id);
    virtual coreNeighbor*  getNeighborhood(int id1, int id2);
    virtual bool    updateNeighbors();
    virtual bool    checkValid();

    void            initial();
    void            update(QList<QPointF> nlist);

    QList<QPointF>  get_keyPoints();
    QPointF         get_keyPoint(int id){return get_GridPoint(myKeyPointIndexs[id]);}
    QPointF         get_keyPointAve(){return myKeyPointAve;}
    QList<float>    get_keyDirections(){return myKeyDirs;}
    float           get_keyDirection(int id){return myKeyDirs[id];}
    float           get_keyDirectionAve(){return myKeyDirAve;}

    virtual void    find_temporal_neighborhood_core(QList<coreNeighborable *>, int, qreal *);
    virtual void    find_spatial_neighborhood_core(QList<coreNeighborable *>, int, qreal *);

    virtual qreal   compute_similarity_neighbor_core(coreNeighborable *otherNeighbor, QVector<short>& assignment, int wSize, qreal *wList);
    virtual qreal   compute_similarity_self_core(coreNeighborable* otherNeighbor, int wSize, qreal *wList);
};


#endif // OPNEIGHBORABLE_H
