#include "cxNeighborable.h"
#include "operation.h"

CxNeighbor::CxNeighbor(const CxNeighbor &n)
    :coreNeighbor(n)
{
    myCutPointIndexs = n.myCutPointIndexs;
    relative_shapes = n.relative_shapes;
    relative_angles = n.relative_angles;
    relative_weights = n.relative_weights;
}


CxNeighbor::CxNeighbor(coreNeighborable *myNeighbor, coreNeighborable *otherNeighbor, bool isT)
    :coreNeighbor()
{
    set_structural(isT);
    neighbor = 0;
    if(!myNeighbor || !otherNeighbor || otherNeighbor->get_GridPointSize() < 3)
        return;
    neighbor = otherNeighbor;
    if(!update(myNeighbor))
       neighbor = 0;
}


bool
CxNeighbor::update(coreNeighborable *myN)
{
    CxNeighborable *myNeighbor = dynamic_cast<CxNeighborable*>(myN);
    CxNeighborable *otherNeighbor = dynamic_cast<CxNeighborable*>(neighbor);
    if(!myNeighbor || !otherNeighbor) return false;

    QList<QPointF> keys = myNeighbor->get_ContextPoints();
    QList<float> dirs = myNeighbor->get_ContextDirections();
    QList<QPointF> ckeys = otherNeighbor->get_GridPoints();

    int size = myNeighbor->myContextPointIndexs.size();

    myCutPointIndexs = QVector<short>(size, -1);
    relative_shapes = QVector<Vec2s>(size, Vec2s(0,0));
    relative_angles = QVector<float>(size, 0);
    relative_weights = QVector<float>(size, 1);

    int validSize = 0;
    qreal minDis = MAXVALUE;
    for(int k = 0; k < size; k++)
    {
        qreal dis2 = MAXVALUE;
        int  minIndex = Core2<QList, QPointF>::j_nearest(ckeys, keys[k], true, &dis2);
        if(dis2 < minDis)
            minDis = dis2;

        QVector2D cdir = j_to_direction(ckeys, minIndex, 5);
        float cangle = j_to_angle(cdir);  //cut angle in the context
        //<<<<<<<<<<<<<relative direction>>>>>>>>>>//
        float rdir = j_to_angle(dirs[k] - cangle);
        relative_angles[k] = rdir;
        //<<<<<<<<<<<<<pend distance>>>>>>>>>>>>>>>//
        Vec2s p = j_to_vec2s(j_rotate(keys[k]-ckeys[minIndex], QVector2D(cdir.x(), -cdir.y())));
        relative_shapes[k] = p;
        relative_weights[k] = j_weight_distance(p);

        if(!get_structural())  //this is context op
        {
            float langle = j_to_angle(ckeys[minIndex], keys[k]);  //angle of *vertical* line connecting cutPoint and keyPoint
            float vangle = j_to_angle(langle - cangle);   //this angle orthogonal
            if(fabs(fabs(vangle)-1.5708) > 0.5 && dis2 > 100) //not valid projection, minDis*minDis > 100 to compensate sample accuracy
                continue;
        }
        validSize++;
        myCutPointIndexs[k] = minIndex;
    }
    if(validSize <= 0)
        return false;
    if(minDis > 800 && get_structural())
        set_active(false);
    return true;
}


qreal
CxNeighbor::compute_similarity(coreNeighbor *otherN, int, qreal *)
{
    CxNeighbor *otherNeighbor = dynamic_cast<CxNeighbor*>(otherN);
    if(!otherNeighbor || get_structural() != otherNeighbor->get_structural())
        return MAXVALUE;

    qreal sumdis = 0;
    for(int i = 0; i < myCutPointIndexs.size(); i++)
    {
        qreal  dis = j_spatial_distance(relative_shapes[i] - otherNeighbor->relative_shapes[i]);
        //<<<<<<<<distance weight>>>>>>>>>//
        qreal  wdis = j_max(relative_weights[i], otherNeighbor->relative_weights[i]);
        //<<<<<<<<direction weight>>>>>>>>//
        qreal  wdir = j_weight_direction(j_to_angle(relative_angles[i] - otherNeighbor->relative_angles[i]));
        //<<<<<<<<index weight>>>>>>>>//
        qreal  wdex = 1, wsid = 1;
        if(get_structural()) //if local struct context, need to consider full shape
        { //<<<<<<<use this as an approximate to full neighborhood match>>>>>>>//
            qreal id1 = myCutPointIndexs[i]*1.0 / (neighbor->get_GridPointSize()-1);
            qreal id2 = otherNeighbor->myCutPointIndexs[i]*1.0 / (otherNeighbor->neighbor->get_GridPointSize()-1);
            wdex = j_weight_index(id1-id2);
        }
        else
        {
            qreal dis = relative_shapes[i][1] * otherNeighbor->relative_shapes[i][1];
            if(dis < 0) wsid = exp(1);
        }
        sumdis += (wdis*wdir*wdex*wsid*dis);
    }
    sumdis /= myCutPointIndexs.size();

    if(!get_structural())  //if not structural, side penalty is context operation
    {
        qreal sdis = 0;
        qreal fdis = 0;
        for(int i = 0; i < myCutPointIndexs.size(); i++)
        {
            qreal dis = (relative_shapes[i][1] * otherNeighbor->relative_shapes[i][1]);
            sdis += dis;
            fdis += fabs(dis);
        }
        qreal wdir= 0.5 - 0.5*sdis/fdis;
        sumdis += wdir*5;
    }
    return sumdis;
}


CxNeighborable::CxNeighborable(const CxNeighborable &neighbor)
    :OpNeighborable(neighbor)
{
    myContextPointIndexs = neighbor.myContextPointIndexs;
    myContextNeighbors = neighbor.myContextNeighbors;
    myTemporalStructNeighbors = neighbor.myTemporalStructNeighbors;
    mySpatialStructNeighbors = neighbor.mySpatialStructNeighbors;
}


int
CxNeighborable::getNeighborhoodSize(int id)
{
    if(id < 0 || id >= 5) return 0;
    if(id < 2)
        return OpNeighborable::getNeighborhoodSize(id);
    if(id == 2)
        return myTemporalStructNeighbors.size();
    else if(id == 3)
        return myContextNeighbors.size();
    return mySpatialStructNeighbors.size();
}


QList<coreNeighbor*>
CxNeighborable::getNeighborhoods(int id)
{
    QList<coreNeighbor*> neighbors;
    if(id < 0 || id >= 5) return neighbors;
    if(id < 2)
        return OpNeighborable::getNeighborhoods(id);
    if(id == 2)
        for(int i = 0; i < myTemporalStructNeighbors.size(); i++)
            neighbors.append(&myTemporalStructNeighbors[i]);
    else if(id == 3)
        for(int i = 0; i < myContextNeighbors.size(); i++)
            neighbors.append(&myContextNeighbors[i]);
    else
        for(int i = 0; i < mySpatialStructNeighbors.size(); i++)
            neighbors.append(&mySpatialStructNeighbors[i]);
    return neighbors;
}



coreNeighbor*
CxNeighborable::getNeighborhood(int id1, int id2)
{
    if(id1 < 0 || id2 < 0 || id1 >= 5) return 0;
    if(id1 < 2)
        return OpNeighborable::getNeighborhood(id1, id2);
    if(id1 == 2)
    {
        if(id2 >= myTemporalStructNeighbors.size()) return 0;
        return &myTemporalStructNeighbors[id2];
    }
    else if(id1 == 3)
    {
        if(id2 >= myContextNeighbors.size()) return 0;
        return &myContextNeighbors[id2];
    }
    else
    {
        if(id2 >= mySpatialStructNeighbors.size()) return 0;
        return &mySpatialStructNeighbors[id2];
    }
    return 0;
}




bool
CxNeighborable::updateNeighbors()
{
    bool isValid = OpNeighborable::updateNeighbors();
    for(int i = myContextNeighbors.size()-1; i >= 0; i--){
        if(!myContextNeighbors[i].update(this)){
            myContextNeighbors.removeAt(i);
            isValid = false;
        }
    }
    for(int i = myTemporalStructNeighbors.size()-1; i >= 0; i--){
        if(!myTemporalStructNeighbors[i].update(this)){
            myTemporalStructNeighbors.removeAt(i);
            isValid = false;
        }
    }
    return isValid;
}



bool
CxNeighborable::checkValid()
{//solve the problem if possible, report the invalid if necessary
    if(!OpNeighborable::checkValid())
        return false;
    for(int i = 0; i < myContextNeighbors.size(); i++)
        if(!myContextNeighbors[i].get_neighbor()->get_active())
            return false;
    for(int i = 0; i < myTemporalStructNeighbors.size(); i++)
        if(!myTemporalStructNeighbors[i].get_neighbor()->get_active())
            return false;
    for(int i = 0; i < mySpatialStructNeighbors.size(); i++)
        if(!mySpatialStructNeighbors[i].get_neighbor()->get_active())
            return false;
    return true;
}



void
CxNeighborable::initial()
{
    OpNeighborable::initial();

    int contextSize = ShadowCoreSystem::getContextSampleSize();
    QList<QPointF> gList = get_keyPoints();
    assert(gList.size() > 0);
    myContextPointIndexs.clear();
    for(int i = 0; i < contextSize; i++)
    {
        int index = (gList.size()-1.0)*i/(contextSize-1);
        myContextPointIndexs.append(index);
    }
}


void
CxNeighborable::update(QList<QPointF> nlist)
{
    if(nlist.size() != myContextPointIndexs.size()) return;
    //<<<<<<<<<transform methods>>>>>>>>>>>>>//
    QList<QPointF> newKeys;
    switch (0) {
    case 0:
    {//<<<<<<<fast transform>>>>>>//
        QList<QPointF> olist = get_ContextPoints();
        QVector2D r1 = j_to_direction(olist.first(),olist.last());
        QVector2D r2 = j_to_direction(nlist.first(),nlist.last());
        QVector2D rotate = j_rotated(r1, r2);
        newKeys = j_transformLine(get_keyPoints(), olist, nlist, rotate, 1);
        break;
    }
    default:
        break;
    }
    //<<<<<<<<<<<update keysPoints>>>>>>>>>//
    OpNeighborable::update(newKeys);
}



QList<QPointF>
CxNeighborable::get_ContextPoints()
{
    QList<QPointF> cplist;
    for(int i = 0; i < myContextPointIndexs.size(); i++)
         cplist.append(get_keyPoint(myContextPointIndexs[i]));
    return cplist;
}



QList<float>
CxNeighborable::get_ContextDirections()
{
    QList<float> cdlist;
    for(int i = 0; i < myContextPointIndexs.size(); i++)
         cdlist.append(get_keyDirection(myContextPointIndexs[i]));
    return cdlist;
}



void
CxNeighborable::find_context_neighborhood_core(QList<coreNeighborable *> space, int wSize, qreal *wList)
{
    if(space.isEmpty()) return;

    assert(wSize >= 2);
    qreal maxdis = wList[0]*wList[0];
    int   maxsize = int(wList[1]);

    int contextSelectionThreashold = j_min(j_max(get_GridPointSize()*2, 10), 30); //10-30
    for(int i = space.size()-1; i >= 0; i--)
        if(!space[i] || space[i]->get_GridPointSize() <= contextSelectionThreashold)
            space.removeAt(i);

    QList<QPointF> keys = get_ContextPoints();

    QVector<qreal> contextDisList(space.size(), MAXVALUE);
    for(int k = 0; k < keys.size(); k++)
    {
        QVector<qreal> disList(space.size(), MAXVALUE);
        QVector<float> dirList(space.size(), 0);
        int vSize = 0;
        for(int i = 0; i < space.size(); i++)
        {
            qreal minDis = MAXVALUE;
            int minIndex = Core2<QList, QPointF>::j_nearest(space[i]->get_GridPoints(), keys[k], true, &minDis);
            if(minDis > maxdis) continue;
            vSize++;
            disList[i] = minDis;
            float cdir = j_to_angle(space[i]->get_GridPoints(), minIndex, 5);
            dirList[i] = cdir;
        }
        if(vSize == 0) continue;
        QVector<int> indexs = Core1<QVector, qreal>::j_sort(disList, vSize, true);
        QList<float> storedirList;
        for(int i = 0; i < indexs.size(); i++)
        {
            int id = indexs[i];
            assert(disList[id] <= maxdis);
            bool isValid = true;
            for(int j = 0; j < storedirList.size(); j++)
            {
                qreal dis = fabs(j_to_angle(dirList[id] - storedirList[j]));
                if(dis < 0.5)
                {
                    isValid = false;
                    break;
                }
            }
            if(isValid)
            {
                storedirList.append(dirList[id]);
                contextDisList[id] = j_min(contextDisList[id], disList[id]);
            }
        }
    }
    myContextNeighbors.clear();
    QVector<int> indexs = Core1<QVector, qreal>::j_sort(contextDisList, maxsize, true);
    for(int i = 0; i < indexs.size(); i++)
    {
        int index = indexs[i];
        if(contextDisList[index] > maxdis) continue;
        CxNeighbor cNeighbor(this, dynamic_cast<CxNeighborable*>(space[index]), false);
        if(!cNeighbor.get_neighbor()) continue;
        myContextNeighbors.append(cNeighbor);
    }
}


void
CxNeighborable::find_tpstruct_neighborhood_core(QList<coreNeighborable *> space, int, qreal *)
{
    myTemporalStructNeighbors.clear();
    for(int i = 0; i < space.size(); i++)
    {
        CxNeighbor CxN(this, dynamic_cast<CxNeighborable*>(space[i]), true);
        if(!CxN.get_neighbor()) continue;
        myTemporalStructNeighbors.append(CxN);
    }
}


void
CxNeighborable::find_spstruct_neighborhood_core(QList<coreNeighborable *> space, int wSize, qreal *wList)
{
    int neighborId = 2;
    //<<<<<<<<<<<find valid context>>>>>>>>>>//
    for(int i = space.size()-1; i >= 0; i--)
    {        
        bool isValid = false;
        for(int j = 0; j < space[i]->getNeighborhoodSize(neighborId) && !isValid; j++)
        {
            QList<short> omatchIds = space[i]->getNeighborhood(neighborId, j)->get_neighborMatchIds();
            if(omatchIds.isEmpty()) continue;
            isValid = true;
        }
        if(!isValid) space.removeAt(i);
    }

    if(space.isEmpty()) return;
    space = find_neighborhood_core(space, 0, wSize, wList);

    QList<int> idList;
    for(int i = 0; i < space.size(); i++)
        idList.append(space[i]->get_identity());
    QVector<int> cList(space.size(), 1); //one is themself
    for(int i = 0; i < space.size(); i++)
        for(int j = 0; j < space[i]->getNeighborhoodSize(neighborId); j++)
            if(j_findElementInCluster(idList, space[i]->getNeighborhood(neighborId, j)->get_neighbor()->get_identity()) >= 0)
                cList[i]++;
    //<<<<<<<<<find the close ones>>>>>>>>//
    QVector<int> indexs = Core1<QVector, int>::j_sort(cList, cList.size(), false);
    mySpatialStructNeighbors.clear();
    for(int i = 0; i < indexs.size(); i++)
    {
        CxNeighbor CxN(this, dynamic_cast<CxNeighborable*>(space[indexs[i]]), true);
        if(!CxN.get_neighbor()) continue;
        mySpatialStructNeighbors.append(CxN);
    }
}


qreal
CxNeighborable::compute_similarity_neighbor_core(coreNeighborable *otherNeighbor, QVector<short>& assignment, int wSize, qreal *wList)
{
    assert(otherNeighbor);

    assert(wSize >= 4);  //neighborId, activeNeighborId, neighborMatchBias

    int neighborId = wList[0];
    int activeNeighborId = wList[1];
    qreal neighborMatchBias = wList[2];

    QList<coreNeighbor*> myNeighbors = getNeighborhoods(neighborId);
    if(activeNeighborId < 0 || activeNeighborId >= myNeighbors.size()) return 0;
    coreNeighbor *activeNeighbor = myNeighbors[activeNeighborId];

    QList<coreNeighbor*> otherNeighbors = otherNeighbor->getNeighborhoods(neighborId);
    if(otherNeighbors.isEmpty()) return MAXVALUE;
    int size = otherNeighbors.size();

    int minId = -1;
    qreal minDis = MAXVALUE;
    for(int i = 0; i < size; i++)
    {
        if(abs(i-activeNeighborId) > neighborMatchBias) continue;

        bool isUsed = false; //the neighborhood has been matched already
        for(int j = 0; j < assignment.size() && !isUsed; j++)
            if(assignment[j] == i)
                isUsed = true;
        if(isUsed) continue;

        qreal dis = activeNeighbor->compute_similarity(otherNeighbors[i], 0, 0);
        if(dis < minDis)
        {
            minDis = dis;
            minId = i;
        }
    }
    assignment[activeNeighborId] = (minDis<wList[3])?minId:-1;
    return minDis;
}


