#include "opNeighborable.h"


OpNeighbor::OpNeighbor(const OpNeighbor &n)
    :coreNeighbor(n)
{
    relation_shape = n.relation_shape;
    relation_weight = n.relation_weight;
    relation_angle = n.relation_angle;
}


OpNeighbor::OpNeighbor(coreNeighborable *myNeighbor, coreNeighborable* otherNeighbor, bool isR)
    :coreNeighbor()
{
    set_relative(isR);
    neighbor = 0;
    if(!myNeighbor || !otherNeighbor || otherNeighbor->get_GridPointSize() < 3)
        return;
    neighbor = otherNeighbor;
    if(!update(myNeighbor))
        neighbor = 0;
}


bool
OpNeighbor::update(coreNeighborable *myN)
{
    OpNeighborable *myNeighbor = dynamic_cast<OpNeighborable*>(myN);
    OpNeighborable *otherNeighbor = dynamic_cast<OpNeighborable*>(neighbor);
    if(!myNeighbor || !otherNeighbor) return false;

    QList<QPointF> keys1 = myNeighbor->get_keyPoints();
    QList<float> dirs1 = myNeighbor->get_keyDirections();
    QList<QPointF> keys2 = otherNeighbor->get_keyPoints();
    QList<float> dirs2 = otherNeighbor->get_keyDirections();
    float aveDir1 = myNeighbor->get_keyDirectionAve();
    QVector2D indir = j_to_direction(-aveDir1);

    int size = keys1.size();

    relation_angle = Matrixf(size, size, 0);
    relation_shape = Matrixcs(size, size, Vec2s(0,0));
    relation_weight = Matrixf(size, size, 1);

    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            relation_angle[i][j] = j_to_angle(dirs2[i] - dirs1[j]);
            relation_shape[i][j] = (!get_relative())?j_to_vec2s(keys2[i] - keys1[j]):j_to_vec2s(j_rotate(keys2[i] - keys1[j], indir));
            relation_weight[i][j] = j_weight_distance(relation_shape[i][j]);
        }
    }
    return true;
}


qreal
OpNeighbor::compute_similarity(coreNeighbor *otherN, int, qreal *)
{
    OpNeighbor *otherNeighbor = dynamic_cast<OpNeighbor*>(otherN);
    if(!otherNeighbor || get_relative() != otherNeighbor->get_relative())
        return MAXVALUE;
    //<<<<<<<<<<distribution or conjunction penalty>>>>>>>>>>//
    qreal width = relation_shape.M_width, height = relation_shape.M_height;
    qreal shapeSumDiff = 0;
    bool isR = get_relative();
    for(int i = 0; i < width; i++)
    {
        for(int j = 0; j < height; j++)
        {
            qreal dis = j_spatial_distance(relation_shape[i][j] - otherNeighbor->relation_shape[i][j]);
            qreal wdis = j_max(relation_weight[i][j], otherNeighbor->relation_weight[i][j]);
            qreal wdir = (!isR)?1:j_weight_direction(j_to_angle(relation_angle[i][j] - otherNeighbor->relation_angle[i][j]));
            shapeSumDiff += (wdis*wdir*dis);
        }
    }
    return shapeSumDiff/(width*height);
}


//<<<<<<<<<<<<<core methods>>>>>>>>>>>>>>>>>//
OpNeighborable::OpNeighborable(const OpNeighborable &neighbor)
    :coreNeighborable(neighbor)
{
    myKeyPointIndexs = neighbor.myKeyPointIndexs;
    myKeyDirs = neighbor.myKeyDirs;
    mySpaceNeighbors = neighbor.mySpaceNeighbors;
    myTempNeighbors = neighbor.myTempNeighbors;
}


int
OpNeighborable::getNeighborhoodSize(int id)
{
    if(id < 0 || id >= 2) return 0;
    if(id == 0)
        return myTempNeighbors.size();
    return mySpaceNeighbors.size();
}


QList<coreNeighbor*>
OpNeighborable::getNeighborhoods(int id)
{
    QList<coreNeighbor*> neighbors;
    if(id < 0 || id >= 2) return neighbors;
    if(id == 0)
        for(int i = 0; i < myTempNeighbors.size(); i++)
            neighbors.append(&myTempNeighbors[i]);
    else
        for(int i = 0; i < mySpaceNeighbors.size(); i++)
            neighbors.append(&mySpaceNeighbors[i]);
    return neighbors;
}


coreNeighbor*
OpNeighborable::getNeighborhood(int id1, int id2)
{
    if(id1 < 0 || id2 < 0 || id1 >= 2) return 0;
    if(id1 == 0)
    {
        if(id2 >= myTempNeighbors.size()) return 0;
        return &myTempNeighbors[id2];
    }
    else
    {
        if(id2 >= mySpaceNeighbors.size()) return 0;
        return &mySpaceNeighbors[id2];
    }
    return 0;
}



bool
OpNeighborable::updateNeighbors()
{
    bool isValid = coreNeighborable::updateNeighbors();
    for(int i = myTempNeighbors.size()-1; i >= 0; i--){
        if(!myTempNeighbors[i].update(this)){
            myTempNeighbors.removeAt(i);
            isValid = false;
        }
    }
    for(int i = mySpaceNeighbors.size()-1; i >= 0; i--){
        if(!mySpaceNeighbors[i].update(this)){
            mySpaceNeighbors.removeAt(i);
            isValid = false;
        }
    }
    return isValid;
}



bool
OpNeighborable::checkValid()
{//solve the problem if possible, report the invalid if necessary
    if(!coreNeighborable::checkValid())
        return false;
    bool isValid = true;
    for(int i = 0; i < myTempNeighbors.size() && !isValid; i++)
        if(!myTempNeighbors[i].get_neighbor()->get_active())
            isValid = false;
    for(int i = 0; i < mySpaceNeighbors.size() && !isValid; i++)
        if(!mySpaceNeighbors[i].get_neighbor()->get_active())
            isValid = false;
    return isValid;
}


void
OpNeighborable::initial()
{
    coreNeighborable::initial();

    int keySize = ShadowCoreSystem::getKeySampleSize();
    QList<QPointF> gList = get_GridPoints();
    assert(gList.size() > 0);
    myKeyPointIndexs.clear();
    for(int i = 0; i < keySize; i++)
    {
        int index = (gList.size()-1.0)*i/(keySize-1);
        myKeyPointIndexs.append(index);
    }

    QList<QPointF> keys = get_keyPoints();
    myKeyPointAve = Core1<QList, QPointF>::j_ave(keys);

    QList<float> dirs;
    for(int i = 0; i < keySize; i++)
       dirs.append(j_to_angle(keys, i, 1));
    myKeyDirs = dirs;
    float base = dirs[0];
    for(int i = 0; i < dirs.size(); i++)
        dirs[i] = j_to_angle(dirs[i] - base);
    float ave = Core1<QList, float>::j_ave(dirs);
    myKeyDirAve = j_to_angle(ave+base);
}


void
OpNeighborable::update(QList<QPointF> nlist)
{
    assert(nlist.size() == myKeyPointIndexs.size());
    //<<<<<<<<<transform methods>>>>>>>>>>>>>//
    QList<QPointF> newGrids;
    switch (0) {
    case 0:
    {
        QList<QPointF> olist = get_keyPoints();
        QVector2D r1 = j_to_direction(olist.first(),olist.last());
        QVector2D r2 = j_to_direction(nlist.first(),nlist.last());
        QVector2D rotate = j_rotated(r1, r2);
        newGrids = j_transformLine(get_GridPoints(), olist, nlist, rotate, 1);
        break;
    }
    default:
        break;
    }
    //<<<<<<<<<update gridPoints>>>>>>>>>>//
    coreNeighborable::update(newGrids);

    //<<<<<<<<update the gridpoints first>>>>>>>>//
    QList<QPointF> keys = get_keyPoints();
    myKeyPointAve = Core1<QList, QPointF>::j_ave(keys);
    QList<float> dirs;
    for(int i = 0; i < keys.size(); i++)
       dirs.append(j_to_angle(keys, i, 1));
    myKeyDirs = dirs;
    float base = dirs[0];
    for(int i = 0; i < dirs.size(); i++)
        dirs[i] = j_to_angle(dirs[i] - base);
    float ave = Core1<QList, float>::j_ave(dirs);
    myKeyDirAve = j_to_angle(ave+base);
}


QList<QPointF>
OpNeighborable::get_keyPoints()
{
    QList<QPointF> plist;
    for(int i = 0; i < myKeyPointIndexs.size(); i++)
        plist.append(get_GridPoint(myKeyPointIndexs[i]));
    return plist;
}




void
OpNeighborable::find_temporal_neighborhood_core(QList<coreNeighborable *> space, int, qreal *)
{
    myTempNeighbors.clear();
    for(int i = 0; i < space.size(); i++)
    {
        OpNeighbor OpN(this, dynamic_cast<OpNeighborable*>(space[i]), true);
        if(!OpN.get_neighbor()) continue;
        myTempNeighbors.append(OpN);
    }
}


void
OpNeighborable::find_spatial_neighborhood_core(QList<coreNeighborable *> space, int, qreal *)
{
    mySpaceNeighbors.clear();
    for(int i = 0; i < space.size(); i++)
    {
        OpNeighbor neighbor(this, dynamic_cast<OpNeighborable*>(space[i]), false);
        if(!neighbor.get_neighbor()) continue;
        mySpaceNeighbors.append(neighbor);
    }
}



qreal
OpNeighborable::compute_similarity_neighbor_core(coreNeighborable *otherNeighbor, QVector<short>& assignment, int wSize, qreal *wList)
{
    assert(otherNeighbor);
    assert(wSize >= 2);  //neighborId, neighborMatchScale

    int neighborId = wList[0];
    qreal neighborMatchScale = wList[1];
    QList<coreNeighbor*> myNeighbors = getNeighborhoods(neighborId);
    if(myNeighbors.isEmpty()) return 0;
    int size1 = int(neighborMatchScale*myNeighbors.size())?int(neighborMatchScale*myNeighbors.size()):myNeighbors.size();  //primariy size

    QList<coreNeighbor*> otherNeighbors = otherNeighbor->getNeighborhoods(neighborId);
    int size2 = otherNeighbors.size();

    if(size2 < size1) return MAXVALUE;

    Matrixd dis_matrix(size1,size2,MAXVALUE);
    for(int i = 0; i < size1; i++)
    {
        for(int j = 0; j < size2; j++)
        {
            qreal dis  = myNeighbors[i]->compute_similarity(otherNeighbors[j], 0, 0);
            dis_matrix[i][j] = dis;
        }
    }
    assignment = QVector<short>(myNeighbors.size(), -1);
    switch (0) {
    case 0:
    {
        CHungarianAlgorithm calculator;
        calculator.FindOptimalAssignment(assignment, dis_matrix);
        break;
    }
    case 1:
    {
        qreal flags[2] = {100, 1};
        j_findOptimalAssignment(assignment, dis_matrix, 2, flags);
        break;
    }
    default:
        break;
    }

    qreal cost = 0.0;
    int validSize = 0;
    for(int i = 0; i < myNeighbors.size(); i++)
    {
        if(assignment[i] < 0 || dis_matrix[i][assignment[i]] >= 1000) continue;
        cost += dis_matrix[i][assignment[i]];
        validSize++;
    }
    return validSize?cost/validSize:MAXVALUE;
}



qreal
OpNeighborable::compute_similarity_self_core(coreNeighborable *otherN, int, qreal *)
{
    OpNeighborable *otherNeighbor = dynamic_cast<OpNeighborable*>(otherN);
    if(!otherNeighbor || myKeyPointIndexs.size() != otherNeighbor->myKeyPointIndexs.size())
        return MAXVALUE;
    int size = myKeyPointIndexs.size();

    QList<QPointF> otherKeys = otherNeighbor->get_keyPoints();
    QList<float>   otherDirs = otherNeighbor->get_keyDirections();
    QList<QPointF> myKeys = get_keyPoints();
    QList<float>   myDirs = get_keyDirections();
    float otherAveDir = otherNeighbor->get_keyDirectionAve();
    float myAveDir = get_keyDirectionAve();
    QPointF otherAveKey = otherNeighbor->get_keyPointAve();
    QPointF myAveKey = get_keyPointAve();
    QVector2D rotate = j_to_direction(otherAveDir-myAveDir);

    qreal sumDis = 0;
    for(int i = 0; i < size; i++)
    {
        //<<<<<<<<<<<distance>>>>>>>>>//
        QPointF diff1 = otherKeys[i] - otherAveKey;
        QPointF diff2 = myKeys[i] - myAveKey;
        QPointF diff = j_rotate(diff2, rotate) - diff1;
        qreal dis = j_spatial_distance(diff);
        //<<<<<<<<<distance weight>>>>>>>//
        qreal wdis = j_weight_distance(j_min(j_mag1(diff1), j_mag1(diff2)));
        //<<<<<<<<direction weight>>>>>>>//
        float dir1 = otherDirs[i] - otherAveDir;
        float dir2 = myDirs[i] - myAveDir;
        qreal wdir = j_weight_direction(j_to_angle(dir1 - dir2));

        sumDis += wdis*wdir*dis;
    }
    return sumDis/size;
}
