#ifndef CXNEIGHBORABLE_H
#define CXNEIGHBORABLE_H

#include "opNeighborable.h"

class CxNeighborable;

//<<<<<<<<<<<<<<to store the context infor>>>>>>>>>>>>>>//
class CxNeighbor : public coreNeighbor
{
public:
    QVector<short>       myCutPointIndexs;
    QVector<Vec2s>       relative_shapes;
    QVector<float>       relative_angles;
    QVector<float>       relative_weights;

#ifdef OPTIMIZATIONPARAMTEST
    QList<float>         optimizationParams;
#endif

    CxNeighbor(const CxNeighbor &n);
    CxNeighbor(coreNeighborable *myNeighbor, coreNeighborable *otherNeighbor, bool isT);

    inline bool      get_structural(){return get_bit(5);}
    inline void      set_structural(bool a){set_bit(a, 5);}

    virtual bool      update(coreNeighborable *myN);
    virtual qreal     compute_similarity(coreNeighbor *otherNeighbor, int wSize, qreal *wList);
};


inline QList<CxNeighbor*>
coreNeighbor_to_cxNeighbor(QList<coreNeighbor*> neighbors)
{
    QList<CxNeighbor*> newNeighbors;
    for(int i = 0; i < neighbors.size(); i++)
        newNeighbors.append(static_cast<CxNeighbor*>(neighbors[i]));
    return newNeighbors;
}


class CxNeighborable : public OpNeighborable
{
public:
    QList<short>            myContextPointIndexs;  //context control points indexed from keypoints, size equals to CONTEXT_CONTROL_POINT_SIZE
    QList<CxNeighbor>       myContextNeighbors;
    QList<CxNeighbor>       myTemporalStructNeighbors;
    QList<CxNeighbor>       mySpatialStructNeighbors;

#ifdef OPTIMIZATIONPARAMTEST
    QList<float>            optimizationParams;
#endif

    CxNeighborable():OpNeighborable(){}
    CxNeighborable(const CxNeighborable &neighbor);
    virtual CxNeighborable* copy(){CxNeighborable *op = new CxNeighborable(*this); return op;}

    virtual int     getNeighborhoodSize(int id);
    virtual QList<coreNeighbor*>   getNeighborhoods(int id);
    virtual coreNeighbor* getNeighborhood(int id1, int id2);
    virtual bool    updateNeighbors();
    virtual bool    checkValid();

    void            initial();
    void            update(QList<QPointF> nlist);

    QList<QPointF>  get_ContextPoints();
    QPointF         get_ContextPoint(int id){return get_keyPoint(myContextPointIndexs[id]);}
    QList<float>    get_ContextDirections();
    float           get_ContextDirection(int id){return get_keyDirection(myContextPointIndexs[id]);}

    virtual void   find_context_neighborhood_core(QList<coreNeighborable *>, int, qreal *);
    virtual void   find_tpstruct_neighborhood_core(QList<coreNeighborable *>, int, qreal *);
    virtual void   find_spstruct_neighborhood_core(QList<coreNeighborable *>, int, qreal *);

    virtual qreal  compute_similarity_neighbor_core(coreNeighborable *otherNeighbor, QVector<short>& assignment, int wSize, qreal *wList);
};


#endif // CXNEIGHBORABLE_H
