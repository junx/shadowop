#include "ShadowCoreSystem.h"

//<<<<<<<<system parameters>>>>>>//
bool ShadowCoreSystem::isSystemFuncOn = true;
bool ShadowCoreSystem::isSystemTestOn = false;
bool ShadowCoreSystem::isSystemDemoOn = false;
bool ShadowCoreSystem::isSystemPlayOn = false;
bool ShadowCoreSystem::isSystemSelectOn = false;
bool ShadowCoreSystem::isSystemSelectBrushOn = false;
bool ShadowCoreSystem::isPenDown     = false;
bool ShadowCoreSystem::isShifted     = false;
bool ShadowCoreSystem::isCommand     = false;
QVector<QColor> ShadowCoreSystem::globalColors = QVector<QColor>(20);
QVector<int> ShadowCoreSystem::globalCounts = QVector<int>(10, 0); //ten separate counts
QVector<bool> ShadowCoreSystem::myNeighborTest = QVector<bool>(6, false);
//<<<<<<<<<<IO>>>>>>>>>>>//
QString ShadowCoreSystem::recordFileName = "";
QFile* ShadowCoreSystem::recordFile = 0;
QTextStream * ShadowCoreSystem::recordReadStream = 0;
QTextStream * ShadowCoreSystem::recordWriteStream = 0;

//<<<<<<<application>>>>>>//
ulong ShadowCoreSystem::application_state = SYSTEM_SETTING_TOOL;
OpParam* ShadowCoreSystem::painting_param = 0;
int ShadowCoreSystem::myKeySampleSize = 3;
int ShadowCoreSystem::myContextSampleSize = 2;
//<<<<<<<application tools>>>>>>//
MainWindow*    ShadowCoreSystem::myMainWindow = 0;
coreWidgetCanvas* ShadowCoreSystem::myMainCanvas = 0;
Operationer*   ShadowCoreSystem::myOperationer = 0;
Analysiser*    ShadowCoreSystem::myAnalysiser = 0;
Synthesiser*   ShadowCoreSystem::mySynthesiser = 0;
Selector*      ShadowCoreSystem::mySelector = 0;


void
ShadowCoreSystem::setGlobalColor()
{
    globalColors[0] = QColor(255, 0, 0);
    globalColors[1] = QColor(0, 255, 0);
    globalColors[2] = QColor(0, 0, 255);
    globalColors[3] = QColor(255, 255, 0);
    globalColors[4] = QColor(255, 0, 255);
    globalColors[5] = QColor(0, 255, 255);
    globalColors[6] = QColor(237, 87, 253);
    globalColors[7] = QColor(66, 75, 80);
    globalColors[8] = QColor(179, 109, 97);
    globalColors[9] = QColor(212, 242, 232);
    globalColors[10] = QColor(179, 39, 43);
    globalColors[11] = QColor(169, 129, 117);
    globalColors[12] = QColor(255, 51, 0);
    globalColors[13] = QColor(212, 242, 232);
    globalColors[14] = QColor(23, 124, 176);
    globalColors[15] = QColor(215, 236, 241);
    globalColors[16] = QColor(65, 80, 101);
    globalColors[17] = QColor(168, 226, 197);
    globalColors[18] = QColor(118, 102, 77);
    globalColors[19] = QColor(239, 222, 176);
}
