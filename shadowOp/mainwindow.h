#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "ui.h"
#include "operationer.h"
#include "analysiser.h"
#include "synthesiser.h"
#include "selector.h"

class QAction;
class QActionGroup;
class QMenu;
class QStatusBar;


//<<<<don't active both at the same time>>>>>//
#define ACTIVETABLET false
#define ACTIVEMOUSE  true

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow(){delete ui;}

    coreWidgetCanvas   *myMainCanvas;
    coreWidgetTool     *myToolWidget;
    coreWidgetParam    *myParamWidget;
    coreWidgetSetting  *mySettingWidget;

    CanvasHandle       *myMainCanvasHandle;
    Operationer        *myOperationer;
    Analysiser         *myAnalysiser;
    Synthesiser        *mySynthesiser;
    Selector           *mySelector;

  //  void    onCursor(qreal width);
    bool    open_setting();
    bool    close_setting();

protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void tabletEvent(QTabletEvent *event);

    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);

public slots:
    void onLeaveApplication(ulong);
    void onEnterApplication(ulong);
    void onPaintingParam(OpParam*);

private slots:
    void saveAllAct(){QString fileName = QFileDialog::getSaveFileName(this, tr("Open Picture"), QDir::currentPath()); myOperationer->saveOperations(fileName);}
    void loadImageAct(){QString fileName = QFileDialog::getOpenFileName(this, tr("Open Picture"), QDir::currentPath());}
    void loadOperationAct(){QString fileName = QFileDialog::getOpenFileName(this, tr("Open Picture"), QDir::currentPath()); myOperationer->loadOperations(fileName);}
    void resetAct();
    void aboutAct();

private:
    void createActions();
    void createMenus();

    Ui_MainWindow  *ui;
    QFrame         *uiFrame;
    QSize           uiSize;

    QAction *exitAction;
    QAction *saveAction;
    QAction *loadAction;
    QAction *loadAction2;
    QAction *resetAction;

    QAction *aboutAction;
    QAction *aboutQtAction;

    QMenu *fileMenu;
    QMenu *helpMenu;
};

#endif // MAINWINDOW_H
