#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    bool is_test = true;
    if(is_test && !w.open_setting())
    {
        a.exit(0);
        return 0;
    }
    int value =  a.exec();
    if(is_test)
        w.close_setting();
    return value;
}
