#ifndef SHADOWCORESYSTEM_H
#define SHADOWCORESYSTEM_H

#include "globalFunction.h"

class OpParam;
class MainWindow;
class Operationer;
class Analysiser;
class Synthesiser;
class coreWidgetCanvas;
class PixmapHandle;
class Selector;

class ShadowCoreSystem : QObject
{
    Q_OBJECT
private:
    //<<<<<<<<<<<<<<<<system paramters>>>>>>>>>>>>//
	static bool                    isSystemFuncOn;
    static bool                    isSystemTestOn;
    static bool                    isSystemDemoOn;
    static bool                    isSystemPlayOn;
    static bool                    isSystemSelectOn;
    static bool                    isSystemSelectBrushOn;

    static bool                    isPenDown;
    static bool                    isShifted;
    static bool                    isCommand;

    static QVector<int>            globalCounts;
    static QVector<QColor>         globalColors;
    static QVector<bool>           myNeighborTest;

    //<<<<<<<<<<<<<<<<<IO stream>>>>>>>>>>>>>>>>//
    static QString                 recordFileName;
    static QFile*                  recordFile;
    static QTextStream*            recordReadStream;
    static QTextStream*            recordWriteStream;

    //<<<<<<<<<<<<application paramters>>>>>>>>>>//
    static ulong                   application_state;
    static OpParam                *painting_param;
    static int                     myKeySampleSize;
    static int                     myContextSampleSize;

    //<<<<<<<<<<<application handles>>>>>>>>>>>>//
    static MainWindow             *myMainWindow;
    static coreWidgetCanvas       *myMainCanvas;
    static Operationer            *myOperationer;
    static Analysiser             *myAnalysiser;
    static Synthesiser            *mySynthesiser;
    static Selector               *mySelector;

public:
    //<<<<<<<<<<System parameters>>>>>>//
    static void                   setSystemFuncOn(bool active){isSystemFuncOn = active;}
    static bool                   getSystemFuncOn(){return isSystemFuncOn;}
    static void                   setSystemTestOn(bool active){isSystemTestOn = active;}
    static bool                   getSystemTestOn(){return isSystemTestOn;}
    static void                   setSystemDemoOn(bool active){isSystemDemoOn = active;}
    static bool                   getSystemDemoOn(){return isSystemDemoOn;}
    static void                   setSystemPlayOn(bool active){isSystemPlayOn = active;}
    static bool                   getSystemPlayOn(){return isSystemPlayOn;}
    static void                   setSystemSelectBrushOn(bool active){isSystemSelectBrushOn = active;}
    static bool                   getSystemSelectBrushOn(){return isSystemSelectBrushOn;}
    static void                   setShiftKey(bool active){isShifted = active;}
    static bool                   getShiftKey(){return isShifted;}
    static void                   setCommandKey(bool active){isCommand = active;}
    static bool                   getCommandKey(){return isCommand;}
    static void                   setPenDown(bool active){isPenDown = active;}
    static bool                   getPenDown(){return isPenDown;}
    static void                   setGlobalColor();
    static QColor                 getGlobalColor(int index){if(index < 0) return QColor(Qt::black); return globalColors[index%20];}
    static int                    getGlobalCount(int index){assert(index >= 0 && index < globalCounts.size()); return globalCounts[index];}
    static void                   setGlobalCount(int id1, int id2){assert(id1 >= 0 && id1 < globalCounts.size()); globalCounts[id1] = id2;}
    static bool                   getNeighborTest(int id){return myNeighborTest[id];}
    static void                   setNeighborTest(int id, bool v){myNeighborTest[id] = v;}
    //<<<<<<<<<<<IO stream>>>>>>>>>>>//
    static QString                getOpFileName(){return recordFileName;}
    static void                   setOpFileName(QString filename){recordFileName = filename;}
    static QFile*                 getOpFile(){return recordFile;}
    static void                   setOpFile(QFile *file){recordFile = file;}
    static QTextStream*           getOpReadStream(){return recordReadStream;}
    static void                   setOpReadStream(QTextStream* in){recordReadStream = in;}
    static QTextStream*           getOpWriteStream(){return recordWriteStream;}
    static void                   setOpWriteStream(QTextStream* out){recordWriteStream = out;}
    //<<<<<<<<<Application parameters>>>>>//
    static void                   setApplicationState(ulong id){application_state = id;}
    static ulong                  getApplicationState(){return application_state;}
    static void                   setPaintingParam(OpParam *param){painting_param = param;}
    static OpParam*               getPaintingParam(){return painting_param;}
    static int                    getKeySampleSize(){return myKeySampleSize;}
    static int                    getContextSampleSize(){return myContextSampleSize;}
    //<<<<<<<<<Application tools>>>>>>>>>>//
    static void                   setMainWindow(MainWindow *win){myMainWindow = win;}
    static MainWindow*            getMainWindow(){return myMainWindow;}
    static void                   setOperationer(Operationer *oper){myOperationer = oper;}
    static Operationer*           getOperationer(){return myOperationer;}
    static void                   setMainCanvas(coreWidgetCanvas *canvas){myMainCanvas = canvas;}
    static coreWidgetCanvas*      getMainCanvas(){return myMainCanvas;}
    static void                   setSynthesiser(Synthesiser *synth){mySynthesiser = synth;}
    static Synthesiser*           getSynthesiser(){return mySynthesiser;}
    static void                   setAnalysiser(Analysiser *analysiser){myAnalysiser = analysiser;}
    static Analysiser*            getAnalysiser(){return myAnalysiser;}
    static void                   setSelector(Selector *selector){mySelector = selector;}
    static Selector*              getSelector(){return mySelector;}
};

#endif // SHADOWANIMSYSTEM_H

