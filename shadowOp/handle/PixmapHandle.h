#ifndef PIXMAPHANDLE_H
#define PIXMAPHANDLE_H

#include "ShadowCoreSystem.h"

#define MASK_COLOR_ANCHOR QColor(Qt::cyan)
#define MASK_COLOR_LINE QColor(Qt::green)
#define MASK_COLOR_CONTROL QColor(Qt::blue)
#define MASK_COLOR_MOVE QColor(Qt::red)
#define MASK_COLOR_HIGHLIGHT QColor(Qt::red)

class Pixmap;
class PixmapHandle
{
    Pixmap                     *myPixmap;

    //<<<<<<<<<interactive drawing>>>>>>>>>>>>>>//
    qreal                       myPathWidth;
    bool                        myIsOut;
    QPointF                     myStartPoint;
    QPointF                     myLastPoint;
    //<<<<<<<<<<fresh pixmap, probably from other (non-GUI) thread>>>>>>>>>>//
    bool                        isFreshReady;
    QImage                      myFreshImage;

public:
    PixmapHandle(Pixmap *pixmap);

    QPainterPath                myPath;
    QList<QPointF>              myPathPoint;
    bool                        myIsCircle;

    void          reset();
    void          clear();

    void          on_start(QEvent *e, qreal width = 2);
    int           on_update(QEvent *e);
    int           on_finish(QEvent *e);

    bool          isUpdateReady(){return isFreshReady;}
    void          prepareUpdate(QImage image){myFreshImage = image; isFreshReady = true;}
    void          freshUpdate();
};


#endif // PIXMAPHANDLE_H
