#ifndef CANVASHANDLE_H
#define CANVASHANDLE_H

#include <QWidget>
#include <QtGui>
#include <QtCore>

#include "coreWidget.h"
#include "pixmap.h"

class coreWidgetCanvas;
class CanvasHandle : public coreWidget
{
public:
    CanvasHandle(coreWidgetCanvas *parent);

    coreWidgetCanvas   *myCanvas;

    bool          receiveEvent(QEvent *event);

    void          on_backgroundScale(int id, qreal scale);
    void          on_backgroundAdjust(QPointF pos, int flag);

protected:
    /*void dragEnterEvent(QDragEnterEvent *e);
    void dragLeaveEvent(QDragLeaveEvent *e);
    void dragMoveEvent(QDragMoveEvent *e);
    void dropEvent(QDropEvent *e);*/
    bool          on_start(QEvent *e);
    bool          on_update(QEvent *e);
    bool          on_finish(QEvent *e);
};

#endif // CANVASHANDLE_H
