#include "canvasHandle.h"
#include "operationer.h"
#include "selector.h"
#include "synthesiser.h"

CanvasHandle::CanvasHandle(coreWidgetCanvas *canvas)
    :coreWidget(0)
{
    myCanvas = canvas;
    canvas->set_handle(this);
}


bool
CanvasHandle::receiveEvent(QEvent *event)
{
    switch (event->type()) {
    case QEvent::TabletPress:
    case QEvent::MouseButtonPress:
    {
        QPointF point = myCanvas->get_eventPosition(event);
        if(point.x() < 0 || point.y() < 0 || point.x() > myCanvas->width() || point.y() > myCanvas->height())
            isStartIn = false;
        else
        {
            isStartIn = true;
            on_start(event);
        }
        break;
    }
    case QEvent::TabletMove:
    case QEvent::MouseMove:
    {
        if(isStartIn)
            on_update(event);
        break;
    }
    case QEvent::TabletRelease:
    case QEvent::MouseButtonRelease:
    {
        if(isStartIn)
            on_finish(event);
        isStartIn = false;
        break;
    }
    default:
        break;
    }
    return isStartIn;
}


bool
CanvasHandle::on_start(QEvent *event)
{
    myCanvas->get_pixmap(CANVASPIXMAP_TEMP)->set_pixmapVisible(true);

    if(ShadowCoreSystem::getShiftKey())
    {
        ShadowCoreSystem::setSystemTestOn(true);
        ShadowCoreSystem::getSelector()->set_griders(ShadowCoreSystem::getOperationer()->get_opGriders());
        ShadowCoreSystem::getSelector()->on_start(event, 1, 20);
    }
    else if(ShadowCoreSystem::getSystemSelectBrushOn())
    {
        ShadowCoreSystem::getSynthesiser()->on_start(event);
    }
    else if(ShadowCoreSystem::getApplicationState() == SYSTEM_SETTING_TOOL)
    {
        myMoveStartFlag = -1;
        Pixmap *pixmap = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND);
        if(!pixmap || !pixmap->get_pixmapVisible())
           return false;
        //<<<<<background move>>>>>>>//
        myMoveStartPos = myCanvas->get_eventPosition(event);
        myMoveStartFlag = get_adjust_flag(pixmap->get_pixmapRect(), myMoveStartPos);
    }
    else
        ShadowCoreSystem::getOperationer()->on_start(event);
    myCanvas->fresh();
    return true;
}


bool
CanvasHandle::on_update(QEvent *event)
{
    if(!myCanvas->get_pixmap(CANVASPIXMAP_TEMP)->get_pixmapVisible())
        return false;
    if(ShadowCoreSystem::getSystemTestOn())
    {
        ShadowCoreSystem::getSelector()->on_update(event);
    }
    else if(ShadowCoreSystem::getSystemSelectBrushOn())
    {
        ShadowCoreSystem::getSynthesiser()->on_update(event);
    }
    else if(ShadowCoreSystem::getApplicationState() == SYSTEM_SETTING_TOOL)
    {
        if(myMoveStartFlag < 0)
            return false;
        //<<<<<background move>>>>>>>//
        QPointF pos = myCanvas->get_eventPosition(event);
        on_backgroundAdjust(pos - myMoveStartPos, myMoveStartFlag);
        myMoveStartPos = pos;
    }
    else
        ShadowCoreSystem::getOperationer()->on_update(event);
    myCanvas->fresh();
    return true;
}


bool
CanvasHandle::on_finish(QEvent *event)
{
    myCanvas->get_pixmap(CANVASPIXMAP_TEMP)->set_pixmapVisible(false);
    if(ShadowCoreSystem::getSystemTestOn())
    {
        ShadowCoreSystem::setSystemTestOn(false);
        ShadowCoreSystem::getSelector()->on_finish(event);
    }
    else if(ShadowCoreSystem::getSystemSelectBrushOn())
    {
        ShadowCoreSystem::getSynthesiser()->on_finish(event);
    }
    else if(ShadowCoreSystem::getApplicationState() == SYSTEM_SETTING_TOOL)
    {}
    else
        ShadowCoreSystem::getOperationer()->on_finish(event);
    myCanvas->fresh();
    return true;
}


void
CanvasHandle::on_backgroundScale(int id, qreal scale)
{
    if(id < 0 || id >= myCanvas->get_pixmapSize()) return;
    if(scale > 2)
        scale = 2;
    else if(scale < 0.2)
        scale = 0.2;
    QRect origR = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUNDORIG)->get_pixmapRect();
    QSize imageS = origR.size() * scale;
    Pixmap *pixmap = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND);
    *(pixmap->get_pixmap()) = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUNDORIG)->get_pixmap()->scaled(imageS);
    QSize topLeft = (myCanvas->size() - imageS)/2;
    pixmap->set_pixmapVisible(true);
    pixmap->set_pixmapRect(QRect(QPoint(topLeft.width(), topLeft.height()), imageS));
    myCanvas->fresh();
}


void
CanvasHandle::on_backgroundAdjust(QPointF diff, int flag)
{
    if(!myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapVisible() || !myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND)->get_pixmapActive())
        return;
    Pixmap *pixmap = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUND);
    QPointF topleft = pixmap->get_pixmapRect().topLeft();
    QSize   backgroundSize = pixmap->get_pixmapRect().size();
    QPointF butright = topleft + QPointF(backgroundSize.width(), backgroundSize.height());
    switch (flag)
    {
    case 0:
    {
        topleft.setX(j_min(topleft.x()+diff.x(), butright.x()));
        topleft.setY(j_min(topleft.y()+diff.y(), butright.y()));
        break;
    }
    case 1:
    {
        topleft.setY(j_min(topleft.y()+diff.y(), butright.y()));
        break;
    }
    case 2:
    {
        topleft.setY(j_min(topleft.y()+diff.y(), butright.y()));
        butright.setX(j_max(topleft.x(), butright.x()+diff.x()));
        break;
    }
    case 3:
    {
        topleft.setX(j_min(topleft.x()+diff.x(), butright.x()));
        break;
    }
    case 4:
    {
        topleft += diff;
        butright += diff;
        break;
    }
    case 5:
    {
        butright.setX(j_max(topleft.x(), butright.x()+diff.x()));
        break;
    }
    case 6:
    {
        topleft.setX(j_min(topleft.x()+diff.x(), butright.x()));
        butright.setY(j_max(topleft.y(), butright.y()+diff.y()));
        break;
    }
    case 7:
    {
        butright.setY(j_max(topleft.y(), butright.y()+diff.y()));
        break;
    }
    case 8:
    {
        butright.setX(j_max(topleft.x(), butright.x()+diff.x()));
        butright.setY(j_max(topleft.y(), butright.y()+diff.y()));
        break;
    }
    default:
        break;
    }
    QPointF backSize = butright - topleft;
    *(pixmap->get_pixmap()) = myCanvas->get_pixmap(CANVASPIXMAP_BACKGROUNDORIG)->get_pixmap()->scaled(backSize.x(), backSize.y());
    if(flag >= -1)  //fresh the background
         pixmap->drawNetMap(QPointF(0,0), backSize, 0.15, myMoveStartFlag, 2, QColor(0,200,200));
    pixmap->set_pixmapRect(QRect(topleft.x(), topleft.y(), backSize.x(), backSize.y()));
}
