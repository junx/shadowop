#include "pixmapHandle.h"
#include "pixmap.h"
#include "coreWidget_canvas.h"

PixmapHandle::PixmapHandle(Pixmap *pixmap)
{
    myIsCircle = false;
    myIsOut = false;
    myStartPoint = QPointF(-1,-1);
    myLastPoint = QPointF(-1,-1);
    myPathWidth = 2;
    myPathPoint.clear();
    myPath = QPainterPath();
    myPixmap = pixmap;

    isFreshReady = false;
}


void
PixmapHandle::reset()
{
    myIsCircle = false;
    myIsOut = false;
    myStartPoint = QPointF(-1,-1);
    myLastPoint = QPointF(-1,-1);
    myPathPoint.clear();
    myPath = QPainterPath();
    myPixmap->clear();

    isFreshReady = false;
}


void
PixmapHandle::clear()
{
    myPixmap->clear();
}


void
PixmapHandle::on_start(QEvent *e, qreal width)
{
    QPointF point = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
    myPathWidth = width;

    myPathPoint.clear();
    myPathPoint.append(point);

    myStartPoint = point;
    myLastPoint = point;

    myIsOut = false;
    myIsCircle = false;
    myPixmap->drawCircle(point, 15, true, MASK_COLOR_ANCHOR);
}



int
PixmapHandle::on_update(QEvent *e)
{
    QPointF point = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
    qreal dis1 = j_dis2(point, myLastPoint);
    if(dis1 < 10)
        return 0;
    myPixmap->drawLine(myLastPoint, point, MASK_COLOR_LINE);
    myPathPoint.append(point);
    myLastPoint = point;

    qreal dis = j_dis2(myLastPoint, myStartPoint);
    if(dis > 900)
        myIsOut = true;
    if(dis < 225 && myIsOut)
    {
        myPixmap->drawCircle(myStartPoint, 15, true, MASK_COLOR_HIGHLIGHT);
    }
    else
    {
        myPixmap->drawCircle(myStartPoint, 15, true, MASK_COLOR_ANCHOR);
    }
    return 1;
}


int
PixmapHandle::on_finish(QEvent *e)
{
    QPointF point = ShadowCoreSystem::getMainCanvas()->get_eventPosition(e);
    qreal dis = j_dis2(point, myStartPoint);
    if(dis < 225 && myIsOut)
    {
        myIsCircle = true;
        myPath = j_to_path(myPathPoint);
        return 1;
    }
    return 0;
}


void
PixmapHandle::freshUpdate()
{
    if(!isFreshReady) return;
    myPixmap->set_pixmap(QPixmap::fromImage(myFreshImage));
    isFreshReady = false;
}

