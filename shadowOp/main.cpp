#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    if(!w.open_setting())
    {
        a.exit(0);
        return 0;
    }
    int value =  a.exec();
    w.close_setting();
    return value;
}
