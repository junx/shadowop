#ifndef GLOBALSHADOWCORE_H
#define GLOBALSHADOWCORE_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include <math.h>

#include "coreToolUtil.h"
                                            // save opId opId prop prop stat
//<<<<<<<<static property>>>>>>>//
#define ADD_SKETCHABLE            0x000010   //0000 0000 0000 0000 0001 0000
#define ADD_CLUSTERABLE           0x000020   //0000 0000 0000 0000 0010 0000

//<<<<<<<<specific operation = static property + hierachy info>>>>>>>// (undo)
#define OPERATION_BRUSH           0x001010   //0000 0000 0001 0000 0001 0000
#define OPERATION_PENCIL          0x002011   //0000 0000 0010 0000 0001 0001
#define OPERATION_STIPPLE         0x003010   //0000 0000 0011 0000 0001 0000

#define OPERATION_SETTING         0x004000   //0000 0000 0100 0000 0000 0000
#define OPERATION_SELECTION       0x005000   //0000 0000 0101 0000 0000 0000
#define OPERATION_UNDO            0x006000   //0000 0000 0110 0000 0000 0000
#define OPERATION_REDO            0x007000   //0000 0000 0111 0000 0000 0000


inline bool is_operation_sketchable(uint id){
    if(id & ADD_SKETCHABLE)
        return true;
    return false;
}

inline bool is_operation_clusterable(uint id){
    if(id & ADD_CLUSTERABLE)
        return true;
    return false;
}

inline int get_operation_undoer(uint id){
    return id & 0x03;
}

inline int get_operation_grider(uint id){
    return id & 0x03;
}


#define SYSTEM_BRUSH_TOOL       OPERATION_BRUSH
#define SYSTEM_PENCIL_TOOL      OPERATION_PENCIL
#define SYSTEM_STIPPLE_TOOL     OPERATION_STIPPLE

#define SYSTEM_SETTING_TOOL     OPERATION_SETTING
#define SYSTEM_SELECTION_TOOL   OPERATION_SELECTION
#define SYSTEM_UNDO_TOOL        OPERATION_UNDO
#define SYSTEM_REDO_TOOL        OPERATION_REDO


//<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>//
//<<<<<<<<<<<<<<<typedef>>>>>>>>>>>>>//


//<<<<<<<<<<<<<<<typedef>>>>>>>>>>>>>//
//<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>//

#define G_window_width 1280.0
#define G_window_height 720.0

#endif // GLOBALSHADOWANIM_H

