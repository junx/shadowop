#ifndef GLOBALUTIL_H
#define GLOBALUTIL_H

//<<<<<<<<add tiny functions here>>>>>>>>//

#include "globalShadowCore.h"

//<<<<<<<<cursor>>>>>>>>>>>>>//
inline QCursor   get_cursor(QColor cursor_color = QColor(0,0,0), qreal cursor_width = 10)
{
    if(cursor_width < 5)
        cursor_width = 5;
    QCursor m_Cursor;
    QPixmap m_LPixmap(cursor_width+10,cursor_width+10);
    m_LPixmap.fill(Qt::transparent); // Otherwise you get a black background :(
    QPainter painter(&m_LPixmap);
    painter.setRenderHint(QPainter::Antialiasing);

    QPen pen(QColor(Qt::white), 2);
    pen.setStyle(Qt::DotLine);
    painter.setPen(pen);
    painter.drawEllipse(5,5,cursor_width,cursor_width);

    painter.setBrush(QBrush(cursor_color));
    painter.setOpacity(0.2);
    painter.drawEllipse(5,5,cursor_width,cursor_width);

    painter.setOpacity(1);
    painter.setPen(QPen(QColor(Qt::black), 2));
    painter.drawPoint(QPointF(5+cursor_width/2,5+cursor_width/2));

    m_Cursor = QCursor(m_LPixmap);
    return m_Cursor;
}


inline float    j_to_pressure(QEvent *e)
{
    if(e->type() == QEvent::TabletRelease ||
            e->type() == QEvent::TabletMove ||
            e->type() == QEvent::TabletPress)
    {
        return dynamic_cast<QTabletEvent *>(e)->pressure();
    }
    else if(e->type() == QEvent::MouseButtonRelease ||
            e->type() == QEvent::MouseMove ||
            e->type() == QEvent::MouseButtonPress)
    {
        return 0.5;
    }
    return 0;
}


template<class T>
inline double j_spatial_distance(T diff)
{
    return j_mag1(diff);
}


template<class T>
inline double j_weight_distance(T disDiff)
{  //dis2Diff = dis1*dis1, the larger, the less penalty
    qreal v = j_mag1(disDiff)*0.02;
    if(v > 6) return 0.00001;
    return exp(-v);
}


template<class T>
inline double j_weight_direction(T dirDiff)
{  //dirDiff in [0, Pi], the larger, the more penalty
    qreal v = j_mag1(dirDiff);
    if(v > 6) return MAXVALUE;
    return exp(v);
}


template<class T>
inline double j_weight_order(T idDiff)
{
    qreal v = j_mag1(idDiff) *0.3;
    if(v > 6) return 0.00001;
    return exp(-v);
}


template<class T>
inline double j_weight_index(T dexDiff)
{//<<<<index, dexDiff [0, 1]>>>>//
    qreal v = j_mag1(j_mag1(dexDiff) / 0.3);
    if(v > 6) return MAXVALUE;
    return exp(v);
}


template<class T>
inline double j_weight_context(T cxtDiv)
{//<<<<<unified context paramters, the larger dev, less stable>>>>>>>>>//
    qreal v = j_mag1(cxtDiv);
    if(v > 6) return 0.00001;
    return exp(-v);
}


inline qreal j_min_dis(QList<QPointF> plist1, QList<QPointF> plist2)
{
    if(plist1.isEmpty() || plist2.isEmpty())
        return MAXVALUE;
    qreal minDis = MAXVALUE;
    for(int i = 0; i < plist1.size(); i++)
    {
        for(int j = 0; j < plist2.size(); j++)
        {
            qreal dis = j_dis2(plist1[i], plist2[j]);
            if(dis < minDis)
                minDis = dis;
        }
    }
    return minDis;
}


inline int j_nearest(QList<QPointF> vlist, QPointF v, QVector2D dir, qreal *mindis)
{
    int   maxI = -1;
    qreal maxV = 0.9;
    for(int i = 0; i < vlist.size(); i++)
    {
        QVector2D dir2 = j_to_direction(v, vlist[i]);
        qreal dis = QVector2D::dotProduct(dir, dir2);
        if(dis > maxV)
        {
            maxV = dis;
            maxI = i;
        }
    }
    if(mindis)
        *mindis = maxV;
    return maxI;
}


inline int get_adjust_flag(QRect rect, QPointF position)
{
    QPoint topleft = rect.topLeft();
    if(!rect.contains(position.toPoint()))
        return -1;
    QPoint flag;
    QPointF diff = position - topleft;
    qreal x = diff.x() / rect.width();
    qreal y = diff.y() / rect.height();
    if(x < 0.15)
        flag.setX(0);
    else if(x > 0.85)
        flag.setX(2);
    else
        flag.setX(1);
    if(y < 0.15)
        flag.setY(0);
    else if(y > 0.85)
        flag.setY(2);
    else
        flag.setY(1);
    return flag.y()*3+flag.x();
}


inline void
j_findOptimalAssignment(QVector<short>& assignment, Matrixd costM, int fSize, qreal *flags)
{
    assert(fSize >= 2);
    bool isMin = (flags[1]>0)?true:false;
    while(true)
    {
        qreal cutValue = flags[0];
        Vec2i matchIndex(-1, -1);
        for(int i = 0; i < costM.M_width; i++)
        {
            for(int j = 0; j < costM.M_height; j++)
            {
                if(isMin && costM[i][j] < cutValue)
                {
                    cutValue = costM[i][j];
                    matchIndex = Vec2i(i, j);
                }
                else if(!isMin && costM[i][j] > cutValue)
                {
                    cutValue = costM[i][j];
                    matchIndex = Vec2i(i, j);
                }
            }
        }
        if(matchIndex[0] < 0 || matchIndex[1] < 0)
            return;
        assignment[matchIndex[0]] = matchIndex[1];
        for(int i = 0; i < costM.M_width; i++)
            costM[i][matchIndex[1]] = flags[0];
        for(int j = 0; j < costM.M_height; j++)
            costM[matchIndex[0]][j] = flags[0];
    }
}



inline QList<QPointF>
j_transformLines(QList<QPointF> origPlist, QList<QList<QPointF> > origCtrls, QList<QList<QPointF> > newCtrls, QList<QVector2D> rotates, QList<qreal> scales)
{//multiple lines, each line has multiple ctrls (isotropic)
    QList<QPointF> newPlist;
    assert(origCtrls.size() == newCtrls.size() && !origCtrls.isEmpty());
    for(int i = 0; i < origPlist.size(); i++)
    {
        QList<QPointF> pre_point;
        QList<qreal>   pre_weight;
        qreal sumW = 0;
        for(int j = 0; j < origCtrls.size(); j++)
        {
            assert(origCtrls[j].size() == newCtrls[j].size() && !origCtrls[j].isEmpty());
            for(int k = 0; k < origCtrls[j].size(); k++)
            {
                QPointF diff = origPlist[i] - origCtrls[j][k];
                diff = j_rotate(diff, rotates[j]);
                diff = diff*scales[j] + newCtrls[j][k];
                pre_point.append(diff);
                //<<<<<<<<<weighting>>>>>>>>//
                qreal w = 1 / (j_mag2(diff)+1);
                sumW += w;
                pre_weight.append(w);
            }
        }
        newPlist.append(Core2<QList, QPointF>::j_wt_ave(pre_point, pre_weight));
    }
    return newPlist;
}


inline QList<QPointF>
j_transformLine(QList<QPointF> origPlist, QList<QPointF> origCtrls, QList<QPointF> newCtrls, QVector2D rotate, qreal scale)
{//one line, with multiple ctrls
    QList<QPointF> newPlist;

    assert(origCtrls.size() == newCtrls.size() && !origCtrls.isEmpty());
    for(int i = 0; i < origPlist.size(); i++)
    {
        QList<QPointF> pre_point;
        QList<qreal>   pre_weight;
        qreal sumW = 0;
        for(int j = 0; j < origCtrls.size(); j++)
        {
            QPointF diff = origPlist[i] - origCtrls[j];
            qreal w = 1 / (j_mag2(diff)+1);
            //<<<<<<<<<weighting>>>>>>>>//
            sumW += w;
            pre_weight.append(w);
            //<<<<<<<<position>>>>>>>>>//
            diff = j_rotate(diff, rotate);
            diff = diff*scale + newCtrls[j];
            pre_point.append(diff);
        }
        QPointF nPoint = (fabs(sumW)<0.0001)?Core2<QList, QPointF>::j_ave(pre_point):Core2<QList, QPointF>::j_wt_ave(pre_point, pre_weight);
        newPlist.append(nPoint);
    }
    return newPlist;
}


#endif // GLOBALUTIL_H
