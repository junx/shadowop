#-------------------------------------------------
#
# Project created by QtCreator 2014-09-27T10:34:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = shadowCore
TEMPLATE = app

SUBDIRS = global external view controller element enable handle HLBFGS analysis

SOURCES += main.cpp\
    mainwindow.cpp \
    ShadowCoreSystem.cpp \
    external/HungarianAlgorithm.cpp \
    controller/operationer.cpp \
    controller/coreUndoerOp.cpp \
    element/opParam.cpp \
    element/operation_sketchCluster.cpp \
    element/operation_sketchable.cpp \
    view/coreWidget_canvas.cpp \
    view/coreWidget_color.cpp \
    view/coreWidget_param.cpp \
    view/coreWidget_setting.cpp \
    view/coreWidget_tool.cpp \
    handle/canvasHandle.cpp \
    handle/pixmapHandle.cpp \
    view/coreWidget.cpp \
    HLBFGS/Lite_Sparse_Matrix.cpp \
    HLBFGS/LineSearch.cpp \
    HLBFGS/ICFS.cpp \
    HLBFGS/HLBFGS.cpp \
    HLBFGS/HLBFGS_BLAS.cpp \
    element/pixmap.cpp \
    view/coreButton.cpp \
    element/thread.cpp \
    enable/opNeighborable.cpp \
    element/eGrider.cpp \
    controller/selector.cpp \
    controller/analysiser.cpp \
    enable/cxNeighborable.cpp \
    element/synthesis.cpp \
    controller/synthesiser.cpp \
    element/synthesis_parallel.cpp \
    element/synthesis_online.cpp


HEADERS  += mainwindow.h \
    ShadowCoreSystem.h \
    external/HungarianAlgorithm.h \
    global/globalUtil.h \
    global/globalFunction.h \
    global/globalShadowCore.h \
    external/HungarianAlgorithm.h \
    controller/coreUndoerOp.h \
    controller/operationer.h \
    element/opParam.h \
    element/operation.h \
    view/ui.h \
    view/coreWidget_canvas.h \
    view/coreWidget_param.h \
    view/coreWidget_color.h \
    view/coreWidget_setting.h \
    view/coreWidget_tool.h \
    handle/canvasHandle.h \
    handle/pixmapHandle.h \
    view/coreWidget.h \
    HLBFGS/Sparse_Entry.h \
    HLBFGS/Lite_Sparse_Matrix.h \
    HLBFGS/LineSearch.h \
    HLBFGS/ICFS.h \
    HLBFGS/HLBFGS.h \
    HLBFGS/HLBFGS_BLAS.h \
    HLBFGS/optimization_HLBFGS.h \
    element/pixmap.h \
    view/coreButton.h \
    element/thread.h \
    enable/opNeighborable.h \
    element/eGrider.h \
    controller/selector.h \
    controller/analysiser.h \
    enable/cxNeighborable.h \
    element/synthesis.h \
    controller/synthesiser.h


FORMS    += mainwindow.ui
RESOURCES += \
    myfile.qrc \


DEPENDPATH += global \
              external \
              view \
              controller  \
              element \
              handle \
              enable \
              HLBFGS \
              analysis \

INCLUDEPATH += global \
              external \
              view \
              controller  \
              element \
              handle \
              enable \
              HLBFGS \
              analysis \


INCLUDEPATH += $$PWD/../_junx
DEPENDPATH += $$PWD/../_junx
INCLUDEPATH += $$PWD/../_junx/*
DEPENDPATH += $$PWD/../_junx/*

macx: LIBS += -L$$PWD/../_junx/build/debug/ -l_junx
macx: PRE_TARGETDEPS += $$PWD/../_junx/build/debug/lib_junx.a
